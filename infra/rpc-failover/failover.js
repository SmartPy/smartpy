"use strict";
exports.__esModule = true;
var fs = require("fs");
var service_1 = require("./service");
var _a = JSON.parse(fs.readFileSync(process.env.FAILOVER_CONFIG_FILE || "config.json", { encoding: "utf-8" })), TASK_DELAY = _a.TASK_DELAY, FAILOVER_ON = _a.FAILOVER_ON, // Disable node after <FAILOVER_ON> failed requests,
ALLOWED_BLOCK_AGE = _a.ALLOWED_BLOCK_AGE, CONFIG_LOCATION = _a.CONFIG_LOCATION, RELOAD_NGINX_CMD = _a.RELOAD_NGINX_CMD, NODES = _a.NODES // List of nodes being monitored
;
(0, service_1.prepare)({
    FAILOVER_ON: FAILOVER_ON,
    ALLOWED_BLOCK_AGE: ALLOWED_BLOCK_AGE,
    CONFIG_LOCATION: CONFIG_LOCATION,
    RELOAD_NGINX_CMD: RELOAD_NGINX_CMD,
    NODES: NODES
});
setInterval(service_1.runTask, TASK_DELAY);
