import axios from "axios";
import * as fs from 'fs';
import { execSync } from "child_process";

interface BlockHeader {
    chain_id: string;
    context: string;
    fitness: string[];
    hash: string;
    level: number;
    liquidity_baking_escape_vote: boolean;
    operations_hash: string;
    predecessor: string;
    priority: number;
    proof_of_work_nonce: string;
    proto: number;
    protocol: string;
    signature: string;
    timestamp: string;
    validation_pass: number;
}

interface Config {
    FAILOVER_ON: number;
    ALLOWED_BLOCK_AGE: number;
    CONFIG_LOCATION: string;
    RELOAD_NGINX_CMD: string;
    NODES: string[],
}

let CONFIG: Config;

const Logger = {
    log: (...args: any[]) => console.log("[LOG]", args),
    debug: (...args: any[]) => console.log("[DEBUG]", args)
};

// Record of (node domain => status)
// | false => node is down
// | true => node is up
const NodeStatus: Record<string, boolean> = {};

// Record of (node domain => head block timestamp)
const BlockWatermark: Record<string, string> = {}

// Record of (node domain => fail count)
const ConnectionFailures: Record<string, number> = {};

export function prepare(config: Config): void {
    CONFIG = config;
    console.debug("[LOADED CONFIG]", CONFIG);
}

function resetNodeConnectionFailures(node: string): void {
    ConnectionFailures[node] = 0;
}

function reloadNginxConfig(content: string): void {
    fs.writeFileSync(CONFIG.CONFIG_LOCATION, content, { encoding: "utf-8" });
    Logger.log(execSync(CONFIG.RELOAD_NGINX_CMD).toString());
}

function disableNode(node: string): void {
    const pattern = new RegExp(`(server[ ]${node}:[0-9]+[ ]weight=[0-9]+).*`);

    try {
        Logger.log(node, "Is down!");
        // Disable server in config
        const content = fs.readFileSync(CONFIG.CONFIG_LOCATION, { encoding: "utf-8" });
        const newContent = content.replace(pattern, "$1 down;");

        if (content !== newContent) {
            reloadNginxConfig(newContent);
            Logger.log(node, "Updated state (up => down).");
        }

        NodeStatus[node] = false;
    } catch (error: any) {
        Logger.debug(error.status, error.message, error.stderr, error.stdout);
    }
}

function enableNode(node: string): void {
    const pattern = new RegExp(`(server[ ]${node}:[0-9]+[ ]weight=[0-9]+).*`);

    try {
        Logger.log(node, "Is Up!");
        // Enable server in config
        const content = fs.readFileSync(CONFIG.CONFIG_LOCATION, { encoding: "utf-8" });
        const newContent = content.replace(pattern, "$1;");

        if (content !== newContent) {
            reloadNginxConfig(newContent);
            Logger.log(node, "Updated state (down => up).");
        }

        NodeStatus[node] = true;
    } catch (error: any) {
        Logger.debug(error.status, error.message, error.stderr, error.stdout);
    }
}

export async function runTask(): Promise<void> {
    for (const node of CONFIG.NODES) {
        try {
            // Fetch block header from the node
            const response = await axios.get<BlockHeader>(`https://${node}/chains/main/blocks/head/header`, { timeout: 1000 });
            // Reset the counter of connection failures for this node.
            resetNodeConnectionFailures(node);
            // Compute block age
            const blockAge = Date.now() - new Date(response.data.timestamp).getTime();
            // Disable the node if the block is older than 3 minutes
            if (BlockWatermark[node] === response.data.timestamp && blockAge > CONFIG.ALLOWED_BLOCK_AGE) {
                disableNode(node);
            } else {
                BlockWatermark[node] = response.data.timestamp;
                // Enable the node if the node is down
                if (!NodeStatus[node]) {
                    enableNode(node);
                }
            }
        } catch (e: any) {
            //
            // The node is down
            //
            Logger.debug(e?.message || e);
            // Increment the fail count
            ConnectionFailures[node] = (ConnectionFailures[node] || 0 ) + 1;
            // Disable node if failover condition has been met
            if (ConnectionFailures[node] === CONFIG.FAILOVER_ON) {
                disableNode(node);
                // Reset counter
                resetNodeConnectionFailures(node);
            }
        }
    }
}
