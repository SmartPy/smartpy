"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.runTask = exports.prepare = void 0;
var axios_1 = require("axios");
var fs = require("fs");
var child_process_1 = require("child_process");
var CONFIG;
var Logger = {
    log: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return console.log("[LOG]", args);
    },
    debug: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return console.log("[DEBUG]", args);
    }
};
// Record of (node domain => status)
// | false => node is down
// | true => node is up
var NodeStatus = {};
// Record of (node domain => head block timestamp)
var BlockWatermark = {};
// Record of (node domain => fail count)
var ConnectionFailures = {};
function prepare(config) {
    CONFIG = config;
    console.debug("[LOADED CONFIG]", CONFIG);
}
exports.prepare = prepare;
function resetNodeConnectionFailures(node) {
    ConnectionFailures[node] = 0;
}
function reloadNginxConfig(content) {
    fs.writeFileSync(CONFIG.CONFIG_LOCATION, content, { encoding: "utf-8" });
    Logger.log((0, child_process_1.execSync)(CONFIG.RELOAD_NGINX_CMD).toString());
}
function disableNode(node) {
    var pattern = new RegExp("(server[ ]" + node + ":[0-9]+[ ]weight=[0-9]+).*");
    try {
        Logger.log(node, "Is down!");
        // Disable server in config
        var content = fs.readFileSync(CONFIG.CONFIG_LOCATION, { encoding: "utf-8" });
        var newContent = content.replace(pattern, "$1 down;");
        if (content !== newContent) {
            reloadNginxConfig(newContent);
            Logger.log(node, "Updated state (up => down).");
        }
        NodeStatus[node] = false;
    }
    catch (error) {
        Logger.debug(error.status, error.message, error.stderr, error.stdout);
    }
}
function enableNode(node) {
    var pattern = new RegExp("(server[ ]" + node + ":[0-9]+[ ]weight=[0-9]+).*");
    try {
        Logger.log(node, "Is Up!");
        // Enable server in config
        var content = fs.readFileSync(CONFIG.CONFIG_LOCATION, { encoding: "utf-8" });
        var newContent = content.replace(pattern, "$1;");
        if (content !== newContent) {
            reloadNginxConfig(newContent);
            Logger.log(node, "Updated state (down => up).");
        }
        NodeStatus[node] = true;
    }
    catch (error) {
        Logger.debug(error.status, error.message, error.stderr, error.stdout);
    }
}
function runTask() {
    return __awaiter(this, void 0, void 0, function () {
        var _i, _a, node, response, blockAge, e_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _i = 0, _a = CONFIG.NODES;
                    _b.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3 /*break*/, 6];
                    node = _a[_i];
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1["default"].get("https://" + node + "/chains/main/blocks/head/header", { timeout: 1000 })];
                case 3:
                    response = _b.sent();
                    // Reset the counter of connection failures for this node.
                    resetNodeConnectionFailures(node);
                    blockAge = Date.now() - new Date(response.data.timestamp).getTime();
                    // Disable the node if the block is older than 3 minutes
                    if (BlockWatermark[node] === response.data.timestamp && blockAge > CONFIG.ALLOWED_BLOCK_AGE) {
                        disableNode(node);
                    }
                    else {
                        BlockWatermark[node] = response.data.timestamp;
                        // Enable the node if the node is down
                        if (!NodeStatus[node]) {
                            enableNode(node);
                        }
                    }
                    return [3 /*break*/, 5];
                case 4:
                    e_1 = _b.sent();
                    //
                    // The node is down
                    //
                    Logger.debug((e_1 === null || e_1 === void 0 ? void 0 : e_1.message) || e_1);
                    // Increment the fail count
                    ConnectionFailures[node] = (ConnectionFailures[node] || 0) + 1;
                    // Disable node if failover condition has been met
                    if (ConnectionFailures[node] === CONFIG.FAILOVER_ON) {
                        disableNode(node);
                        // Reset counter
                        resetNodeConnectionFailures(node);
                    }
                    return [3 /*break*/, 5];
                case 5:
                    _i++;
                    return [3 /*break*/, 1];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.runTask = runTask;
