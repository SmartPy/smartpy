# Copyright 2019-2022 Smart Chain Arena LLC.

LANG := C
SOURCES := $(wildcard ./smartML/**/*.ml) $(wildcard ./smartML/**/*.mli)

# Recursive wildcard method
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

build: .phony _build/cli.ok _build/web-js.ok

all: .phony
	@$(MAKE) -s build
	@$(MAKE) -s manual
	@$(MAKE) -s test-common
	@git status --short
	@$(MAKE) test-mockup-only-warn

full: .phony test manual doc

clean: .phony
	rm -rf _build smartML/*.opam

test: .phony test-common test-mockup
	git status --short

test-quick: .phony test-common test-mockup-only-warn
	git status --short

test-common: .phony \
  test-scenario-ml \
  test-scenario-ts \
  test-scenario \
  test-scenario-via-js \
  test-scenario-michel \
  test-compile \
  test-compile-ml \
  test-compile-ts \
  test-pdoc

touch_ok=@mkdir -p $(@D); touch $@

########################################################################
# SmartML

build-cli: _build/cli.ok

_build/opams.ok: dune-project
	dune build smartML/{driver,michel,michelson_base,ppx_smartml_lib,smartML,tools,utils,utils_pure}.opam
	$(touch_ok)

_build/cli.ok: $(SOURCES) $(wildcard ./smartML/**/dune) $(wildcard ./smartML/**/.) _build/opams.ok smartML/smartpyc/prelude.js
	dune build --root . \
	  smartML/smartpyc/smartpyc_js.bc.js \
	  smartML/smartpyc/smartpyc_unix.exe \
	  smartML/ppx_smartml/driver.exe
	smartML/smartpyc/stitch_smartpyc_js
	dune build --root . @install
	dune install 2> /dev/null
	ocamlfind ocamlmktop -o _build/smarttop.exe -package num,utils_pure,smartML -linkpkg
	ocamlformat --check $(SOURCES)
	$(touch_ok)

build-web-js: _build/web-js.ok

_build/web-js.ok: _build/cli.ok
	dune build --root . smartML/web_js/smartmljs.bc smartML/core/smartML.cma smartML/ppx_smartml/ppx_smartml_lib.cma
	jsoo_listunits -o _build/export.txt \
	  stdlib \
	  _opam/lib/num/big_int.cmi \
	  _build/default/smartML/utils_pure/.utils_pure.objs/byte/utils_pure.cmi \
	  _build/default/smartML/utils_pure/.utils_pure.objs/byte/utils_pure__Bigint.cmi \
	  _build/default/smartML/core/.smartML.objs/byte/smartML.cmi \
	  _build/default/smartML/core/.smartML.objs/byte/smartML__{Basics,Command,Config,Export,Expr,Hole,Ids,Library,Literal,Primitives,Target,Type,VarId}.cmi \
	  _build/default/smartML/ppx_smartml/.ppx_smartml_lib.objs/byte/ppx_smartml_lib.cmi \
	  _build/default/smartML/ppx_smartml/.ppx_smartml_lib.objs/byte/ppx_smartml_lib__Transformer.cmi
	js_of_ocaml \
          --export ./_build/export.txt \
          --toplevel \
          +toplevel.js +dynlink.js +base/runtime.js +zarith_stubs_js/runtime.js +zarith_stubs_js/biginteger.js +base/base_internalhash_types/runtime.js +bigstringaf/runtime.js \
          _build/default/smartML/web_js/smartmljs.bc \
          -o _build/default/smartML/web_js/smartmljs.bc.js
	$(touch_ok)

########################################################################
# Documentation

doc: _build/doc.ok .phony

_build/doc.ok:
	dune build --root . @doc
	@echo -e 'SmartML doc:\n  open _build/default/_doc/_html/smartml/index.html'

manual: _build/manual.ok test-pdoc .phony

_build/manual.ok: doc/*.md doc/*.css packages/doc/docs/**/*.md packages/doc/docs/**/*.mdx scripts/build_completion.py _build/packages/deps.ok
	@mkdir -p packages/frontend/build
	@echo "asciidoctor ..."
	@asciidoctor doc/reference.md      -a toc=left -a linkcss -a stylesheet=reference.css --destination-dir packages/frontend/public -d book
	@asciidoctor doc/releases.md       -a toc=left -a linkcss --destination-dir packages/frontend/public
	@python3 scripts/build_completion.py
	@cd packages/frontend; ../../wrapper --silent ../../_build/packages/frontend/build.log.txt prettier --write 'build/completers.ts'
	@cp packages/frontend/build/completers.ts packages/frontend/src/features/editor/language/completers/python.ts
	$(touch_ok)

########################################################################
# Versioning

patch-version:
	./scripts/bump-version.sh patch
	python scripts/add_release_template.py

minor-version:
	./scripts/bump-version.sh minor
	python scripts/add_release_template.py

major-version:
	./scripts/bump-version.sh major
	python scripts/add_release_template.py

with-rev-version: export WITH_REV := true
with-rev-version:
	./scripts/bump-version.sh

custom-version:
	./scripts/bump-version.sh $(VERSION)

########################################################################
# Frontend and its tests

packages-deps: .phony _build/packages/deps.ok

_build/packages/deps.ok: packages/package.json $(wildcard packages/**/**/package.json)
	cd packages/smartpy-ui; make install
	cd packages/smartpy-ui; make build
	cd packages; ../wrapper --silent ../_build/packages/deps.log.txt lerna bootstrap
	@mkdir -p packages/doc/build
	$(touch_ok)

packages-build: _build/packages/build.ok

_build/packages/build.ok: .phony _build/packages/deps.ok packages/package.json _build/web-js.ok $(wildcard packages/**/**/*.js) $(wildcard packages/**/**/*.ts*) _build/manual.ok
	$(MAKE) -s update-ts-cli
	cd packages/frontend; ../../wrapper --silent ../../_build/packages/check.log.txt prettier --check 'src/**/*.ts'
	cd packages; ../wrapper --silent ../_build/packages/build.log.txt lerna run build
	$(touch_ok)

packages-test: .phony _build/packages/test.ok

_build/packages/test.ok: _build/packages/deps.ok packages/package.json _build/web-js.ok $(wildcard packages/**/**/*.js) $(wildcard packages/**/**/*.ts*)
	cd packages; lerna run test
	$(touch_ok)

frontend-start: .phony _build/packages/ts_cli.ok
	cd packages/frontend; npx craco start

frontend-build: .phony _build/packages/ts_cli.ok _build/web-js.ok
	cd packages/frontend; npx craco build

doc-start: .phony _build/packages/deps.ok
	cd packages/doc; npx docusaurus start

packages-ci-test: .phony _build/packages/deps.ok _build/packages/ts_cli.ok
	cd packages; ../wrapper --silent ../_build/packages/ci-test.log.txt lerna run ci-test

packages-fmt: .phony
	cd packages/frontend; prettier --write 'src/**/*.(ts|tsx)'

packages-publish: .phony
	@echo "please use npm i --package-lock-only on various packages if necessary."
	@echo "please execute the following lines"
	@echo "(cd packages/ts-types; npm publish)"
	@echo "(cd packages/ts-syntax; npm publish)"
	@echo "(cd packages/originator; npm publish)"
	@echo "(cd packages/smartml; npm publish)"
	@echo "(cd packages/common; npm publish)"
	@echo "(cd packages/create-smartpy-project; npm publish)"

update-ts-cli: .phony _build/packages/ts_cli.ok

_build/packages/ts_cli.ok: _build/packages/deps.ok $(call rwildcard,packages/ts-syntax/src,*.ts) $(call rwildcard,packages/smart-ts-cli/src,*.ts) _build/manual.ok
	cd packages/ts-syntax; node scripts/generate-ide-typings.js && npx rollup -c
	cd packages/smart-ts-cli; npx rollup -c
	$(touch_ok)

update-ts-snapshots: .phony update-ts-cli
	cd packages/ts-syntax; CI=true TRACE_ERRORS=true npx jest --no-watch --all -u

update-frontend-snapshots: .phony
	cd packages/frontend; CI=true npm run updateSnapshot

########################################################################
# SmartML tests

NOT_TEMPLATES := python/templates/state_channel_games/model_wrap.py python/templates/state_channel_games/types.py

TEMPLATES := $(filter-out $(NOT_TEMPLATES), $(wildcard python/templates/*.py python/templates/*/*.py python/templates/*/*/*.py))
TEMPLATES_OK := $(TEMPLATES:python/templates/%.py=%.ok)
TEMPLATES_ML := $(wildcard ml_templates/*.ml)
TEMPLATES_ML_OK := $(TEMPLATES_ML:ml_templates/%.ml=%.ok)
TEMPLATES_TS := $(wildcard ts_templates/*.ts)
TEMPLATES_TS_OK := $(TEMPLATES_TS:ts_templates/%.ts=%.ok)

FLAG_HTML=$(if $(filter $(addprefix python/templates/, welcome.py stateChannels.py),$<), --html,)
FLAG_NO_ERROR=$(if $(filter $(addprefix python/templates/, test_stop_on_error.py),$<), --no_error,)

FLAG_NATIVE=$(if $(filter $(addprefix python/templates/,	\
  ),$<),, --native)

FLAG_DECOMPILE=$(if $(filter $(addprefix python/templates/, FA1.2.py	\
  FA2.py admin_multisig.py check_dfs.py chess.py collatz.py		\
  constants.py fibonacci.py game_of_life.py inlineMichelson.py		\
  lambdas.py price_feed.py stringManipulations.py test_for.py		\
  test_variant.py timelock.py fa2_fungible_minimal.py			\
  state_channel_games/%), $<), --no-decompile, --decompile)

FLAG_DECOMPILE_ML=$(if $(filter $(addprefix ml_templates/, py_FA2.ml	\
  py_check_dfs.ml py_collatz.ml py_fibonacci.ml), $<),			\
  --no-decompile, --decompile)

FLAG_PROTOCOL=$(if $(filter $(addprefix python/templates/, test_baker_hash.py), $<), --protocol baking_accounts, --protocol lima)

FLAG_LAYOUT=$(if $(filter $(addprefix python/templates/, ), $<),, --default_record_layout comb)

FLAGS_EXTRA ?=

FLAGS_SCENARIO=$(FLAG_HTML) $(FLAG_NATIVE) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_VIA_JS=$(FLAG_HTML) $(FLAG_NATIVE) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty

FLAGS_SCENARIO_ML=$(FLAG_HTML) $(FLAG_NATIVE) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_TS=$(FLAG_HTML) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_MICHEL=$(FLAGS_SCENARIO) --simplify-via-michel --dump-michel $(FLAGS_EXTRA) --accept_empty

FLAGS_MOCKUP=$(FLAG_PROTOCOL) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty

FLAGS_COMPILE=$(FLAG_PROTOCOL) $(FLAG_DECOMPILE) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_COMPILE_ML=$(FLAG_PROTOCOL) $(FLAG_DECOMPILE_ML) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_COMPILE_TS=$(FLAG_PROTOCOL) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty

TEST_SCENARIO := $(addprefix _build/test/scenario/,            \
  $(filter-out __init__.ok state_channel_games/tests/test_game_platform_chess.ok, $(TEMPLATES_OK)))

TEST_SCENARIO_VIA_JS := $(addprefix _build/test/scenario_via_js/,	\
  $(filter-out __init__.ok						\
  state_channel_games/% bls12_381.ok	\
  test_stop_on_error.ok chess.ok, $(TEMPLATES_OK)))

TEST_SCENARIO_ML := $(addprefix _build/test/scenario_ml/, $(TEMPLATES_ML_OK))

TEST_SCENARIO_TS := $(addprefix _build/test/scenario_ts/, $(TEMPLATES_TS_OK))

_build/test/scenario/%.ok: python/templates/%.py _build/cli.ok python/smartpy.py
	@./wrapper $(FLAG_NO_ERROR) _build/test/scenario/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario/$* --purge $(FLAGS_SCENARIO)
	$(touch_ok)

test-scenario: $(TEST_SCENARIO) .phony
	@scripts/remove_unused.sh test_baselines/scenario $(TEST_SCENARIO)

_build/test/scenario_via_js/%.ok: python/templates/%.py _build/cli.ok python/smartpy.py
	@./wrapper _build/test/scenario_via_js/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_via_js/$* --purge $(FLAGS_SCENARIO_VIA_JS) --no-native
	$(touch_ok)

test-scenario-via-js: $(TEST_SCENARIO_VIA_JS) .phony


TEST_SCENARIO_MICHEL := $(addprefix _build/test/scenario_michel/,	\
  $(filter-out __init__.ok admin_multisig.ok chess.ok constants.ok	\
  state_channel_games/% test_eval_order.ok, $(TEMPLATES_OK)))

_build/test/scenario_michel/%.ok: python/templates/%.py _build/cli.ok python/smartpy.py
	@./wrapper $(FLAG_NO_ERROR) _build/test/scenario_michel/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_michel/$* --purge $(FLAGS_SCENARIO_MICHEL)
	$(touch_ok)

test-scenario-michel: $(TEST_SCENARIO_MICHEL) .phony
	@scripts/remove_unused.sh test_baselines/scenario_michel $(TEST_SCENARIO_MICHEL)


TEST_MOCKUP := $(addprefix _build/test/mockup/, $(filter-out		\
  __init__.ok constants.ok price_feed_multisign_admin.ok chess.ok	\
  state_channel_games/models/chess.ok					\
  state_channel_games/models/chess_test1.ok				\
  state_channel_games/models/chess_test2.ok				\
  test_expression_compilation.ok test_michelson_error.ok,		\
  $(TEMPLATES_OK)))

ifeq ($(shell uname),Darwin)
  mockup_root=$(shell realpath test_baselines/mockup)
  export TEZOS_CLIENT=docker run --rm -v $(mockup_root):$(mockup_root) tezos/tezos:v15.1 octez-client
endif

_build/test/mockup/%.ok: python/templates/%.py _build/cli.ok python/smartpy.py
	@./wrapper --silent $(FLAG_NO_ERROR) _build/test/mockup/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/mockup/$* --mockup --purge $(FLAGS_MOCKUP)
	$(touch_ok)

test-mockup: $(TEST_MOCKUP) .phony
	@scripts/remove_unused.sh test_baselines/mockup $(TEST_MOCKUP)

test-mockup-only-warn:
	@for i in $(TEST_MOCKUP); do \
	  test -f $$i || (echo $$i does not exist; false) ; \
	done

test-mockup-only-new:
	$(MAKE) $(filter-out $(wildcard test_baselines/mockup/*.tsv), $(TEST_MOCKUP))


TEST_PDOC := $(addprefix _build/test/pdoc/, $(filter-out	\
  FA1.2.ok \
  price_feed_multisign_admin.ok \
  tests/admin_hand_over.test.ok tests/baking_swap.test.ok tests/multisig_lambda.test.ok \
  __init__.ok, $(TEMPLATES_OK)))

_build/test/pdoc/%.ok: python/templates/%.py python/smartpy.py
	@./wrapper _build/test/pdoc/$*.log.txt smartpy-cli/SmartPy.sh doc $< test_baselines/pdoc/$*
	$(touch_ok)

test-pdoc: $(TEST_PDOC) .phony


TEST_COMPILE := $(addprefix _build/test/compile/, $(filter-out	\
  __init__.ok, $(TEMPLATES_OK)))

TEST_COMPILE_ML := $(addprefix _build/test/compile_ml/, $(TEMPLATES_ML_OK))

TEST_COMPILE_TS := $(addprefix _build/test/compile_ts/, $(TEMPLATES_TS_OK))

_build/test/compile/%.ok: python/templates/%.py _build/cli.ok python/smartpy.py
	@./wrapper _build/test/compile/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile/$* --purge $(FLAGS_COMPILE)
	$(touch_ok)

_build/test/compile_ml/%.ok: ml_templates/%.ml _build/cli.ok
	@./wrapper _build/test/compile_ml/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile_ml/$* --purge $(FLAGS_COMPILE_ML)
	$(touch_ok)

_build/test/compile_ts/%.ok: ts_templates/%.ts _build/cli.ok _build/packages/ts_cli.ok
	@./wrapper _build/test/compile_ts/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile_ts/$* --purge $(FLAGS_COMPILE_TS)
	$(touch_ok)

test-compile: $(TEST_COMPILE) .phony
	@scripts/remove_unused.sh test_baselines/compile $(TEST_COMPILE)

test-compile-ml: $(TEST_COMPILE_ML) .phony
	@scripts/remove_unused.sh test_baselines/compile_ml $(TEST_COMPILE_ML)

test-compile-ts: $(TEST_COMPILE_TS) .phony
	@scripts/remove_unused.sh test_baselines/compile_ts $(TEST_COMPILE_TS)

_build/test/scenario_ml/%.ok: ml_templates/%.ml _build/cli.ok
	@./wrapper _build/test/scenario_ml/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_ml/$* --purge $(FLAGS_SCENARIO_ML)
	$(touch_ok)

_build/test/scenario_ts/%.ok: ts_templates/%.ts _build/cli.ok _build/packages/ts_cli.ok
	@./wrapper _build/test/scenario_ts/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_ts/$* --purge $(FLAGS_SCENARIO_TS)
	$(touch_ok)

test-scenario-ml: $(TEST_SCENARIO_ML) .phony
	@scripts/remove_unused.sh test_baselines/scenario_ml $(TEST_SCENARIO_ML)

test-scenario-ts: $(TEST_SCENARIO_TS) .phony
	@scripts/remove_unused.sh test_baselines/scenario_ts $(TEST_SCENARIO_TS)

test_baselines/scenario/%: .phony
	@$(MAKE) -s _build/test/scenario/$*.ok

test_baselines/scenario_michel/%: .phony
	@$(MAKE) -s _build/test/scenario_michel/$*.ok

test_baselines/scenario_ml/%: .phony
	@$(MAKE) -s _build/test/scenario_ml/$*.ok

test_baselines/scenario_ts/%: .phony
	@$(MAKE) -s _build/test/scenario_ts/$*.ok

test_baselines/compile/%: .phony
	@$(MAKE) -s _build/test/compile/$*.ok

test_baselines/compile_ml/%: .phony
	@$(MAKE) -s _build/test/compile_ml/$*.ok

test_baselines/compile_ts/%: .phony
	@$(MAKE) -s _build/test/compile_ts/$*.ok


########################################################################
# Miscellaneous phony targets

fmt-check: .phony
	ocamlformat --check $(SOURCES)

fmt-fix: .phony
	ocamlformat --inplace $(SOURCES)

TEMPLATES_BLACK := $(addprefix python/templates/, \
	admin_hand_over.py baking_swap.py fibonacci_view.py \
	fa2_fungible_minimal.py fa2_lib.py fa2_lib_testing.py fa2_nft_minimal.py \
	inheritance.py multisig_action.py multisig_view.py \
	welcome.py)

black-check: .phony
	black --check $(TEMPLATES_BLACK)

black-fix: .phony
	black $(TEMPLATES_BLACK)

www: .phony
	(sleep 1; open http://localhost:$(PORT)) &
	cd packages/build && python3 -m http.server $(PORT)

open-index: .phony
	open _build/default/_doc/_html/smartml/index.html

open-manual: .phony manual
	open packages/frontend/public/reference.html


########################################################################
# Inter-template dependencies

ml_templates/py_%.ml: _build/test/compile/%.ok
	@touch $@

ml_templates/ml_%.ml: _build/test/compile_ml/%.ok
	@touch $@

python/templates/py_%.py: _build/test/compile/%.ok
	@touch $@

python/templates/ml_%.py: _build/test/compile_ml/%.ok
	@touch $@

python/templates/chess.py: python/templates/chess_logic.py
	@touch $@

python/templates/tests/%.test.py: python/templates/%.py
	@touch $@

python/templates/fa2_lib_test1.py: python/templates/fa2_lib.py python/templates/fa2_lib_testing.py

python/templates/fa2_lib_test2.py: python/templates/fa2_lib.py python/templates/fa2_lib_testing.py

python/templates/fa2_lib_test3.py: python/templates/fa2_lib.py python/templates/fa2_lib_testing.py

python/templates/FA2_test1.py: python/templates/FA2.py

python/templates/FA2_test2.py: python/templates/FA2.py

python/templates/FA2_test3.py: python/templates/FA2.py

python/templates/oracle_test1.py: python/templates/oracle.py

python/templates/oracle_test2.py: python/templates/oracle.py

python/templates/state_channel_games/models/chess.py: \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py \
 python/templates/chess_logic.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_adversarial.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_transfers1.py: \
 python/templates/state_channel_games/tests/test_game_platform_transfers_helpers.py \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_transfers2.py: \
 python/templates/state_channel_games/tests/test_game_platform_transfers_helpers.py \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_transfers3.py: \
 python/templates/state_channel_games/tests/test_game_platform_transfers_helpers.py \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_transfers4.py: \
 python/templates/state_channel_games/tests/test_game_platform_transfers_helpers.py \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_tictactoe.py: \
$(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_chess.py: \
$(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py \
 python/templates/chess_logic.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_admin.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@


python/templates/state_channel_games/tests/test_game_tester.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

python/templates/state_channel_games/scenarios/scenario_alice.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

python/templates/state_channel_games/scenarios/scenario_bob.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

#########################################################################
# Bundling

_build/cli-bundle.ok: _build/cli.ok _build/packages/ts_cli.ok
	rm -rf _build/cli-bundle/
	mkdir -p _build/cli-bundle/
	tar czf _build/cli-bundle/smartpy-cli.tar.gz \
    --dereference \
    --owner=0 \
    --group=0 \
    -C ./smartpy-cli \
    --files-from scripts/smartpy-cli.files
	tar czf _build/cli-bundle/smartML.tar.gz \
    --dereference \
    --owner=0 \
    --group=0 \
    --files-from scripts/smartML.files
	$(touch_ok)

_build/cli-bundle-test.ok: _build/cli-bundle.ok
	./smartpy-cli/install.sh --from file://$(shell pwd)/_build/cli-bundle/ --prefix _build/test/cli-install/ --yes --with-smartml
	@rm -rf _build/test/cli-run/
	_build/test/cli-install/SmartPy.sh test python/templates/welcome.py _build/test/cli-run/welcome_py
	_build/test/cli-install/SmartPy.sh test ml_templates/welcome.ml _build/test/cli-run/welcome_ml
	$(touch_ok)

########################################################################
# Makefile and environment plumbing

.phony:

.PHONY: .phony


ifeq (,$(wildcard _opam))
  $(error Please run './env/init' before running make)
endif

ifndef SMARTPY_ENV
  $(warning Initializing a new nix shell for each command. Run 'make' under './envsh' or directly as './with_env make' for better performance.)
  SHELL=./envsh
endif

MAKEFLAGS += --warn-undefined-variables --no-builtin-rules --output-sync

# Disable further implicit rules, resulting in a slight speed-up:
.SUFFIXES:
