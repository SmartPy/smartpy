#!/usr/bin/env bash

function usage() {
    echo -e "
        Usage:
            $(basename "$0") [ <newversion> | major | minor | patch ]
    "
}

function make_version() {
    (cd packages; lerna run version -- $1)
    new_version=$(npm --prefix packages/frontend -s run env echo '$npm_package_version')

    sed -i "s%VERSION=\".*\"%VERSION=\"$new_version\"%g" "$(realpath "$(dirname "$0")")"/../smartpy-cli/SmartPy.sh

    echo "Updating frontend snapshots..."
    cd "$(realpath "$(dirname "$0")")/../packages/frontend"
    CI=true npx craco test --no-watch -u --silent
    cd -

    echo -e "

    New Version: $new_version

    Updated files:

        * packages/frontend/package.json
        * packages/**/package.json
        * smartpy-cli/SmartPy.sh
    "
}

if [ -z "$WITH_REV" ]; then
    [ "$#" -ne 1 ] && usage && exit 1;

    make_version "$@"
else
    new_version="$(npm --prefix packages/frontend -s run env echo '$npm_package_version')-$(git rev-parse --short HEAD)"
    make_version "$new_version"
fi
