import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TPair(sp.TInt, sp.TInt), b = sp.TPair(sp.TInt, sp.TInt)).layout(("a", "b")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, let [x67; x68] = Unpair(2, __storage.a)
                  let _ = x67
                  Pair(Cdr(__storage.a), x68))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
