#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_send_back.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/py_send_back/scenario.json"
else
  exit 1;;