open SmartML

module Contract = struct
  let%entry_point compare_eq () =
    verify ((compare () ()) = (int 0));
    verify ((compare (set_type_expr None (option nat)) (set_type_expr None (option nat))) = (int 0));
    verify ((compare (nat 2) (nat 1)) = (int 1))

  let%entry_point compare_inferior () =
    verify ((compare (nat 1) (nat 5)) = (int ((-1))));
    verify ((compare (int 1) (int 5)) = (int ((-1))));
    verify ((compare timestamp 1 timestamp 5) = (int ((-1))));
    verify ((compare address "KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT" address "KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG") = (int ((-1))));
    verify ((compare false true) = (int ((-1))));
    verify ((compare bytes "0x00" bytes "0x01") = (int ((-1))));
    verify ((compare chain_id('0x00') chain_id('0x01')) = (int ((-1))));
    verify ((compare key "edpkuBknW28nW72KG6RoH" key "edpkuJqtDcA2m2muMxViS") = (int ((-1))));
    verify ((compare key_hash "tz1KqTpEZ7Yob7QbPE4Hy" key_hash "tz1XPTDmvT3vVE5Uunngm") = (int ((-1))));
    verify ((compare (mutez 1) (mutez 5)) = (int ((-1))));
    verify ((compare "a" "e") = (int ((-1))));
    verify ((compare (1, 5) (5, 1)) = (int ((-1))));
    verify ((compare None (some ())) = (int ((-1))));
    verify ((compare (some 1) (some 5)) = (int ((-1))));
    verify ((compare (set_type_expr (variant Left (nat 5)) (`Left nat + `Right nat)) (variant Right (nat 1))) = (int ((-1))));
    verify ((compare (set_type_expr (variant Left (nat 1)) (`Left nat + `Right nat)) (variant Left (nat 5))) = (int ((-1))));
    verify ((compare (set_type_expr (variant Left (nat 1)) (`Left nat + `Right nat)) (variant Left (nat 5))) = (int ((-1))));
    verify ((compare (set_type_expr (variant Right (nat 1)) (`Left nat + `Right nat)) (variant Right (nat 5))) = (int ((-1))))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [compare_eq; compare_inferior]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())