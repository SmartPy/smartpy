import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l = sp.TLambda(sp.TInt, sp.TRecord(address = sp.TAddress, operation = sp.TOperation).layout(("operation", "address"))), x = sp.TOption(sp.TAddress)).layout(("l", "x")))
    self.init(l = lambda,
              x = sp.none)

  @sp.entry_point
  def create1(self):
    create_contract_create_contract_32 = sp.local("create_contract_create_contract_32", create contract ...)
    sp.operations().push(create_contract_create_contract_32.value.operation)
    self.data.x = sp.some(create_contract_create_contract_32.value.address)

  @sp.entry_point
  def create2(self):
    create_contract_create_contract_37 = sp.local("create_contract_create_contract_37", create contract ...)
    sp.operations().push(create_contract_create_contract_37.value.operation)
    create_contract_create_contract_38 = sp.local("create_contract_create_contract_38", create contract ...)
    sp.operations().push(create_contract_create_contract_38.value.operation)

  @sp.entry_point
  def create3(self):
    create_contract_create_contract_42 = sp.local("create_contract_create_contract_42", create contract ...)
    sp.operations().push(create_contract_create_contract_42.value.operation)
    self.data.x = sp.some(create_contract_create_contract_42.value.address)

  @sp.entry_point
  def create4(self, params):
    sp.for x in params:
      create_contract_create_contract_48 = sp.local("create_contract_create_contract_48", create contract ...)
      sp.operations().push(create_contract_create_contract_48.value.operation)

  @sp.entry_point
  def create5(self):
    create_contract_create_contract_52 = sp.local("create_contract_create_contract_52", create contract ...)
    sp.operations().push(create_contract_create_contract_52.value.operation)
    self.data.x = sp.some(create_contract_create_contract_52.value.address)

  @sp.entry_point
  def create_op(self):
    operation_create_contract_59, address_create_contract_59 = sp.match_record(self.data.l(42), "operation", "address")
    sp.operations().push(operation_create_contract_59)
    self.data.x = sp.some(address_create_contract_59)

sp.add_compilation_target("test", Contract())