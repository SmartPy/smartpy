Comment...
 h1: FA2 Contract Name: FA2-single_asset
Table Of Contents

 FA2 Contract Name: FA2-single_asset
# Accounts
# Initial Minting
# Transfers Alice -> Bob
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Robert")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edsk3SUiUcR33jiBmxRDke8MKfd18dxmq2fUbZWZFYoiEsTkpAz5F7')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edsk34XphRR5Rs6EeAGrxktxAhstbwPr5YZ4m7RMzjaed3n9g5JcBB')), sp.record(seed = 'Robert', address = sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), public_key = sp.key('edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2'), public_key_hash = sp.key_hash('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), secret_key = sp.secret_key('edsk341UHyVJcvpXpHjHL8FaHnbnCR59RNyTFj7fmLcrHFzvkLcx6A'))]
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {} {})))))))
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_storage.tz 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_storage.json 25
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_sizes.csv 2
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_storage.py 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_types.py 7
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_metadata.metadata_base.json 179
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_contract.tz 644
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_contract.json 917
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_contract.py 87
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_004_cont_0_contract.ml 93
Comment...
 h2: Initial Minting
Comment...
 p: The administrator mints 100 token-0's to Alice.
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_007_cont_0_params.py 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_007_cont_0_params.tz 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_007_cont_0_params.json 23
Executing mint(sp.record(address = sp.resolve(sp.test_account("Alice").address), amount = 50, metadata = {'decimals' : sp.bytes('0x32'), 'name' : sp.bytes('0x54686520546f6b656e205a65726f'), 'symbol' : sp.bytes('0x544b30')}, token_id = 0))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 50} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {Elt 0 (Pair 0 {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30})} {Elt 0 50})))))))
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_008_cont_0_params.py 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_008_cont_0_params.tz 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_008_cont_0_params.json 23
Executing mint(sp.record(address = sp.resolve(sp.test_account("Alice").address), amount = 50, metadata = {'decimals' : sp.bytes('0x32'), 'name' : sp.bytes('0x54686520546f6b656e205a65726f'), 'symbol' : sp.bytes('0x544b30')}, token_id = 0))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 100} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {Elt 0 (Pair 0 {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30})} {Elt 0 100})))))))
Comment...
 h2: Transfers Alice -> Bob
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_010_cont_0_params.py 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_010_cont_0_params.tz 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_010_cont_0_params.json 9
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Robert").address), token_id = 0, amount = 10)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 10; Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 90} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {Elt 0 (Pair 0 {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30})} {Elt 0 100})))))))
Verifying sp.contract_data(0).ledger[sp.set_type_expr(sp.resolve(sp.test_account("Alice").address), sp.TAddress)].balance == 90...
 OK
Verifying sp.contract_data(0).ledger[sp.set_type_expr(sp.resolve(sp.test_account("Robert").address), sp.TAddress)].balance == 10...
 OK
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_013_cont_0_params.py 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_013_cont_0_params.tz 1
 => test_baselines/scenario/FA2_test1/FA2-single_asset/step_013_cont_0_params.json 12
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Robert").address), token_id = 0, amount = 10), sp.record(to_ = sp.resolve(sp.test_account("Robert").address), token_id = 0, amount = 11)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 31; Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 69} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {Elt 0 (Pair 0 {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30})} {Elt 0 100})))))))
Verifying sp.contract_data(0).ledger[sp.set_type_expr(sp.resolve(sp.test_account("Alice").address), sp.TAddress)].balance == 69...
 OK
Verifying sp.contract_data(0).ledger[sp.set_type_expr(sp.resolve(sp.test_account("Robert").address), sp.TAddress)].balance == 31...
 OK
