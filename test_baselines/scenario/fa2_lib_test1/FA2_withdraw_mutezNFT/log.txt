Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {})))))
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_storage.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_storage.json 22
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_storage.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_types.py 7
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_metadata.metadata_base.json 174
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_contract.tz 564
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_contract.json 762
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_contract.py 83
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_000_cont_0_contract.ml 90
Comment...
 h1: FA2_withdraw_mutezNFT
Comment...
 h2: Mutez receiver contract
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> Unit
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_storage.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_storage.json 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_sizes.csv 2
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_storage.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_types.py 7
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_contract.tz 9
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_contract.json 5
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_contract.py 12
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_003_cont_1_contract.ml 18
Comment...
 h2: Non admin cannot withdraw_mutez
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_005_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_005_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_005_cont_0_params.json 1
Executing withdraw_mutez(sp.record(amount = sp.tez(10), destination = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.administrator : sp.TBool) (templates/fa2_lib.py, line 513)
Message: 'FA2_NOT_ADMIN'
 (templates/fa2_lib.py, line 545)
Comment...
 h3: Admin withdraw_mutez
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_007_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_007_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_withdraw_mutezNFT/step_007_cont_0_params.json 1
Executing withdraw_mutez(sp.record(amount = sp.tez(10), destination = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {})))))
  + Transfer
     params: sp.unit
     amount: sp.tez(10)
     to:     sp.contract(sp.TUnit, sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')).open_some()
Executing (queue) default(sp.unit)...
 -> Unit
Verifying sp.contract_balance(0) == sp.tez(32)...
 OK
Verifying sp.contract_balance(1) == sp.tez(10)...
 OK
