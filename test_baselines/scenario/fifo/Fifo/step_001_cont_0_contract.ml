open SmartML

module Contract = struct
  let%entry_point pop () =
    verify (data.first < data.last);
    Map.delete data.saved data.first;
    data.first <- data.first + (int 1)

  let%entry_point push params =
    data.last <- data.last + (int 1);
    Map.set data.saved data.last params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {first = int; last = int; saved = map int intOrNat}]
      ~storage:[%expr
                 {first = int 0;
                  last = int ((-1));
                  saved = Map.make []}]
      [pop; push]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())