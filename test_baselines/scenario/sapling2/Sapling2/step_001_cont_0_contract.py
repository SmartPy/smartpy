import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(ledger = sp.TSaplingState(8)).layout("ledger"))
    self.init(ledger = [])

  @sp.entry_point
  def handle(self, params):
    sp.for operation in params:
      compute_sapling2_16 = sp.local("compute_sapling2_16", sp.sapling_verify_update(self.data.ledger, operation.transaction).open_some())
      bound_data, amount, ledger = sp.match_tuple(compute_sapling2_16.value, "bound_data", "amount", "ledger")
      self.data.ledger = ledger
      amount_tez = sp.local("amount_tez", sp.mutez(abs(amount)))
      sp.if amount > 0:
        sp.transfer(sp.unit, amount_tez.value, sp.implicit_account(operation.key.open_some()))
      sp.else:
        sp.verify(~ operation.key.is_some())
        sp.verify(sp.amount == amount_tez.value)

sp.add_compilation_target("test", Contract())