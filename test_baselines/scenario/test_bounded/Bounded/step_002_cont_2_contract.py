import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TBounded([1, 2, 3], t=sp.TInt), y = sp.TInt).layout(("x", "y")))
    self.init(x = 1,
              y = 0)

  @sp.entry_point
  def ep(self):
    self.data.x = 1

  @sp.entry_point
  def ep2(self):
    self.data.x = 2

  @sp.entry_point
  def ep3(self):
    self.data.y = sp.unbound(self.data.x)

sp.add_compilation_target("test", Contract())