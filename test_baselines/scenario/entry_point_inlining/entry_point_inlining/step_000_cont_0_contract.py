import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 11)

  @sp.entry_point
  def entry_point_1(self):
    self.data.x += 12
    self.data.x += 15

  @sp.entry_point(private = True)
  def f(self, params):
    self.data.x += params

  @sp.entry_point
  def g(self, params):
    self.data.x += params

sp.add_compilation_target("test", Contract())