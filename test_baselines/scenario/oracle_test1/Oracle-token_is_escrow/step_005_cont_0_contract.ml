open SmartML

module Contract = struct
  let%entry_point balance_of params =
    verify (not data.paused) ~msg:"FA2_PAUSED";
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    let%mutable responses = (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
if contains (set_type_expr _x0.owner address) data.ledger then
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (Map.get data.ledger (set_type_expr _x0.owner address)).balance}
else
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (nat 0)}) params.requests) in ();
    transfer responses (tez 0) (set_type_expr params.callback (contract (list {balance = nat; request = {owner = address; token_id = nat}})))

  let%entry_point cancel_request params =
    let%mutable compute_oracle_304 = {client = sender; client_request_id = params.client_request_id} in ();
    verify (contains compute_oracle_304 data.locked) ~msg:"EscrowRequestIdUnknownForClient";
    verify (now >= (Map.get data.locked compute_oracle_304).cancel_timeout) ~msg:"EscrowCantCancelBeforeTimeout";
    transfer [{from_ = self_address; txs = [{to_ = sender; token_id = (nat 0); amount = (Map.get data.locked compute_oracle_304).amount}]}] (tez 0) (self_entry_point "transfer");
    Map.delete data.locked compute_oracle_304;
    if not params.force then
      transfer compute_oracle_304 (tez 0) (open_some ~message:"EscrowOracleNotFound" (contract {client = address; client_request_id = nat} params.oracle , entry_point='cancel_request'))

  let%entry_point force_fulfill_request params =
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_324, result_oracle_324 = match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    let%mutable compute_oracle_330 = {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id} in ();
    verify (contains compute_oracle_330 data.locked) ~msg:"EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) ~msg:"EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") ~msg:"TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) ~msg:"TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) ~msg:"TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) ~msg:"TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = self_address; txs = [{to_ = ticket_oracle_325_content.oracle; token_id = (nat 0); amount = (Map.get data.locked compute_oracle_330).amount}]}] (tez 0) (self_entry_point "transfer");
    Map.delete data.locked compute_oracle_330

  let%entry_point fulfill_request params =
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_324, result_oracle_324 = match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    let%mutable compute_oracle_330i = {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id} in ();
    verify (contains compute_oracle_330i data.locked) ~msg:"EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) ~msg:"EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") ~msg:"TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) ~msg:"TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) ~msg:"TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) ~msg:"TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = self_address; txs = [{to_ = ticket_oracle_325_content.oracle; token_id = (nat 0); amount = (Map.get data.locked compute_oracle_330i).amount}]}] (tez 0) (self_entry_point "transfer");
    Map.delete data.locked compute_oracle_330i;
    transfer {request = ticket_oracle_325_copy; result = ticket_oracle_326_copy} (tez 0) (open_some ~message:"EscrowTargetNotFound" (contract {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}} ticket_oracle_325_content.target ))

  let%entry_point mint params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    verify (params.token_id = (nat 0)) ~msg:"single-asset: token-id <> 0";
    if contains (set_type_expr params.address address) data.ledger then
      (Map.get data.ledger (set_type_expr params.address address)).balance <- (Map.get data.ledger (set_type_expr params.address address)).balance + params.amount
    else
      Map.set data.ledger (set_type_expr params.address address) {balance = params.amount};
    if not (params.token_id < data.all_tokens) then
      (
        verify (data.all_tokens = params.token_id) ~msg:"Token-IDs should be consecutive";
        data.all_tokens <- params.token_id + (nat 1);
        Map.set data.token_metadata params.token_id {token_id = params.token_id; token_info = params.metadata}
      );
    Map.set data.total_supply params.token_id (params.amount + (Map.get ~default_value:(nat 0) data.total_supply params.token_id))

  let%entry_point send_request params =
    set_type params {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}};
    amount_oracle_273, request_oracle_273 = match_record(params, "amount", "request")
    ticket_oracle_276_data, ticket_oracle_276_copy = match_tuple(read_ticket_raw request_oracle_273, "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    verify (sender = ticket_oracle_276_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = sender; txs = [{to_ = self_address; token_id = (nat 0); amount = amount_oracle_273}]}] (tez 0) (self_entry_point "transfer");
    verify (ticket_oracle_276_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    let%mutable compute_oracle_292 = {client = ticket_oracle_276_ticketer; client_request_id = ticket_oracle_276_content.client_request_id} in ();
    verify (not (contains compute_oracle_292 data.locked)) ~msg:"EscrowRequestIdAlreadyKnownForClient";
    Map.set data.locked compute_oracle_292 {amount = amount_oracle_273; cancel_timeout = ticket_oracle_276_content.cancel_timeout};
    transfer {amount = amount_oracle_273; request = ticket_oracle_276_copy} (tez 0) (open_some ~message:"EscrowOracleNotFound" (contract {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}} ticket_oracle_276_content.oracle , entry_point='create_request'))

  let%entry_point set_administrator params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.administrator <- params

  let%entry_point set_metadata params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    Map.set data.metadata params.k params.v

  let%entry_point set_pause params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.paused <- params

  let%entry_point transfer params =
    verify (not data.paused) ~msg:"FA2_PAUSED";
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    List.iter (fun transfer ->
      List.iter (fun tx ->
        verify (tx.token_id = (nat 0)) ~msg:"single-asset: token-id <> 0";
        verify ((((sender = data.administrator) || (transfer.from_ = sender)) || (contains (set_type_expr {owner = transfer.from_; operator = sender; token_id = tx.token_id} {operator = address; owner = address; token_id = nat}) data.operators)) || (sender = self_address)) ~msg:"FA2_NOT_OPERATOR";
        verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
        if tx.amount > (nat 0) then
          (
            verify ((Map.get data.ledger (set_type_expr transfer.from_ address)).balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
            (Map.get data.ledger (set_type_expr transfer.from_ address)).balance <- open_some (is_nat ((Map.get data.ledger (set_type_expr transfer.from_ address)).balance - tx.amount));
            if contains (set_type_expr tx.to_ address) data.ledger then
              (Map.get data.ledger (set_type_expr tx.to_ address)).balance <- (Map.get data.ledger (set_type_expr tx.to_ address)).balance + tx.amount
            else
              Map.set data.ledger (set_type_expr tx.to_ address) {balance = tx.amount}
          )
      ) transfer.txs
    ) params

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    List.iter (fun update ->
      match update with
        | `add_operator add_operator ->
          verify ((add_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.set data.operators (set_type_expr {owner = add_operator.owner; operator = add_operator.operator; token_id = add_operator.token_id} {operator = address; owner = address; token_id = nat}) ()
        | `remove_operator remove_operator ->
          verify ((remove_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.delete data.operators (set_type_expr {owner = remove_operator.owner; operator = remove_operator.operator; token_id = remove_operator.token_id} {operator = address; owner = address; token_id = nat})

    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; all_tokens = nat; ledger = big_map address {balance = nat}; locked = big_map {client = address; client_request_id = nat} {amount = nat; cancel_timeout = timestamp}; metadata = big_map string bytes; operators = big_map {operator = address; owner = address; token_id = nat} unit; paused = bool; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; total_supply = big_map nat nat}]
      ~storage:[%expr
                 {administrator = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  all_tokens = nat 0;
                  ledger = Map.make [];
                  locked = Map.make [];
                  metadata = Map.make [("", bytes "0x")];
                  operators = Map.make [];
                  paused = false;
                  token_metadata = Map.make [];
                  total_supply = Map.make []}]
      [balance_of; cancel_request; force_fulfill_request; fulfill_request; mint; send_request; set_administrator; set_metadata; set_pause; transfer; update_operators]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())