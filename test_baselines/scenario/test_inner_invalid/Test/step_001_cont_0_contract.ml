open SmartML

module Contract = struct
  let%entry_point compute params =
    transfer params (tez 0) (self_entry_point "run")

  let%entry_point reset () =
    data.counter <- int 0

  let%entry_point run params =
    verify ((data.counter * params) <> (int 9));
    if params > (int 1) then
      (
        data.counter <- data.counter + (int 1);
        transfer (params - (int 1)) (tez 0) (self_entry_point "run")
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = int}]
      ~storage:[%expr
                 {counter = int 0}]
      [compute; reset; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())