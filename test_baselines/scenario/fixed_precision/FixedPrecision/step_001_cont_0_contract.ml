open SmartML

module Contract = struct
  let%entry_point log params =
    verify (params <> (nat 0));
    let%mutable y = (int 0) in ();
    let%mutable x = params in ();
    while x < (nat 65536) do
      x <- x << (nat 1);
      y <- y - (int 65536)
    done;
    while x >= (nat 131072) do
      x <- x >> (nat 1);
      y <- y + (int 65536)
    done;
    let%mutable b = (nat 32768) in ();
    while b > (nat 0) do
      x <- (x * x) >> (nat 16);
      if x > (nat 131072) then
        (
          x <- x >> (nat 1);
          y <- y + (to_int b)
        );
      b <- b >> (nat 1)
    done;
    data.value <- y

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = int}]
      ~storage:[%expr
                 {value = int 0}]
      [log]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())