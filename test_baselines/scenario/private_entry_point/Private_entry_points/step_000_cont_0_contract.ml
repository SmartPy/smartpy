open SmartML

module Contract = struct
  let%entry_point change params =
    data.x <- params.x;
    data.y <- params.y

  let%entry_point go_x () =
    data.x <- data.x + (int 1);
    data.y <- data.y - (int 1)

  let%entry_point go_y () =
    data.x <- data.x - (int 1);
    data.y <- data.y + (int 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = int; y = int}]
      ~storage:[%expr
                 {x = int 0;
                  y = int 0}]
      [change; go_x; go_y]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())