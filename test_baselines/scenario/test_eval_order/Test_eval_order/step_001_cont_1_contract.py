import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(sub = sp.TAddress, x = sp.TString).layout(("sub", "x")))
    self.init(sub = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              x = '')

  @sp.entry_point
  def put_int(self, params):
    sp.set_type(params, sp.TInt)

  @sp.entry_point
  def test_expr_other(self):
    compute_test_eval_order_313 = sp.local("compute_test_eval_order_313", self.f_option(('A', sp.some(1))).open_some(message = self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_315 = sp.local("compute_test_eval_order_315", sp.some(1).open_some(message = self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_318 = sp.local("compute_test_eval_order_318", self.f_map(('A', {1 : 2}))[self.f_int(('B', 1))])
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_320 = sp.local("compute_test_eval_order_320", (self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_322 = sp.local("compute_test_eval_order_322", sp.record(a = self.f_int(('A', 1)), b = self.f_int(('B', 1))))
    compute_test_eval_order_324 = sp.local("compute_test_eval_order_324", sp.list([self.f_int(('A', 1)), self.f_int(('B', 1))]))
    compute_test_eval_order_326 = sp.local("compute_test_eval_order_326", sp.set([self.f_int(('A', 1)), self.f_int(('B', 1))]))
    compute_test_eval_order_328 = sp.local("compute_test_eval_order_328", {self.f_int(('A', 1)) : self.f_int(('B', 1)), self.f_int(('C', 1)) : self.f_int(('D', 1))})
    compute_test_eval_order_330 = sp.local("compute_test_eval_order_330", {1 : self.f_int(('B', 1)), self.f_int(('C', 1)) : self.f_int(('D', 1))})
    compute_test_eval_order_333 = sp.local("compute_test_eval_order_333", self.f_list(('A', sp.list([]))).map(self.f_lambda(('B', sp.build_lambda(lambda _x40: _x40)))))
    sp.verify(self.data.x == 'A')
    create_contract_test_eval_order_337 = sp.local("create_contract_test_eval_order_337", create contract ...)
    compute_test_eval_order_337 = sp.local("compute_test_eval_order_337", create_contract_test_eval_order_337.value)
    sp.verify(self.data.x == 'A')
    create_contract_test_eval_order_339 = sp.local("create_contract_test_eval_order_339", create contract ...)
    compute_test_eval_order_339 = sp.local("compute_test_eval_order_339", create_contract_test_eval_order_339.value)
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_343 = sp.local("compute_test_eval_order_343", sp.slice(self.f_string(('A', 'abc')), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_345 = sp.local("compute_test_eval_order_345", sp.slice(self.f_string(('A', 'abc')), 0, self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_348 = sp.local("compute_test_eval_order_348", sp.transfer_operation(self.f_int(('A', 1)), self.f_mutez(('B', sp.mutez(1))), self.f_contract(('C', sp.self_entry_point('put_int')))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_350 = sp.local("compute_test_eval_order_350", sp.transfer_operation(0, self.f_mutez(('B', sp.mutez(1))), self.f_contract(('C', sp.self_entry_point('put_int')))))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_mprim2(self):
    compute_test_eval_order_258 = sp.local("compute_test_eval_order_258", self.f_nat(('A', 1)) << self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_261 = sp.local("compute_test_eval_order_261", self.f_nat(('A', 1)) >> self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')

  @sp.entry_point
  def test_mprim3(self, params):
    sp.verify(sp.check_signature(self.f_key(('A', params.key)), self.f_signature(('B', params.signature)), self.f_bytes(('C', sp.bytes('0x00')))))
    sp.verify(self.data.x == 'A')
    sp.verify(sp.check_signature(params.key, self.f_signature(('B', params.signature)), self.f_bytes(('C', sp.bytes('0x00')))))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_prim2(self):
    ticket_136 = sp.local("ticket_136", sp.ticket(1, 1))
    compute_test_eval_order_144 = sp.local("compute_test_eval_order_144", self.f_map(('A', {})).get_opt(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_146 = sp.local("compute_test_eval_order_146", self.f_map(('A', {})).contains(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_148 = sp.local("compute_test_eval_order_148", self.f_lambda(('A', sp.build_lambda(lambda _x42: _x42)))(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_150 = sp.local("compute_test_eval_order_150", self.f_lambda2(('A', sp.build_lambda(lambda _x44: 0))).apply(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_152 = sp.local("compute_test_eval_order_152", sp.cons(self.f_int(('A', 1)), self.f_list(('B', sp.list([])))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_154 = sp.local("compute_test_eval_order_154", sp.add_seconds(self.f_timestamp(('A', sp.timestamp(1))), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    ticket_156 = sp.local("ticket_156", sp.ticket(self.f_int(('A', 1)), self.f_nat(('B', 1))))
    compute_test_eval_order_156 = sp.local("compute_test_eval_order_156", ticket_156.value)
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_161 = sp.local("compute_test_eval_order_161", self.f_int(('A', 1)) + self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_163 = sp.local("compute_test_eval_order_163", self.f_int(('A', 1)) * self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_165 = sp.local("compute_test_eval_order_165", self.f_int(('A', 1)) - self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_167 = sp.local("compute_test_eval_order_167", self.f_int(('A', 1)) + self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_169 = sp.local("compute_test_eval_order_169", self.f_nat(('A', 1)) // self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_171 = sp.local("compute_test_eval_order_171", self.f_int(('A', 1)) == self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_173 = sp.local("compute_test_eval_order_173", self.f_int(('A', 1)) != self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_175 = sp.local("compute_test_eval_order_175", self.f_int(('A', 1)) < self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_177 = sp.local("compute_test_eval_order_177", self.f_int(('A', 1)) <= self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_179 = sp.local("compute_test_eval_order_179", self.f_int(('A', 1)) > self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_181 = sp.local("compute_test_eval_order_181", self.f_int(('A', 1)) >= self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_183 = sp.local("compute_test_eval_order_183", self.f_int(('A', 1)) % self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_185 = sp.local("compute_test_eval_order_185", self.f_nat(('A', 1)) ^ self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_187 = sp.local("compute_test_eval_order_187", sp.min(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_189 = sp.local("compute_test_eval_order_189", sp.max(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_191 = sp.local("compute_test_eval_order_191", sp.ediv(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_194 = sp.local("compute_test_eval_order_194", self.f_bool(('A', False)) | self.f_bool(('B', False)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_196 = sp.local("compute_test_eval_order_196", self.f_bool(('A', False)) | self.f_bool(('B', True)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_198 = sp.local("compute_test_eval_order_198", self.f_bool(('A', True)) | self.f_bool(('B', False)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_200 = sp.local("compute_test_eval_order_200", self.f_bool(('A', True)) | self.f_bool(('B', True)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_203 = sp.local("compute_test_eval_order_203", self.f_bool(('A', False)) & self.f_bool(('B', False)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_205 = sp.local("compute_test_eval_order_205", self.f_bool(('A', False)) & self.f_bool(('B', True)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_207 = sp.local("compute_test_eval_order_207", self.f_bool(('A', True)) & self.f_bool(('B', False)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_209 = sp.local("compute_test_eval_order_209", self.f_bool(('A', True)) & self.f_bool(('B', True)))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_prim3(self):
    compute_test_eval_order_223 = sp.local("compute_test_eval_order_223", sp.split_tokens(self.f_mutez(('A', sp.mutez(1))), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_225 = sp.local("compute_test_eval_order_225", sp.split_tokens(sp.tez(0), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_228 = sp.local("compute_test_eval_order_228", sp.range(self.f_nat(('A', 1)), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_232 = sp.local("compute_test_eval_order_232", sp.update_map(self.f_map(('A', {})), self.f_int(('B', 1)), self.f_option(('C', sp.some(1)))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_234 = sp.local("compute_test_eval_order_234", sp.update_map(self.f_map(('A', {})), 0, self.f_option(('C', sp.some(1)))))
    sp.verify(self.data.x == 'C')
    match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd = sp.match_tuple(sp.get_and_update(self.f_map(('A', {})), self.f_int(('B', 1)), self.f_option(('C', sp.some(1)))), "match_pair_test_eval_order_238_fst", "match_pair_test_eval_order_238_snd")
    compute_test_eval_order_238 = sp.local("compute_test_eval_order_238", (match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd))
    sp.verify(self.data.x == 'B')
    match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd = sp.match_tuple(sp.get_and_update(self.f_map(('A', {})), 0, self.f_option(('C', sp.some(1)))), "match_pair_test_eval_order_240_fst", "match_pair_test_eval_order_240_snd")
    compute_test_eval_order_240 = sp.local("compute_test_eval_order_240", (match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_243 = sp.local("compute_test_eval_order_243", sp.eif(self.f_bool(('A', True)), self.f_int(('B', 1)), self.f_int(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_245 = sp.local("compute_test_eval_order_245", sp.eif(self.f_bool(('A', True)), 0, self.f_int(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_248 = sp.local("compute_test_eval_order_248", sp.eif(self.f_bool(('A', False)), self.f_int(('B', 1)), self.f_int(('C', 1))))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_250 = sp.local("compute_test_eval_order_250", sp.eif(self.f_bool(('A', False)), self.f_int(('B', 1)), 0))
    sp.verify(self.data.x == 'A')

  @sp.private_lambda()
  def f_address(_x0):
    self.data.x = sp.fst(_x0)
    sp.result(sp.set_type_expr(sp.snd(_x0), sp.TAddress))

  @sp.private_lambda()
  def f_bool(_x2):
    self.data.x = sp.fst(_x2)
    sp.result(sp.set_type_expr(sp.snd(_x2), sp.TBool))

  @sp.private_lambda()
  def f_bytes(_x4):
    self.data.x = sp.fst(_x4)
    sp.result(sp.set_type_expr(sp.snd(_x4), sp.TBytes))

  @sp.private_lambda()
  def f_contract(_x6):
    self.data.x = sp.fst(_x6)
    sp.result(sp.set_type_expr(sp.snd(_x6), sp.TContract(sp.TInt)))

  @sp.private_lambda()
  def f_int(_x8):
    self.data.x = sp.fst(_x8)
    sp.result(sp.set_type_expr(sp.snd(_x8), sp.TInt))

  @sp.private_lambda()
  def f_key(_x10):
    x1, x2 = sp.match_tuple(_x10, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TKey))

  @sp.private_lambda()
  def f_lambda(_x12):
    self.data.x = sp.fst(_x12)
    sp.result(sp.set_type_expr(sp.snd(_x12), sp.TLambda(sp.TInt, sp.TInt)))

  @sp.private_lambda()
  def f_lambda2(_x14):
    self.data.x = sp.fst(_x14)
    sp.result(sp.set_type_expr(sp.snd(_x14), sp.TLambda(sp.TPair(sp.TInt, sp.TInt), sp.TInt)))

  @sp.private_lambda()
  def f_list(_x16):
    self.data.x = sp.fst(_x16)
    sp.result(sp.set_type_expr(sp.snd(_x16), sp.TList(sp.TInt)))

  @sp.private_lambda()
  def f_map(_x18):
    self.data.x = sp.fst(_x18)
    sp.result(sp.set_type_expr(sp.snd(_x18), sp.TMap(sp.TInt, sp.TInt)))

  @sp.private_lambda()
  def f_mutez(_x20):
    self.data.x = sp.fst(_x20)
    sp.result(sp.set_type_expr(sp.snd(_x20), sp.TMutez))

  @sp.private_lambda()
  def f_nat(_x22):
    self.data.x = sp.fst(_x22)
    sp.result(sp.set_type_expr(sp.snd(_x22), sp.TNat))

  @sp.private_lambda()
  def f_option(_x24):
    self.data.x = sp.fst(_x24)
    sp.result(sp.set_type_expr(sp.snd(_x24), sp.TOption(sp.TInt)))

  @sp.private_lambda()
  def f_option_key_hash(_x26):
    self.data.x = sp.fst(_x26)
    sp.result(sp.set_type_expr(sp.snd(_x26), sp.TOption(sp.TKeyHash)))

  @sp.private_lambda()
  def f_pair(_x28):
    self.data.x = sp.fst(_x28)
    sp.result(sp.set_type_expr(sp.snd(_x28), sp.TPair(sp.TNat, sp.TNat)))

  @sp.private_lambda()
  def f_signature(_x30):
    x1, x2 = sp.match_tuple(_x30, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TSignature))

  @sp.private_lambda()
  def f_string(_x32):
    self.data.x = sp.fst(_x32)
    sp.result(sp.set_type_expr(sp.snd(_x32), sp.TString))

  @sp.private_lambda()
  def f_ticket(_x34):
    x1, x2 = sp.match_tuple(_x34, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TTicket(sp.TInt)))

  @sp.private_lambda()
  def f_timestamp(_x36):
    self.data.x = sp.fst(_x36)
    sp.result(sp.set_type_expr(sp.snd(_x36), sp.TTimestamp))

  @sp.private_lambda()
  def f_unit(_x38):
    self.data.x = _x38
    pass

sp.add_compilation_target("test", Contract())