open SmartML

module Contract = struct
  let%entry_point ep1 () =
    with modify(data, "x") as "x":
      result (x + 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ intOrNat]
      ~storage:[%expr 42]
      [ep1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())