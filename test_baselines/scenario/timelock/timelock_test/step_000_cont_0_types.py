import smartpy as sp

tstorage = sp.TRecord(chest = sp.TChest, decrypted = sp.TOption(sp.TString), time = sp.TNat).layout(("chest", ("decrypted", "time")))
tparameter = sp.TVariant(ep = sp.TChest_key).layout("ep")
tprivates = { }
tviews = { }
