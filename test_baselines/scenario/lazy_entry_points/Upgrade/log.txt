Table Of Contents

 Upgradable and lazy entrypoints
# Use initial entrypoint
 Updating an entrypoint
# Switch it to incrementing
# Switch it to decrementing
# Switch it to decrementing with additional transfers
 Testing the presence of an entrypoint
# entrypoint is still absent (lazy_no_code)
# Add entrypoint and call it
 Use entrypoint from another contract
Comment...
 h1: Upgradable and lazy entrypoints
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair (Pair 1 True) {Elt 1 { UNPAIR; IF_LEFT { PUSH int -1; FAILWITH } { IF_LEFT {} { PUSH int -1; FAILWITH } }; UPDATE 1; NIL operation; PAIR }; Elt 2 { UNPAIR; IF_LEFT { PUSH int -1; FAILWITH } { IF_LEFT { PUSH int -1; FAILWITH } {} }; PUSH int 2; MUL; UPDATE 1; NIL operation; PAIR }})
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_storage.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_storage.json 48
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_storage.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_types.py 7
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_contract.tz 152
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_contract.json 281
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_contract.py 42
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_002_cont_0_contract.ml 42
Comment...
 h2: Use initial entrypoint
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_004_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_004_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_004_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 1 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_005_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_005_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_005_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 2 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_006_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_006_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_006_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 3 True)
Verifying sp.contract_data(0).x == 3...
 OK
Comment...
 h1: Updating an entrypoint
Comment...
 h2: Switch it to incrementing
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_010_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_010_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_010_cont_0_params.json 20
Executing update_modify_x(sp.build_lambda(f_x0))...
 -> (Pair 3 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_011_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_011_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_011_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 4 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_012_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_012_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_012_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 6 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_013_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_013_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_013_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 9 True)
Verifying sp.contract_data(0).x == 9...
 OK
Comment...
 h2: Switch it to decrementing
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_016_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_016_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_016_cont_0_params.json 21
Executing update_modify_x(sp.build_lambda(f_x2))...
 -> (Pair 9 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_017_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_017_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_017_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 8 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_018_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_018_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_018_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 6 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_019_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_019_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_019_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 3 True)
Verifying sp.contract_data(0).x == 3...
 OK
Comment...
 h2: Switch it to decrementing with additional transfers
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_022_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_022_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_022_cont_0_params.json 11
Executing update_modify_x(sp.build_lambda(f_x4))...
 -> (Pair 3 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_023_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_023_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_023_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 2 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_024_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_024_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_024_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 0 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_025_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_025_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_025_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1e4mwHKk2WYbNmWbSh6qP2mgxcjDxrZtno')).open_some()
Verifying sp.contract_data(0).x == (-3)...
 OK
Comment...
 h1: Testing the presence of an entrypoint
Comment...
 h2: entrypoint is still absent (lazy_no_code)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_029_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_029_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_029_cont_0_params.json 1
Executing check_bounce(sp.record())...
 -> --- Expected failure in transaction --- Wrong condition: (sp.entrypoint_map().contains(sp.entrypoint_id("bounce")) : sp.TBool) (python/templates/lazy_entry_points.py, line 39)
 (python/templates/lazy_entry_points.py, line 39)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_030_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_030_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_030_cont_0_params.json 1
Executing bounce(sp.resolve(sp.test_account("Alice").address))...
 -> --- Expected failure in transaction --- lazy entry point not found
Comment...
 h2: Add entrypoint and call it
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_032_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_032_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_032_cont_0_params.json 15
Executing update_bounce(sp.build_lambda(f_x6))...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C')).open_some()
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_033_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_033_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_033_cont_0_params.json 1
Executing bounce(sp.resolve(sp.test_account("Alice").address))...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.mutez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi')).open_some()
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_034_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_034_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_034_cont_0_params.json 1
Executing check_bounce(sp.record())...
 -> (Pair -3 True)
Comment...
 h1: Use entrypoint from another contract
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> (Pair (Pair 1 True) {Elt 1 { UNPAIR; IF_LEFT { PUSH int -1; FAILWITH } { IF_LEFT {} { PUSH int -1; FAILWITH } }; UPDATE 1; NIL operation; PAIR }; Elt 2 { UNPAIR; IF_LEFT { PUSH int -1; FAILWITH } { IF_LEFT { PUSH int -1; FAILWITH } {} }; PUSH int 2; MUL; UPDATE 1; NIL operation; PAIR }})
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_storage.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_storage.json 48
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_sizes.csv 2
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_storage.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_types.py 7
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_contract.tz 152
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_contract.json 281
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_contract.py 42
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_036_cont_1_contract.ml 42
Comment...
 p: Original entry point:
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_038_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_038_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_038_cont_0_params.json 19
Executing update_modify_x(sp.contract_view(1, "get_entrypoint", sp.contract_view(1, "modify_x_id", sp.unit).open_some(message = 'View modify_x_id is invalid!')).open_some(message = 'View get_entrypoint is invalid!'))...
 -> (Pair -3 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_039_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_039_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_039_cont_0_params.json 1
Executing modify_x(42)...
 -> (Pair 42 True)
Verifying sp.contract_data(0).x == 42...
 OK
Comment...
 p: Modified entry point (obtained via view, using entrypoint):
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_042_cont_1_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_042_cont_1_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_042_cont_1_params.json 21
Executing update_modify_x(sp.build_lambda(f_x8))...
 -> (Pair 1 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_043_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_043_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_043_cont_0_params.json 21
Executing update_modify_x(sp.contract_view(1, "get_entrypoint", sp.contract_view(1, "modify_x_id", sp.unit).open_some(message = 'View modify_x_id is invalid!')).open_some(message = 'View get_entrypoint is invalid!'))...
 -> (Pair 42 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_044_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_044_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_044_cont_0_params.json 1
Executing modify_x(5)...
 -> (Pair 37 True)
Verifying sp.contract_data(0).x == 37...
 OK
Comment...
 p: Modified entry point (obtained via sp.contract_entrypoint):
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_047_cont_1_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_047_cont_1_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_047_cont_1_params.json 20
Executing update_modify_x(sp.build_lambda(f_x10))...
 -> (Pair 1 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_048_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_048_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_048_cont_0_params.json 20
Executing update_modify_x(sp.contract_entrypoint_map(1)[sp.contract_entrypoint_map(1,"modify_x")])...
 -> (Pair 37 True)
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_049_cont_0_params.py 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_049_cont_0_params.tz 1
 => test_baselines/scenario/lazy_entry_points/Upgrade/step_049_cont_0_params.json 1
Executing modify_x(5)...
 -> (Pair 42 True)
Verifying sp.contract_data(0).x == 42...
 OK
Comment...
 p: Entrypoint map and ids
Verifying sp.contract_view(1, "modify_x_id", sp.unit).open_some(message = 'View modify_x_id is invalid!') == 1...
 OK
Verifying sp.contract_entrypoint_map(0,"modify_x") == 1...
 OK
Verifying sp.contract_entrypoint_map(0).contains(1)...
 OK
