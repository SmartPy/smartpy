import smartpy as sp

tstorage = sp.TRecord(counterparty = sp.TAddress, epoch = sp.TTimestamp, hashedSecret = sp.TBytes, notional = sp.TMutez, owner = sp.TAddress).layout(("counterparty", ("epoch", ("hashedSecret", ("notional", "owner")))))
tparameter = sp.TVariant(allSigned = sp.TUnit, cancelSwap = sp.TUnit, knownSecret = sp.TRecord(secret = sp.TBytes).layout("secret")).layout(("allSigned", ("cancelSwap", "knownSecret")))
tprivates = { }
tviews = { }
