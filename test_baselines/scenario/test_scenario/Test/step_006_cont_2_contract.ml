open SmartML

module Contract = struct
  let%entry_point ep2 () =
    let%mutable f = (fun _x0 -> result (scenario_var 1)) in ();
    verify ((0 f) = 43)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())