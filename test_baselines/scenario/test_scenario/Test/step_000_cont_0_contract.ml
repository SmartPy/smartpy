open SmartML

module Contract = struct
  let%entry_point ep params =
    data.x <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = option intOrNat}]
      ~storage:[%expr
                 {x = None}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())