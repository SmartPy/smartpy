import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt).layout("x"))
    self.init(x = 0)

  @sp.entry_point
  def run(self, params):
    sp.set_type(params.f, sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write"))
    compute_test_with_storage_33 = sp.local("compute_test_with_storage_33", params.f(params.x))

  @sp.entry_point
  def save(self, params):
    sp.set_type(params.f, sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write"))
    self.data.x = params.f(params.x)

sp.add_compilation_target("test", Contract())