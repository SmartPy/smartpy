open SmartML

module Contract = struct
  let%entry_point run params =
    set_type params.f lambda int int ~with_storage:"read-write";
    let%mutable compute_test_with_storage_33 = (params.x params.f) in ()

  let%entry_point save params =
    set_type params.f lambda int int ~with_storage:"read-write";
    data.x <- params.x params.f

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = int}]
      ~storage:[%expr
                 {x = int 0}]
      [run; save]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())