import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def call(self, params):
    compute_test_with_storage_44 = sp.local("compute_test_with_storage_44", sp.view("get_set_f", sp.unit, params.lib, sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write")))
    compute_test_with_storage_45 = sp.local("compute_test_with_storage_45", sp.contract(sp.TRecord(f = sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write"), x = sp.TInt).layout(("f", "x")), params.main, entry_point='run'))
    sp.transfer(sp.record(f = compute_test_with_storage_44.value.open_some(), x = params.x), sp.tez(0), compute_test_with_storage_45.value.open_some())

sp.add_compilation_target("test", Contract())