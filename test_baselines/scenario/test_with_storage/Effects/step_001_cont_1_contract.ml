open SmartML

module Contract = struct

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {y = string}]
      ~storage:[%expr
                 {y = "abc"}]
      []
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())