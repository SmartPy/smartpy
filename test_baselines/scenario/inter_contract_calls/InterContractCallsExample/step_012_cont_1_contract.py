import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(worker_contract_address = sp.TAddress).layout("worker_contract_address"))
    self.init(worker_contract_address = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'))

  @sp.entry_point
  def append_multiple_messages(self, params):
    sp.set_type(params, sp.TRecord(messages = sp.TList(sp.TString), separator = sp.TString).layout(("messages", "separator")))
    sp.for message in params.messages:
      sp.transfer(sp.record(message = message, separator = params.separator), sp.tez(0), sp.contract(sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), self.data.worker_contract_address, entry_point='append_message').open_some())

  @sp.entry_point
  def store_single_message(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TString, self.data.worker_contract_address, entry_point='set_message').open_some())

sp.add_compilation_target("test", Contract())