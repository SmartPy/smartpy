import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l1 = sp.TList(sp.TString), l2 = sp.TList(sp.TString), m1 = sp.TMap(sp.TString, sp.TString), m2 = sp.TMap(sp.TString, sp.TString), o1 = sp.TOption(sp.TString)).layout(("l1", ("l2", ("m1", ("m2", "o1"))))))
    self.init(l1 = [],
              l2 = ['c'],
              m1 = {},
              m2 = {'a' : 'b'},
              o1 = sp.none)

  @sp.entry_point
  def ep1(self):
    self.data.m1 = {'e' : 'f'}
    self.data.l1 = sp.list(['g'])
    self.data.o1 = sp.some('h')

  @sp.entry_point
  def ep2(self):
    self.data.m2 = {}
    self.data.l2 = sp.list([])

sp.add_compilation_target("test", Contract())