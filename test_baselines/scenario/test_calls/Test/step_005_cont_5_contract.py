import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def send(self, params):
    sp.send(params.address, params.amount)

sp.add_compilation_target("test", Contract())