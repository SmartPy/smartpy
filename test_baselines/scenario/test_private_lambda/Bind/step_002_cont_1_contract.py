import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x"))
    self.init(x = sp.none)

  @sp.entry_point
  def check(self, params):
    sp.verify(self.f(params.params) == params.result)

  @sp.entry_point
  def update(self, params):
    self.data.x = sp.some(self.f(params))

  @sp.private_lambda()
  def f(_x12):
    sp.result(_x12 * 2)

sp.add_compilation_target("test", Contract())