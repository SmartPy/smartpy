import smartpy as sp

tstorage = sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x")
tparameter = sp.TVariant(check = sp.TRecord(params = sp.TIntOrNat, result = sp.TIntOrNat).layout(("params", "result")), update = sp.TIntOrNat).layout(("check", "update"))
tprivates = { "f": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat) }
tviews = { }
