import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, admin = sp.TAddress, value = sp.TOption(sp.TInt)).layout(("active", ("admin", "value"))))
    self.init(active = False,
              admin = sp.address('tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5'),
              value = sp.none)

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, 'NOT ADMIN')
    sp.set_type(params, sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin"))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('setActive') as setActive:
          self.data.active = setActive
        with arg.match('setAdmin') as setAdmin:
          self.data.admin = setAdmin


  @sp.entry_point
  def setValue(self, params):
    sp.verify(self.data.active, 'NOT ACTIVE')
    self.data.value = params

sp.add_compilation_target("test", Contract())