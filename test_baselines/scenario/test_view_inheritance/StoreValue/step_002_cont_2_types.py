import smartpy as sp

tstorage = sp.TRecord(storedValue = sp.TIntOrNat).layout("storedValue")
tparameter = sp.TVariant(add = sp.TRecord(value = sp.TIntOrNat).layout("value")).layout("add")
tprivates = { }
tviews = { "new_name": ((), sp.TIntOrNat) }
