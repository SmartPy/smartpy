open SmartML

module Contract = struct
  let%entry_point f params =
    data.a <- data.a + params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = int; b = bool}]
      [f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())