import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l = sp.TLambda(sp.TInt, sp.TPair(sp.TOperation, sp.TAddress)), x = sp.TOption(sp.TAddress)).layout(("l", "x")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TList(sp.TInt), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    x68 = sp.bind_block("x68"):
    with x68:
      with params.match_cases() as arg:
        with arg.match('Left') as l3:
          with l3.match_cases() as arg:
            with arg.match('Left') as _l32:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps287 ->
                        let x303 =
                          let [x423; x424] = Unpair(2, Cdr(ps287))
                          let _ = x423
                          Pair(Add(Car(Car(ps287)), Car(Cdr(ps287))), x424)
                        Pair
                          ( Nil<operation>
                          , let [x421; x422] = Unpair(2, x303)
                            Pair
                              ( x421
                              , let _ = x422
                                Add(Cdr(Car(ps287)), Cdr(x303))
                              )
                          ), None<key_hash>, 0#mutez, Pair(12, 15#nat))]')
              sp.failwith(sp.build_lambda(ferror)(sp.unit))
            with arg.match('Right') as r137:
              with r137.match_cases() as arg:
                with arg.match('Left') as _l35:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps193 ->
                        let x209 =
                          let [x419; x420] = Unpair(2, Cdr(ps193))
                          let _ = x419
                          Pair(Add(Car(Car(ps193)), Car(Cdr(ps193))), x420)
                        Pair
                          ( Nil<operation>
                          , let [x417; x418] = Unpair(2, x209)
                            Pair
                              ( x417
                              , let _ = x418
                                Add(Cdr(Car(ps193)), Cdr(x209))
                              )
                          ), None<key_hash>, 2000000#mutez, Pair(12, 15#nat))]')
                  sp.failwith(sp.build_lambda(ferror)(sp.unit))
                with arg.match('Right') as _r36:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps146 ->
                        let x162 =
                          let [x411; x412] = Unpair(2, Cdr(ps146))
                          let _ = x411
                          Pair(Add(Car(Car(ps146)), Car(Cdr(ps146))), x412)
                        Pair
                          ( Nil<operation>
                          , let [x409; x410] = Unpair(2, x162)
                            Pair
                              ( x409
                              , let _ = x410
                                Add(Cdr(Car(ps146)), Cdr(x162))
                              )
                          ), None<key_hash>, 0#mutez, Pair(12, 15#nat))]')
                  sp.failwith(sp.build_lambda(ferror)(sp.unit))


        with arg.match('Right') as r4:
          with r4.match_cases() as arg:
            with arg.match('Left') as l5:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r352] =
          iter [ l5; Nil<operation> ]
          step s81; s82 ->
            let [x92; _] =
              create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                              storage   :  { a : int ; b : nat }
                              ps94 ->
                              let x110 =
                                let [x407; x408] = Unpair(2, Cdr(ps94))
                                let _ = x407
                                Pair
                                  ( Add(Car(Car(ps94)), Car(Cdr(ps94)))
                                  , x408
                                  )
                              Pair
                                ( Nil<operation>
                                , let [x405; x406] = Unpair(2, x110)
                                  Pair
                                    ( x405
                                    , let _ = x406
                                      Add(Cdr(Car(ps94)), Cdr(x110))
                                    )
                                ), None<key_hash>, 0#mutez, Pair(s81, 15#nat))
            [ Cons(x92, s82) ]
        [ r352; Pair(__storage.l, __storage.x) ]]')
              sp.failwith(sp.build_lambda(ferror)(sp.unit))
            with arg.match('Right') as r6:
              with r6.match_cases() as arg:
                with arg.match('Left') as _l6:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps34 ->
                        let x50 =
                          let [x403; x404] = Unpair(2, Cdr(ps34))
                          let _ = x403
                          Pair(Add(Car(Car(ps34)), Car(Cdr(ps34))), x404)
                        Pair
                          ( Nil<operation>
                          , let [x401; x402] = Unpair(2, x50)
                            Pair
                              ( x401
                              , let _ = x402
                                Add(Cdr(Car(ps34)), Cdr(x50))
                              )
                          ), None<key_hash>, 0#mutez, Pair(1, 2#nat))]')
                  sp.failwith(sp.build_lambda(ferror)(sp.unit))
                with arg.match('Right') as _r7:
                  x14 = sp.local("x14", self.data.l(42))
                  sp.result((sp.cons(sp.fst(x14.value), sp.list([])), (self.data.l, sp.some(sp.snd(x14.value)))))



    s331 = sp.local("s331", sp.fst(x68.value))
    s332 = sp.local("s332", sp.snd(x68.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s332)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
