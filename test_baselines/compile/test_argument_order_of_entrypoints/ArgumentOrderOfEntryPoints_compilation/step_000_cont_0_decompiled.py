import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c"))))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TPair(sp.TBool, sp.TString)))
    self.data = sp.record(a = sp.fst(params), b = sp.snd(sp.snd(params)), c = sp.fst(sp.snd(params)))

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TString, sp.TBool)))
    self.data = sp.record(a = self.data.a, b = self.data.b, c = self.data.c)

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TString)
    self.data = sp.record(a = self.data.a, b = params, c = self.data.c)

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
