import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)
    self.init(0)

  @sp.entry_point
  def ep(self, params):
    self.data = sp.constant("c1", t = sp.TLambda(sp.TNat, sp.TNat))(params) + sp.constant("c2", t = sp.TNat)

sp.add_compilation_target("test", Contract())