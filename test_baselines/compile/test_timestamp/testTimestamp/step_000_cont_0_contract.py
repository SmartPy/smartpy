import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(next = sp.TTimestamp, out = sp.TBool).layout(("next", "out")))
    self.init(next = sp.timestamp(0),
              out = False)

  @sp.entry_point
  def ep(self):
    self.data.out = sp.now > sp.add_seconds(sp.now, 1)
    sp.verify((sp.add_seconds(sp.now, 12) - sp.now) == 12)
    sp.verify((sp.now - sp.add_seconds(sp.now, 12)) == (-12))
    sp.verify((sp.now - sp.add_seconds(sp.now, 12)) == (-12))
    sp.verify(sp.add(sp.now, 500) == sp.timestamp(1500))
    sp.verify(sp.add(500, sp.now) == sp.timestamp(1500))
    self.data.next = sp.add_seconds(sp.now, 86400)

sp.add_compilation_target("test", Contract())