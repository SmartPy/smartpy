import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TInt)).layout(("first", ("last", "saved"))))

  @sp.entry_point
  def pop(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(self.data.first < self.data.last, 'WrongCondition: self.data.fif.first < self.data.fif.last')
    x19 = sp.local("x19", (1 + self.data.first, (self.data.last, sp.update_map(self.data.saved, self.data.first, sp.none))))
    x77 = sp.local("x77", x19.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x77)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def push(self, params):
    sp.set_type(params, sp.TInt)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x14 =
                              Pair
                                ( __storage.first
                                , Pair
                                    ( Add(1, __storage.last)
                                    , __storage.saved
                                    )
                                )
                            let [x79; x80] = Unpair(2, x14)
                            Pair
                              ( x79
                              , let [x81; x82] = Unpair(2, x80)
                                Pair
                                  ( x81
                                  , let _ = x82
                                    Update(Car(Cdr(x14)), Some_(r4), Cdr(Cdr(x14)))
                                  )
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
