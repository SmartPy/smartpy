import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x1 : { nat ; (list(operation) ; (nat ; (string ; nat))) }) : { nat ; (list(operation) ; (nat ; (string ; nat))) } ->
let [x2; x3; x4] = Unpair(3, x1)
match Contract("tz1h4EsGunH2Ue1T2uNs8mfKZ8XZoQji3HcK"#address, unit) with
| None _ -> Failwith(10)
| Some s9 ->
    let x21 = Mul(x2, 1000000#mutez)
    match Contract("tz1hJgZdhnRGvg5XD6pYxRCsbWh4jg5HQ476"#address, unit) with
    | None _ -> Failwith(11)
    | Some s27 ->
        let x47 =
          let [x269; x270] = Unpair(2, x4)
          let _ = x269
          Pair(Add(1#nat, Car(x4)), x270)
        Pair
          ( Mul(x2, Car(x47))
          , Pair
              ( Cons
                  ( Transfer_tokens(Unit, 2000000#mutez, s27)
                  , Cons(Transfer_tokens(Unit, x21, s9), x3)
                  )
              , x47
              )
          )
    end
end)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
