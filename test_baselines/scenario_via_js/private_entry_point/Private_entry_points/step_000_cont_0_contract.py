import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt, y = sp.TInt).layout(("x", "y")))
    self.init(x = 0,
              y = 0)

  @sp.entry_point(private = True)
  def change(self, params):
    self.data.x = params.x
    self.data.y = params.y

  @sp.entry_point
  def go_x(self):
    self.data.x += 1
    self.data.y -= 1

  @sp.entry_point
  def go_y(self):
    self.data.x -= 1
    self.data.y += 1

sp.add_compilation_target("test", Contract())