import smartpy as sp

tstorage = sp.TRecord(inactiveBefore = sp.TNat, lambdas = sp.TBigMap(sp.TNat, sp.TLambda(sp.TUnit, sp.TList(sp.TOperation))), members = sp.TSet(sp.TAddress), nextId = sp.TNat, required_votes = sp.TNat, votes = sp.TBigMap(sp.TNat, sp.TSet(sp.TAddress))).layout(("inactiveBefore", ("lambdas", ("members", ("nextId", ("required_votes", "votes"))))))
tparameter = sp.TVariant(submit_lambda = sp.TLambda(sp.TUnit, sp.TList(sp.TOperation)), vote_lambda = sp.TNat).layout(("submit_lambda", "vote_lambda"))
tprivates = { }
tviews = { "get_lambda": (sp.TNat, sp.TPair(sp.TLambda(sp.TUnit, sp.TList(sp.TOperation)), sp.TBool)) }
