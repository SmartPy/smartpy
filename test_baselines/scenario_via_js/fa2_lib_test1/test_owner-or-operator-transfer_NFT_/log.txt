Comment...
 h1: test_owner-or-operator-transfer_NFT_
Table Of Contents

 test_owner-or-operator-transfer_NFT_
# Accounts
# FA2 with OwnerOrOperatorTransfer policy
# Alice can transfer
# Admin cannot transfer alices's token: FA2_NOT_OPERATOR
# Alice adds Bob as operator
# Bob can transfer Alice's token id 0
# Bob cannot transfer Alice's token id 1
# Alice can remove Bob as operator and add Charlie
# Bob cannot transfer Alice's token 0 anymore
# Charlie can transfer Alice's token
# Add then Remove in the same batch is transparent
# Remove then Add do add the operator
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz'))]
Comment...
 h2: FA2 with OwnerOrOperatorTransfer policy
Comment...
 p: Owner or operators can transfer.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_storage.json 81
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_storage.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_types.py 7
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_metadata.metadata_base.json 150
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_contract.tz 251
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_contract.json 384
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_006_cont_0_contract.py 45
Comment...
 h2: Alice can transfer
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_008_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Comment...
 h2: Admin cannot transfer alices's token: FA2_NOT_OPERATOR
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_010_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_010_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_010_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((sp.sender == transfer.from_) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_lib.py, line 111)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_lib.py, line 110)
Comment...
 h2: Alice adds Bob as operator
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_012_cont_0_params.json 11
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))...
 OK
Comment...
 h2: Bob can transfer Alice's token id 0
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_015_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_015_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_015_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Comment...
 h2: Bob cannot transfer Alice's token id 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_017_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_017_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_017_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 1, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((sp.sender == transfer.from_) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_lib.py, line 111)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_lib.py, line 110)
Comment...
 h2: Alice can remove Bob as operator and add Charlie
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_019_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_019_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_019_cont_0_params.json 20
Executing update_operators(sp.list([variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Charlie").address), token_id = 0))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Verifying ~ (sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)))...
 OK
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Charlie").address), token_id = 0))...
 OK
Comment...
 h2: Bob cannot transfer Alice's token 0 anymore
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_023_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_023_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_023_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((sp.sender == transfer.from_) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_lib.py, line 111)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_lib.py, line 110)
Comment...
 h2: Charlie can transfer Alice's token
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_025_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_025_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_025_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Charlie").address), token_id = 0, amount = 1)]))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Charlie").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Comment...
 h2: Add then Remove in the same batch is transparent
Comment...
 p: TZIP-12: If two different commands in the list add and remove an
                operator for the same token owner and token ID, the last command
                in the list MUST take effect.
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_029_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_029_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_029_cont_0_params.json 20
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Verifying ~ (sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)))...
 OK
Comment...
 h2: Remove then Add do add the operator
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_032_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_032_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test1/test_owner-or-operator-transfer_NFT_/step_032_cont_0_params.json 20
Executing update_operators(sp.list([variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair 3 (Pair {Elt 0 "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))...
 OK
