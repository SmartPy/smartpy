import smartpy as sp

tstorage = sp.TRecord(x = sp.TBool, y = sp.TBool).layout(("x", "y"))
tparameter = sp.TVariant(ep = sp.TAddress, test = sp.TPair(sp.TAddress, sp.TAddress)).layout(("ep", "test"))
tprivates = { }
tviews = { }
