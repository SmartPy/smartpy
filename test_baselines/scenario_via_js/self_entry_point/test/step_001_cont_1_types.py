import smartpy as sp

tstorage = sp.TRecord(a = sp.TAddress).layout("a")
tparameter = sp.TVariant(ep3 = sp.TBool, ep4 = sp.TTimestamp).layout(("ep3", "ep4"))
tprivates = { }
tviews = { }
