import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit).layout(("ep1", "ep2"))
tprivates = { }
tviews = { }
