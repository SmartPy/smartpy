import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def deposit(self):
    pass

  @sp.entry_point
  def withdraw(self, params):
    sp.send(params.address, params.quantity)

  @sp.entry_point
  def withdraw_all(self, params):
    sp.send(params, sp.balance)

sp.add_compilation_target("test", Contract())