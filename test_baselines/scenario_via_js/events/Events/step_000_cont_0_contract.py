import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self):
    sp.emit('Hello')
    sp.emit('World', tag = "mytag")
    sp.emit(sp.record(a = 'ABC', b = 'XYZ'), tag = "mytag2")
    sp.emit(sp.record(a = 'ABC', b = 'XYZ'), tag = "mytag2", with_type = false)

sp.add_compilation_target("test", Contract())