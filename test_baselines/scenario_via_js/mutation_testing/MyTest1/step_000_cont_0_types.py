import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(entry_point_1 = sp.TUnit, entry_point_2 = sp.TIntOrNat).layout(("entry_point_1", "entry_point_2"))
tprivates = { }
tviews = { }
