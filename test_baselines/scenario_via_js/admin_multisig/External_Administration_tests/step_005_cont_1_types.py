import smartpy as sp

tstorage = sp.TRecord(active = sp.TBool, admin = sp.TAddress).layout(("active", "admin"))
tparameter = sp.TVariant(administrate = sp.TBytes, verifyActive = sp.TUnit).layout(("administrate", "verifyActive"))
tprivates = { }
tviews = { }
