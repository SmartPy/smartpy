import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TMap(sp.TRecord(a = sp.TNat, b = sp.TAddress, c = sp.TNat).layout(("a", ("b", "c"))), sp.TUnit)).layout("x"))
    self.init(x = {})

  @sp.entry_point
  def ep(self):
    sp.verify(self.data.x.contains(sp.record(a = 0, b = sp.sender, c = 0)))

sp.add_compilation_target("test", Contract())