import smartpy as sp

tstorage = sp.TRecord(a = sp.TNat).layout("a")
tparameter = sp.TVariant(test_sub_incr = sp.TUnit, test_sub_incr_both = sp.TUnit, test_sub_incr_store = sp.TUnit, test_with_storage = sp.TUnit).layout((("test_sub_incr", "test_sub_incr_both"), ("test_sub_incr_store", "test_with_storage")))
tprivates = { "double": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat, with_storage="read-write"), "incr": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat, with_storage="read-write"), "sub_double": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat, with_operations=True), "sub_incr": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat, with_operations=True), "sub_incr_both": sp.TLambda(sp.TNat, sp.TNat, with_storage="read-write", with_operations=True) }
tviews = { }
