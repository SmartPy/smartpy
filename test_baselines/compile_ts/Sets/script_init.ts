interface TStorage {
    set: TSet<TString>;
    elements: TOption<TList<TString>>;
}

@Contract
export class Sets {
    storage: TStorage = {
        set: [],
        elements: Sp.none,
    };

    @EntryPoint
    add(value: TString) {
        this.storage.set.add(value);
    }

    @EntryPoint
    contains(value: TString): void {
        if (this.storage.set.contains(value) && this.storage.set.size() === 3) {
            this.storage.elements = Sp.some(this.storage.set.elements());
            for (const x of this.storage.set.elements()) {
                this.storage.set.remove(x);
            }
        }
    }
}

Dev.test({ name: 'Sets' }, () => {
    const c1 = Scenario.originate(new Sets());
    Scenario.transfer(c1.add('String 1'));
    Scenario.transfer(c1.add('String 2'));
    Scenario.transfer(c1.add('String 3'));
    Scenario.transfer(c1.contains('String 2'));
});

Dev.compileContract('Sets_compiled', new Sets());
