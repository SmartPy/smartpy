import smartpy as sp

tstorage = sp.TInt
tparameter = sp.TUnit
tprivates = { }
tviews = { "getStorageState": ((), sp.TInt), "increment": ((), sp.TInt), "decrement": ((), sp.TInt) }
