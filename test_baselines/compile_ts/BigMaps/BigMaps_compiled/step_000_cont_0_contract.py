import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(map = sp.TBigMap(sp.TString, sp.TNat)).layout("map"))
    self.init(map = {})

  @sp.entry_point
  def set(self, params):
    sp.set_type(params, sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value")))
    self.data.map[params.key] = params.value

  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TString)
    value = sp.local("value", self.data.map[params])
    sp.if (self.data.map.contains(params)) & (value.value == 1):
      del self.data.map[params]

sp.add_compilation_target("test", Contract())