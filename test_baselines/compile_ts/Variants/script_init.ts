interface TStorage {
    value: TOption<TNat>;
}

type Variant1 = TVariant<
    | {
          kind: 'action1';
          value: TString;
      }
    | {
          kind: 'action2';
          value: TNat;
      },
    Layout.right_comb
>;

type Variant2 = TVariant<
    | {
          kind: 'action1';
          value: TString;
      }
    | {
          kind: 'action2';
          value: TNat;
      }
>;

type Variant3 = TVariant<
    | {
          kind: 'action1';
          value: TString;
      }
    | {
          kind: 'action2';
          value: TNat;
      },
    ['action1', 'action2']
>;

@Contract
export class Variants {
    storage: TStorage = {
        value: Sp.none,
    };

    @EntryPoint
    switchCase1(variant: Variant1): void {
        switch (variant.kind) {
            case 'action1':
                {
                    Sp.verify(variant.value === 'A String', "Expected value to be ('A String').");
                }
                break;
            case 'action2': {
                Sp.verify(variant.value === 10, 'Expected value to be (10).');
            }
        }
        if (variant.isVariant('action2')) {
            this.storage.value = Sp.some(variant.openVariant('action2'));
        }
    }

    @EntryPoint
    switchCase2(variant: Variant2): void {
        switch (variant.kind) {
            case 'action1':
                {
                    Sp.verify(variant.value === 'A String', "Expected value to be ('A String').");
                }
                break;
            case 'action2': {
                Sp.verify(variant.value === 10, 'Expected value to be (10).');
            }
        }
        if (variant.isVariant('action2')) {
            this.storage.value = Sp.some(variant.openVariant('action2'));
        }
    }

    @EntryPoint
    switchCase3(variant: Variant3): void {
        switch (variant.kind) {
            case 'action1':
                {
                    Sp.verify(variant.value === 'A String', "Expected value to be ('A String').");
                }
                break;
            case 'action2': {
                Sp.verify(variant.value === 10, 'Expected value to be (10).');
            }
        }
        if (variant.isVariant('action2')) {
            this.storage.value = Sp.some(variant.openVariant('action2'));
        }
    }
}

Dev.test({ name: 'Variants' }, () => {
    const c1 = Scenario.originate(new Variants());
    Scenario.transfer(c1.switchCase1({ kind: 'action1', value: 'A String' }));
    Scenario.transfer(c1.switchCase2(Sp.variant<Variant2>('action2', 10)));
    Scenario.transfer(c1.switchCase3(Sp.variant<Variant3>('action2', 10)));
});

Dev.compileContract('Variants_compiled', new Variants());
