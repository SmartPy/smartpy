import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def test_type0(self):
    sp.set_type(sp.unit, sp.TUnit)
    sp.set_type(True, sp.TBool)
    sp.set_type(0, sp.TNat)
    sp.set_type(sp.tez(0), sp.TMutez)
    sp.set_type('', sp.TString)
    sp.set_type(sp.bytes('0x'), sp.TBytes)
    sp.set_type(sp.chain_id('0x'), sp.TChainId)
    sp.set_type(sp.address('tz1'), sp.TAddress)
    sp.set_type(sp.key('edpkaaa'), sp.TKey)
    sp.set_type(sp.key_hash('tz1aaa'), sp.TKeyHash)
    sp.set_type(sp.signature('sigaaa'), sp.TSignature)
    sp.set_type(sp.bls12_381_g1('0x'), sp.TBls12_381_g1)
    sp.set_type(sp.bls12_381_g2('0x'), sp.TBls12_381_g2)
    sp.set_type(sp.bls12_381_fr('0x'), sp.TBls12_381_fr)

  @sp.entry_point
  def test_type1(self, params):
    sp.set_type(sp.some(sp.unit), sp.TOption(sp.TUnit))
    sp.set_type(sp.list([sp.unit]), sp.TList(sp.TUnit))
    sp.set_type(sp.set([sp.unit]), sp.TSet(sp.TUnit))
    sp.set_type(params.a, sp.TContract(sp.TUnit))
    sp.set_type(params.b, sp.TTicket(sp.TUnit))

  @sp.entry_point
  def test_type2(self):
    sp.set_type(sp.build_lambda(lambda x: sp.unit), sp.TLambda(sp.TUnit, sp.TUnit))
    sp.set_type({}, sp.TMap(sp.TUnit, sp.TUnit))
    sp.set_type((sp.unit, sp.unit), sp.TPair(sp.TUnit, sp.TUnit))
    sp.set_type((sp.unit, sp.unit), sp.TPair(sp.TUnit, sp.TUnit))

  @sp.entry_point
  def test_type(self):
    sp.set_type(sp.record(x = 0, y = 0, z = 0), sp.TRecord(x = sp.TInt, y = sp.TNat, z = sp.TNat).layout(("x", ("y", "z"))))
    sp.set_type(variant('A', 0), sp.TVariant(A = sp.TInt, B = sp.TNat).layout(("A", "B")))
    sp.set_type((1, 2, sp.unit), sp.TTuple(sp.TInt, sp.TNat, sp.TUnit))

  @sp.entry_point
  def test_literal(self):
    sp.verify(sp.unit == sp.unit)
    sp.verify(True != False)
    sp.verify(1 == 1)
    sp.verify((-1) == (-1))
    sp.verify(sp.mutez(1) == sp.mutez(1))
    sp.verify('abc' == 'abc')
    sp.verify(sp.bytes('0xabcd') == sp.bytes('0xabcd'))
    sp.verify(sp.chain_id('0xabcd') == sp.chain_id('0xabcd'))
    sp.verify(sp.timestamp(42) == sp.timestamp(42))
    sp.verify(sp.address('tz2aaa') == sp.address('tz2aaa'))
    sp.verify(sp.key('edpkabcd') == sp.key('edpkabcd'))
    sp.verify(sp.key_hash('tz1aaa') == sp.key_hash('tz1aaa'))
    sp.verify(sp.signature('sigaaa') == sp.signature('sigaaa'))
    sp.verify(sp.len(sp.list([sp.bls12_381_g1('0xabcd')])) == 1)
    sp.verify(sp.len(sp.list([sp.bls12_381_g2('0xabcd')])) == 1)
    sp.verify(sp.len(sp.list([sp.bls12_381_fr('0xabcd')])) == 1)

  @sp.entry_point
  def test_mprim0(self):
    sp.verify(sp.amount == sp.tez(0))
    sp.verify(sp.balance == sp.tez(0))
    sp.verify(sp.chain_id == sp.chain_id('0x'))
    sp.verify(sp.level == 0)
    sp.verify(sp.now == sp.now)
    sp.verify(sp.total_voting_power == sp.total_voting_power)

  @sp.entry_point
  def test_mprim1(self):
    sp.verify((abs(2)) == 2)
    sp.verify((abs(-2)) == 2)
    sp.verify((~ True) == False)
    sp.verify((~ False) == True)
    sp.verify(sp.is_nat(2) == sp.some(2))
    sp.verify(sp.is_nat(-1) == sp.none)
    p = sp.local("p", sp.bytes('0x050707010000000161010000000162'))
    r1 = sp.local("r1", sp.bytes('0x87b2e62f0bbc3002af9ebb095724a1787c7a824ee22b7fe5debf584a2ac76ecb'))
    sp.verify(sp.blake2b(p.value) == r1.value)
    r2 = sp.local("r2", sp.bytes('0xa6c3b9db1b22687139376251744b2d62700897dd14fc75baae7dd1bbdd320ee3'))
    sp.verify(sp.sha256(p.value) == r2.value)
    r3 = sp.local("r3", sp.bytes('0x0dd9fbbd042a7ce815abad7f15279d9a80f2fb98c561f1e424343ed46b3d20e8da5da4eca6a6b96b71884893b8ab67ceff90cc96da78f8aa48a87874349c089a'))
    sp.verify(sp.sha512(p.value) == r3.value)
    r4 = sp.local("r4", sp.bytes('0xb6a0662b2a96c770c47a5c61eaea9002775cd0ab66e48b76389c1be5a6d96a22'))
    sp.verify(sp.keccak(p.value) == r4.value)
    r5 = sp.local("r5", sp.bytes('0x13592317c972b6cbfb1804935dfdc4ecee477836e3a073172e860fca23b23952'))
    sp.verify(sp.sha3(p.value) == r5.value)

  @sp.entry_point
  def test_mprim2(self):
    sp.verify((5 << 2) == 20)
    sp.verify((23 >> 2) == 5)

  @sp.entry_point
  def test_mprim3(self):
    pass

  @sp.entry_point
  def test_prim0(self):
    pass

  @sp.entry_point
  def test_prim1(self, params):
    sp.verify(sp.record(x = 0).x == 0)
    sp.verify(sp.set_type_expr(0, sp.TInt) == 0)
    sp.verify(sp.fst((1, 2)) == 1)
    sp.verify(sp.snd((1, 2)) == 2)
    sp.verify(sp.sum(sp.list([1, 2, 3, 4, 5])) == 15)
    two = sp.local("two", 2)
    sp.verify((- two.value) == (-2))
    sp.verify(sp.sign(2) == 1)
    sp.verify(sp.sign(0) == 0)
    sp.verify(sp.sign(-2) == (-1))
    sp.verify(sp.len(sp.list([1, 2, 3])) == 3)
    sp.verify(sp.to_int(2) == 2)
    sp.verify(sp.pack(('a', 'b')) == sp.bytes('0x050707010000000161010000000162'))
    sp.verify(sp.some(('a', 'b')) == sp.unpack(sp.bytes('0x050707010000000161010000000162'), sp.TPair(sp.TString, sp.TString)))
    sp.verify(variant('A', 42) == variant('A', 42))
    sp.verify(sp.concat(sp.list(['a', 'bc', 'd'])) == 'abcd')
    sp.verify(sp.concat(sp.list([sp.bytes('0xab'), sp.bytes('0xcd01'), sp.bytes('0x23')])) == sp.bytes('0xabcd0123'))
    sp.verify(2 == 2)
    sp.verify(sp.pack(sp.list([1, 2, 3]).rev()) == sp.pack(sp.list([3, 2, 1])))
    s = sp.local("s", sp.set([3, 1, 2]))
    m = sp.local("m", {1 : 'a', 2 : 'b', 3 : 'c'})
    sp.verify(sp.pack(m.value.items()) == sp.pack(sp.list([(1, 'a'), (2, 'b'), (3, 'c')])))
    sp.verify(sp.pack(m.value.keys()) == sp.pack(sp.list([1, 2, 3])))
    sp.verify(sp.pack(m.value.values()) == sp.pack(sp.list(['a', 'b', 'c'])))
    sp.verify(sp.pack(s.value.elements()) == sp.pack(sp.list([1, 2, 3])))
    t = sp.local("t", sp.ticket('foo', 42))
    sp.verify(sp.fst(sp.read_ticket_raw(t.value)) == (sp.self_address, 'foo', 42))
    t1 = sp.local("t1", sp.ticket('foo', 2))
    t2 = sp.local("t2", sp.ticket('foo', 3))
    with sp.join_tickets_raw((t1.value, t2.value)).match_cases() as arg:
      with arg.match('None') as _no_arg:
        sp.failwith(':(')
      with arg.match('Some') as t12:
        sp.verify(sp.fst(sp.read_ticket_raw(t12.value)) == (sp.self_address, 'foo', 5))

    sp.set_type(params.a, sp.TContract(sp.TUnit))
    sp.verify(sp.to_address(params.a) == params.b)
    sp.verify(sp.to_address(sp.implicit_account(params.c)) == params.d)
    sp.verify(sp.pairing_check(params.e) == params.f)
    sp.verify(sp.voting_power(params.g) == params.h)
    sp.verify(variant('A', 42).is_variant('A'))
    sp.verify(sp.add_seconds(params.i, 60) == params.k)

  @sp.entry_point
  def test_prim2(self):
    f = sp.local("f", sp.build_lambda(lambda xy: (10 * sp.fst(xy.value)) + sp.snd(xy.value)).apply(1))
    sp.verify(f.value(2) == 12)
    sp.verify(2 == 2)
    sp.verify(2 != 3)
    sp.verify((True | True) == True)
    sp.verify((True | False) == True)
    sp.verify((False | True) == True)
    sp.verify((False | False) == False)
    sp.verify((True & True) == True)
    sp.verify((True & False) == False)
    sp.verify((False & True) == False)
    sp.verify((False & False) == False)
    sp.verify((True ^ True) == False)
    sp.verify((True ^ False) == True)
    sp.verify((False ^ True) == True)
    sp.verify((False ^ False) == False)
    sp.verify((2 * 3) == 6)
    sp.verify(2 < 3)
    sp.verify(~ (2 < 2))
    sp.verify(3 > 2)
    sp.verify(~ (2 > 2))
    sp.verify(2 <= 2)
    sp.verify(2 <= 3)
    sp.verify(2 >= 2)
    sp.verify(3 >= 3)
    sp.verify((299 // 100) == 2)
    sp.verify((299 % 100) == 99)
    sp.verify(sp.ediv(299, 100) == sp.some((2, 99)))
    sp.verify((2 + 3) == 5)
    sp.verify((2 - 3) == (-1))
    sp.verify(sp.mul(2, 3) == 6)
    sp.verify(sp.min(2, 3) == 2)
    sp.verify(sp.max(2, 3) == 3)
    s = sp.local("s", sp.set([3, 1, 2]))
    m = sp.local("m", {1 : 'a', 2 : 'b', 3 : 'c'})
    sp.verify(s.value.contains(2))
    sp.verify(m.value.get_opt(1) == sp.some('a'))
    sp.verify(sp.pack(sp.cons(1, sp.list([2, 3]))) == sp.pack(sp.list([1, 2, 3])))
    sp.verify(sp.len(sp.list([sp.ticket('foo', 42)])) == 1)

  @sp.entry_point
  def test_prim3(self, params):
    sp.verify(sp.pack(sp.range(1, 5)) == sp.pack(sp.list([1, 2, 3, 4])))
    sp.verify(sp.split_tokens(params.a, params.b, params.c) == params.d)
    m = sp.local("m", {1 : 'a', 2 : 'b', 3 : 'c'})
    sp.verify(sp.len(sp.list([sp.update_map(m.value, params.b2, params.c2)])) == 1)
    sp.verify(sp.len(sp.list([sp.get_and_update(m.value, params.b4, params.c4)])) == 1)

  @sp.entry_point
  def test_expr(self):
    x = sp.local("x", 0)
    sp.verify(variant('Left', 2) != variant('Right', 2))
    sp.verify(sp.none != sp.some('abc'))
    s = sp.local("s", sp.set([3, 1, 2]))
    sp.verify(sp.len(s.value) == 3)
    sp.verify(s.value.contains(2))
    m = sp.local("m", {1 : 'a', 2 : 'b', 3 : 'c'})
    sp.verify(sp.len(m.value) == 3)
    sp.verify(m.value.get_opt(1) == sp.some('a'))
    t' = sp.local("t'", sp.ticket('foo', 50))
    with sp.split_ticket_raw(t'.value, (20, 30)).match_cases() as arg:
      with arg.match('None') as _no_arg:
        sp.failwith(':(')
      with arg.match('Some') as t12:
        sp.verify(sp.fst(sp.read_ticket_raw(sp.fst(t12.value))) == (sp.self_address, 'foo', 20))


sp.add_compilation_target("test", Contract())