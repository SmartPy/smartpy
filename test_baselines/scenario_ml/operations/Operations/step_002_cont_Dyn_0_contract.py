import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b")))
    self.init(a = 0,
              b = 0)

  @sp.entry_point
  def myEntryPoint(self, params):
    self.data.a += params.x
    self.data.b += params.y

sp.add_compilation_target("test", Contract())