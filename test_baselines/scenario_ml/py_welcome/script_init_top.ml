#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_welcome.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/scenario_ml/py_welcome/scenario.json"
else
  exit 1;;