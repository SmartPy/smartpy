open SmartML

module Contract = struct
  let%entry_point unpack () =
    data <- open_some ~message:"Could not extract unpacked data." (unpack (pack address "tz1") address)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ address]
      ~storage:[%expr address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"]
      [unpack]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())