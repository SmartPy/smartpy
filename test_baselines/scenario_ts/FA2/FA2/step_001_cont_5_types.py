import smartpy as sp

tstorage = sp.TNat
tparameter = sp.TVariant(receive_balance = sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), request_balance = sp.TAddress).layout(("receive_balance", "request_balance"))
tprivates = { }
tviews = { }
