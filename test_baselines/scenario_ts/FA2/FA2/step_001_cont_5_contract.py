import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)
    self.init(0)

  @sp.entry_point
  def request_balance(self, params):
    sp.set_type(params, sp.TAddress)
    request = sp.local("request", sp.record(requests = sp.list([sp.record(owner = sp.self_address, token_id = 0)]), callback = sp.self_entry_point('receive_balance')))
    contact = sp.local("contact", sp.contract(sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")), params, entry_point='balance_of').open_some(message = 'Invalid Interface'))
    sp.transfer(request.value, sp.tez(0), contact.value)

  @sp.entry_point
  def receive_balance(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))
    sp.for response in params:
      sp.if response.request.owner == sp.self_address:
        self.data = response.balance

sp.add_compilation_target("test", Contract())