import smartpy as sp

tstorage = sp.TRecord(value = sp.TNat).layout("value")
tparameter = sp.TUnit
tprivates = { }
tviews = { "other": (sp.TNat, sp.TNat), "square_exn": (sp.TInt, sp.TUnknown()) }
