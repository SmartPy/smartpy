import * as RemoteModule from 'https://smartpy.io/releases/20211106-51985c20a6a77e31b6de0d3b0400cccce74e38ad/typescript/templates/Minimal.ts';
class RemoteImport extends RemoteModule.Minimal {
    @EntryPoint
    setValue(value: TNat) {
        this.storage.value = value;
    }
}

Dev.test({ name: 'RemoteImport' }, () => {
    const c1 = Scenario.originate(new RemoteImport());
    Scenario.transfer(c1.setValue(1));
});

Dev.compileContract('compile_RemoteImport', new RemoteImport());
