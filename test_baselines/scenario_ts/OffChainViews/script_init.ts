@Contract
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView({ name: 'getStorageState', description: 'Get Storage State' })
    state = (): TInt => {
        return this.storage;
    };

    @OffChainView({ pure: true, description: 'Increment value' })
    increment = (): TInt => {
        return this.storage + 1;
    };

    @OffChainView({ pure: true, description: 'Decrement value' })
    decrement = (): TInt => {
        return this.storage - 1;
    };

    @MetadataBuilder
    metadata = {
        name: 'SOME CONTRACT',
        decimals: 1,
        views: [this.state, this.increment, this.decrement],
        preferSymbol: true,
        source: {
            tools: ['SmartPy'],
        },
    };
}

Dev.test({ name: 'test' }, () => {
    const c1 = Scenario.originate(new OffChainViews());
    // Test views
    Scenario.verify(c1.state() === 1);
    Scenario.verify(c1.increment() === 2);
    Scenario.verify(c1.decrement() === 0);
});

Dev.compileContract('compilation', new OffChainViews(10));
