open SmartML

module Contract = struct
  let%entry_point push params =
    set_type params string;
    data.list <- params :: data.list

  let%entry_point concat params =
    set_type params (list string);
    data.list <- (concat params) :: data.list

  let%entry_point reverse () =
    if (len data.list) = (nat 4) then
      data.list <- List.rev data.list

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {list = list string}]
      ~storage:[%expr
                 {list = []}]
      [push; concat; reverse]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())