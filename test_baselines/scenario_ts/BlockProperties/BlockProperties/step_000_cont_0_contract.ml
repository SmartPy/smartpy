open SmartML

module Contract = struct
  let%entry_point ep () =
    data.amount <- some amount;
    data.balance <- some sp_balance;
    data.sender <- some sender;
    data.source <- some source;
    data.chainId <- some chain_id;
    data.now <- some now;
    data.level <- some level;
    data.totalVotingPower <- some total_voting_power;
    data.votingPower <- some (voting_power key_hash "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w")

  let%entry_point epFail () =
    failwith "FAIL ON PURPOSE"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {amount = option mutez; balance = option mutez; chainId = option chain_id; level = option nat; now = option timestamp; sender = option address; source = option address; totalVotingPower = option nat; votingPower = option nat}]
      ~storage:[%expr
                 {amount = None;
                  balance = None;
                  chainId = None;
                  level = None;
                  now = None;
                  sender = None;
                  source = None;
                  totalVotingPower = None;
                  votingPower = None}]
      [ep; epFail]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())