import smartpy as sp

tstorage = sp.TRecord(tokens = sp.TBigMap(sp.TNat, sp.TRecord(balance = sp.TNat).layout("balance"))).layout("tokens")
tparameter = sp.TUnit
tprivates = { }
tviews = { "getTokenById": (sp.TNat, sp.TRecord(balance = sp.TNat).layout("balance")) }
