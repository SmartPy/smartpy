import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admins = sp.TSet(sp.TAddress)).layout("admins"))
    self.init(admins = sp.set([sp.address('tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm')]))

  @sp.entry_point
  def changeAdmins(self, params):
    sp.set_type(params.remove, sp.TOption(sp.TSet(sp.TAddress)))
    sp.set_type(params.add, sp.TOption(sp.TSet(sp.TAddress)))
    sp.verify(self.data.admins.contains(sp.sender), 'onlyAdmin')
    sp.if params.remove.is_some():
      sp.for admin in params.remove.open_some().elements():
        sp.verify(admin != sp.sender, 'SelfRemoving')
        self.data.admins.remove(admin)
    sp.if params.add.is_some():
      sp.for admin in params.add.open_some().elements():
        self.data.admins.add(admin)

sp.add_compilation_target("test", Contract())