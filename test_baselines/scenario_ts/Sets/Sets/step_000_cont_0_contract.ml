open SmartML

module Contract = struct
  let%entry_point add params =
    set_type params string;
    Set.add data.set params

  let%entry_point contains params =
    set_type params string;
    if (contains params data.set) && ((len data.set) = (nat 3)) then
      (
        data.elements <- some (Set.elements data.set);
        List.iter (fun x ->
          Set.remove data.set x
        ) (Set.elements data.set)
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {elements = option (list string); set = set string}]
      ~storage:[%expr
                 {elements = None;
                  set = Set.make([])}]
      [add; contains]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())