import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def test_factorial(self):
    sp.verify(self.factorial(5) == 120)
    sp.verify(self.factorial_rec(5) == 120)
    sp.verify(self.factorial_five(sp.unit) == 120)

  @sp.entry_point
  def test_failing(self):
    aaa = sp.local("aaa", self.failing(''))

  @sp.private_lambda()
  def f(_x0):
    sp.result(_x0 * 2)

  @sp.private_lambda()
  def factorial(_x2):
    r = sp.local("r", 1)
    sp.for y in sp.range(2, _x2 + 1):
      r.value *= y
    sp.result(r.value)

  @sp.private_lambda()
  def factorial_five(_x4):
    sp.set_type(_x4, sp.TUnit)
    r = sp.local("r", 1)
    sp.for y in sp.range(2, 6):
      r.value *= y
    sp.result(r.value)

  @sp.private_lambda()
  def factorial_rec(_x6):
    sp.if _x6 <= 1:
      sp.result(_x6)
    sp.else:
      sp.result(_x6 * _f7(_x6 - 1))

  @sp.private_lambda()
  def failing(_x8):
    with sp.set_result_type(sp.TInt):
      sp.failwith('Aaa' + _x8)

  @sp.private_lambda()
  def g(_x10):
    sp.result(_x10 * 2)

sp.add_compilation_target("test", Contract())