Comment...
 h1: Escrow
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_pre_michelson.michel 167
 -> (Pair 0 (Pair 0 (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi")))))))
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.json 40
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.tz 212
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.json 255
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.py 44
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.ml 48
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.json 1
Executing addBalanceOwner(sp.record())...
 -> (Pair 0 (Pair 50000000 (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi")))))))
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.json 1
Executing addBalanceCounterparty(sp.record())...
 -> (Pair 4000000 (Pair 50000000 (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi")))))))
Comment...
 h3: Erronous secret
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223343')))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.hashedSecret == sp.blake2b(params.secret) : sp.TBool) (python/templates/escrow.py, line 37)
 (python/templates/escrow.py, line 37)
Comment...
 h3: Correct secret
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223344')))...
 -> (Pair 0 (Pair 0 (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi")))))))
  + Transfer
     params: sp.unit
     amount: sp.tez(54)
     to:     sp.contract(sp.TUnit, sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C')).open_some()
