open SmartML

module Contract = struct
  let%entry_point map params =
    data.x <- map params data.x

  let%entry_point set params =
    data.x <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = option int}]
      ~storage:[%expr
                 {x = Some(int 42)}]
      [map; set]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())