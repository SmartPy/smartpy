import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TInt)).layout("x"))
    self.init(x = sp.some(42))

  @sp.entry_point
  def map(self, params):
    self.data.x = self.data.x.map(params)

  @sp.entry_point
  def set(self, params):
    self.data.x = params

sp.add_compilation_target("test", Contract())