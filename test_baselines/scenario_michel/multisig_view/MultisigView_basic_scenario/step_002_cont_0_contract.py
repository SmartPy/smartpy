import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(members = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TBytes, sp.TBool), required_votes = sp.TNat, votes = sp.TBigMap(sp.TBytes, sp.TSet(sp.TAddress))).layout(("members", ("proposals", ("required_votes", "votes")))))
    self.init(members = sp.set([sp.address('tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm'), sp.address('tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W'), sp.address('tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx')]),
              proposals = {},
              required_votes = 2,
              votes = {})

  @sp.entry_point
  def submit_proposal(self, params):
    sp.verify(self.data.members.contains(sp.sender), 'You are not a member')
    self.data.proposals[params] = False
    self.data.votes[params] = sp.set([])

  @sp.entry_point
  def vote_proposal(self, params):
    sp.verify(self.data.members.contains(sp.sender), 'You are not a member')
    sp.verify(self.data.proposals.contains(params), 'Proposal not found')
    self.data.votes[params].add(sp.sender)
    sp.if sp.len(self.data.votes[params]) >= self.data.required_votes:
      self.data.proposals[params] = True

sp.add_compilation_target("test", Contract())