open SmartML

module Contract = struct
  let%entry_point submit_proposal params =
    verify (contains sender data.members) ~msg:"You are not a member";
    Map.set data.proposals params false;
    Map.set data.votes params (Set.make [])

  let%entry_point vote_proposal params =
    verify (contains sender data.members) ~msg:"You are not a member";
    verify (contains params data.proposals) ~msg:"Proposal not found";
    Set.add (Map.get data.votes params) sender;
    if (len (Map.get data.votes params)) >= data.required_votes then
      Map.set data.proposals params true

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {members = set address; proposals = big_map bytes bool; required_votes = nat; votes = big_map bytes (set address)}]
      ~storage:[%expr
                 {members = Set.make([address "tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; address "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; address "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"]);
                  proposals = Map.make [];
                  required_votes = nat 2;
                  votes = Map.make []}]
      [submit_proposal; vote_proposal]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())