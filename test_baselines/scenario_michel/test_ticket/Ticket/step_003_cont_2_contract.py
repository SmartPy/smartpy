import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(t = sp.TTicket(sp.TNat), x = sp.TInt).layout(("t", "x")))

  @sp.entry_point
  def run(self):
    with sp.match_record(self.data, "data") as data:
      ticket_test_ticket_49_data, ticket_test_ticket_49_copy = sp.match_tuple(sp.read_ticket_raw(data.t), "ticket_test_ticket_49_data", "ticket_test_ticket_49_copy")
      ticket_test_ticket_49_ticketer, ticket_test_ticket_49_content, ticket_test_ticket_49_amount = sp.match_tuple(ticket_test_ticket_49_data, "ticket_test_ticket_49_ticketer", "ticket_test_ticket_49_content", "ticket_test_ticket_49_amount")
      sp.verify(ticket_test_ticket_49_content == 42)
      ticket1_test_ticket_51, ticket2_test_ticket_51 = sp.match_tuple(sp.split_ticket_raw(ticket_test_ticket_49_copy, (ticket_test_ticket_49_amount // 2, ticket_test_ticket_49_amount // 2)).open_some(), "ticket1_test_ticket_51", "ticket2_test_ticket_51")
      data.t = sp.join_tickets_raw((ticket2_test_ticket_51, ticket1_test_ticket_51)).open_some()

sp.add_compilation_target("test", Contract())