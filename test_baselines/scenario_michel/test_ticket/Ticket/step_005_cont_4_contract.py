import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt)), x = sp.TInt).layout(("m", "x")))

  @sp.entry_point
  def ep1(self):
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_71_fst, match_pair_test_ticket_71_snd = sp.match_tuple(sp.get_and_update(data.m, 42, sp.none), "match_pair_test_ticket_71_fst", "match_pair_test_ticket_71_snd")
      data.m = match_pair_test_ticket_71_snd
      data.x = 0

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data, "data") as data:
      ticket_78 = sp.local("ticket_78", sp.ticket('a', 1))
      ticket_79 = sp.local("ticket_79", sp.ticket('b', 2))
      sp.transfer((ticket_78.value, ticket_79.value), sp.tez(0), params)

sp.add_compilation_target("test", Contract())