open SmartML

module Contract = struct
  let%entry_point run_record params =
    set_type params {a = int; b = string; c = bool}

  let%entry_point run_record_2 params =
    set_type params {a = int; b = string; c = bool; d = nat}

  let%entry_point run_type_record params =
    set_type params {a = int; b = string}

  let%entry_point run_type_variant params =
    set_type params (`d string + `e int)

  let%entry_point run_variant params =
    set_type params (`a int + `b string + `c bool)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = intOrNat; b = nat}]
      ~storage:[%expr
                 {a = 1;
                  b = nat 12}]
      [run_record; run_record_2; run_type_record; run_type_variant; run_variant]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())