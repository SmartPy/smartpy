import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, value = sp.TInt).layout(("admin", "value")))
    self.init(admin = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              value = 0)

  @sp.entry_point
  def set_value(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.value = params

sp.add_compilation_target("test", Contract())