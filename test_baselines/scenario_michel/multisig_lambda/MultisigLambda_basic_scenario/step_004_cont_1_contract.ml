open SmartML

module Contract = struct
  let%entry_point set_value params =
    verify (sender = data.admin);
    data.value <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; value = int}]
      ~storage:[%expr
                 {admin = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  value = int 0}]
      [set_value]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())