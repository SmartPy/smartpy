import smartpy as sp

tstorage = sp.TRecord(alive_delta = sp.TInt, heir = sp.TAddress, owner = sp.TAddress, timeout = sp.TTimestamp).layout(("alive_delta", ("heir", ("owner", "timeout"))))
tparameter = sp.TVariant(default = sp.TUnit, withdraw = sp.TRecord(amount = sp.TMutez, receiver = sp.TAddress).layout(("amount", "receiver"))).layout(("default", "withdraw"))
tprivates = { }
tviews = { }
