import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))
    self.init(x = 2,
              y = 'aaa',
              z = 0)

  @sp.entry_point
  def f(self):
    compute_sub_entry_point_25 = sp.local("compute_sub_entry_point_25", self.a(5))
    compute_sub_entry_point_25i = sp.local("compute_sub_entry_point_25i", self.a(10))
    self.data.z = compute_sub_entry_point_25.value + compute_sub_entry_point_25i.value

  @sp.entry_point
  def g(self):
    compute_sub_entry_point_29 = sp.local("compute_sub_entry_point_29", self.a(6))
    self.data.z = compute_sub_entry_point_29.value

  @sp.private_lambda()
  def a(_x0):
    sp.set_type(_x0, sp.TNat)
    sp.send(sp.address('tz1h4EsGunH2Ue1T2uNs8mfKZ8XZoQji3HcK'), sp.mul(_x0, sp.tez(1)))
    sp.send(sp.address('tz1hJgZdhnRGvg5XD6pYxRCsbWh4jg5HQ476'), sp.tez(2))
    self.data.x += 1
    sp.result(_x0 * self.data.x)

  @sp.private_lambda()
  def b(_x2):
    sp.set_type(_x2, sp.TNat)
    sp.send(sp.address('tz1h4EsGunH2Ue1T2uNs8mfKZ8XZoQji3HcK'), sp.mul(_x2, sp.tez(1)))
    sp.send(sp.address('tz1hJgZdhnRGvg5XD6pYxRCsbWh4jg5HQ476'), sp.tez(2))
    self.data.x += 1
    sp.result(_x2 * self.data.x)

sp.add_compilation_target("test", Contract())