import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, z = sp.TNat).layout(("x", "z")))
    self.init(x = 10,
              z = 42)

sp.add_compilation_target("test", Contract())