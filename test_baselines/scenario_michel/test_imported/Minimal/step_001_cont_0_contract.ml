open SmartML

module Contract = struct
  let%entry_point entry_point_1 params =
    match_pair_test_imported_9_fst, match_pair_test_imported_9_snd = match_tuple(params, "match_pair_test_imported_9_fst", "match_pair_test_imported_9_snd")
    match_pair_test_imported_10_fst, match_pair_test_imported_10_snd = match_tuple(match_pair_test_imported_9_snd, "match_pair_test_imported_10_fst", "match_pair_test_imported_10_snd")
    data.x <- data.x + ((match_pair_test_imported_9_fst * match_pair_test_imported_10_fst) * match_pair_test_imported_10_snd)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 0}]
      [entry_point_1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())