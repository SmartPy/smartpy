import smartpy as sp

tstorage = sp.TUnit
tparameter = sp.TVariant(deposit = sp.TUnit, withdraw = sp.TRecord(address = sp.TAddress, quantity = sp.TMutez).layout(("address", "quantity")), withdraw_all = sp.TAddress).layout(("deposit", ("withdraw", "withdraw_all")))
tprivates = { }
tviews = { }
