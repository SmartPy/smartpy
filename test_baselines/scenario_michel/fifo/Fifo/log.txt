Comment...
 h1: Simple Fifo Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_pre_michelson.michel 55
 -> (Pair 0 (Pair -1 {}))
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_storage.json 1
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_contract.tz 64
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_contract.json 76
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_contract.py 21
 => test_baselines/scenario_michel/fifo/Fifo/step_001_cont_0_contract.ml 27
 => test_baselines/scenario_michel/fifo/Fifo/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_002_cont_0_params.json 1
Executing push(4)...
 -> (Pair 0 (Pair 0 {Elt 0 4}))
 => test_baselines/scenario_michel/fifo/Fifo/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_003_cont_0_params.json 1
Executing push(5)...
 -> (Pair 0 (Pair 1 {Elt 0 4; Elt 1 5}))
 => test_baselines/scenario_michel/fifo/Fifo/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_004_cont_0_params.json 1
Executing push(6)...
 -> (Pair 0 (Pair 2 {Elt 0 4; Elt 1 5; Elt 2 6}))
 => test_baselines/scenario_michel/fifo/Fifo/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_005_cont_0_params.json 1
Executing push(7)...
 -> (Pair 0 (Pair 3 {Elt 0 4; Elt 1 5; Elt 2 6; Elt 3 7}))
 => test_baselines/scenario_michel/fifo/Fifo/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_006_cont_0_params.json 1
Executing pop(sp.record())...
 -> (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7}))
Comment...
 h1: Using the Fifo Data Type
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_pre_michelson.michel 55
 -> (Pair 0 (Pair -1 {}))
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_storage.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_storage.json 1
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_storage.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_types.py 7
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_contract.tz 64
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_contract.json 78
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_contract.py 20
 => test_baselines/scenario_michel/fifo/Fifo/step_008_cont_1_contract.ml 26
 => test_baselines/scenario_michel/fifo/Fifo/step_009_cont_1_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_009_cont_1_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_009_cont_1_params.json 1
Executing push(4)...
 -> (Pair 0 (Pair 0 {Elt 0 4}))
 => test_baselines/scenario_michel/fifo/Fifo/step_010_cont_1_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_010_cont_1_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_010_cont_1_params.json 1
Executing push(5)...
 -> (Pair 0 (Pair 1 {Elt 0 4; Elt 1 5}))
 => test_baselines/scenario_michel/fifo/Fifo/step_011_cont_1_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_011_cont_1_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_011_cont_1_params.json 1
Executing push(6)...
 -> (Pair 0 (Pair 2 {Elt 0 4; Elt 1 5; Elt 2 6}))
 => test_baselines/scenario_michel/fifo/Fifo/step_012_cont_1_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_012_cont_1_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_012_cont_1_params.json 1
Executing push(7)...
 -> (Pair 0 (Pair 3 {Elt 0 4; Elt 1 5; Elt 2 6; Elt 3 7}))
 => test_baselines/scenario_michel/fifo/Fifo/step_013_cont_1_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_013_cont_1_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_013_cont_1_params.json 1
Executing pop(sp.record())...
 -> (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7}))
Comment...
 h1: A more complex example using the Fifo Data Type
Creating contract KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_pre_michelson.michel 133
 -> (Pair (Pair 0 (Pair -1 {})) (Pair 0 (Pair -1 {})))
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_storage.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_storage.json 7
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_sizes.csv 2
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_storage.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_types.py 7
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_contract.tz 146
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_contract.json 178
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_contract.py 28
 => test_baselines/scenario_michel/fifo/Fifo/step_015_cont_2_contract.ml 33
 => test_baselines/scenario_michel/fifo/Fifo/step_016_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_016_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_016_cont_2_params.json 1
Executing push(4)...
 -> (Pair (Pair 0 (Pair 0 {Elt 0 4})) (Pair 0 (Pair 0 {Elt 0 4})))
 => test_baselines/scenario_michel/fifo/Fifo/step_017_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_017_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_017_cont_2_params.json 1
Executing push(5)...
 -> (Pair (Pair 0 (Pair 1 {Elt 0 4; Elt 1 5})) (Pair 0 (Pair 1 {Elt 0 4; Elt 1 5})))
 => test_baselines/scenario_michel/fifo/Fifo/step_018_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_018_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_018_cont_2_params.json 1
Executing push(6)...
 -> (Pair (Pair 0 (Pair 2 {Elt 0 4; Elt 1 5; Elt 2 6})) (Pair 0 (Pair 2 {Elt 0 4; Elt 1 5; Elt 2 6})))
 => test_baselines/scenario_michel/fifo/Fifo/step_019_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_019_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_019_cont_2_params.json 1
Executing push(7)...
 -> (Pair (Pair 0 (Pair 3 {Elt 0 4; Elt 1 5; Elt 2 6; Elt 3 7})) (Pair 0 (Pair 3 {Elt 0 4; Elt 1 5; Elt 2 6; Elt 3 7})))
 => test_baselines/scenario_michel/fifo/Fifo/step_020_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_020_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_020_cont_2_params.json 1
Executing pop(sp.record())...
 -> (Pair (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7})) (Pair 0 (Pair 3 {Elt 0 4; Elt 1 5; Elt 2 6; Elt 3 7})))
 => test_baselines/scenario_michel/fifo/Fifo/step_021_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_021_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_021_cont_2_params.json 1
Executing popQ(sp.record())...
 -> (Pair (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7})) (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7})))
 => test_baselines/scenario_michel/fifo/Fifo/step_022_cont_2_params.py 1
 => test_baselines/scenario_michel/fifo/Fifo/step_022_cont_2_params.tz 1
 => test_baselines/scenario_michel/fifo/Fifo/step_022_cont_2_params.json 1
Executing popQ(sp.record())...
 -> (Pair (Pair 1 (Pair 3 {Elt 1 5; Elt 2 6; Elt 3 7})) (Pair 2 (Pair 3 {Elt 2 6; Elt 3 7})))
Verifying sp.contract_data(0).saved[sp.contract_data(0).first] == 5...
 OK
Verifying sp.contract_data(1).fif.saved[sp.contract_data(1).fif.first] == 5...
 OK
Verifying sp.contract_data(2).fif.saved[sp.contract_data(2).fif.first] == 5...
 OK
Verifying sp.contract_data(2).queue.saved[sp.contract_data(2).queue.first] == 6...
 OK
