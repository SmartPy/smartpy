import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(storedValue = sp.TIntOrNat).layout("storedValue"))
    self.init(storedValue = 14)

  @sp.entry_point
  def add(self, params):
    self.data.storedValue += params.value

sp.add_compilation_target("test", Contract())