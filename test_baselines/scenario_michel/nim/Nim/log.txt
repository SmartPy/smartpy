Comment...
 h1: Nim game
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_pre_michelson.michel 147
 -> (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5} (Pair 1 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_storage.json 19
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_storage.py 1
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_types.py 7
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_contract.tz 178
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_contract.json 167
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_contract.py 30
 => test_baselines/scenario_michel/nim/Nim/step_002_cont_0_contract.ml 36
Comment...
 h2: Message execution
Comment...
 h3: A first move
 => test_baselines/scenario_michel/nim/Nim/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_005_cont_0_params.json 1
Executing remove(sp.record(cell = 2, k = 1))...
 -> (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 2; Elt 3 4; Elt 4 5} (Pair 2 (Pair 5 0))))
Comment...
 h3: A second move
 => test_baselines/scenario_michel/nim/Nim/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_007_cont_0_params.json 1
Executing remove(sp.record(cell = 2, k = 2))...
 -> (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 0; Elt 3 4; Elt 4 5} (Pair 1 (Pair 5 0))))
Comment...
 h3: An illegal move
 => test_baselines/scenario_michel/nim/Nim/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_009_cont_0_params.json 1
Executing remove(sp.record(cell = 2, k = 1))...
 -> --- Expected failure in transaction --- Wrong condition: (params.k <= self.data.deck[params.cell] : sp.TBool) (python/templates/nim.py, line 22)
 (python/templates/nim.py, line 22)
Comment...
 h3: Another illegal move
 => test_baselines/scenario_michel/nim/Nim/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_011_cont_0_params.json 1
Executing claim(sp.record(winner = 1))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sum(self.data.deck.values()) == 0 : sp.TBool) (python/templates/nim.py, line 28)
 (python/templates/nim.py, line 28)
Comment...
 h3: A third move
 => test_baselines/scenario_michel/nim/Nim/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_013_cont_0_params.json 1
Executing remove(sp.record(cell = 1, k = 2))...
 -> (Pair False (Pair {Elt 0 1; Elt 1 0; Elt 2 0; Elt 3 4; Elt 4 5} (Pair 2 (Pair 5 0))))
Comment...
 h3: More moves
 => test_baselines/scenario_michel/nim/Nim/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_015_cont_0_params.json 1
Executing remove(sp.record(cell = 0, k = 1))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 4; Elt 4 5} (Pair 1 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_016_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_016_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_016_cont_0_params.json 1
Executing remove(sp.record(cell = 3, k = 1))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 3; Elt 4 5} (Pair 2 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_017_cont_0_params.json 1
Executing remove(sp.record(cell = 3, k = 1))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 2; Elt 4 5} (Pair 1 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_018_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_018_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_018_cont_0_params.json 1
Executing remove(sp.record(cell = 3, k = 2))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 5} (Pair 2 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_019_cont_0_params.json 1
Executing remove(sp.record(cell = 4, k = 1))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 4} (Pair 1 (Pair 5 0))))
 => test_baselines/scenario_michel/nim/Nim/step_020_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_020_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_020_cont_0_params.json 1
Executing remove(sp.record(cell = 4, k = 2))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 2} (Pair 2 (Pair 5 0))))
Comment...
 h3: A failed attempt to claim
 => test_baselines/scenario_michel/nim/Nim/step_022_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_022_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_022_cont_0_params.json 1
Executing claim(sp.record(winner = 1))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sum(self.data.deck.values()) == 0 : sp.TBool) (python/templates/nim.py, line 28)
 (python/templates/nim.py, line 28)
Comment...
 h3: A last removal
 => test_baselines/scenario_michel/nim/Nim/step_024_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_024_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_024_cont_0_params.json 1
Executing remove(sp.record(cell = 4, k = 2))...
 -> (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 0} (Pair 1 (Pair 5 0))))
Comment...
 h3: And a final claim
 => test_baselines/scenario_michel/nim/Nim/step_026_cont_0_params.py 1
 => test_baselines/scenario_michel/nim/Nim/step_026_cont_0_params.tz 1
 => test_baselines/scenario_michel/nim/Nim/step_026_cont_0_params.json 1
Executing claim(sp.record(winner = 1))...
 -> (Pair True (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 0} (Pair 1 (Pair 5 1))))
Comment...
 p: The winner is
Computing sp.contract_data(0).winner...
 => 1
