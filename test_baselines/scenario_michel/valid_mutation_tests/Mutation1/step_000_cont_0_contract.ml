open SmartML

module Contract = struct
  let%entry_point fail_if_negative params =
    failwith ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ int]
      ~storage:[%expr int 0]
      [fail_if_negative]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())