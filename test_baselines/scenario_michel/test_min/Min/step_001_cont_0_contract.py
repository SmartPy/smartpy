import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(r = sp.TIntOrNat).layout("r"))
    self.init(r = 0)

  @sp.entry_point
  def ep1(self):
    a = sp.local("a", 1)
    b = sp.local("b", 2)
    self.data.r = sp.min(a.value, b.value)

  @sp.entry_point
  def ep1b(self, params):
    a = sp.local("a", params)
    b = sp.local("b", 2)
    self.data.r = sp.min(a.value, b.value)

  @sp.entry_point
  def ep2(self):
    a = sp.local("a", 3)
    b = sp.local("b", 4)
    self.data.r = sp.max(a.value, b.value)

  @sp.entry_point
  def ep2b(self, params):
    a = sp.local("a", params)
    b = sp.local("b", 4)
    self.data.r = sp.max(a.value, b.value)

sp.add_compilation_target("test", Contract())