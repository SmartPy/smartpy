import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TList(sp.TUnknown())).layout("x"))
    self.init(x = [])

sp.add_compilation_target("test", Contract())