open SmartML

module Contract = struct
  let%entry_point test_sub_incr () =
    data.a <- nat 0;
    verify ((100 self.sub_incr) = 101)

  let%entry_point test_sub_incr_both () =
    data.a <- nat 0;
    verify (((nat 100) self.sub_incr_both) = (nat 101));
    verify (data.a = (nat 1))

  let%entry_point test_sub_incr_store () =
    data.a <- (nat 100) self.sub_incr_both

  let%entry_point test_with_storage () =
    data.a <- nat 0;
    verify ((100 self.incr) = 101);
    verify (data.a = (nat 1));
    data.a <- nat 10;
    verify ((100 self.double) = 200);
    verify (data.a = (nat 20));
    data.a <- nat 10;
    verify (((100 self.incr) self.double) = 202);
    verify (data.a = (nat 22));
    data.a <- nat 10;
    verify (((100 self.double) + (1000 self.incr)) = 1201);
    verify (data.a = (nat 22));
    data.a <- nat 10;
    verify (((1000 self.incr) + (100 self.double)) = 1201);
    verify (data.a = (nat 21))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat}]
      ~storage:[%expr
                 {a = nat 0}]
      [test_sub_incr; test_sub_incr_both; test_sub_incr_store; test_with_storage]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())