open SmartML

module Contract = struct
  let%entry_point add_admins params =
    verify (contains sender data.admins) ~msg:"Only an admin can call this entrypoint.";
    List.iter (fun admin ->
      Set.add data.admins admin
    ) params

  let%entry_point protected () =
    verify (contains sender data.admins) ~msg:"Only an admin can call this entrypoint."

  let%entry_point remove_admins params =
    verify (contains sender data.admins) ~msg:"Only an admin can call this entrypoint.";
    List.iter (fun admin ->
      verify (admin <> sender) ~msg:"An admin cannot remove themselves.";
      Set.remove data.admins admin
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admins = set address}]
      ~storage:[%expr
                 {admins = Set.make([address "tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb"])}]
      [add_admins; protected; remove_admins]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())