open SmartML

module Contract = struct
  let%entry_point send params =
    transfer {receiver_balance = (tez 1); sender_balance = (tez 110)} (tez 1) (open_some (contract {receiver_balance = mutez; sender_balance = mutez} params , entry_point='receive_and_check'));
    transfer {receiver_balance = (tez 11); sender_balance = (tez 100)} (tez 10) (open_some (contract {receiver_balance = mutez; sender_balance = mutez} params , entry_point='receive_and_check'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [send]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())