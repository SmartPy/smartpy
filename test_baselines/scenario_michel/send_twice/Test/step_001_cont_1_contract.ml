open SmartML

module Contract = struct
  let%entry_point receive_and_check params =
    verify (params.sender_balance = (open_some view("get_balance", (), sender, mutez)));
    verify (params.receiver_balance = sp_balance)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [receive_and_check]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())