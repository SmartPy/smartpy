import smartpy as sp

tstorage = sp.TRecord(checkResult = sp.TOption(sp.TBool), fr = sp.TBls12_381_fr, g1 = sp.TBls12_381_g1, g2 = sp.TBls12_381_g2, toIntResult = sp.TOption(sp.TInt)).layout(("checkResult", ("fr", ("g1", ("g2", "toIntResult")))))
tparameter = sp.TVariant(add = sp.TRecord(fr = sp.TBls12_381_fr, g1 = sp.TBls12_381_g1, g2 = sp.TBls12_381_g2).layout(("fr", ("g1", "g2"))), conv = sp.TMutez, mul = sp.TRecord(fr = sp.TBls12_381_fr, i = sp.TInt, n = sp.TNat).layout(("fr", ("i", "n"))), negate = sp.TUnit, pairing_check = sp.TList(sp.TPair(sp.TBls12_381_g1, sp.TBls12_381_g2)), toInt = sp.TUnit).layout((("add", ("conv", "mul")), ("negate", ("pairing_check", "toInt"))))
tprivates = { }
tviews = { }
