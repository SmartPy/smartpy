open SmartML

module Contract = struct
  let%entry_point ep1 params =
    match_pair_test_match_9_fst, match_pair_test_match_9_snd = match_tuple(params, "match_pair_test_match_9_fst", "match_pair_test_match_9_snd")
    verify (match_pair_test_match_9_fst = "x");
    verify (match_pair_test_match_9_snd = 2)

  let%entry_point ep2 params =
    my_x, my_y, my_z = match_tuple(params, "my_x", "my_y", "my_z")
    verify (my_x = "x");
    verify (my_y = 2);
    verify my_z

  let%entry_point ep3 params =
    set_type params {x = string; y = int; z = bool};
    x_test_match_23, z_test_match_23 = match_record(params, "x", "z")
    verify (x_test_match_23 = "x");
    verify z_test_match_23

  let%entry_point ep4 params =
    set_type params.x01 int;
    set_type params.x02 key;
    set_type params.x03 string;
    set_type params.x04 timestamp;
    set_type params.x05 bytes;
    set_type params.x06 address;
    set_type params.x07 bool;
    set_type params.x08 key_hash;
    set_type params.x09 signature;
    set_type params.x10 mutez;
    x07_test_match_39, x03_test_match_39 = match_record(params, "x07", "x03")
    verify (x03_test_match_39 = "x");
    verify x07_test_match_39

  let%entry_point ep5 params =
    a, b, c, d = match_tuple(params, "a", "b", "c", "d")
    verify (((a * b) + (c * d)) = 12)

  let%entry_point ep6 params =
    a, b, c, d = match_tuple(params, "a", "b", "c", "d")
    set_type c int;
    verify (((a * b) + d) = 12)

  let%entry_point ep7 () =
    let%mutable k = "abc" in ();
    with match_record(Map.get data.m k, "data") as data:
      k <- "xyz" + k;
    data.out <- k

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {m = map string {a = intOrNat; b = intOrNat}; out = string}]
      ~storage:[%expr
                 {m = Map.make [("abc", {a = 10; b = 20})];
                  out = "z"}]
      [ep1; ep2; ep3; ep4; ep5; ep6; ep7]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())