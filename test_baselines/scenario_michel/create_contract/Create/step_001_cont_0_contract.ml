open SmartML

module Contract = struct
  let%entry_point create1 () =
    let%mutable create_contract_create_contract_32 = create contract ... in ();
    operations <- create_contract_create_contract_32.operation :: operations;
    data.x <- some create_contract_create_contract_32.address

  let%entry_point create2 () =
    let%mutable create_contract_create_contract_37 = create contract ... in ();
    operations <- create_contract_create_contract_37.operation :: operations;
    let%mutable create_contract_create_contract_38 = create contract ... in ();
    operations <- create_contract_create_contract_38.operation :: operations

  let%entry_point create3 () =
    let%mutable create_contract_create_contract_42 = create contract ... in ();
    operations <- create_contract_create_contract_42.operation :: operations;
    data.x <- some create_contract_create_contract_42.address

  let%entry_point create4 params =
    List.iter (fun x ->
      let%mutable create_contract_create_contract_48 = create contract ... in ();
      operations <- create_contract_create_contract_48.operation :: operations
    ) params

  let%entry_point create5 () =
    let%mutable create_contract_create_contract_52 = create contract ... in ();
    operations <- create_contract_create_contract_52.operation :: operations;
    data.x <- some create_contract_create_contract_52.address

  let%entry_point create_op () =
    operation_create_contract_59, address_create_contract_59 = match_record((int 42) data.l, "operation", "address")
    operations <- operation_create_contract_59 :: operations;
    data.x <- some address_create_contract_59

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {l = lambda int {address = address; operation = operation}; x = option address}]
      ~storage:[%expr
                 {l = lambda;
                  x = None}]
      [create1; create2; create3; create4; create5; create_op]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())