(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Utils_pure
open Control
module Set = VarId.Set

let sort_row r = List.sort (fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2) r

type 't row = (string * 't) list
[@@deriving eq, ord, map, fold, show {with_path = false}]

type access =
  | Read_only
  | Read_write
[@@deriving eq, ord, show {with_path = false}]

type 't effects_f = {
    with_storage : (access * 't) option Hole.t
  ; with_operations : bool Hole.t
}
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 't f =
  | T0 of Michelson_base.Type.type0
  | T1 of Michelson_base.Type.type1 * 't
  | T2 of Michelson_base.Type.type2 * 't * 't
  | TInt of {isNat : bool Hole.t}
  | TLambda of 't effects_f * 't * 't
  | TBounded of {
        base : 't
      ; cases : Literal.t list
      ; var : VarId.t option
    }
  | TRecord of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | TVariant of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | TUnknown of VarId.t * string
  | TTuple of (int * 't) list * VarId.t option
  | TSecretKey
  | TSaplingState of {memo : int Hole.t}
  | TSaplingTransaction of {memo : int Hole.t}
[@@deriving eq, ord, show {with_path = false}, map, fold]

module F = struct
  type 'a t = 'a f [@@deriving eq, ord, show {with_path = false}, map]
end

include Fix (F)
include FixEQ (F)
include FixORD (F)
include FixSHOW (F)
include FixFUNCTOR (F)

type effects = t effects_f

type tvariable = string * t [@@deriving eq, show {with_path = false}]

let tree_layout l =
  let l = ref l in
  let rec layout n =
    if n = 1
    then (
      match !l with
      | [] -> assert false
      | a :: rest ->
          l := rest;
          Layout.leaf a a)
    else
      let n2 = n / 2 in
      let l1 = layout n2 in
      let l2 = layout (n - n2) in
      Binary_tree.Node (l1, l2)
  in
  let size = List.length !l in
  if size = 0 then failwith "default_layout of size 0";
  layout size

let rec comb_layout right_left = function
  | [] -> failwith "comb_layout"
  | [lbl] -> Layout.leaf lbl lbl
  | lbl :: xs -> (
      match right_left with
      | `Right ->
          Binary_tree.node (Layout.leaf lbl lbl) (comb_layout right_left xs)
      | `Left ->
          Binary_tree.node (comb_layout right_left xs) (Layout.leaf lbl lbl))

let comb_layout_of_row right_left r = comb_layout right_left r

let default_layout_of_row layout r =
  match layout with
  | Config.Tree -> tree_layout r
  | Comb -> comb_layout_of_row `Right r

let build t = F t

let unknown_raw x = F (TUnknown (VarId.mk (), x))

let mk0 t = build (T0 t)

let mk1 t t1 = build (T1 (t, t1))

let mk2 t t1 t2 = build (T2 (t, t1, t2))

let unit = build (T0 T_unit)

let address = build (T0 T_address)

let contract t = build (T1 (T_contract, t))

let bool = build (T0 T_bool)

let bytes = build (T0 T_bytes)

let variant layout row =
  build (TVariant {layout; row = sort_row row; var = None})

let variant_default_layout default_layout row =
  build
    (TVariant
       {
         layout = Value (default_layout_of_row default_layout (List.map fst row))
       ; row = sort_row row
       ; var = None
       })

let key_hash = build (T0 T_key_hash)

let int = build (TInt {isNat = Value false})

let nat = build (TInt {isNat = Value true})

let intOrNat () = F (TInt {isNat = Hole.mk ()})

let key = build (T0 T_key)

let chain_id = build (T0 T_chain_id)

let secret_key = build TSecretKey

let operation = build (T0 T_operation)

let sapling_state memo =
  let memo = Option.cata (Hole.mk ()) Hole.value memo in
  build (TSaplingState {memo})

let sapling_transaction memo =
  let memo = Option.cata (Hole.mk ()) Hole.value memo in
  build (TSaplingTransaction {memo})

let never = build (T0 T_never)

let map ~big ~tkey ~tvalue =
  if big
  then build (T2 (T_big_map, tkey, tvalue))
  else build (T2 (T_map, tkey, tvalue))

let set ~telement = build (T1 (T_set, telement))

let record layout row = build (TRecord {layout; row = sort_row row; var = None})

let record_default_layout layout row =
  let layout = Hole.Value (default_layout_of_row layout (List.map fst row)) in
  build (TRecord {layout; row = sort_row row; var = None})

let record_or_unit layout = function
  | [] -> unit
  | l -> record layout l

let signature = build (T0 T_signature)

let option t = build (T1 (T_option, t))

let key_value tkey tvalue =
  record_default_layout Config.Comb [("key", tkey); ("value", tvalue)]

let head_tail thead ttail =
  record_default_layout Config.Comb [("head", thead); ("tail", ttail)]

let tor t u = variant_default_layout Config.Comb [("Left", t); ("Right", u)]

let string = build (T0 T_string)

let bounded base cases var =
  let cases = List.sort Literal.compare cases in
  build (TBounded {base; cases; var})

let timestamp = build (T0 T_timestamp)

let mutez = build (T0 T_mutez)

let uvariant name t =
  let row = [(name, t)] in
  let var = Some (VarId.mk ()) in
  let layout = Hole.mk () in
  F (TVariant {row; var; layout})

let urecord fields =
  let cmp (x, _) (y, _) = Stdlib.compare x y in
  let row = List.sort cmp fields in
  let var = Some (VarId.mk ()) in
  let layout = Hole.mk () in
  F (TRecord {row; var; layout})

let utuple i t = F (TTuple ([(i, t)], Some (VarId.mk ())))

let account =
  record_default_layout Config.Comb
    [
      ("seed", string)
    ; ("address", address)
    ; ("public_key", key)
    ; ("public_key_hash", key_hash)
    ; ("secret_key", secret_key)
    ]

let pair t1 t2 = build (TTuple ([(0, t1); (1, t2)], None))

let tuple ts = build (TTuple (List.mapi (fun i x -> (i, x)) ts, None))

let list t = build (T1 (T_list, t))

let ticket t = build (T1 (T_ticket, t))

let lambda effects t1 t2 = build (TLambda (effects, t1, t2))

let bls12_381_g1 = build (T0 T_bls12_381_g1)

let bls12_381_g2 = build (T0 T_bls12_381_g2)

let bls12_381_fr = build (T0 T_bls12_381_fr)

let chest_key = build (T0 T_chest_key)

let chest = build (T0 T_chest)

let has_unknowns =
  cata (function
    | TUnknown _ -> true
    | x -> fold_f ( || ) false x)

let has_holes =
  let is_variable = function
    | Hole.Value _ -> false
    | Variable _ -> true
  in
  let f x =
    let r =
      match x with
      | T0 _ | T1 _ | T2 _ | TLambda _ -> false
      | TInt {isNat} -> is_variable isNat
      | TBounded {var} -> Option.is_some var
      | TRecord {var} | TVariant {var} | TTuple (_, var) -> Option.is_some var
      | TUnknown _ -> true
      | TSecretKey -> false
      | TSaplingState {memo} | TSaplingTransaction {memo} -> is_variable memo
    in
    fold_f ( || ) r x
  in
  cata f

let is_hot =
  let open Ternary in
  cata (function
    | T1 (T_ticket, _) -> Yes
    | TLambda _ -> No
    | T2 (T_lambda, _, _) -> assert false
    | T1 (T_contract, _) -> No
    | TUnknown _ -> Maybe
    | t -> fold_f or_ No t)

let view_variant t =
  match unF t with
  | TVariant {layout; row} -> Some (layout, row)
  | T1 (T_option, t) ->
      let row = [("None", unit); ("Some", t)] in
      let layout = default_layout_of_row Comb (List.map fst row) in
      Some (Value layout, row)
  | _ -> None

let of_mtype =
  let f ?annot_type:_ ?annot_variable:_ : _ Michelson_base.Type.mtype_f -> _ =
    function
    | MT0 T_nat -> nat
    | MT0 T_int -> int
    | MT0 (T_sapling_state {memo}) -> F (TSaplingState {memo = Value memo})
    | MT0 (T_sapling_transaction {memo}) ->
        F (TSaplingTransaction {memo = Value memo})
    | MT0 c -> F (T0 c)
    | MT1 (c, t1) -> F (T1 (c, t1))
    | MT2 (T_map, tkey, tvalue) -> map ~big:false ~tkey ~tvalue
    | MT2 (T_big_map, tkey, tvalue) -> map ~big:true ~tkey ~tvalue
    | MT2 (T_pair _, t1, t2) -> pair t1 t2
    | MT2 (T_or {annot_left = Some left; annot_right = Some right}, t1, t2) ->
        variant_default_layout Config.Comb [(left, t1); (right, t2)]
    | MT2 (T_or _, t1, t2) ->
        variant_default_layout Config.Comb [("Left", t1); ("Right", t2)]
    | MT2 (c, t1, t2) -> F (T2 (c, t1, t2))
    | MT_var _ -> assert false
  in
  Michelson_base.Type.cata_mtype f

let type_of_literal = function
  | Literal.Unit -> unit
  | Bool _ -> bool
  | Int {is_nat} -> build (TInt {isNat = is_nat})
  | String _ -> string
  | Bytes _ -> bytes
  | Chain_id _ -> chain_id
  | Timestamp _ -> timestamp
  | Mutez _ -> mutez
  | Address _ -> address
  | Key _ -> key
  | Secret_key _ -> secret_key
  | Key_hash _ -> key_hash
  | Signature _ -> signature
  | Sapling_test_state {memo} -> sapling_state (Some memo)
  | Sapling_test_transaction {memo} -> sapling_transaction (Some memo)
  | Bls12_381_g1 _ -> bls12_381_g1
  | Bls12_381_g2 _ -> bls12_381_g2
  | Bls12_381_fr _ -> bls12_381_fr
  | Chest_key _ -> chest_key
  | Chest _ -> chest

let no_effects = {with_storage = Value None; with_operations = Value false}

let unknown_effects () =
  {with_storage = Hole.mk (); with_operations = Hole.mk ()}

let rawify_lambda ~with_storage ~with_operations t1 t2 =
  let r t =
    match (with_storage, with_operations) with
    | None, false -> t
    | Some tstorage, false -> pair t tstorage
    | None, true -> pair t (list operation)
    | Some tstorage, true -> pair t (pair (list operation) tstorage)
  in
  (r t1, r t2)

let has_effects {with_storage; with_operations} =
  let msg = "rawify_lambda: unknown effect" in
  let with_storage = Option.of_some ~msg (Hole.get with_storage) in
  let with_operations = Option.of_some ~msg (Hole.get with_operations) in
  Option.is_some with_storage || with_operations

let frees_f t =
  let frees = fold_f Set.union Set.empty t in
  match t with
  | TBounded {var = Some i} -> Set.union frees (Set.singleton i)
  | TUnknown (i, _)
  | TInt {isNat = Variable i}
  | TSaplingState {memo = Variable i}
  | TSaplingTransaction {memo = Variable i} -> Set.singleton i
  | TLambda ({with_storage; with_operations}, _, _) ->
      let with_storage =
        match with_storage with
        | Variable x -> Set.singleton x
        | _ -> Set.empty
      in
      let with_operations =
        match with_operations with
        | Variable x -> Set.singleton x
        | _ -> Set.empty
      in
      Set.union frees (Set.union with_storage with_operations)
  | TTuple (_, var) ->
      let var = Set.of_option var in
      Set.(union frees var)
  | TVariant {var; layout} | TRecord {var; layout} ->
      let layout =
        match layout with
        | Variable i -> Set.singleton i
        | _ -> Set.empty
      in
      let var = Set.of_option var in
      Set.(union frees (union layout var))
  | t -> fold_f Set.union Set.empty t

let frees = cata frees_f
