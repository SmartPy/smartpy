(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Utils_pure

type l = {
    source : string
  ; target : string
}
[@@deriving eq, ord, show]

type t = l Binary_tree.t [@@deriving eq, ord, show]

val leaf : string -> string -> t

val on_row : (string * 'a) list -> l Binary_tree.t -> (l * 'a) Binary_tree.t
