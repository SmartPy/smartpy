(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Basics
open Untyped

type t = expr [@@deriving show {with_path = false}]

type nullary_expr = line_no:line_no -> t

type unary_expr = line_no:line_no -> t -> t

type binary_expr = line_no:line_no -> t -> t -> t

type ternary_expr = line_no:line_no -> t -> t -> t -> t

let build ~line_no e = {e; line_no}

let lsl_ ~line_no x y = build ~line_no (EMPrim2 (Lsl, x, y))

let lsr_ ~line_no x y = build ~line_no (EMPrim2 (Lsr, x, y))

let sub_mutez ~line_no x y = build ~line_no (EMPrim2 (Sub_mutez, x, y))

let add ~line_no x y = build ~line_no (EPrim2 (EAdd, x, y))

let add_prim ~line_no x y = build ~line_no (EMPrim2 (Add, x, y))

let and_ ~line_no x y = build ~line_no (EPrim2 (EAnd, x, y))

let div ~line_no x y = build ~line_no (EPrim2 (EDiv, x, y))

let ediv ~line_no x y = build ~line_no (EPrim2 (EEDiv, x, y))

let eq ~line_no x y = build ~line_no (EPrim2 (EEq, x, y))

let ge ~line_no x y = build ~line_no (EPrim2 (EGe, x, y))

let gt ~line_no x y = build ~line_no (EPrim2 (EGt, x, y))

let le ~line_no x y = build ~line_no (EPrim2 (ELe, x, y))

let lt ~line_no x y = build ~line_no (EPrim2 (ELt, x, y))

let max ~line_no x y = build ~line_no (EPrim2 (EMax, x, y))

let min ~line_no x y = build ~line_no (EPrim2 (EMin, x, y))

let mod_ ~line_no x y = build ~line_no (EPrim2 (EMod, x, y))

let mul_homo ~line_no x y = build ~line_no (EPrim2 (EMul_homo, x, y))

let mul_prim ~line_no x y = build ~line_no (EMPrim2 (Mul, x, y))

let compare ~line_no x y = build ~line_no (EMPrim2 (Compare, x, y))

let neq ~line_no x y = build ~line_no (EPrim2 (ENeq, x, y))

let or_ ~line_no x y = build ~line_no (EPrim2 (EOr, x, y))

let sub ~line_no x y = build ~line_no (EPrim2 (ESub, x, y))

let xor ~line_no x y = build ~line_no (EPrim2 (EXor, x, y))

let storage ~line_no = build ~line_no (EVar ("__storage__", Local))

let attr ~name ~line_no x = build ~line_no (EPrim1 (EAttr name, x))

let variant ~name ~line_no x = build ~line_no (EPrim1 (EVariant name, x))

let is_variant ~name ~line_no x = build ~line_no (EPrim1 (EIs_variant name, x))

let variable ~line_no arg_name = build ~line_no (EVar (arg_name, Simple))

let open_variant ~line_no name x missing_message =
  build ~line_no (EOpen_variant (name, x, missing_message))

let update_map ~line_no key value map =
  build ~line_no (EPrim3 (EUpdate_map, key, value, map))

let params ~line_no = build ~line_no (EVar ("__parameter__", Local))

let local ~line_no n = build ~line_no (EVar (n, Local))

let meta_local ~line_no n = build ~line_no (EPrim0 (EMeta_local n))

let operations ~line_no = local ~line_no "__operations__"

let private_ ~line_no n = build ~line_no (EPrivate n)

let item ~line_no items key default_value missing_message =
  build ~line_no (EItem {items; key; default_value; missing_message})

let contains ~line_no member items =
  build ~line_no (EPrim2 (EContains, member, items))

let sum ~line_no l = build ~line_no (EPrim1 (ESum, l))

let range ~line_no a b step = build ~line_no (EPrim3 (ERange, a, b, step))

let cons ~line_no x l = build ~line_no (EPrim2 (ECons, x, l))

let literal ~line_no x = build ~line_no (EPrim0 (ELiteral x))

let bounded ~line_no x = build ~line_no (EPrim0 (EBounded x))

let unbounded ~line_no x = build ~line_no (EPrim1 (EUnbounded, x))

let is_failing ~line_no x = build ~line_no (EIs_failing x)

let catch_exception ~t ~line_no x = build ~line_no (ECatch_exception (t, x))

let unit = literal ~line_no:[] Literal.unit

let type_annotation ~t ~line_no e =
  build ~line_no (EPrim1 (EType_annotation t, e))

let convert ~line_no x = build ~line_no (EPrim1 (EConvert, x))

let record ~line_no entries = build ~line_no (ERecord entries)

let list ~line_no ~elems = build ~line_no (EList elems)

let map ~line_no ~big ~entries = build ~line_no (EMap (big, entries))

let set ~line_no ~entries = build ~line_no (ESet entries)

let hash_key ~line_no e = build ~line_no (EMPrim1 (Hash_key, e))

let blake2b ~line_no e = {e = EMPrim1 (Blake2b, e); line_no}

let sha256 ~line_no e = {e = EMPrim1 (Sha256, e); line_no}

let sha512 ~line_no e = {e = EMPrim1 (Sha512, e); line_no}

let keccak ~line_no e = {e = EMPrim1 (Keccak, e); line_no}

let sha3 ~line_no e = {e = EMPrim1 (Sha3, e); line_no}

let pack ~line_no e = build ~line_no (EPrim1 (EPack, e))

let unpack ~line_no e t = build ~line_no (EPrim1 (EUnpack t, e))

let check_signature ~line_no message signature pk =
  build ~line_no (EMPrim3 (Check_signature, message, signature, pk))

let account_of_seed ~seed ~line_no =
  build ~line_no (EPrim0 (EAccount_of_seed {seed}))

let make_signature ~line_no ~secret_key ~message ~message_format =
  build ~line_no (EMake_signature {secret_key; message; message_format})

let scenario_var ~line_no id = build ~line_no (EVar (id, Scenario))

let resolve ~line_no e = build ~line_no (EPrim1 (EResolve, e))

let constant ~line_no e t = build ~line_no (EPrim0 (EConstant (e, t)))

let constant_scenario_var ~line_no e = build ~line_no (EPrim0 (EConstant_var e))

let split_tokens ~line_no mutez quantity total =
  build ~line_no (EPrim3 (ESplit_tokens, mutez, quantity, total))

let now = build (EMPrim0 Now)

let chain_id = build (EMPrim0 Chain_id)

let balance = build (EMPrim0 Balance)

let sender = build (EMPrim0 Sender)

let source = build (EMPrim0 Source)

let amount = build (EMPrim0 Amount)

let level = build (EMPrim0 Level)

let total_voting_power = build (EMPrim0 Total_voting_power)

let self = build (EMPrim0 (Self None))

let self_address = build (EMPrim0 Self_address)

let add_seconds ~line_no t s = build ~line_no (EPrim2 (EAdd_seconds, t, s))

let notE ~line_no x = build ~line_no (EMPrim1 (Not, x))

let absE ~line_no x = build ~line_no (EMPrim1 (Abs, x))

let to_int ~line_no x = build ~line_no (EPrim1 (ETo_int, x))

let is_nat ~line_no x = build ~line_no (EMPrim1 (IsNat, x))

let negE ~line_no x = build ~line_no (EPrim1 (ENeg, x))

let signE ~line_no x = build ~line_no (EPrim1 (ESign, x))

let slice ~line_no ~offset ~length ~buffer =
  build ~line_no (ESlice {offset; length; buffer})

let concat_list ~line_no l = build ~line_no (EPrim1 (EConcat_list, l))

let size ~line_no s = build ~line_no (EPrim1 (ESize, s))

let match_cons ~line_no name = build ~line_no (EPrim0 (EMatch_cons name))

let self_entry_point ~line_no name = build ~line_no (EMPrim0 (Self (Some name)))

let to_address ~line_no e =
  match e.e with
  | EMPrim0 (Self None) -> build ~line_no (EMPrim0 Self_address)
  | _ -> build ~line_no (EPrim1 (EAddress, e))

let implicit_account ~line_no e = build ~line_no (EPrim1 (EImplicit_account, e))

let voting_power ~line_no e = build ~line_no (EPrim1 (EVoting_power, e))

let list_rev ~line_no e = build ~line_no (EPrim1 (EList_rev, e))

let list_items ~line_no e rev = build ~line_no (EPrim1 (EList_items rev, e))

let list_keys ~line_no e rev = build ~line_no (EPrim1 (EList_keys rev, e))

let list_values ~line_no e rev = build ~line_no (EPrim1 (EList_values rev, e))

let list_elements ~line_no e rev =
  build ~line_no (EPrim1 (EList_elements rev, e))

let contract ~line_no entry_point arg_type address =
  build ~line_no (EContract {entry_point; arg_type; address})

let view ~line_no name param address return_type =
  build ~line_no (EPrim2 (EView (name, return_type), param, address))

let static_view ~line_no static_id name param =
  build ~line_no (EPrim1 (EStatic_view (static_id, name), param))

let tuple ~line_no es = build ~line_no (ETuple es)

let proj ~line_no i e = build ~line_no (EPrim1 (EProject i, e))

let first ~line_no = proj ~line_no 0

let second ~line_no = proj ~line_no 1

let none ~line_no = variant ~line_no ~name:"None" unit

let some ~line_no e = variant ~line_no ~name:"Some" e

let left ~line_no l = variant ~line_no ~name:"Left" l

let right ~line_no r = variant ~line_no ~name:"Right" r

let inline_michelson ~line_no michelson exprs =
  build ~line_no (EMichelson (michelson, exprs))

let map_function ~line_no l f = build ~line_no (EMap_function {l; f})

let call_lambda ~line_no parameter lambda =
  build ~line_no (EPrim2 (ECall_lambda, parameter, lambda))

let apply_lambda ~line_no parameter lambda =
  build ~line_no (EPrim2 (EApply_lambda, parameter, lambda))

let lambda ~line_no name body ~with_storage ~with_operations ~recursive =
  build ~line_no
    (ELambda
       {
         name
       ; body
       ; clean_stack = true
       ; with_storage
       ; with_operations
       ; recursive
       ; derived = U
       })

let lambda_params ~line_no name = build ~line_no (EVar (name, Simple))

let create_contract ~line_no ~baker ~balance ~storage ({contract} : contract) =
  build ~line_no
    (ECreate_contract {contract_template = contract; baker; balance; storage})

let sapling_empty_state memo =
  build ~line_no:[] (EMPrim0 (Sapling_empty_state {memo}))

let sapling_verify_update ~line_no state transaction =
  build ~line_no (ESapling_verify_update {state; transaction})

let set_delegate ~line_no e = build ~line_no (EPrim1 (ESet_delegate, e))

let transfer ~line_no ~arg ~amount ~destination =
  build ~line_no (ETransfer {arg; amount; destination})

let emit ~line_no tag with_type x =
  build ~line_no (EPrim1 (EEmit (tag, with_type), x))

let contract_address ~line_no entry_point e =
  build ~line_no (EPrim0 (EContract_address (e, entry_point)))

let contract_typed ~line_no entry_point e =
  build ~line_no (EPrim0 (EContract_typed (e, entry_point)))

let contract_entrypoint_map ~line_no c =
  build ~line_no (EPrim0 (EContract_entrypoint_map c))

let contract_entrypoint_id ~line_no c ep =
  build ~line_no (EPrim0 (EContract_entrypoint_id (c, ep)))

let contract_data ~line_no e = build ~line_no (EPrim0 (EContract_data e))

let contract_balance ~line_no e = build ~line_no (EPrim0 (EContract_balance e))

let contract_baker ~line_no e = build ~line_no (EPrim0 (EContract_baker e))

let entrypoint_map ~line_no = build ~line_no (EPrim0 EEntrypoint_map)

let entrypoint_id ~line_no ep = build ~line_no (EPrim0 (EEntrypoint_id ep))

let ematch ~line_no scrutinee clauses =
  build ~line_no (EMatch (scrutinee, clauses))

let eif ~line_no cond a b = build ~line_no (EIf (cond, a, b))

let allow_lambda_full_stack ({e; line_no} as x) =
  match e with
  | ELambda params -> build ~line_no (ELambda {params with clean_stack = false})
  | _ -> x

let test_ticket ~line_no ticketer content amount =
  build ~line_no (EPrim3 (ETest_ticket, ticketer, content, amount))

let ticket ~line_no content amount =
  build ~line_no (EPrim2 (ETicket, content, amount))

let read_ticket ~line_no ticket = build ~line_no (EPrim1 (ERead_ticket, ticket))

let split_ticket ~line_no ticket decomposition =
  build ~line_no (EPrim2 (ESplit_ticket, ticket, decomposition))

let join_tickets ~line_no tickets =
  build ~line_no (EPrim1 (EJoin_tickets, tickets))

let pairing_check ~line_no pairs =
  build ~line_no (EPrim1 (EPairing_check, pairs))

let get_and_update ~line_no key value map =
  build ~line_no (EPrim3 (EGet_and_update, key, value, map))

let open_chest ~line_no chest_key chest time =
  build ~line_no (EMPrim3 (Open_chest, chest_key, chest, time))

let get_opt ~line_no k m = build ~line_no (EPrim2 (EGet_opt, k, m))

let of_value_f t v =
  let line_no = [] in
  match v with
  | Literal l -> literal ~line_no l
  | Bounded (_, l) -> bounded ~line_no l
  | Contract {address; entrypoint} -> (
      match Type.unF t with
      | T1 (T_contract, t) ->
          {
            e =
              EPrim0
                (ECst_contract {address; entry_point = entrypoint; type_ = t})
          ; line_no
          }
      | _ -> assert false)
  | Record entries -> build ~line_no (ERecord entries)
  | Variant (lbl, arg) -> build ~line_no (EPrim1 (EVariant lbl, arg))
  | List elems -> build ~line_no (EList elems)
  | Set entries -> set ~line_no ~entries
  | Map entries -> (
      match Type.unF t with
      | T2 (((T_map | T_big_map) as tm), _, _) ->
          let big = tm = T_big_map in
          build ~line_no
            (EMap (big, entries |> List.map (fun (k, v) -> (k, v))))
      | _ -> assert false)
  | Tuple vs -> tuple ~line_no vs
  | Closure ({name; body; recursive}, args) ->
      List.fold_left
        (fun f arg -> apply_lambda ~line_no f arg)
        (lambda ~line_no name (erase_types_command body) ~with_storage:None
           ~with_operations:false ~recursive)
        args
  | Operation _ -> failwith "TODO expr. Operation"
  | Ticket (ticketer, content, amount) ->
      let ticketer = literal ~line_no (Literal.address ticketer) in
      let content = content in
      let amount = literal ~line_no (Literal.int amount) in
      test_ticket ~line_no ticketer content amount

let of_value = cata_tvalue of_value_f
