(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Utils

type env = {
    unknowns : (string, Type.t) Hashtbl.t
  ; entrypoint : string option
}

let init_env () = {unknowns = Hashtbl.create 5; entrypoint = None}

let attribute_source name =
  match Base.String.split (Base.String.strip name) ~on:' ' with
  | [name] -> name
  | [name; "as"; _] -> name
  | [name; "as"] -> name
  | _ -> Printf.ksprintf failwith "Bad attribute format %S" name

let attribute_target name =
  match Base.String.split (Base.String.strip name) ~on:' ' with
  | [name] -> name
  | [_; "as"; target] -> target
  | [_; "as"] -> ""
  | _ -> Printf.ksprintf failwith "Bad attribute format %S" name

let rec import_inner_layout = function
  | Base.Sexp.List [Atom leaf] ->
      ( [attribute_source leaf]
      , Layout.leaf (attribute_source leaf) (attribute_target leaf) )
  | Base.Sexp.List [n1; n2] ->
      let n1, l1 = import_inner_layout n1 in
      let n2, l2 = import_inner_layout n2 in
      (n1 @ n2, Binary_tree.node l1 l2)
  | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l)

let import_layout ~line_no l layout =
  match l with
  | [] -> Hole.mk ()
  | _ -> (
      match layout with
      | Base.Sexp.Atom "None" -> Hole.mk ()
      | List [Atom "Some"; Atom "Right"] ->
          Value (Type.comb_layout_of_row `Right l)
      | List [Atom "Some"; layout] ->
          let n, layout = import_inner_layout layout in
          let l1 = List.sort compare n in
          let l2 = List.sort compare l in
          if l1 <> l2
          then
            raise
              (SmartExcept
                 [
                   `Text "Bad layout for type"
                 ; `Br
                 ; `Text (String.concat ", " l1)
                 ; `Br
                 ; `Text "!="
                 ; `Br
                 ; `Text (String.concat ", " l2)
                 ; `Br
                 ; `Line line_no
                 ]);
          Value layout
      | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l))

let rec assocLists (ns : string list) = function
  | Base.Sexp.Atom (a : string) :: List l :: _ when List.mem a ns -> l
  | _ :: l -> assocLists ns l
  | _ -> failwith ("Cannot find " ^ List.show String.pp ns)

let rec assocList (n : string) = function
  | Base.Sexp.Atom (a : string) :: List l :: _ when Stdlib.(a = n) -> l
  | _ :: l -> assocList n l
  | _ -> failwith ("Cannot find " ^ n)

let default_line_no_f =
  let id x = x in
  let f_expr line_no e default_line_no =
    let line_no = if line_no = [] then default_line_no else line_no in
    let e = map_expr_f (fun e -> e line_no) id id elim_untyped e in
    {Untyped.line_no; e}
  in
  let f_command line_no c =
    let c = map_command_f (fun e -> e line_no) id id c in
    {Untyped.line_no; c}
  in
  let f_type = id in
  {f_expr; f_command; f_type}

let _default_line_no_expr e line_no = cata_expr default_line_no_f e line_no

let default_line_no_command e = cata_command default_line_no_f e

let int_option_of_string = function
  | "None" -> None
  | l -> Some (int_of_string l)

let import_line_no = function
  | Base.Sexp.Atom "None" -> []
  | Atom l -> [("", int_of_string l)]
  | List [Atom s; Atom l] -> [(s, int_of_string l)]
  | x -> failwith ("import_line_no: " ^ Base.Sexp.to_string x)

let string_of_line_no l =
  String.concat "_" (List.map (fun (_, i) -> string_of_int i) l)

let import_bool = function
  | "True" -> true
  | "False" -> false
  | x -> failwith ("import_bool: " ^ x)

let import_opt_bool = function
  | "None" -> None
  | "True" -> Some true
  | "False" -> Some false
  | x -> failwith ("import_opt_bool: " ^ x)

let import_hole f = function
  | "None" -> Hole.mk ()
  | x -> Hole.value (f x)

let unAtom = function
  | Base.Sexp.Atom n -> n
  | _ -> failwith "unAtom"

let rec import_flags =
  let open Base.Sexp in
  function
  | [] -> []
  | List x :: xs -> (
      let x = List.map unAtom x in
      match Config.parse_flag x with
      | Some x -> x :: import_flags xs
      | _ -> failwith ("invalid flag usage: " ^ String.concat ", " x))
  | xs -> failwith ("import_flags: " ^ to_string (List xs))

let import_binding = function
  | Base.Sexp.List [Atom "binding"; Atom var; Atom field] -> {var; field}
  | x -> failwith ("import_binding: " ^ Base.Sexp.to_string x)

let rec import_literal =
  let open Literal in
  let chop_prefix n =
    Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
  in
  function
  | [Base.Sexp.Atom "unit"] -> unit
  | [Atom "string"; Atom n] -> string n
  | [Atom "bytes"; Atom n] ->
      let n = chop_prefix n in
      bytes (Hex.to_string (`Hex n))
  | [Atom "bls12_381_g1"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_g1 (Hex.to_string (`Hex n))
  | [Atom "bls12_381_g2"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_g2 (Hex.to_string (`Hex n))
  | [Atom "bls12_381_fr"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_fr (Hex.to_string (`Hex n))
  | [Atom "chest_key"; Atom n] ->
      let n = chop_prefix n in
      chest_key (Hex.to_string (`Hex n))
  | [Atom "chest"; Atom n] ->
      let n = chop_prefix n in
      chest (Hex.to_string (`Hex n))
  | [Atom "chain_id_cst"; Atom n] ->
      let n = chop_prefix n in
      chain_id (Hex.to_string (`Hex n))
  | [Atom "int"; Atom n] -> int (Bigint.of_string ~msg:"import" n)
  | [Atom "intOrNat"; Atom n] ->
      intOrNat (Hole.mk ()) (Bigint.of_string ~msg:"import" n)
  | [Atom "nat"; Atom n] -> nat (Bigint.of_string ~msg:"import" n)
  | [Atom "timestamp"; Atom n] -> timestamp (Bigint.of_string ~msg:"import" n)
  | [Atom "bool"; Atom "True"] -> bool true
  | [Atom "bool"; Atom "False"] -> bool false
  | [Atom "key"; Atom n] -> key n
  | [Atom "secret_key"; Atom n] -> secret_key n
  | [Atom "signature"; Atom n] -> signature n
  | [Atom "address"; Atom n] -> address n
  | [Atom "key_hash"; Atom n] -> key_hash n
  | [Atom "mutez"; List l] ->
      mutez (Base.Option.value_exn (unInt (import_literal l)))
  | [Atom "mutez"; Atom n] -> mutez (Bigint.of_string ~msg:"import" n)
  | [
      Atom "sapling_test_transaction"
    ; Atom memo
    ; Atom source
    ; Atom target
    ; Atom amount
    ; Atom boundData
    ] ->
      let source = if source = "" then None else Some source in
      let target = if target = "" then None else Some target in
      sapling_test_transaction (int_of_string memo) source target
        (Bigint.of_string ~msg:"import" amount)
        boundData
  | l ->
      Format.kasprintf failwith "Literal format error: %a" Base.Sexp.pp (List l)

let import_atom_type s =
  let open Type in
  match s with
  | "bool" -> bool
  | "int" -> int
  | "nat" -> nat
  | "intOrNat" -> intOrNat ()
  | "string" -> string
  | "bytes" -> bytes
  | "chain_id" -> chain_id
  | "mutez" -> mutez
  | "timestamp" -> timestamp
  | "address" -> address
  | "key_hash" -> key_hash
  | "key" -> key
  | "signature" -> signature
  | "operation" -> operation
  | "bls12_381_g1" -> bls12_381_g1
  | "bls12_381_g2" -> bls12_381_g2
  | "bls12_381_fr" -> bls12_381_fr
  | "chest_key" -> chest_key
  | "chest" -> chest
  | "never" -> never
  | s -> failwith ("Type format error atom " ^ s)

let import_type env =
  let open Type in
  let rec import_type = function
    | Base.Sexp.Atom "unit" -> unit
    | Atom s -> import_atom_type s
    | List (Atom "bounded" :: t :: Atom final :: cases) ->
        let import_case = function
          | Sexplib0.Sexp.List [_; Atom "literal"; List l] -> import_literal l
          | _ -> failwith "import type bounded"
        in
        let var = if import_bool final then None else Some (VarId.mk ()) in
        bounded (import_type t) (List.map import_case cases) var
    | List [Atom "sapling_state"; Atom memo_size] ->
        sapling_state (int_option_of_string memo_size)
    | List [Atom "sapling_transaction"; Atom memo_size] ->
        sapling_transaction (int_option_of_string memo_size)
    | List [Atom "unknown"; Atom id] -> (
        let i = "sp:" ^ id in
        if i = ""
        then failwith "empty unknown"
        else
          match Hashtbl.find_opt env.unknowns i with
          | Some t -> t
          | None ->
              let r = unknown_raw i in
              Hashtbl.replace env.unknowns i r;
              r)
    | List [Atom "record"; List l; layout; line_no] ->
        let line_no = import_line_no line_no in
        let l = List.map importField l in
        record_or_unit (import_layout ~line_no (List.map fst l) layout) l
    | List [Atom "variant"; List l; layout; line_no] ->
        let line_no = import_line_no line_no in
        let l = List.map importField l in
        variant (import_layout ~line_no (List.map fst l) layout) l
    | List [Atom "list"; t] -> list (import_type t)
    | List [Atom "ticket"; t] -> ticket (import_type t)
    | List [Atom "option"; t] -> option (import_type t)
    | List [Atom "contract"; t] -> contract (import_type t)
    | List (Atom "tuple" :: l) -> tuple (List.map import_type l)
    | List [Atom "set"; t] -> set ~telement:(import_type t)
    | List [Atom "map"; k; v] ->
        map ~big:false ~tkey:(import_type k) ~tvalue:(import_type v)
    | List [Atom "bigmap"; k; v] ->
        map ~big:true ~tkey:(import_type k) ~tvalue:(import_type v)
    | List [Atom "lambda"; t1; t2] ->
        lambda no_effects (import_type t1) (import_type t2)
    | List
        [
          Atom "lambda"
        ; Atom with_storage
        ; Atom with_operations
        ; tstorage
        ; t1
        ; t2
        ] ->
        let tstorage =
          match tstorage with
          | Atom "None" -> Type.unknown_raw "storage"
          | t -> import_type t
        in
        let with_storage =
          match with_storage with
          | "read-only" -> Some (Read_only, tstorage)
          | "read-write" | "True" -> Some (Read_write, tstorage)
          | "None" | "False" -> None
          | _ -> assert false
        in
        let with_storage = Hole.value with_storage in
        let with_operations = import_hole import_bool with_operations in
        let effects = {with_storage; with_operations} in
        lambda effects (import_type t1) (import_type t2)
    | List l as t ->
        failwith
          ("Type format error list " ^ Base.Sexp.to_string t ^ "  "
          ^ string_of_int (List.length l))
  and importField = function
    | List [Atom name; v] -> (name, import_type v)
    | l -> failwith ("Type field format error " ^ Base.Sexp.to_string l)
  in
  import_type

let import_contract_id = function
  | Base.Sexp.List [_; Atom "static_id"; Atom id] ->
      C_static {static_id = int_of_string id}
  | Base.Sexp.List [_; Atom "dynamic_id"; Atom id] ->
      C_dynamic {dynamic_id = int_of_string id}
  | l -> Format.kasprintf failwith "Contract_id format error: %a" Base.Sexp.pp l

let rec import_expr_inline_michelson env =
  let import_type t = import_type env t in
  let rec import_expr_inline_michelson = function
    | Base.Sexp.List [Atom "call_michelson"; instr; Atom _line_no] ->
        import_expr_inline_michelson instr
    | Base.Sexp.List (Atom "op" :: Atom name :: args) ->
        let parsed = Micheline_encoding.parse_node name in
        let rec extractTypes acc = function
          | [] -> assert false
          | Base.Sexp.Atom "out" :: out ->
              (List.rev acc, List.map import_type out)
          | t :: args -> extractTypes (import_type t :: acc) args
        in
        let typesIn, typesOut = extractTypes [] args in
        {name; parsed; typesIn; typesOut}
    | input ->
        failwith
          (Printf.sprintf "Cannot parse inline michelson %s"
             (Base.Sexp.to_string input))
  in
  import_expr_inline_michelson

and import_expr env =
  let import_type t = import_type env t in
  let import_expr_inline_michelson = import_expr_inline_michelson env in
  let open Expr in
  let rec import_expr = function
    | Base.Sexp.List (line_no :: Atom f :: args) as input -> (
        let line_no = import_line_no line_no in
        match (f, args) with
        | "sender", [] -> sender ~line_no
        | "source", [] -> source ~line_no
        | "amount", [] -> amount ~line_no
        | "balance", [] -> balance ~line_no
        | "now", [] -> now ~line_no
        | "self", [] -> self ~line_no
        | "self", [Atom name] ->
            let name =
              if name = ""
              then
                match env.entrypoint with
                | None -> failwith "import: params type yet unknown"
                | Some name -> name
              else name
            in
            self_entry_point ~line_no name
        | "self_address", [] -> self_address ~line_no:[]
        | "chain_id", [] -> chain_id ~line_no:[]
        | "total_voting_power", [] -> total_voting_power ~line_no:[]
        | "sapling_empty_state", [Atom memo] ->
            sapling_empty_state (int_of_string memo)
        | "level", [] -> level ~line_no:[]
        | "eq", [e1; e2] -> Expr.eq ~line_no (import_expr e1) (import_expr e2)
        | "neq", [e1; e2] -> Expr.neq ~line_no (import_expr e1) (import_expr e2)
        | "le", [e1; e2] -> Expr.le ~line_no (import_expr e1) (import_expr e2)
        | "lt", [e1; e2] -> Expr.lt ~line_no (import_expr e1) (import_expr e2)
        | "ge", [e1; e2] -> Expr.ge ~line_no (import_expr e1) (import_expr e2)
        | "gt", [e1; e2] -> Expr.gt ~line_no (import_expr e1) (import_expr e2)
        | "add", [e1; e2] -> Expr.add ~line_no (import_expr e1) (import_expr e2)
        | "sub", [e1; e2] -> Expr.sub ~line_no (import_expr e1) (import_expr e2)
        | "sub_mutez", [e1; e2] ->
            Expr.sub_mutez ~line_no (import_expr e1) (import_expr e2)
        | "ADD", [e1; e2] ->
            Expr.add_prim ~line_no (import_expr e1) (import_expr e2)
        | "mul_homo", [e1; e2] ->
            Expr.mul_homo ~line_no (import_expr e1) (import_expr e2)
        | "MUL", [e1; e2] ->
            Expr.mul_prim ~line_no (import_expr e1) (import_expr e2)
        | "ediv", [e1; e2] ->
            Expr.ediv ~line_no (import_expr e1) (import_expr e2)
        | "div", [e1; e2] -> Expr.div ~line_no (import_expr e1) (import_expr e2)
        | "mod", [e1; e2] ->
            Expr.mod_ ~line_no (import_expr e1) (import_expr e2)
        | "or", [e1; e2] -> Expr.or_ ~line_no (import_expr e1) (import_expr e2)
        | "and", [e1; e2] ->
            Expr.and_ ~line_no (import_expr e1) (import_expr e2)
        | "COMPARE", [e1; e2] ->
            compare ~line_no (import_expr e1) (import_expr e2)
        | "max", [e1; e2] -> Expr.max ~line_no (import_expr e1) (import_expr e2)
        | "min", [e1; e2] -> Expr.min ~line_no (import_expr e1) (import_expr e2)
        | "sum", [a] -> sum ~line_no (import_expr a)
        | "to_address", [e] -> to_address ~line_no (import_expr e)
        | "implicit_account", [e] -> implicit_account ~line_no (import_expr e)
        | "cons", [e1; e2] -> cons ~line_no (import_expr e1) (import_expr e2)
        | "range", [e1; e2; e3] ->
            range ~line_no (import_expr e1) (import_expr e2) (import_expr e3)
        | "literal", [List l] -> literal ~line_no (import_literal l)
        | "bounded", [x] ->
            let l =
              match import_expr x with
              | {e = EPrim0 (ELiteral l)} -> l
              | _ ->
                  raise
                    (SmartExcept
                       [
                         `Text
                           "sp.bounded can only be applied to simple literals \
                            (integers, strings, addresses, etc.), not \
                            containers (records, variants, lists, etc.) nor \
                            general expressions."
                       ; `Line line_no
                       ])
            in
            bounded ~line_no l
        | "unbounded", [x] -> unbounded ~line_no (import_expr x)
        | "is_failing", [x] -> is_failing ~line_no (import_expr x)
        | "catch_exception", [x; t] ->
            catch_exception ~line_no (import_expr x) ~t:(import_type t)
        | "convert", [x] -> convert ~line_no (import_expr x)
        | "list", l -> list ~line_no ~elems:(List.map import_expr l)
        | "test_ticket", [ticketer; content; amount] ->
            test_ticket ~line_no (import_expr ticketer) (import_expr content)
              (import_expr amount)
        | "ticket", [content; amount] ->
            ticket ~line_no (import_expr content) (import_expr amount)
        | "read_ticket", [ticket] -> read_ticket ~line_no (import_expr ticket)
        | "split_ticket", [ticket; decomposition] ->
            split_ticket ~line_no (import_expr ticket)
              (import_expr decomposition)
        | "join_tickets", [tickets] ->
            join_tickets ~line_no (import_expr tickets)
        | "pairing_check", [pairs] -> pairing_check ~line_no (import_expr pairs)
        | "voting_power", [e] -> voting_power ~line_no (import_expr e)
        | "first", [e] -> first ~line_no (import_expr e)
        | "second", [e] -> second ~line_no (import_expr e)
        | "tuple", es -> tuple ~line_no (List.map import_expr es)
        | "neg", [e1] -> negE ~line_no (import_expr e1)
        | "abs", [e1] -> absE ~line_no (import_expr e1)
        | "to_int", [e1] -> to_int ~line_no (import_expr e1)
        | "is_nat", [e1] -> is_nat ~line_no (import_expr e1)
        | "sign", [e1] -> signE ~line_no (import_expr e1)
        | "not", [e1] -> notE ~line_no (import_expr e1)
        | "contract_address", [id; Atom entry_point] ->
            let entry_point =
              match entry_point with
              | "" -> None
              | x -> Some x
            in
            contract_address ~line_no entry_point (import_contract_id id)
        | "contract_typed", [id; Atom entry_point] ->
            let entry_point =
              match entry_point with
              | "" -> None
              | x -> Some x
            in
            contract_typed ~line_no entry_point (import_contract_id id)
        | "contract_balance", [id] ->
            contract_balance ~line_no (import_contract_id id)
        | "contract_baker", [id] ->
            contract_baker ~line_no (import_contract_id id)
        | "contract_data", [id] ->
            contract_data ~line_no (import_contract_id id)
        | "contract_entrypoint_map", [id] ->
            contract_entrypoint_map ~line_no (import_contract_id id)
        | "contract_entrypoint_id", [id; Atom ep] ->
            contract_entrypoint_id ~line_no (import_contract_id id) ep
        | "entrypoint_map", [] -> entrypoint_map ~line_no
        | "entrypoint_id", [Atom ep] -> entrypoint_id ~line_no ep
        | "contract", [Atom entry_point; t; addr] ->
            let entry_point =
              match entry_point with
              | "" -> None
              | _ -> Some entry_point
            in
            let address = import_expr addr in
            contract ~line_no entry_point (import_type t) address
        | "view", [Atom name; param; addr; t] ->
            let param = import_expr param in
            let address = import_expr addr in
            let ty = import_type t in
            view ~line_no name param address ty
        | "static_view", [Atom name; contract_id; param] ->
            let static_id =
              match import_contract_id contract_id with
              | C_static static_id -> static_id
              | C_dynamic _ -> assert false
            in
            let param = import_expr param in
            static_view ~line_no static_id name param
        | "data", [] -> local ~line_no:[] "__storage__"
        | "operations", [] -> operations ~line_no
        | "attr", [x; Atom name] -> attr ~line_no (import_expr x) ~name
        | "match_cons", [_e] ->
            let name =
              Printf.sprintf "match_cons_%s" (string_of_line_no line_no)
            in
            match_cons ~line_no name
        | "is_variant", [x; Atom name] ->
            is_variant ~line_no ~name (import_expr x)
        | "variant_arg", [Atom arg_name] -> variable ~line_no arg_name
        | "open_variant", [x; Atom name; missing_message] ->
            let missing_message =
              match missing_message with
              | Atom "None" -> None
              | _ -> Some (import_expr missing_message)
            in
            open_variant ~line_no name (import_expr x) missing_message
        | "variant", [Atom name; x] -> variant ~line_no ~name (import_expr x)
        | "blake2b", [e] -> blake2b ~line_no (import_expr e)
        | "sha256", [e] -> sha256 ~line_no (import_expr e)
        | "sha512", [e] -> sha512 ~line_no (import_expr e)
        | "keccak", [e] -> keccak ~line_no (import_expr e)
        | "sha3", [e] -> sha3 ~line_no (import_expr e)
        | "hash_key", [e] -> hash_key ~line_no (import_expr e)
        | "pack", [e] ->
            let e = import_expr e in
            pack ~line_no e
        | "unpack", [e; t] -> unpack ~line_no (import_expr e) (import_type t)
        | "get_local", [Atom name] -> local ~line_no name
        | "get_meta_local", [Atom name] -> meta_local ~line_no name
        | "params", [] -> params ~line_no
        | "update_map", [map; key; v] ->
            update_map ~line_no (import_expr key) (import_expr v)
              (import_expr map)
        | "get_and_update", [map; key; v] ->
            get_and_update ~line_no (import_expr key) (import_expr v)
              (import_expr map)
        | "open_chest", [chest_key; chest; time] ->
            open_chest ~line_no (import_expr chest_key) (import_expr chest)
              (import_expr time)
        | "get_item", [a; pos] ->
            item ~line_no (import_expr a) (import_expr pos) None None
        | "get_item_default", [a; pos; def] ->
            item ~line_no (import_expr a) (import_expr pos)
              (Some (import_expr def))
              None
        | "get_item_message", [a; pos; message] ->
            item ~line_no (import_expr a) (import_expr pos) None
              (Some (import_expr message))
        | "get_opt", [m; k] -> get_opt ~line_no (import_expr k) (import_expr m)
        | "add_seconds", [t; s] ->
            add_seconds ~line_no (import_expr t) (import_expr s)
        | "iter", [Atom name] -> variable ~line_no name
        | "rev", [e] -> list_rev ~line_no (import_expr e)
        | "items", [e] -> list_items ~line_no (import_expr e) false
        | "keys", [e] -> list_keys ~line_no (import_expr e) false
        | "values", [e] -> list_values ~line_no (import_expr e) false
        | "elements", [e] -> list_elements ~line_no (import_expr e) false
        | "rev_items", [e] -> list_items ~line_no (import_expr e) true
        | "rev_keys", [e] -> list_keys ~line_no (import_expr e) true
        | "rev_values", [e] -> list_values ~line_no (import_expr e) true
        | "rev_elements", [e] -> list_elements ~line_no (import_expr e) true
        | "contains", [items; x] ->
            contains ~line_no (import_expr x) (import_expr items)
        | "check_signature", [pk; s; msg] ->
            check_signature ~line_no (import_expr pk) (import_expr s)
              (import_expr msg)
        | "resolve", [e] -> resolve ~line_no (import_expr e)
        | "constant", [Atom e; t] ->
            let tExpr = import_type t in
            constant ~line_no e tExpr
        | "constant_scenario_var", [Atom e] -> constant_scenario_var ~line_no e
        | "scenario_var", [Atom id] -> scenario_var ~line_no id
        | "make_signature", [sk; msg; Atom fmt] ->
            let secret_key = import_expr sk in
            let message = import_expr msg in
            let message_format =
              match String.lowercase_ascii fmt with
              | "raw" -> `Raw
              | "hex" -> `Hex
              | other ->
                  Format.kasprintf failwith
                    "make_signature: Wrong message format : %S (l. %s)" other
                    (string_of_line_no line_no)
            in
            make_signature ~secret_key ~message ~message_format ~line_no
        | "account_of_seed", [Atom seed] -> account_of_seed ~seed ~line_no
        | "split_tokens", [mutez; quantity; total] ->
            split_tokens ~line_no (import_expr mutez) (import_expr quantity)
              (import_expr total)
        | "slice", [ofs; len; buf] ->
            let offset = import_expr ofs in
            let length = import_expr len in
            let buffer = import_expr buf in
            slice ~offset ~length ~buffer ~line_no
        | "concat", [l] -> concat_list (import_expr l) ~line_no
        | "size", [s] -> size (import_expr s) ~line_no
        | "type_annotation", [e; t] ->
            let e = import_expr e in
            let t = import_type t in
            type_annotation ~line_no e ~t
        | "map", entries ->
            map ~line_no ~big:false ~entries:(List.map import_map_entry entries)
        | "set", entries -> set ~line_no ~entries:(List.map import_expr entries)
        | "big_map", entries ->
            map ~line_no ~big:true ~entries:(List.map import_map_entry entries)
        | "record", l ->
            let import_exprField = function
              | Base.Sexp.List [Atom name; e] -> (name, import_expr e)
              | l ->
                  failwith
                    ("Expression field format error " ^ Base.Sexp.to_string l)
            in
            let cmp (fld1, _) (fld2, _) = Stdlib.compare fld1 fld2 in
            record ~line_no (List.sort cmp (List.map import_exprField l))
        | "ematch", scrutinee :: l ->
            let import_clause = function
              | Base.Sexp.List [Atom name; v] ->
                  let e = import_expr v in
                  (name, e)
              | l -> failwith ("Clause format error: " ^ Base.Sexp.to_string l)
            in
            let scrutinee = import_expr scrutinee in
            ematch ~line_no scrutinee (List.map import_clause l)
        | "eif", [cond; a; b] ->
            let cond = import_expr cond in
            let a = import_expr a in
            let b = import_expr b in
            eif ~line_no cond a b
        | "call_michelson", instr :: args ->
            inline_michelson ~line_no
              (import_expr_inline_michelson instr)
              (List.map import_expr args)
        | ("private" | "global"), [Atom name] -> private_ ~line_no name
        | "map_function", [l; f] ->
            map_function ~line_no (import_expr l)
              (allow_lambda_full_stack (import_expr f))
        | "emit", [Atom tag; Atom with_type; x] ->
            let tag = if tag = "" then None else Some tag in
            emit ~line_no tag (import_bool with_type) (import_expr x)
        | "lambda", [Atom name; List commands] ->
            import_lambda name line_no commands ~with_storage:None
              ~with_operations:false ~recursive:None
        | "lambda", [Atom id; Atom name; List commands] ->
            let name = if name = "" then "_x" ^ id else name in
            import_lambda name line_no commands ~with_storage:None
              ~with_operations:false ~recursive:None
        | ( "lambda_general"
          , [
              Atom with_storage
            ; Atom with_operations
            ; tstorage
            ; Atom name
            ; Atom recursive
            ; List commands
            ] ) ->
            let tstorage =
              match tstorage with
              | Atom "None" -> None
              | t -> Some (import_type t)
            in
            let with_storage =
              match with_storage with
              | "read-only" -> Some (Type.Read_only, tstorage)
              | "read-write" | "True" -> Some (Read_write, tstorage)
              | "None" | "False" -> None
              | _ -> assert false
            in
            let recursive =
              match recursive with
              | "" -> None
              | f -> Some f
            in
            import_lambda name line_no commands ~with_storage
              ~with_operations:(import_bool with_operations)
              ~recursive
        | ( "lambda"
          , [
              Atom id
            ; Atom with_storage
            ; Atom with_operations
            ; tstorage
            ; Atom name
            ; List commands
            ] ) ->
            let name = if name = "" then "_x" ^ id else name in
            let tstorage =
              match tstorage with
              | Atom "None" -> None
              | t -> Some (import_type t)
            in
            let with_storage =
              match with_storage with
              | "read-only" -> Some (Type.Read_only, tstorage)
              | "read-write" | "True" -> Some (Read_write, tstorage)
              | "None" | "False" -> None
              | _ -> assert false
            in
            import_lambda name line_no commands ~with_storage
              ~with_operations:(import_bool with_operations)
              ~recursive:None
        | "lambda_params", [Atom name] -> lambda_params ~line_no name
        | "lambda_params", [Atom id; Atom name; _tParams] ->
            let name = if name = "" then "_x" ^ id else name in
            lambda_params ~line_no name
        | "call_lambda", [lambda; parameter] ->
            call_lambda ~line_no (import_expr parameter) (import_expr lambda)
        | "apply_lambda", [lambda; parameter] ->
            apply_lambda ~line_no (import_expr parameter) (import_expr lambda)
        | "lsl", [expression; shift] ->
            lsl_ ~line_no (import_expr expression) (import_expr shift)
        | "lsr", [expression; shift] ->
            lsr_ ~line_no (import_expr expression) (import_expr shift)
        | "xor", [e1; e2] -> Expr.xor ~line_no (import_expr e1) (import_expr e2)
        | "set_delegate", [x] -> set_delegate ~line_no (import_expr x)
        | "sapling_verify_update", [state; transaction] ->
            sapling_verify_update ~line_no (import_expr state)
              (import_expr transaction)
        | "transfer", [e1; e2; e3] ->
            transfer ~line_no ~arg:(import_expr e1) ~amount:(import_expr e2)
              ~destination:(import_expr e3)
        | ( "create_contract"
          , [
              List [Atom "contract"; contract]
            ; List [Atom "storage"; storage]
            ; List [Atom "baker"; baker]
            ; List [Atom "amount"; amount]
            ] ) ->
            let c = import_contract {env with entrypoint = None} contract in
            let storage = import_expr storage in
            let balance = import_expr amount in
            let baker =
              match baker with
              | Atom "None" -> none ~line_no:[]
              | e -> import_expr e
            in
            create_contract ~line_no ~baker ~balance ~storage c
        | _, l ->
            failwith
              (Printf.sprintf "Expression format error (a %i) %s"
                 (List.length l)
                 (Base.Sexp.to_string_hum input)))
    | x -> failwith ("Expression format error (b) " ^ Base.Sexp.to_string_hum x)
  and import_map_entry = function
    | Base.Sexp.List [k; v] -> (import_expr k, import_expr v)
    | e ->
        failwith
          (Printf.sprintf "import_map_entry: '%s'" (Base.Sexp.to_string e))
  and import_lambda name line_no body ~with_storage ~with_operations ~recursive
      =
    lambda ~line_no name
      (Command.seq ~line_no (import_commands env body))
      ~with_storage ~with_operations ~recursive
  in
  import_expr

and import_meta_expr env =
  let import_expr = import_expr env in
  let rec import_meta_expr = function
    | Base.Sexp.List (Atom f :: args) as input -> (
        match (f, args) with
        | "meta_list", _loc :: l -> Meta.List (List.map import_meta_expr l)
        | "meta_map", _loc :: l ->
            let import_elem = function
              | Base.Sexp.List [Atom "elem"; k; v] ->
                  (import_expr k, import_meta_expr v)
              | _ -> assert false
            in
            Meta.Map (List.map import_elem l)
        | "meta_expr", [e] -> Meta.Other (import_expr e)
        | "meta_view", [Atom name; _line_no] -> Meta.View name
        | _ ->
            failwith
              ("Meta expression format error " ^ Base.Sexp.to_string_hum input))
    | input ->
        failwith
          ("Meta expression format error " ^ Base.Sexp.to_string_hum input)
  in
  import_meta_expr

and import_command env =
  let import_type t = import_type t in
  let import_expr e = import_expr env e in
  let import_commands cs = import_commands env cs in
  let open Command in
  function
  | Base.Sexp.List (line_no :: Atom f :: args) as input -> (
      let line_no = import_line_no line_no in
      match (f, args) with
      | "result", [x] -> result ~line_no (import_expr x)
      | "failwith", [x] -> sp_failwith ~line_no (import_expr x)
      | "never", [x] -> never ~line_no (import_expr x)
      | "verify", [x] -> verify ~line_no (import_expr x) None
      | "verify", [x; message] ->
          verify ~line_no (import_expr x) (Some (import_expr message))
      | "define_local", [Atom name; expr; Atom mutable_] ->
          define_local ~line_no name (import_expr expr) (import_bool mutable_)
      | "define_local", [Atom name; expr] ->
          define_local ~line_no name (import_expr expr) true
      | "while_block", [e; List l] ->
          while_loop ~line_no (import_expr e) (seq ~line_no (import_commands l))
      | "for_group", [Atom name; e; List l] ->
          let e = import_expr e in
          for_loop ~line_no name e (seq ~line_no (import_commands l))
      | "match", [scrutinee; Atom constructor; Atom arg_name; List body] ->
          mk_match ~line_no (import_expr scrutinee)
            [(constructor, arg_name, seq ~line_no (import_commands body))]
      | "match_cases", [scrutinee; _arg; List cases] ->
          let parse_case = function
            | Base.Sexp.List
                [
                  line_no
                ; Atom "match"
                ; _
                ; Atom constructor
                ; Atom arg_name
                ; List body
                ] ->
                let line_no = import_line_no line_no in
                (constructor, arg_name, seq ~line_no (import_commands body))
            | input ->
                failwith ("Bad case parsing: " ^ Base.Sexp.to_string input)
          in
          let cases = List.map parse_case cases in
          mk_match ~line_no (import_expr scrutinee) cases
      | "set_type", [e; t] ->
          set_type ~line_no (import_expr e) (import_type env t)
      | "set_result_type", [List cs; t] ->
          set_result_type ~line_no
            (seq ~line_no (import_commands cs))
            (import_type env t)
      | "set", [v; e] -> set ~line_no (import_expr v) (import_expr e)
      | "del_item", [e; k] -> del_item ~line_no (import_expr e) (import_expr k)
      | "update_set", [e; k; Atom b] ->
          update_set ~line_no (import_expr e) (import_expr k) (import_bool b)
      | "trace", [e] -> trace ~line_no (import_expr e)
      | "bind", _ ->
          raise
            (SmartExcept
               [
                 `Text
                   "Command format error. sp.bind_block() can be used as \
                    follows:"
               ; `Br
               ; `Text "x = sp.bind_block('x'):"
               ; `Br
               ; `Text "with x:"
               ; `Br
               ; `Text "    ..."
               ; `Br
               ; `Text "... x ..."
               ; `Br
               ; `Text (Base.Sexp.to_string input)
               ])
      | _ -> failwith ("Command format error (a) " ^ Base.Sexp.to_string input))
  | Base.Sexp.List cs -> seq ~line_no:[] (import_commands cs)
  | input -> failwith ("Command format error (b) " ^ Base.Sexp.to_string input)

and import_match_cons env expr ok_match ko_match line_no =
  let line_no = import_line_no line_no in
  Command.mk_match_cons ~line_no (import_expr env expr)
    (Printf.sprintf "match_cons_%s" (string_of_line_no line_no))
    (Command.seq ~line_no (import_commands env ok_match))
    (Command.seq ~line_no (import_commands env ko_match))

and import_commands env =
  let import_expr = import_expr env in
  let import_command = import_command env in
  let open Command in
  let rec import_commands r = function
    | Base.Sexp.List [Atom "bind"; Atom name; c1] :: c2 ->
        List.rev_append r
          [
            bind ~line_no:[] (Some name) (import_command c1)
              (seq ~line_no:[] (import_commands [] c2))
          ]
    | Base.Sexp.List [Atom "seq"; Atom seq_name; List seq_block; seq_line_no]
      :: List [Atom "bind"; Atom bind_name; List bind_block; bind_line_no]
      :: rest
      when seq_name = bind_name ->
        let c =
          bind
            ~line_no:(import_line_no seq_line_no)
            (Some seq_name)
            (seq
               ~line_no:(import_line_no seq_line_no)
               (import_commands [] seq_block))
            (seq
               ~line_no:(import_line_no bind_line_no)
               (import_commands [] bind_block))
        in
        import_commands (c :: r) rest
    | Base.Sexp.List [line_no; Atom "if_block"; e; List tBlock]
      :: List [Atom "else_block"; List eBlock]
      :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifte ~line_no (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no (import_commands [] eBlock))
        in
        import_commands (c :: r) rest
    | List [line_no; Atom "if_block"; e; List tBlock] :: rest ->
        let c =
          let line_no = import_line_no line_no in
          ifte ~line_no (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no [])
        in
        import_commands (c :: r) rest
    | List [line_no; Atom "if_some_block"; e; Atom _; List tBlock]
      :: List [Atom "else_block"; List eBlock]
      :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifteSome ~line_no (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no (import_commands [] eBlock))
        in
        import_commands (c :: r) rest
    | List [line_no; Atom "if_some_block"; e; Atom _; List tBlock] :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifteSome ~line_no (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no [])
        in
        import_commands (c :: r) rest
    | List (seq_line_no :: Atom "match_tuple" :: x :: ns) :: rest ->
        let line_no = import_line_no seq_line_no in
        let rest = seq ~line_no (import_commands [] rest) in
        let p = Pattern_tuple (List.map unAtom ns) in
        List.rev_append r [mk_match_product ~line_no (import_expr x) p rest]
    | List (seq_line_no :: Atom "match_record" :: x :: bs) :: rest ->
        let line_no = import_line_no seq_line_no in
        let rest = seq ~line_no (import_commands [] rest) in
        let p = Pattern_record ("", List.map import_binding bs) in
        List.rev_append r [mk_match_product ~line_no (import_expr x) p rest]
    | List [seq_line_no; Atom "modify"; x; List body; Atom v] :: rest ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let p = Pattern_single v in
        let c = mk_modify_product ~line_no (import_expr x) p body in
        import_commands (c :: r) rest
    | List (seq_line_no :: Atom "modify_tuple" :: x :: List body :: ns) :: rest
      ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let p = Pattern_tuple (List.map unAtom ns) in
        let c = mk_modify_product ~line_no (import_expr x) p body in
        import_commands (c :: r) rest
    | List
        (seq_line_no
        :: Atom "modify_record"
        :: x
        :: Atom var
        :: List body
        :: _bs)
      :: rest ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let bs = Pattern_record (var, []) in
        let c = mk_modify_product ~line_no (import_expr x) bs body in
        import_commands (c :: r) rest
    | List [line_no_; Atom "match_cons"; expr; List ok_match]
      :: List [Atom "else_block"; List ko_match]
      :: rest ->
        let m = import_match_cons env expr ok_match ko_match line_no_ in
        import_commands (m :: r) rest
    | List [line_no_; Atom "match_cons"; expr; List ok_match] :: rest ->
        let m = import_match_cons env expr ok_match [] line_no_ in
        import_commands (m :: r) rest
    | x :: l ->
        let x = import_command x in
        import_commands (x :: r) l
    | [] -> List.rev r
  in
  import_commands []

and entrypoint env = function
  | Base.Sexp.List
      [
        Atom name
      ; Atom originate
      ; Atom lazify
      ; Atom lazy_no_code
      ; Atom check_no_incoming_transfer
      ; Atom has_param
      ; parameter_type
      ; line_no
      ; List command
      ] ->
      let originate = import_bool originate in
      let lazify = import_opt_bool lazify in
      let lazy_no_code = import_opt_bool lazy_no_code in
      let check_no_incoming_transfer =
        import_opt_bool check_no_incoming_transfer
      in
      let has_param = import_bool has_param in
      let parameter_type =
        match parameter_type with
        | Atom "None" -> None
        | t -> Some (import_type env t)
      in
      let tparameter_ep =
        match parameter_type with
        | Some t -> `Annotated t
        | None -> if has_param then `Present else `Absent
      in
      let line_no = import_line_no line_no in
      let body =
        default_line_no_command
          (Command.seq ~line_no:[] (import_commands env command))
      in
      {
        channel = name
      ; tparameter_ep
      ; originate
      ; check_no_incoming_transfer
      ; lazify
      ; lazy_no_code
      ; line_no
      ; body
      ; derived = U
      }
  | x -> failwith ("Message format error " ^ Base.Sexp.to_string x)

and import_contract env = function
  | Base.Sexp.Atom _ -> failwith "Parse error contract"
  | List l ->
      let template_id =
        match import_contract_id (List (assocList "template_id" l)) with
        | C_static cid -> cid
        | C_dynamic _ -> assert false
      in
      let storage =
        match assocList "storage" l with
        | [] -> None
        | storage -> Some (import_expr env (Base.Sexp.List storage))
      in
      let tstorage_explicit =
        match assocList "storage_type" l with
        | [List []] -> None
        | [t] -> Some (import_type env t)
        | _ -> assert false
      in
      let entrypoints_layout = assocList "entry_points_layout" l in
      let messages = assocList "entry_points" l in
      let flags = assocList "flags" l in
      let private_variables = assocLists ["privates"; "globals"] l in
      let metadata = assocList "initial_metadata" l in
      let views = assocList "views" l in
      let entrypoints = List.map (entrypoint env) messages in
      let private_variables =
        List.map
          (function
            | Base.Sexp.List [Atom name; variable] -> (name, variable)
            | x ->
                failwith
                  ("Private variable format error " ^ Base.Sexp.to_string x))
          private_variables
      in
      let metadata =
        List.map
          (function
            | Base.Sexp.List [Atom name; variable] -> (name, variable)
            | x -> failwith ("Metadata format error " ^ Base.Sexp.to_string x))
          metadata
      in
      let views =
        List.map
          (function
            | Base.Sexp.List
                [
                  Atom kind
                ; Atom name
                ; Atom has_param
                ; line_no
                ; Atom pure
                ; Atom doc
                ; List commands
                ] ->
                let kind =
                  match kind with
                  | "offchain" -> Offchain
                  | "onchain" -> Onchain
                  | _ -> assert false
                in
                ( kind
                , name
                , import_bool has_param
                , import_line_no line_no
                , import_bool pure
                , commands
                , doc )
            | x -> failwith ("View format error " ^ Base.Sexp.to_string x))
          views
      in
      let balance =
        match Base.Sexp.List (assocList "balance" l) with
        | List [] -> None
        | l -> Some (import_expr env l)
      in
      let build_private_variable (name, variable) =
        let expression = import_expr env variable in
        (name, expression)
      in
      let private_variables =
        List.map build_private_variable private_variables
      in
      let build_metadata (name, variable) =
        let expression = import_meta_expr env variable in
        (name, expression)
      in
      let metadata = List.map build_metadata metadata in
      let view (kind, name, has_param, line_no, pure, commands, doc) =
        let body = Command.seq ~line_no (import_commands env commands) in
        {kind; name; has_param; pure; body; doc; tparameter_derived = U}
      in
      let views = List.map view views in
      let entrypoints_layout =
        match entrypoints_layout with
        | [] -> None
        | _ ->
            let fields, layout =
              import_inner_layout (Base.Sexp.List entrypoints_layout)
            in
            let l1 = List.sort compare fields in
            let l2 =
              List.sort compare
                (List.map (fun (ep : _ entrypoint) -> ep.channel) entrypoints)
            in
            if l1 <> l2
            then
              raise
                (SmartExcept
                   [
                     `Text "Bad entry point layout for contract"
                   ; `Text (string_of_int template_id.static_id)
                   ; `Br
                   ; `Text (String.concat ", " l1)
                   ; `Br
                   ; `Text "!="
                   ; `Br
                   ; `Text (String.concat ", " l2)
                   ]);
            Some layout
      in
      let flags = import_flags flags in
      {
        contract =
          {
            template_id = Some template_id
          ; balance
          ; storage
          ; baker = None
          ; tstorage_explicit
          ; entrypoints
          ; entrypoints_layout
          ; unknown_parts = None
          ; flags
          ; private_variables
          ; metadata
          ; views
          ; derived = U
          }
      }
