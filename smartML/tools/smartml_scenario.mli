(* Copyright 2019-2022 Smart Chain Arena LLC. *)

module Scenario_bak = Scenario
open SmartML
module Scenario = Scenario_bak
open Basics

(** {1 In-browser} *)

val run_scenario_browser :
     primitives:(module Primitives.Primitives)
  -> scenario:string
  -> Config.t
  -> unit
(** Load and execute the scenario in-browser. *)

val run_all_scenarios_browser :
     primitives:(module Primitives.Primitives)
  -> scenario:string
  -> Config.t
  -> unit
(** Load and execute the scenario in-browser. *)

(** {1 Lower-level Functions} *)

val run :
     config:SmartML.Config.t
  -> primitives:(module Primitives.Primitives)
  -> html:bool
  -> install:string
  -> output_dir:string option
  -> all_scenarios:Scenario.loaded_scenario list
  -> scenario:Scenario.loaded_scenario
  -> ([ `Warning | `Error ] * smart_except list) list
