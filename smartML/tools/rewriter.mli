(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics

val embellish_contract : config:Config.t -> tcontract -> tcontract
