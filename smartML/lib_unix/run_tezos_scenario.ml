(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Tools
open Utils
open Basics
open Printf
open Cmd_unix

module Printer = (val Tools.Printer.get_by_language ~config:Config.default
                        SmartML.Config.SmartPy : Tools.Printer.Printer)

let print_taction : taction -> string * line_no = function
  | New_contract {line_no} -> ("New_contract", line_no)
  | Message {line_no} -> ("Message", line_no)
  | Verify {line_no} -> ("Verify", line_no)
  | Compute {line_no} -> ("Compute", line_no)
  | Simulation {line_no} -> ("Simulation", line_no)
  | ScenarioError _ -> ("ScenarioError", [])
  | Html {line_no} -> ("Html", line_no)
  | Show {line_no} -> ("Show", line_no)
  | Set_delegate {line_no} -> ("Set_delegate", line_no)
  | DynamicContract {line_no} -> ("DynamicContract", line_no)
  | Exception _ -> ("Exception", [])
  | Add_flag {line_no} -> ("Add_flag", line_no)
  | Prepare_constant_value {line_no} -> ("Prepare_constant_value", line_no)
  | Mutation_test {line_no} -> ("Mutation_test", line_no)

exception Scenario_error of string

let fail fmt = kprintf (fun e -> raise (Scenario_error e)) fmt

type event = {
    action : Basics.taction option
  ; status : [ `Success | `Failure | `None ]
}

type connection =
  | Node of {
        address : string option
      ; port : int
      ; tls : bool
    }
  | Mockup

type client_state = {
    connection : connection
  ; mutable funder : string
  ; mutable root_dir : string
  ; confirmation : [ `Bake | `Wait of int | `Auto ]
  ; mutable originator : string option
  ; mutable originator_funded : bool
  ; mutable accounts : string list
}

let base_dir s = Filename.concat s.root_dir "base"

(* per scenario *)
type state = {
    name : string
  ; client : client_state
  ; smartml : scenario_state
  ; compute_variables : (string, string) Hashtbl.t
  ; mutable counter : int
}

let run_dir s = Filename.concat (Filename.concat s.client.root_dir "run") s.name

let command state args =
  let outpath =
    Filename.concat (run_dir state) (sprintf "command_%04d" state.counter)
  in
  state.counter <- state.counter + 1;
  let outq = Filename.quote outpath in
  sys_command "mkdir -p %s" outq;
  let cmd = try Sys.getenv "TEZOS_CLIENT" with Not_found -> "octez-client" in
  let args =
    match state.client.connection with
    | Node {address; port; tls} ->
        let base_dir = Filename.concat state.client.root_dir "client_dir" in
        sys_command "mkdir -p %s" (Filename.quote base_dir);
        (if tls then ["--tls"] else [])
        @ ["--port"; Int.to_string port]
        @ Option.cata [] (fun a -> ["--addr"; a]) address
        @ ["--base-dir"; base_dir] @ args
    | Mockup -> ["--mode"; "mockup"; "--base-dir"; base_dir state.client] @ args
  in
  let args = Base.String.concat ~sep:" " (List.map Filename.quote args) in
  let cmd = sprintf "%s %s > %s/out.txt 2> %s/err.txt" cmd args outq outq in
  Utils.Io.write_file (Filename.concat outpath "cmd.txt") cmd;
  let ret = Sys.command cmd in
  if ret <> 0 then fail "Command failed with exit status %d: %s" ret cmd;
  Utils.Io.read_file (Filename.concat outpath "out.txt")

let default_originator = "smartml-originator"

let potential_bake state =
  match state.client.confirmation with
  | `Wait _ | `Auto -> ()
  | `Bake ->
      let _ =
        command state
          ["bake"; "for"; state.client.funder; "--force"; "--minimal-timestamp"]
      in
      ()

let wait_arg state =
  match state.confirmation with
  | `Wait n -> Int.to_string n
  | `Bake | `Auto -> "none"

let account_name (acc : SmartML.Primitives.account) =
  "Acc-" ^ Base.String.prefix acc.pkh 10

let get_contract_storage state ~address =
  command state ["get"; "contract"; "storage"; "for"; address]

let run_script state ~contract ~parameter ~storage =
  command state
    [
      "run"
    ; "script"
    ; contract
    ; "on"
    ; "storage"
    ; storage
    ; "and"
    ; "input"
    ; parameter
    ; "-G"
    ; "1000000000"
    ]

module Make (Primitives : SmartML.Primitives.Primitives) = struct
  let make_account ?sk state name =
    let secret_key =
      let preclean =
        match sk with
        | Some s -> s
        | None ->
            let account = Primitives.Crypto.account_of_seed name in
            account.sk
      in
      match String.is_prefix "unencrypted:" preclean with
      | true -> preclean
      | false -> "unencrypted:" ^ preclean
    in
    let _ =
      command state ["import"; "secret"; "key"; name; secret_key; "--force"]
    in
    ()

  let rec ensure_account state (acc : SmartML.Primitives.account) ~balance =
    let name = account_name acc in
    if List.mem name state.client.accounts
    then name
    else begin
      state.client.accounts <- name :: state.client.accounts;
      make_account state ~sk:acc.sk name;
      let balance_cmd = command state ["get"; "balance"; "for"; name] in
      let current_balance =
        match
          Base.Option.(
            Base.String.lsplit2 balance_cmd ~on:' ' >>= fun (o, _) ->
            try_with (fun () -> Base.Float.(of_string o * 1_000_000. |> to_int)))
        with
        | Some o -> o
        | None -> fail "Failed to get the balance for %s" acc.pkh
      in
      if balance <= current_balance
      then account_name acc
      else (
        transfer state ~dst:name ~amount:(balance - current_balance);
        name)
    end

  and transfer ?arg ?entry_point ?(amount = 0) ?sender ?source state ~dst =
    let extras =
      Option.cata [] (fun a -> ["--arg"; a]) arg
      @ Option.cata [] (fun e -> ["--entrypoint"; e]) entry_point
    in
    let r =
      match (sender, source) with
      | None, None -> None
      | Some e, Some o when Base.Poly.equal e o -> Some e
      | Some _, Some _ ->
          fail "source and sender but different: Not Implemented"
      | Some e, None -> Some e
      | None, Some o -> Some o
    in
    let src =
      match r with
      | None -> state.client.funder
      | Some (aoa : _ SmartML.Basics.account_or_address) -> (
          SmartML.Basics.(
            match aoa with
            | Account acc ->
                ensure_account state acc ~balance:(10_000_000 + (20 * amount))
            | Address _ -> fail "source|sender = address: not implemented"))
    in
    let _ =
      command state
        ([
           "-wait"
         ; wait_arg state.client
         ; "transfer"
         ; (let tez = amount / 1_000_000 in
            let decimals = Base.(amount % 1_000_000) in
            sprintf "%d%s" tez
              (if decimals = 0 then "" else sprintf ".%06d" decimals))
         ; "from"
         ; src
         ; "to"
         ; dst
         ; "--burn-cap"
         ; "1"
         ]
        @ extras)
    in
    potential_bake state

  let init state =
    if String.is_prefix "unencrypted:" state.client.funder
    then (
      let name = "the-funding-authority" in
      make_account ~sk:state.client.funder state name;
      state.client.funder <- name);
    List.iter (fun _ -> potential_bake state) (List.init 3 Base.Fn.id)

  let fund_originator state originator =
    if not state.client.originator_funded
    then (
      state.client.originator_funded <- true;
      transfer state ~dst:originator ~amount:10_000_000_000)

  let originator state =
    let originator =
      match state.client.originator with
      | None ->
          state.client.originator <- Some default_originator;
          make_account state default_originator;
          default_originator
      | Some s -> s
    in
    fund_originator state originator

  let originate state ~id ~contract ~storage ~balance =
    originator state;
    let name =
      sprintf "c%03d-%s" id
        (Digest.to_hex
           (Digest.string
              (sprintf "%s %s %f" contract storage (Unix.gettimeofday ()))))
    in
    let burn_cap = "20" in
    let contract_path =
      Filename.concat (run_dir state) (sprintf "contract-%s.tz" name)
    in
    Utils.Io.write_file contract_path contract;
    let origination =
      command state
        [
          "--wait"
        ; wait_arg state.client
        ; "originate"
        ; "contract"
        ; name
        ; "transferring"
        ; balance
        ; "from"
        ; "bootstrap1"
        ; "running"
        ; contract_path
        ; "--init"
        ; storage
        ; "--burn-cap"
        ; burn_cap
        ]
    in
    let out = Base.String.strip origination in
    let lines = Base.String.split_lines out in
    match
      Base.List.find lines ~f:(fun s -> String.is_prefix "New contract" s)
    with
    | None -> fail "Cannot find contract"
    | Some line ->
        let addr =
          match Base.String.split line ~on:' ' with
          | "New" :: "contract" :: addr :: _ -> addr
          | _ -> fail "Cannot find contract address"
        in
        potential_bake state;
        addr

  let of_smartml ~client name smartml =
    {client; name; smartml; compute_variables = Hashtbl.create 5; counter = 0}

  let add_contract_address {smartml; _} ~id ~address =
    Hashtbl.replace smartml.addresses id address

  let get_contract state ~id =
    Option.(
      let* kt1 = Hashtbl.find_opt state.smartml.addresses id in
      let* c = Hashtbl.find_opt state.smartml.contracts id in
      return (kt1, c))

  let primitives = (module Primitives : SmartML.Primitives.Primitives)

  let update_tcontract ~config state ~id ~contract =
    let contract =
      match Hashtbl.find_opt state.smartml.contracts id with
      | Some contract -> contract
      | None ->
          Value.typecheck_instance
            (Interpreter.interpret_contract ~config ~primitives
               ~scenario_state:state.smartml contract)
    in
    Hashtbl.replace state.smartml.contracts id contract;
    contract

  let pp_event r item =
    let action =
      match item.action with
      | None -> ""
      | Some a ->
          let name, line_no = print_taction a in
          sprintf "%s,%s"
            (match line_no with
            | [] -> "____"
            | (_, s) :: _ -> sprintf "%04d" s)
            name
    in
    let result =
      match item.status with
      | `Failure -> "KO"
      | `Success -> "OK"
      | `None -> "__"
    in
    fprintf r "  %s,%s\n" result action

  let deal_with_result r ?not_valid ?action making_result =
    let result =
      try `Ok (making_result ()) with Scenario_error e -> `Error e
    in
    let ev status = {action; status} in
    match (result, not_valid) with
    | `Ok o, None ->
        pp_event r (ev `Success);
        o
    | `Ok _, Some _ ->
        pp_event r (ev `Failure);
        fail "Unexpected success"
    | `Error e, None ->
        pp_event r (ev `Failure);
        raise (Scenario_error e)
    | `Error _e, Some v ->
        pp_event r (ev `Success);
        v

  let compute_texpr_contract ~config ~primitives ~scenario_state expr =
    let tparameter_ep =
      let fields =
        Utils.List.sort compare
          (Hashtbl.fold
             (fun key {template = {tcontract = {derived}}} l ->
               let tstorage = (get_extra derived).tstorage in
               let key = Printer.string_of_contract_id key in
               (sprintf "k%s" key, tstorage) :: l)
             scenario_state.contracts [])
        @ Utils.List.sort compare
            (Hashtbl.fold
               (fun id v l ->
                 let key = sprintf "v%s" id in
                 (key, v.t) :: l)
               scenario_state.variables [])
      in
      let layout =
        Hole.Value (Type.comb_layout_of_row `Right (List.map fst fields))
      in
      Type.record_or_unit layout fields
    in
    let params = Expr.params ~line_no:[] in
    let subst =
      let contracts = Hashtbl.create 10 in
      Hashtbl.iter
        (fun key _ ->
          let getData =
            Expr.attr ~line_no:params.line_no params
              ~name:(sprintf "k%s" (Printer.string_of_contract_id key))
          in
          Hashtbl.replace contracts key getData)
        scenario_state.contracts;
      let scenario_variables = Hashtbl.create 10 in
      Hashtbl.iter
        (fun id _ ->
          let key = sprintf "v%s" id in
          let getData = Expr.attr ~line_no:params.line_no params ~name:key in
          Hashtbl.replace scenario_variables id getData)
        scenario_state.variables;
      {Replacer.contracts; scenario_variables}
    in
    let expr = Reducer.reduce_expr ~config ~primitives ~scenario_state expr in
    let expr = erase_types_expr expr in
    let expr = Replacer.replace_expr subst expr in
    let line_no = expr.line_no in
    let entryPoint =
      {
        channel = "compute"
      ; tparameter_ep = `Annotated tparameter_ep
      ; originate = true
      ; check_no_incoming_transfer = Some false
      ; lazify = None
      ; lazy_no_code = None
      ; line_no = []
      ; body =
          Command.set ~line_no:expr.line_no (Expr.storage ~line_no)
            (Expr.some ~line_no expr)
      ; derived = U
      }
    in
    let contract =
      ({
         template_id = None
       ; balance = None
       ; tstorage_explicit = None
       ; storage = Some (Expr.none ~line_no)
       ; baker = None
       ; entrypoints = [entryPoint]
       ; entrypoints_layout = None
       ; flags = []
       ; private_variables = []
       ; metadata = []
       ; views = []
       ; unknown_parts = None
       ; derived = U
       }
        : _ contract_f)
    in
    let c = {contract} in
    let c = Replacer.replace_contract subst c in
    let c = Checker.check_contract config c in
    let c = Reducer.reduce_contract ~primitives ~scenario_state c in
    Closer.close_contract c

  let compute r ~(state : state) ~expression ~config ~action continuation =
    let protocol = config.Config.protocol in
    let scenario_state = state.smartml in
    let compute_contract =
      try
        let compute_contract =
          compute_texpr_contract ~config ~primitives ~scenario_state expression
        in
        Some
          (Interpreter.interpret_contract ~config ~primitives
             ~scenario_state:state.smartml compute_contract)
      with e ->
        print_endline (Printer.exception_to_string false e);
        None
    in
    let michelson_contract =
      let scenario_vars = String.Map.of_hashtbl scenario_state.variables in
      Option.map
        (fun compute_contract ->
          Michelson.display_tcontract ~protocol
            (Compiler.compile_instance ~scenario_vars compute_contract))
        (Option.map Value.typecheck_instance compute_contract)
    in
    let storage_layout =
      match compute_contract with
      | None -> None
      | Some compute_contract -> (
          match
            Type.unF
              (get_extra compute_contract.template.tcontract.derived).tparameter
          with
          | TVariant {row = [("compute", F (TRecord {layout = Value l; _}))]} ->
              Some l
          | _ -> None)
    in
    deal_with_result r ~action (fun () ->
        let bug s = fail "BUG: %s" s in
        match michelson_contract with
        | None -> bug "no michelson contract"
        | Some michelson_contract ->
            let storages =
              let get_storage id kt1 prev_m =
                let prev = prev_m in
                let res = get_contract_storage state ~address:kt1 in
                (* This has to be the same format as in
                     smartML/lib/smartml_scenario.ml *)
                ( sprintf "k%s" (Printer.string_of_contract_id id)
                , Base.String.(tr ~target:'\n' ~replacement:' ' res |> strip) )
                :: prev
              in
              Hashtbl.fold get_storage state.smartml.addresses []
            in
            let storages_laid_out =
              match storage_layout with
              | None -> bug "storage-layout-not-set"
              | Some layout ->
                  let get_field f =
                    if Base.String.prefix f 1 = "v"
                    then
                      match
                        Hashtbl.find_opt state.compute_variables
                          (String.sub f 1 (String.length f - 1))
                      with
                      | None -> ksprintf bug "missing variable: %S" f
                      | Some s -> s
                    else
                      match
                        Base.List.Assoc.find storages ~equal:String.equal f
                      with
                      | Some s -> s
                      | None -> ksprintf bug "missing storage: %S" f
                  in
                  let rec make_pairs = function
                    | Binary_tree.Leaf Layout.{target = field} ->
                        get_field field
                    | Binary_tree.Node (l, r) ->
                        let left = make_pairs l in
                        let right = make_pairs r in
                        let paren ppf s =
                          let needs_paren =
                            match s with
                            | "" -> assert false
                            | _ ->
                                if s.[0] = '"'
                                then false
                                else String.contains s ' '
                          in
                          if needs_paren
                          then Format.fprintf ppf "(%s)" s
                          else Format.fprintf ppf "%s" s
                        in
                        Format.asprintf "Pair %a %a" paren left paren right
                  in
                  make_pairs layout
            in
            print_endline "======== run script ===========";
            let compute_result =
              run_script state ~parameter:storages_laid_out ~storage:"None"
                ~contract:michelson_contract
            in
            let pos1 = String.index compute_result '(' in
            let pos2 = String.rindex compute_result ')' in
            let computed =
              String.sub compute_result (pos1 + 6) (pos2 - pos1 - 6)
            in
            let computed =
              if computed.[0] = '('
              then
                let pos1 = String.index computed '(' in
                let pos2 = String.rindex computed ')' in
                String.sub computed (pos1 + 1) (pos2 - pos1 - 1)
              else computed
            in
            let computed =
              Base.String.(tr ~target:'\n' ~replacement:' ' computed |> strip)
            in
            ksprintf print_endline "===== Computed: %s ======" computed;
            continuation ~bug computed)

  let handle_action r ~config (state : state) _name action =
    let protocol = config.Config.protocol in
    let add_simple_history status action =
      let e = {action = Some action; status} in
      pp_event r e;
      ()
    in
    match action with
    | New_contract {id; contract; line_no; accept_unknown_types; address} -> (
        let config = (get_extra contract.derived).config in
        printf "New contract %s (l. %d)\n"
          (Printer.string_of_contract_id id)
          (head_line_no line_no);
        let contract =
          Reducer.reduce_contract ~primitives ~scenario_state:state.smartml
            {tcontract = contract}
        in
        let contract_full = update_tcontract ~config state ~id ~contract in
        let scenario_vars = String.Map.of_hashtbl state.smartml.variables in
        let compiled_contract =
          Compiler.compile_instance ~scenario_vars contract_full
        in
        let storage =
          match contract_full.state.storage with
          | None -> "missing storage"
          | Some storage ->
              let storage =
                Compiler.compile_value ~config ~scenario_vars storage
              in
              let storage =
                match compiled_contract.lazy_entry_points with
                | None -> storage
                | Some entry_points ->
                    Michelson.MLiteral.pair storage
                      (Michelson.erase_types_literal entry_points)
              in
              Michelson.string_of_literal ~protocol storage
        in
        let balance =
          sprintf "%.06f"
            (Big_int.float_of_big_int contract_full.state.balance /. 1000000.)
        in
        match
          Michelson.has_error_tcontract ~accept_missings:accept_unknown_types
            compiled_contract
        with
        | [] ->
            let address =
              deal_with_result r ~action (fun () ->
                  let static_id =
                    match id with
                    | C_static {static_id} -> static_id
                    | C_dynamic _ -> assert false
                  in
                  if address = Aux.address_of_contract_id ~html:false id None
                  then
                    originate state ~id:static_id
                      ~contract:
                        (Michelson.display_tcontract ~protocol compiled_contract)
                      ~storage ~balance
                  else address)
            in
            add_contract_address state ~id ~address;
            ()
        | errors ->
            let _ =
              deal_with_result r ~action (fun () ->
                  fail "%s" (String.concat " " errors))
            in
            ())
    | Set_delegate _ -> ()
    | Message
        {
          id
        ; valid
        ; params
        ; line_no
        ; title = _
        ; messageClass = _
        ; context = {sender; source}
        ; amount
        ; message
        } -> (
        printf "Calling message %s#%s (l. %d)\n"
          (Printer.string_of_contract_id id)
          message (head_line_no line_no);
        match get_contract state ~id with
        | None -> assert false
        | Some (kt1, smartml_contract) ->
            let scenario_state = state.smartml in
            let amount =
              let pp () =
                [`Text "Computing amount"; `Expr amount; `Line amount.line_no]
              in
              Value.unMutez ~pp
                (Interpreter.interpret_expr_external ~config ~primitives
                   ~no_env:(pp ()) ~scenario_state amount)
            in
            let valid =
              let pp () =
                [`Text "Computing valid"; `Expr valid; `Line valid.line_no]
              in
              Value.unBool ~pp
                (Interpreter.interpret_expr_external ~config ~primitives
                   ~no_env:(pp ()) ~scenario_state valid)
            in
            let parse_address = function
              | Account x -> Account x
              | Address (address : Basics.Typed.texpr) ->
                  let pp () =
                    [
                      `Text "Computing address"
                    ; `Expr address
                    ; `Line address.line_no
                    ]
                  in
                  Address
                    (Value.unAddress ~pp
                       (Interpreter.interpret_expr_external ~config ~primitives
                          ~no_env:(pp ()) ~scenario_state address))
            in
            let params =
              let tvalue =
                Interpreter.interpret_expr_external ~config ~primitives
                  ~no_env:
                    [
                      `Text "Computing params"
                    ; `Expr params
                    ; `Line params.line_no
                    ]
                  ~scenario_state params
              in
              let tvalue = Value.typecheck params.et tvalue in
              let scenario_vars =
                String.Map.of_hashtbl scenario_state.variables
              in
              let mich = Compiler.compile_value ~config ~scenario_vars tvalue in
              Michelson.string_of_literal ~protocol mich
            in
            let entry_point =
              match
                List.length smartml_contract.template.tcontract.entrypoints
              with
              | 1 -> None
              | _ -> Some message
            in
            let not_valid = if valid then None else Some () in
            deal_with_result r ?not_valid ~action (fun () ->
                transfer state
                  ?sender:(Option.map parse_address sender)
                  ?source:(Option.map parse_address source)
                  ~dst:kt1 ?entry_point
                  ~amount:(Big_int.int_of_big_int amount)
                  ~arg:params))
    | Verify {condition; line_no} ->
        printf "Verifying condition (l. %d)\n" (head_line_no line_no);
        compute r ~state ~expression:condition ~config ~action
          (fun ~bug computed -> if computed <> "True" then bug "Verify Error")
    | Compute {var; expression; line_no} ->
        printf "Compute variable %s (l. %d)\n" var (head_line_no line_no);
        let value =
          let pp () =
            [
              `Text "Computing expression"
            ; `Expr expression
            ; `Line expression.line_no
            ]
          in
          Interpreter.interpret_expr_external ~config ~primitives
            ~no_env:(pp ()) ~scenario_state:state.smartml expression
        in
        ksprintf print_endline "compute tezos";
        compute r ~state ~expression ~config ~action (fun ~bug:_ v ->
            ksprintf print_endline "compute store %s"
              (Printer.type_to_string expression.et);
            Hashtbl.replace state.compute_variables var v;
            Hashtbl.replace state.smartml.variables var
              (Value.typecheck expression.et value);
            ())
    | ScenarioError _ | Exception _ | DynamicContract _ ->
        add_simple_history `Failure action
    | Html _ | Add_flag _ -> add_simple_history `Success action
    | Simulation _ | Show _ | Prepare_constant_value _ | Mutation_test _ ->
        add_simple_history `None action

  let run_scenario client (name, sc) =
    printf "Running scenario %s...\n" name;
    let state = of_smartml name sc.Scenario.scenario_state ~client in
    sys_command "mkdir -p %s" (run_dir state);
    init state;
    printf "Client initialized.\n";
    try
      Utils.Io.with_out
        (Filename.concat client.root_dir (sprintf "%s.tsv" name))
        (fun r ->
          fprintf r "%s\n" name;
          List.iter
            (fun (config, action) -> handle_action r ~config state name action)
            sc.scenario.actions)
    with Scenario_error err -> printf "ERROR: %s\n%!" err

  let run ~(config : Config.t) ~funder ~confirmation ~connection ~root_dir
      ~scenarios =
    let root_dir = Unix.realpath root_dir in
    let client_state =
      {
        connection
      ; funder
      ; root_dir
      ; confirmation
      ; originator = None
      ; originator_funded = false
      ; accounts = []
      }
    in
    (match connection with
    | Mockup ->
        let base_dir = base_dir client_state in
        let protocol =
          match config.protocol with
          | Jakarta -> "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY"
          | Kathmandu -> "PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg"
          | Lima -> "PtLimaPtLMwfNinJi9rCfDPWea8dFgTZ1MeJ9f1m2SRic6ayiwW"
        in
        sys_command "rm -rf %S" base_dir;
        sys_command "mkdir -p %S" base_dir;
        let cmd =
          try Sys.getenv "TEZOS_CLIENT" with Not_found -> "octez-client"
        in
        sys_command "%s --protocol %s --base-dir %S create mockup" cmd protocol
          base_dir
    | _ -> ());
    List.iter (run_scenario client_state) scenarios
end
