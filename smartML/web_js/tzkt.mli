(* Copyright 2019-2022 Smart Chain Arena LLC. *)

val parse_api :
     config:SmartML.Config.t
  -> string
  -> Yojson.Safe.t
  -> Explorer_interface.transaction
