
/* Copyright 2019-2022 Smart Chain Arena LLC. */

const fs = require('fs');

const args = process.argv.slice(2);

process.on('unhandledRejection', error => {
  // Print "unhandled rejections"
  console.log('unhandledRejection', JSON.stringify(error, null, 4));
});

// begin from eztz cli example

const library = {
  bs58check: require('bs58check'),
  sodium: require('libsodium-wrappers-sumo'),
};

const prefix = {
  tz1: new Uint8Array([6, 161, 159]),
  edpk: new Uint8Array([13, 15, 37, 217]),
  edsk2: new Uint8Array([13, 15, 58, 7]),
  edsk: new Uint8Array([43, 246, 78, 7]),
  edsig: new Uint8Array([9, 245, 205, 134, 18]),
};

const utility = {
  mergebuf : function(b1,b2){
    var r = new Uint8Array(b1.length+b2.length);
    r.set(b1);
    r.set(b2, b1.length);
    return r;
  },
};

global.eztz = {
  prefix : prefix,
  utility : utility,
  library : library
};

// end from eztz cli example

// sudo npm install libsodium-wrappers xmlhttprequest
(async() => {
  global.smartpyContext = {};
  await library.sodium.ready;
  await library.bs58check.ready;
  global.sodium = library.sodium;
  global.smartpyContext.Keccak256 = require('js-sha3').keccak_256;
  global.smartpyContext.Bls12 = require('tezos-bls12-381');
  global.smartpyContext.Timelock = require('@smartpy/timelock');
  await global.smartpyContext.Timelock.ensureHaclWasmLoaded();

// __INSERT_HERE__
