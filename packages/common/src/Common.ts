import Tezos from './services/Tezos';
import ContractOriginator from './services/ContractOriginator';
import ContractCaller from './services/ContractCaller';


export async function originateContract(code: string, storage: string, secret_key: string, config: Record<string, any>, amount: number = 0): Promise<string>{
    const tezos = await Tezos.getWallet(config['RPC'], secret_key);
    const contract = await ContractOriginator.originateContract(tezos, code, storage, amount.toString());
    return contract.address;
}

export async function callContract(address: string, secret_key: string, config: Record<string, any>, parameter: object, entrypoint = '', amount: number = 0): Promise<string> {
    const tezos = await Tezos.getWallet(config['RPC'], secret_key);
    const op = await ContractCaller.invokeContractWithParameter(tezos, address, parameter, entrypoint, amount);
    return op.hash
}

export default {
    originateContract,
    callContract,
};