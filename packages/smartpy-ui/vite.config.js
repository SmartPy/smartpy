import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
    plugins: [vue()],
    build: {
        lib: {
            entry: resolve(__dirname, 'src/lib.js'),
            formats: ['es'],
        },
        rollupOptions: { external: ['vue'] } // don't bundle vue
    }
})
