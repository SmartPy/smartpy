import { ShallowRef } from 'vue'
import { EditorView, ViewUpdate } from '@codemirror/view'
import { openSearchPanel, closeSearchPanel, searchPanelOpen } from '@codemirror/search'
import { redo, undo } from '@codemirror/commands'

export function handleStateUpdate (
    state: any,
    cmView: ShallowRef<EditorView | undefined>,
    viewUpdate: ViewUpdate,
    emit: (event: string, ...args: any[]) => void
    ) {
    // selected
    const ranges = viewUpdate.state.selection.ranges
    state.selected = ranges.reduce((plus, range) => plus + range.to - range.from, 0)
    state.cursor = ranges[0].anchor
    // length
    state.length = viewUpdate.state.doc.length
    state.lines = viewUpdate.state.doc.lines

    if (cmView?.value) {
        state.searchPanelOpen = searchPanelOpen(cmView.value!.state)
    }
    if (viewUpdate.docChanged) {
        emit('onChange', { value: viewUpdate.state.doc.toString() });
    }
}

// https://github.com/codemirror/commands/blob/main/test/test-history.ts
export function handleUndo(cmView: ShallowRef<EditorView | undefined>) {
    undo({
        state: cmView.value!.state,
        dispatch: cmView.value!.dispatch
    })
}

export function handleRedo(cmView: ShallowRef<EditorView | undefined>) {
    redo({
        state: cmView.value!.state,
        dispatch: cmView.value!.dispatch
    })
}

export function handleOpenSearchPanel(cmView: ShallowRef<EditorView | undefined>) {
    openSearchPanel({
        state: cmView.value!.state,
        dispatch: cmView.value!.dispatch
    } as EditorView);
}

export function handleCloseSearchPanel(cmView: ShallowRef<EditorView | undefined>) {
    closeSearchPanel(cmView.value!);
}