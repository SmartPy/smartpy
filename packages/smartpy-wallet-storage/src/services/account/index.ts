import ImplicitAccountError, { ImplicitAccountErrorCodes } from './exceptions/ImplicitAccountError';
import { generateID } from '../../utils/rand';
import encUTF8 from 'crypto-js/enc-utf8';
import AES from 'crypto-js/aes';

export interface TezosAccount {
    id: string;
    name: string;
    accountType: any;
    address: string;
    publicKey: string;
    derivationPath?: string;
    privateKey?: string;
    encryptedContent?: string;
    preferredRpc?: string;
};

export default class AccountService {
    private accounts: {
        [id: string]: TezosAccount
    };
    public LOCAL_STORAGE_KEY = 'wallet2';

    constructor() {
        const localStorage = window.localStorage.getItem(this.LOCAL_STORAGE_KEY);
        if (!localStorage) {
            this.accounts = {};
            this.persistLocalStorage();
        } else {
            this.accounts = JSON.parse(localStorage);
        }
    }

    /**
     *  Find all implicit accounts.
     *  @returns {{ [address: string]: TezosAccount }}
     */
    public findAll = (accountTypes?: string[], searchTerm?: string): { [address: string]: TezosAccount } => {
        let accounts = JSON.parse(JSON.stringify(this.accounts));

        // Validate accountTypes parameter
        if (accountTypes) {
            if (!Array.isArray(accountTypes)) {
                throw new ImplicitAccountError(ImplicitAccountErrorCodes.FIND_ALL_INVALID_ACCOUNT_TYPES_PARAMETER);
            }
            // Apply account type filter
            accounts = Object.keys(accounts).reduce<{ [address: string]: TezosAccount }>((prev, cur) => {
                if (accountTypes.includes(accounts[cur].accountType)) {
                    prev[cur] = accounts[cur];
                }
                return prev;
            }, {});
        }

        // Apply account (name U address) filter if provided
        if (searchTerm) {
            accounts = Object.keys(accounts).reduce<{ [address: string]: TezosAccount }>((prev, cur) => {
                if (
                    accounts[cur].name.toLowerCase().startsWith(searchTerm.toLowerCase()) ||
                    accounts[cur].address.toLowerCase().startsWith(searchTerm.toLowerCase())
                ) {
                    prev[cur] = accounts[cur];
                }
                return prev;
            }, {});
        }
        return accounts;
    };
    /**
     *  Find implicit account by identifier.
     *  @param {string} id account identifier.
     *  @returns {TezosAccount}
     */
    public findById = (id: string): TezosAccount => {
        throwIfAccountNotFound(this.accounts, id);
        return this.accounts[id];
    };
    /**
     *  Add an implicit account.
     *  @param {TezosAccount} account tezos implicit account.
     *  @param {string?} password an optional password encoded with btoa to encrypt the account.
     */
    public addAccount = (account: TezosAccount, password?: string): void => {
        validateAccountInput(account);
        account.id = generateID();
        if (password) {
            account = this.getEncryptedAccount(account, password);
        }
        // Save mutation
        this.accounts = {
            ...this.accounts,
            [account.id]: account,
        };
        this.persistLocalStorage();
    };
    /**
     *  Update a implicit account's name.
     *  @param {string} id account's identifier.
     *  @param {string} name account's new name.
     */
    public updateName = (id: string, name: string): void => {
        // TezosAccount must exist
        throwIfAccountNotFound(this.accounts, id);
        // Save mutation
        this.accounts[id].name = name;
        this.persistLocalStorage();
    };
    /**
     *  Update a implicit account's name.
     *  @param {string} id account's identifier.
     *  @param {string} preferredRpc account's preferred rpc.
     */
    public updatePreferredRpc = (id: string, preferredRpc: string): void => {
        // TezosAccount must exist
        throwIfAccountNotFound(this.accounts, id);
        // Save mutation
        this.accounts[id].preferredRpc = preferredRpc;
        this.persistLocalStorage();
    };
    /**
     *  Remove a implicit account.
     *  @param {string} id Account identifier.
     */
    public removeById = (id: string): void => {
        // TezosAccount must exist
        throwIfAccountNotFound(this.accounts, id);
        // Delete account and persist
        delete this.accounts[id];
        this.persistLocalStorage();
    };
    /**
     * Return the decrypted account.
     *  @param {TezosAccount} account tezos implicit account.
     *  @param {string?} password an optional password encoded with btoa to decrypt the account.
     *  @returns {TezosAccount}
     */
    public getDecryptedAccount = (account: TezosAccount, password: string): TezosAccount => {
        if (!account.encryptedContent) {
            return account;
        }
        if (!password) {
            throw new ImplicitAccountError(ImplicitAccountErrorCodes.CANNOT_DECRYPT_WITHOUT_PASSWORD);
        }
        let unEncrypted = JSON.parse(JSON.stringify(account));
        try {
            const { derivationPath, privateKey } = JSON.parse(
                AES.decrypt(account.encryptedContent, atob(password)).toString(encUTF8),
            );
            unEncrypted = {
                ...unEncrypted,
                derivationPath,
                privateKey,
                encryptedContent: undefined
            }
        } catch (e) {
            throw new ImplicitAccountError(ImplicitAccountErrorCodes.INCORRECT_PASSWORD);
        }
        return unEncrypted;
    }
    /**
     *  Clear the storage.
     */
    public clear = (): void => {
        window.localStorage.removeItem(this.LOCAL_STORAGE_KEY);
        this.accounts = {};
        this.persistLocalStorage();
    }
    /**
     * Persist to local storage
     */
    private persistLocalStorage = (): void => {
        window.localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this.accounts));
    }
    /**
     * Encrypt an account content or return the encrypted content
     */
    private getEncryptedAccount = (account: TezosAccount, password: string): TezosAccount => {
        if (!password && !account.encryptedContent) {
            throw new ImplicitAccountError(ImplicitAccountErrorCodes.CANNOT_ENCRYPT_WITHOUT_PASSWORD);
        }
        const { id, name, accountType, address, publicKey, preferredRpc } = account;
        const encryptedContent = !password
            ? account.encryptedContent
            : AES.encrypt(
                JSON.stringify({
                    derivationPath: account.derivationPath,
                    privateKey: account.privateKey,
                }),
                atob(password),
            ).toString();
        return { id, name, accountType, address, publicKey, encryptedContent: encryptedContent };
    }
    /**
     * Reload the whole accounts from local storage.
     */
    public reloadFromStorage = (): void => {
        const localStorage = window.localStorage.getItem(this.LOCAL_STORAGE_KEY);
        if (!localStorage) {
            this.accounts = {};
        } else {
            this.accounts = JSON.parse(localStorage);
        }
    }
}

/**
 *  Validate account input.
 *  @param {TezosAccount} account tezos implicit account.
 */
const validateAccountInput = (account: TezosAccount) => {
    // TezosAccount must not be null
    if (!account) {
        throw new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_NULL);
    }
    if (!account.address) {
        throw new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_ADDRESS_NULL);
    }
};

/**
 *  Throw if the account doesn't exist.
 *  @param {AccountsStorageFormat} storage storage state.
 *  @throws {ImplicitAccountError} if the account doesn't exist.
 */
const throwIfAccountNotFound = (storage: { [id: string]: TezosAccount }, id: string) => {
    if (!storage[id]) {
        throw new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_NOT_FOUND);
    }
};
