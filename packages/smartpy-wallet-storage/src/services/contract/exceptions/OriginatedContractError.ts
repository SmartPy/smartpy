export default class OriginatedContractError extends Error {
    constructor(message: OriginatedContractErrorCodes) {
        super(message); // 'Error' breaks prototype chain here

        Object.setPrototypeOf(this, new.target.prototype); // Restore prototype chain
        this.name = 'OriginatedContractError';
    }
}

export enum OriginatedContractErrorCodes {
    CONTRACT_ALREADY_EXISTS = 'originated_contract.already_exists',
    CONTRACT_NOT_FOUND = 'originated_contract.not_found',
    CONTRACT_NULL = 'originated_contract.must_not_be_null',
    CONTRACT_ADDRESS_NULL = 'originated_contract.fields.address.null',
}
