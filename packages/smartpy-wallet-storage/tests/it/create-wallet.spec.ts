import { Builder, By, Capabilities, WebDriver } from 'selenium-webdriver';

const itIf = (name: string, test: () => Promise<void>) =>
    it(name, async () => process.env.RUN_IT_TESTS === 'true' && test());

jest.setTimeout(60000); // Set timeout of 1 minute;

describe('Integration Tests', () => {
    let driver: WebDriver;

    beforeAll(async () => {
        if (process.env.RUN_IT_TESTS === 'true') {
            driver = await new Builder()
                .forBrowser('chrome')
                .withCapabilities(Capabilities.chrome().set('acceptInsecureCerts', true))
                .build();
        }
    });

    // Create Wallet
    itIf('Create Wallet', async function () {
        // Load the page
        await driver.get('https://localhost:9000/demo/wallet.html');
        // Insert password
        await driver.findElement(By.id('cw-password')).sendKeys('123456');
        // Insert password confirmation
        await driver.findElement(By.id('cw-confirm')).sendKeys('123456');
        // Click create wallet button
        await driver.findElement(By.id('btn-create-wallet')).click();
        // Expects that wallet-accounts div is displayed in the browser DOM.
        expect(await driver.findElement(By.id('wallet-accounts')).isDisplayed()).toBeTruthy();
    });

    // Close the browser after running the tests.
    afterAll(() => process.env.RUN_IT_TESTS === 'true' && driver && driver.quit());
});
