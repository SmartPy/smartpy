export default class OriginatedContractError extends Error {
    constructor(message: OriginatedContractErrorCodes);
}
export declare enum OriginatedContractErrorCodes {
    CONTRACT_ALREADY_EXISTS = "originated_contract.already_exists",
    CONTRACT_NOT_FOUND = "originated_contract.not_found",
    CONTRACT_NULL = "originated_contract.must_not_be_null",
    CONTRACT_ADDRESS_NULL = "originated_contract.fields.address.null"
}
