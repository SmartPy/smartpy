import { OriginatedContract } from './storage';
declare const ContractService: {
    /**
     *  Find all contracts.
     *  @param {boolean} deployedOnly answer with originated contracts only.
     *  @param {string} network
     *  @param {string} contractNameTerm search term
     *  @returns {{ [address: string]: OriginatedContract }}
     */
    findAll: (network?: string, deployedOnly?: boolean, searchTerm?: string) => {
        [address: string]: OriginatedContract;
    };
    /**
     *  Find originated contract by identifier.
     *  @param {string} id contract identifier
     *  @returns {OriginatedContract}
     */
    findById: (id: string) => OriginatedContract;
    /**
     *  Find originated contract by address.
     *  @param {string} address contract address.
     *  @returns {OriginatedContract}
     */
    findByAddress: (address: string) => OriginatedContract;
    /**
     *  Persist the originated contract.
     *  @param {OriginatedContract} contract originated contract.
     */
    persist: (contract: OriginatedContract) => void;
    /**
     *  Update a originated contract.
     *  @param {OriginatedContract} contract originated contract.
     */
    merge: (contract: OriginatedContract) => void;
    /**
     *  Remove a originated contract.
     *  @param {string} id contract identifier
     */
    removeById: (id: string) => void;
    /**
     *  Select originated contract.
     *  @param {string} id contract identifier
     */
    select: (id: string) => void;
    /**
     *  Get selected contract.
     *  @returns {OriginatedContract}
     */
    getSelected: () => OriginatedContract | undefined;
    /**
     *  Clear the storage.
     */
    clear: () => void;
};
export default ContractService;
