export interface ContractsStorageFormat {
    selectedContract?: OriginatedContract;
    contracts: {
        [id: string]: OriginatedContract;
    };
}
export interface OriginatedContract {
    id?: string;
    name: string;
    address?: string;
    network?: string;
    storage?: string;
    code?: string;
}
export default class ContractsStorage {
    CONTRACTS_STORAGE_KEY: string;
    private contractsStorage;
    constructor();
    /**
     *  Get Contracts Storage.
     *  @returns {ContractsStorageFormat}
     */
    get: () => ContractsStorageFormat;
    /**
     *  Persist new storage.
     *  @param {ContractsStorageFormat} storage new storage.
     */
    persist: (storage: ContractsStorageFormat) => void;
    /**
     *  Clear storage.
     */
    clear: () => void;
}
