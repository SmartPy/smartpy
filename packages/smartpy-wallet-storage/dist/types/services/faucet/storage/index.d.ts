export interface FaucetStorageFormat {
    selectedFaucet?: FaucetAccount;
    faucet: {
        [id: string]: FaucetAccount;
    };
}
export interface FaucetAccount {
    id?: string;
    name: string;
    address: string;
    publicKey: string;
    privateKey: string;
    secret: string;
}
export default class FaucetStorage {
    FAUCET_STORAGE_KEY: string;
    private faucetStorage;
    constructor();
    /**
     *  Get Faucet Storage.
     *  @returns {FaucetStorageFormat}
     */
    get: () => FaucetStorageFormat;
    /**
     *  Persist new storage.
     *  @param {FaucetStorageFormat} storage new storage.
     */
    persist: (storage: FaucetStorageFormat) => void;
    /**
     *  Clear storage.
     */
    clear: () => void;
}
