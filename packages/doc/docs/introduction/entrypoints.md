# Entrypoints

## Declaration

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

An entrypoint is a method of a contract class that can be called from the outside. entrypoints need to be marked with the `@sp.entry_point` decorator.

For example, the following entrypoint checks that the argument given is larger than 2:

```python
@sp.entry_point
def check_big_enough(self, params):
    sp.verify(params > 2)
```

See reference [Store Value](https://smartpy.io/ide?template=storeValue.py) template for more entrypoints examples.

`@sp.entry_point` can also take optional parameters such as an
alternative string `name`, bool `private`, `lazify`, `lazy_no_code`,
`parameter_type`, and `check_no_incoming_transfer` flags.

| Parameter                  | Type   | Description                                                                                                                               |
|----------------------------|--------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name                       | string | Defines the entrypoint name (**By default, the function name is used as name**)                                                           |
| private                    | bool   | Defines a entrypoint that is only available in tests                                                                                      |
| lazify                     | bool   | Marks the entrypoint as lazy (see [lazy entrypoints](#lazy-entrypoints)).                                                                 |
| lazy_no_code               | bool   | Doesn't originate any implementation of the entrypoint (see [lazy entrypoints](#entrypoints-without-implementation)).                                       |
| check_no_incoming_transfer | bool   | Verifies that `sp.amount` is `0` and fails with `sp.amount` otherwise.                                                                    |
| parameter_type             | [type] | The entry point input type (**By default, the type input gets inferred**)                                                                 |


```python
@sp.entry_point(name = "another_name", private = True, lazify = False, lazy_no_code = False)
def check_big_enough(self, params):
    sp.verify(params > 2)
```

The `lazify` option is `False` by default.  Its default can be changed
to `True` by specifying the `"lazy-entry-points"` flag.  For an
example of this see the [Send
back](https://smartpy.io/ide?template=send_back.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

An entrypoint is a method of a contract class that can be called from the outside. entrypoints need to be marked with the `@EntryPoint` decorator.

For example, the following entrypoint checks that the argument given is larger than 2:

```typescript
@EntryPoint
check_big_enough(param: TNat) {
    Sp.verify(param > 2)
}
```

See reference [Store Value](https://smartpy.io/ts-ide?template=StoreValue.ts) template for more entrypoints examples.

`@EntryPoint<parameter_type>()` can also take optional parameters such as an alternative
string `name`, bool `mock`, `lazify`, `lazy_no_code`, and `check_no_incoming_transfer` flags.

|Parameter     | Type   | Description                                                                          |
|--------------|--------|--------------------------------------------------------------------------------------|
| name         | string | Defines the entrypoint name (**By default, the function name is used as name**)     |
| mock         | bool   | Defines a entrypoint that is only available in tests                                |
| lazify       | bool   | Sets the entrypoint as upgradable (**The logic code gets stored in a big_map**)     |
| lazy_no_code | bool   | Sets the entrypoint as upgradable (**Doesn't store any code logic at origination**) |
| check_no_incoming_transfer | bool   | Verifies that `Sp.amount` is `0` and fails with `Sp.amount` otherwise. |
| parameter_type  | [type] | The entry point input type (**By default, the type input gets inferred**)            |


```typescript
@EntryPoint({
    name: "another_name",
    mock: true,
    lazify: false,
    lazy_no_code: false
})
check_big_enough(param: TNat) {
    Sp.verify(param > 2)
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Dummy entrypoints

<Snippet syntax={SYNTAX.PY}>

It is possible to create entrypoints for testing that only appear in SmartPy but are not included in the Michelson contract.

They help to implement checks in SmartPy tests after the contract origination. One can use the entire SmartPy machinery such as `sp.verify`, etc.

See reference [Private Entrypoint](https://smartpy.io/ide?template=private_entry_point.py) template.

This is also useful to build custom UI through simulation by doing:

```python
@sp.entry_point(private = True)
def set_y(self, params):
    self.data.y = param
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is possible to create entrypoints for testing that only appear in SmartPy but are not included in the Michelson contract.

They help to implement checks in SmartPy tests after the contract origination. One can use the entire SmartPy machinery such as `Sp.verify`, etc.

This is also useful to build custom UI through simulation by doing:

```typescript
@EntryPoint({ mock: true })
set_y(param: TNat) {
    this.storage.y = param
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Return values

<Snippet syntax={SYNTAX.PY}>

Entrypoints do not return values in Michelson.

SmartPy respects this constraint but allows other functions to return values.
These functions typically use `sp.result(value)` to return values.

See [Lambdas](/types/lambdas) for examples on how to use `@sp.private_lambda()`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Entrypoints do not return values in Michelson.

SmartTS respects this constraint but allows other functions to return values.

See [Lambdas](/types/lambdas) for examples on how to use `@GlobalLambda`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Lazy entrypoints

<Snippet syntax={SYNTAX.PY}>

An entrypoint marked as _lazy_ (see [`sp.entry_point`](#declaration))
has two characteristics:

- Its code is not loaded when other entrypoints are invoked. This reduces gas usage.
- It can be updated after the contract is originated.

</Snippet>

<Snippet syntax={SYNTAX.TS}>Work in progress...</Snippet>
<Snippet syntax={SYNTAX.ML}>Work in progress...</Snippet>

### The entrypoint map

<Snippet syntax={SYNTAX.PY}>

A contract with at least one lazy entrypoint has an _entrypoint map_,
accessible as `sp.entrypoint_map()`. It is of type
`sp.TBigMap(sp.TNat, sp.TLambda(...))`.  For each lazy entrypoint it
contains a lambda that is called upon invocation. The entrypoint map
is part of the contract's storage, but not visible within `self.data`.

For efficiency reasons the keys of the entrypoint map (its _ids_) are
integers of type `sp.TNat`. The id for entrypoint `ep` can be obtained
as `sp.entrypoint_id("ep")`.

Using these expressions, the entrypoint map can be arbitrarily
manipulated. For example, an entrypoint can be updated with the following
command:

```python
sp.entrypoint_map()[sp.entrypoint_id("ep")] = ...
```

As a convenience, this can be abbreviated to:
```python
sp.set_entrypoint("ep") = ...
```

We can check whether the entrypoint map currently has an
entry for `ep` with the following expression:

```python
sp.entrypoint_map().contains(sp.entrypoint_id("ep"))
```

Again, this can be abbreviated as:
```python
sp.has_entrypoint("ep")
```

Note that the entrypoint map is accessible only from inside non-lazy
entrypoints.

</Snippet>

<Snippet syntax={SYNTAX.TS}>Work in progress...</Snippet>
<Snippet syntax={SYNTAX.ML}>Work in progress...</Snippet>

### Entrypoints without implementation

<Snippet syntax={SYNTAX.PY}>

Initially the big map contains lambdas for all
entrypoints that are marked as `lazy`, except for those marked as
`lazy_no_code`. For example calling an entrypoint decorated with

```python
@sp.entry_point(lazify = True, lazy_no_code = True)
```

will result in an error until it has been set in the entrypoint map.

</Snippet>

<Snippet syntax={SYNTAX.TS}>Work in progress...</Snippet>
<Snippet syntax={SYNTAX.ML}>Work in progress...</Snippet>

### In scenarios

<Snippet syntax={SYNTAX.PY}>

From outside the contract (in a scenario) the entrypoint map is
accessible as `sp.contract_entrypoint_map(c)`, where `c` is the
contract. The entrypoint ids are accessible as
`sp.contract_entrypoint_id(c, "ep")`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>Work in progress...</Snippet>
<Snippet syntax={SYNTAX.ML}>Work in progress...</Snippet>

### Example

<Snippet syntax={SYNTAX.PY}>

Suppose we have a lazy entrypoint `ep`. In the same contract we can
define a non-lazy entrypoint that allows the administrator to update
it:

```python
@sp.entry_point
def update_ep(self, new_code):
    sp.verify(sp.sender == self.data.admin)
    sp.set_entry_point("ep", new_code)
```

In order to perform an update in a scenario, we first define the new
entrypoint as a function:

```python
def f(self, params):
    sp.verify(params == 42)
```

We then wrap the Python function `f` so that it can be passed to
`update_ep`.

```python
new_ep = sp.utils.wrap_entry_point("ep", entry_point)
c.update_ep(new_ep)
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@Entry_point({ lazify: true })
```

Furthermore, a lazy entrypoint can be initially excluded from the big map:

```typescript
@Entry_point({ lazify: true, lazy_no_code: true })
```
In this case, calling it will result in an error until it has been updated.

</Snippet>

<Snippet syntax={SYNTAX.ML}>Work in progress...</Snippet>

## Calling entrypoints

### Entrypoints without parameters

All entrypoints require an input type, this even applies when no parameters are provided.

The type used in this situations is `TUnit`.

<Snippet syntax={SYNTAX.PY}>

```python
class MyContract(sp.Contract):
    @sp.entry_point
    def ep1():
        contract = sp.contract(sp.TUnit, sp.self_address, "ep2").open_some()
        sp.transfer(sp.unit, sp.tez(0), contract)

    @sp.entry_point
    def ep2():
        pass
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
class MyContract {
    @EntryPoint
    ep1() {
        const contract: TUnit = Sp.contract(Sp.selfAddress, "ep2")
        Sp.transfer(Sp.unit, 0, contract)
    }

    @EntryPoint
    ep2() {
        // ...
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Define a default entrypoint

Default entrypoints are useful to implement the same transfer behavior as `implicit accounts`.

```
(unit %default)
```

<Snippet syntax={SYNTAX.PY}>

```python
class MyContract(sp.Contract):
    @sp.entry_point
    def default():
        pass
```

**Or as an `@sp.entry_point` argument**

```python
class MyContract(sp.Contract):
    @sp.entry_point(name = "default")
    def ep():
        pass
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
class MyContract {
    @EntryPoint
    default() {
        #
    }
}
```

**Or as an `@EntryPoint` argument**

```typescript
class MyContract {
    @EntryPoint({ name: "default" })
    ep() {
        #
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
