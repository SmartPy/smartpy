# Constants vs Expressions

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

It is usually quite natural when an expression needs to be computed on-chain versus a readily available constant.

By default, SmartPy fully computes _at compile time_ expressions that are put in the storage.

However, this behavior is not always desired or even possible.

Some specific expressions (such as `sp.make_signature`, see [Signatures](/types/signatures)) need to be computed at compile time since there is no corresponding construction in Michelson. This is done automatically for mandatory cases.

This computation can be forced by using `sp.resolve(..)`.

```python
sp.resolve(e)
```

Forces computation of `e` at compile time.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

In progress...

</Snippet>

<Snippet syntax={SYNTAX.ML}>

In progress...

</Snippet>
