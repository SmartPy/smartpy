# Never

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

This notion is both quite advanced and almost useless.

<Snippet syntax={SYNTAX.PY}>

**`sp.never(expression)`** <br />
Values of type [sp.TNever](/general/types#never) cannot occur in a well-typed program. However, they can be abstracted in the parameter declaration of a contract—or by using the [Lambda](/types/lambdas) operation—thus indicating that the corresponding branches in the code are forbidden.

The corresponding type in Michelson is <MichelsonDocLink placeholder="never" url="https://tezos.gitlab.io/michelson-reference/#type-never"/>.

Close a forbidden branch, where expression is of type [sp.TNever](/general/types#never).

#### Example

```python
sp.never(expression)
```

See reference [Never](https://smartpy.io/ide?template=test_never.py) template.


The corresponding instruction in Michelson is <MichelsonDocLink placeholder="NEVER" url="https://tezos.gitlab.io/michelson-reference/#instr-NEVER"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.never(expression)`** <br />
Values of type [TNever](/general/types#never) cannot occur in a well-typed program. However, they can be abstracted in the parameter declaration of a contract—or by using the [Lambda](/types/lambdas) operation—thus indicating that the corresponding branches in the code are forbidden.

The corresponding type in Michelson is <MichelsonDocLink placeholder="never" url="https://tezos.gitlab.io/michelson-reference/#type-never"/>.

Close a forbidden branch, where expression is of type [sp.TNever](/general/types#never).

#### Example

```typescript
Sp.never(expression)
```

The corresponding instruction in Michelson is <MichelsonDocLink placeholder="NEVER" url="https://tezos.gitlab.io/michelson-reference/#instr-NEVER"/>.

</Snippet>
