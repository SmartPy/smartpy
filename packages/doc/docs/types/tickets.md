# Tickets

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Tickets of content type `t` have type [sp.TTicket](/general/types#ticket)(`t`).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="ticket" url="https://tezos.gitlab.io/michelson-reference/#type-ticket"/>

See reference [Tickets](https://smartpy.io/ide?template=test_ticket.py) template

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Tickets of content type `t` have type [TTicket](/general/types#ticket)(`t`).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="ticket" url="https://tezos.gitlab.io/michelson-reference/#type-ticket"/>

</Snippet>

## Create a ticket

<Snippet syntax={SYNTAX.PY}>

**`sp.ticket(content, amount)`** <br />
Create a ticket with `content` and `amount`. If `content` is of type `t`, the return type is [sp.TTicket](/general/types#ticket)(`t`). The `amount` has to be of type [sp.TNat](/general/types#nat).

#### Example

```python
ticket = sp.ticket("Can be a string", sp.nat(1))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="TICKET" url="https://tezos.gitlab.io/michelson-reference/#instr-TICKET"/>

## High Level Functions

### Read ticket

<Snippet syntax={SYNTAX.PY}>

**`sp.read_ticket(ticket)`** <br />
Read the data of a `ticket` and return a Python object with four fields `ticketer`, `content`, `amount`, and `copy`.

#### Example

```python
read_ticket = sp.read_ticket(ticket)
```

The output `read_ticket` object is *not* a SmartPy expression and cannot be passed as an expression to regular SmartPy functions. However, it can be passed to regular Python functions thanks to meta-programming.

:::caution
It is an error to access `ticket` again after this command, and `read_ticket.copy` must be used instead.
::::

If `ticket` is of type [sp.TTicket](/general/types#ticket)(`t`), then `read_ticket.ticketer` is of type [sp.TAddress](/general/types#address), `read_ticket.content` of type `t`, and `read_ticket.amount` of type [sp.TNat](/general/types#nat).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="READ_TICKET" url="https://tezos.gitlab.io/michelson-reference/#instr-READ_TICKET"/>

### Split ticket

<Snippet syntax={SYNTAX.PY}>

**`sp.split_ticket(ticket, q1, q2)`** <br />
Split `ticket` into two tickets of amounts `q1` and `q2`. These two amounts must sum up to ``ticket``'s value, otherwise `sp.none` is returned.

#### Example

```python
sp.split_ticket(ticket, q1, q2)
```

If `ticket` is of type [sp.TTicket](/general/types#ticket)(`t`), this returns a value of type [sp.TOption](/general/types#option)([sp.TPair](/general/types#pair)([sp.TTicket](/general/types#ticket)(`t`), [sp.TTicket](/general/types#ticket)(`t`))).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="SPLIT_TICKET" url="https://tezos.gitlab.io/michelson-reference/#instr-SPLIT_TICKET"/>


### Join tickets

<Snippet syntax={SYNTAX.PY}>

**`sp.join_tickets(ticket1, ticket2)`** <br />
Return a new ticket with the sum of the amounts in `ticket1` and `ticket2`.  The two tickets must have the same contents, or an error is raised.

#### Example

```python
sp.join_tickets(ticket1, ticket2)
```

Both tickets must have the same type [sp.TTicket](/general/types#ticket)(`t`), which is also the return type.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="JOIN_TICKETS" url="https://tezos.gitlab.io/michelson-reference/#instr-JOIN_TICKETS"/>


## Low Level Functions

### Read ticket

<Snippet syntax={SYNTAX.PY}>

**`sp.read_ticket_raw(ticket)`** <br />
Like `sp.read_ticket(ticket)` when the output is two elements.

#### Example

```python
sp.read_ticket_raw(ticket)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="READ_TICKET" url="https://tezos.gitlab.io/michelson-reference/#instr-READ_TICKET"/>

### Split ticket

<Snippet syntax={SYNTAX.PY}>

**`sp.split_ticket_raw(ticket, q1, q2)`** <br />
Like `sp.split_ticket(ticket, q1, q2)` when the output is two elements.

#### Example

```python
sp.split_ticket_raw(ticket, q1, q2)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="SPLIT_TICKET" url="https://tezos.gitlab.io/michelson-reference/#instr-SPLIT_TICKET"/>


### Join tickets

<Snippet syntax={SYNTAX.PY}>

**`sp.join_tickets_raw(tickets)`** <br />
Like `sp.join_tickets(ticket1, ticket2)` where `tickets` is a pair of tickets.

#### Example

```python
sp.join_tickets_raw(tickets)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="JOIN_TICKETS" url="https://tezos.gitlab.io/michelson-reference/#instr-JOIN_TICKETS"/>

## Testing Tickets

<Snippet syntax={SYNTAX.PY}>

**`sp.test_ticket(ticketer, content, amount)`** <br />
Create a new ticket issued by the locally defined contract `ticketer`. This is meant to be used for testing purposes in scenarios.

#### Example

```python
ticket = sp.test_ticket(ticketer, content, amount)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>
