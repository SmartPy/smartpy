# Lambdas

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of functions in SmartPy is
[sp.TLambda](/general/types#lambda)(`t1`, `t2`, `with_storage=None`,
`with_operations=False`) where `t1` is the parameter type and `t2` the
result type; `with_storage` and `with_operations` are the lambda context parameters.

By default, lambdas have no effect or context (besides possibly
failure) but it is often useful to introduce them.

## Contexts

The two possible context parameters are `with_storage` and `with_operations`.
- `with_storage="read-only"` or `with_storage="read-write"`, for functions that read or read-write the storage.
- `with_operations=True`, for functions that create operations.

Intuitively, a lambda with a non-trivial context (`with_storage` and/or
`with_operations`) is a regular lambda with:
- an augmented type describing contextual use of storage and/or operations;
- some code around its input (for storage) and/or output (for storage and/or operations) to take care of reading
and writing needs.

The corresponding type in Michelson is <MichelsonDocLink placeholder="lambda" url="https://tezos.gitlab.io/michelson-reference/#type-lambda"/>.

See reference [Lambdas](https://smartpy.io/ide?template=lambdas.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of functions in SmartTS is [TLambda](/general/types#lambda)<`t1`, `t2`> where `t1` is the parameter type and `t2` the result type.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="lambda" url="https://tezos.gitlab.io/michelson-reference/#type-lambda"/>.

See reference [Lambdas](https://smartpy.io/ts-ide?template=Lambdas.ts) template.

</Snippet>

## Operations

### Build a lambda

<Snippet syntax={SYNTAX.PY}>

**`sp.build_lambda(l, with_storage=None, with_operations=False, recursive=False)`** <br />
Build a SmartPy lambda from a Python function or lambda.

- `with_storage="read-only"` or `with_storage="read-write"`, for functions that read or read-write the storage.
- `with_operations=True`, for functions that create operations.

- `recursive=True`, for recursive lambdas. A second parameter
  specifies the function name used for recursive calls, e.g.:
  `sp.build_lambda((lambda x, f: ...f(x-1)....),
  recursive=True)`. Recursive lambdas cannot have storage or
  operations effects.

For example, `sp.build_lambda(lambda x: x + 3)` represents a function that takes an argument `x` and returns `x + 3`.<br/>
This function is usually useless as it is called automatically by SmartPy in most contexts.

#### Example

```python
# Explicit call
logic = sp.build_lambda(lambda x : x + 3)
x = logic(3); # A SmartPy expression that evaluates to 6

# sp.build_lambda is called automatically
# because it needs to be converted to a SmartPy expression
self.data.f = lambda x : x + 3

# This is regular Python
logic = lambda x : x + 3
x = logic(3); # 6

# Recursive factorial function
sp.build_lambda((lambda n, fact: sp.eif(n<=1, 1, n*fact(n-1))), recursive=True)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`() => some logic`** <br />
Build a SmartTS lambda from a Typescript function.

For example, `(x: TNat) => x + 3` represents a function that takes an argument `x` and returns `x + 3`.

#### Example

```typescript
const logic: TLambda<TNat, TNat> = (x: TNat) => x + 3;
logic(3); // 6
```

</Snippet>

<MichelsonDocLink placeholder="LAMBDA" url="https://tezos.gitlab.io/michelson-reference/#instr-LAMBDA"/>
<MichelsonDocLink placeholder="LAMBDA_REC" url="https://tezos.gitlab.io/michelson-reference/#instr-LAMBDA_REC"/>

### Define a private lambda

<Snippet syntax={SYNTAX.PY}>

**`@sp.private_lambda(with_storage=None, with_operations=False,
wrap_call=False)`** <br />
Decorator to introduce a lambda that is also a private variable.<br/>
This is used for functions that are expected to be used more than
once.<br/>
Values are returned by using `sp.result(value)`.

:::caution Deprecated aliases

- `@sp.global_lambda` is deprecated in favor of
```python
@sp.private_lambda(with_storage=None, with_operations=False, wrap_call=False)
def f(self, params): # Please note that we need a self parameter here.
   ...
```
- `@sp.sub_entry_point` is deprecated in favor of
```python
@sp.private_lambda(with_storage="read-write", with_operations=True, wrap_call=True)
def f(self, params):
   ...
```

:::


We have the usual parameters to describe the context plus `wrap_call`:
- `with_storage="read-only"` or `with_storage="read-write"`, for functions that read or read-write the storage.
- `with_operations=True`, for functions that create operations.
- `wrap_call=True`, resolves calls immediately (with an implicit `sp.compute`).
- `recursive=True`, for recursive lambdas. A second parameter
  specifies the function name used for recursive calls (see example below).

Any combination of the parameters for `sp.private_lambda(...)` is
allowed, but `recursive=True` excludes storage and operations effects.

See reference [WorldCalculator](https://smartpy.io/ide?template=worldCalculator.py) template.

#### Example

```python
class MyContract(sp.Contract):
    # ...

    @sp.private_lambda()
    def transformer(self, x):
        sp.result(x + 5)

    @sp.private_lambda(with_operations=True, with_storage="read-write", wrap_call=True)
    def set_delegate(self):
        sp.set_delegate(sp.some(self.data.baker))

    @sp.entry_point
    def ep(self, params):
        self.data.result = self.transformer(params)
        self.set_delegate() # wrap_call=True is necessary because "self.set_delegate()" result is not assigned to the storage or used in a later step

    # A recursive factorial function. You can also replace 'f' with 'factorial'.
    @sp.private_lambda(recursive=True)
    def factorial(self, n, f):
        sp.if n <= 1:
             sp.result(n)
        sp.else:
             sp.result(n * f(n-1))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`@PrivateLambda({ withStorage: null, withOperations: false })`** <br
/>
Decorator to introduce a lambda that is also a private variable.<br/>
This is used for functions that are expected to be used more than once.

- `withStorage: "read-only"` or `withStorage: "read-write"`: For functions that read or read-write the storage.
- `withOperations: true` : For functions that create operations.

See reference [Lambdas](https://smartpy.io/ts-ide?template=Lambdas.ts) template.

:::caution Deprecated aliases

- @GlobalLambda

:::


```typescript
class MyContract {
    storage: TRecord<{
        result: TNat,
        baker: TKey_hash
    }> = {
        valresultue: 0,
        baker: "tz1dtXJpDg4e8tYcGE5b7dzStfvHGyVZivaY"
    };

    @PrivateLambda({})
    transformer = (value: TNat): TNat => {
        return value + 5;
    };

    @PrivateLambda({ withStorage: "read-write", withOperations: true })
    setDelegate = (): TUnit => {
        Sp.setDelegate(Sp.some(this.storage.baker));
    };

    @EntryPoint
    ep(params: TNat): void {
        this.storage.result = this.transformer(params)
        this.setDelegate();
    }
}
```

</Snippet>

<MichelsonDocLink placeholder="LAMBDA" url="https://tezos.gitlab.io/michelson-reference/#instr-LAMBDA"/>


### Calling lambdas

<Snippet syntax={SYNTAX.PY}>

**`f(x)`** <br />
Call a lambda.

If `f` is of type `sp.TLambda(t1, t2)` and `x` is of type `t1` then `f(x)` is of type `t2`.<br/>

<MichelsonDocLink placeholder="EXEC" url="https://tezos.gitlab.io/michelson-reference/#instr-EXEC"/>
<br/>
<br/>

:::note
As for every SmartPy expression, simply writing `y = f(x)`
doesn't directly compute `f(x)`. It builds an expression that will be
computed _if_ used in some action so its output needs to be used or we
need to wrap the call in some `sp.compute(...)`.
:::

**`f.apply(x)`** <br />
Partially apply a lambda.

If `f` is of type `sp.TLambda(sp.TPair(tp1, tp2), target)` and `x` is of type `tp1` then `f.apply(x)` is of type `sp.TLambda(tp2, target)`.<br/>

<MichelsonDocLink placeholder="APPLY" url="https://tezos.gitlab.io/michelson-reference/#instr-APPLY"/>


</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`f(x)`** <br />
Call a lambda.

If `f` is of type `TLambda(t1, t2)` and `x` is of type `t1` then `f(x)` is of type `t2`.<br/>

<MichelsonDocLink placeholder="EXEC" url="https://tezos.gitlab.io/michelson-reference/#instr-EXEC"/>
<br/>
<br/>

:::note
As for every SmartTS expression, simply writing `y = f(x)`
doesn't directly compute `f(x)`. It builds an expression that will be computed _if_ used in some action.
:::


</Snippet>
