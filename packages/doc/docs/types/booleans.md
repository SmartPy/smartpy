# Booleans

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type used to represent SmartPy booleans is [sp.TBool](/general/types#bool).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="bool" url="https://tezos.gitlab.io/michelson-reference/#type-bool"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type used to represent SmartTS booleans is [TBool](/general/types#bool).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="bool" url="https://tezos.gitlab.io/michelson-reference/#type-bool"/>.

</Snippet>

Have also a look at [Comparison Operators](/types/comparison_operators).

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.bool(<boolean>)`** <br />
Create a literal of type [sp.TBool](/general/types#bool) when `boolean` is a Python bool literal (`True` or `False`).

**`True`**, **`False`** <br />
The usual way to input [sp.TBool](/general/types#bool) literals in SmartPy.

#### Example

```python
value = True
# or
value = sp.bool(True)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`true, false`** <br />
The usual way to input [TBool](/general/types#bool) literals in SmartTS.

#### Example

```typescript
const value: TBool = true
```

</Snippet>

## Operations

### Boolean negation

<Snippet syntax={SYNTAX.PY}>

**`~expr`** <br />
Return the negation of `expr`, where `expr` must be a boolean.

#### Example

```python
sp.if ~expr:
    #...
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`!expr`** <br />
Return the negation of `expr`, where `expr` must be a boolean.

#### Example

```typescript
if (!expr) {
    //...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="NOT" url="https://tezos.gitlab.io/michelson-reference/#instr-NOT"/>


### Or


<Snippet syntax={SYNTAX.PY}>

**`expr1 | expr2`** <br />
Return `True` if `expr1` or `expr2` are `True`. Both `expr1` and `expr2` must be boolean expressions.

#### Example

```python
sp.if (expr1) | (expr2):
    #...
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 || expr2`** <br />
Return `true` if `expr1` or `expr2` are `true`. Both `expr1` and `expr2` must be boolean expressions.

#### Example

```typescript
if (expr1 || expr2) {
    //...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="OR" url="https://tezos.gitlab.io/michelson-reference/#instr-OR"/>


### And


<Snippet syntax={SYNTAX.PY}>

**`expr1 & expr2`** <br />
Return `True` if `expr1` and `expr2` are `True`. Both `expr1` and `expr2` must be boolean expressions.

#### Example

```python
sp.if (expr1) & (expr2):
    #...
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 && expr2`** <br />
Return `true` if `expr1` and `expr2` are `true`. Both `expr1` and `expr2` must be boolean expressions.

#### Example

```typescript
if (expr1 && expr2) {
    //...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="AND" url="https://tezos.gitlab.io/michelson-reference/#instr-AND"/>


### Exclusive or


<Snippet syntax={SYNTAX.PY}>

**`expr1 ^ expr2`** <br />
Return `True` if `expr1` and `expr2` are different. Both `expr1` and `expr2` must be boolean expressions.

#### Example

```python
sp.if (expr1) ^ (expr2):
    #...
```

<MichelsonDocLink placeholder="XOR" url="https://tezos.gitlab.io/michelson-reference/#instr-XOR"/>
<br/>
<br/>

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The `^` operator is not allowed for boolean types. Consider using `!==` instead.

#### Example

```typescript
expr1 !== expr2
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<Snippet syntax={SYNTAX.PY}>

:::note
Unlike in Python, `&` and `|` do [short-circuiting](https://en.wikipedia.org/wiki/Short-circuit_evaluation) on SmartPy boolean expressions.<br/>

For example, the evaluation of `((x==x) | (self.data.xs[2] == 0))` will not fail.
:::

:::caution
Please note that `not`, `and`, and `or` cannot be overloaded in Python. Hence, we cannot use them to construct SmartPy expressions and, as is customary for custom Python libraries, we use `~`, `&`, and `|` instead.
::::

</Snippet>
