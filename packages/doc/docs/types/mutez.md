# Mutez

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of amounts in SmartPy is [sp.TMutez](/general/types#mutez).

The corresponding type in Michelson is <MichelsonDocLink placeholder="mutez" url="https://tezos.gitlab.io/michelson-reference/#type-mutez"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of amounts in SmartTS is [TMutez](/general/types#mutez).

The corresponding type in Michelson is <MichelsonDocLink placeholder="mutez" url="https://tezos.gitlab.io/michelson-reference/#type-mutez"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.tez(<natural number>)`**, **`sp.mutez(<natural number>)`** <br/>
Introduce a mutez.

#### Example
```python
# Two identical amounts (one tez):
sp.tez(1)
sp.mutez(1000000)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`10 as TMutez`** <br />
Represent 10 `mutez`.

</Snippet>

## Global properties

### Get transferred amount

<Snippet syntax={SYNTAX.PY}>

**`sp.amount`** <br />
The amount of the current transaction, which is of type [sp.TMutez](/general/types#mutez).

#### Example

```python
amount = sp.amount
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.amount`** <br />
The amount of the current transaction, which is of type [TMutez](/general/types#mutez).

#### Example

```typescript python
const amount: TMutez = Sp.amount
```

</Snippet>

<MichelsonDocLink placeholder="AMOUNT" url="https://tezos.gitlab.io/michelson-reference/#instr-AMOUNT"/>

### Get contract balance

<Snippet syntax={SYNTAX.PY}>

**`sp.balance`** <br />
The balance of the current contract.<br/>
Due to the not intuitive semantics in Michelson, we suggest that developers do _not_ rely on balance too much.

See [Tezos Agora Post](https://forum.tezosagora.org/t/problems-with-balance/2194/3).<br/>
In tests, a contract's balance is accessible through the `<contract>.balance` field.

#### Example

```python
balance = sp.balance
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.balance`** <br />
The balance of the current contract.<br/>
Due to the not intuitive semantics in Michelson, we suggest that developers do _not_ rely on balance too much.

See [Tezos Agora Post](https://forum.tezosagora.org/t/problems-with-balance/2194/3).<br/>
In tests, a contract's balance is accessible through the `<contract>.balance` field.

#### Example

```typescript
const balance: TMutez = Sp.balance
```

</Snippet>

<MichelsonDocLink placeholder="BALANCE" url="https://tezos.gitlab.io/michelson-reference/#instr-BALANCE"/>

## Operations

### Addition

<Snippet syntax={SYNTAX.PY}>

**`expr1 + expr2`** <br />
Add two values of type [sp.TMutez](/general/types#mutez), `expr1` and `expr2`.

#### Example

```python
result = sp.mutez(10) + sp.tez(1) # 1000010 of type sp.TMutez
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 + expr2`** <br />
Add two values of type [TMutez](/general/types#mutez), `expr1` and `expr2`.

#### Example

```typescript
const result: TMutez = (10 as TMutez) + (1000000 as TMutez) // 1000010
```

</Snippet>

<MichelsonDocLink placeholder="ADD" url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/>

### Subtraction


<Snippet syntax={SYNTAX.PY}>

**`expr1 - expr2`** <br />
Subtract two values of type [sp.TMutez](/general/types#mutez), `expr1` and `expr2`.<br />
This operation fails if the result is negative.

#### Example

```python
result = sp.mutez(20) - sp.mutez(10) # 10 of type sp.TMutez
```

**`sp.sub_mutez(expr1, expr2)`** <br />
Subtract two values of type [sp.TMutez](/general/types#mutez), `expr1` and `expr2`.<br />
Returns a [sp.TOption](/general/types#option)([sp.TMutez](/general/types#mutez)).
This operation doesn't fail.

`sp.sub_mutez(a, b) == sp.some(a-b)` when `a-b` is a non negative and `sp.none` otherwise.

#### Example

```python
result = sp.sub_mutez(sp.mutez(20) - sp.mutez(10)) # sp.some(sp.mutez(10))
result2 = sp.sub_mutez(sp.mutez(10) - sp.mutez(20)) # sp.none
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 - expr2`** <br />
Subtract two values of type [TMutez](/general/types#mutez), `expr1` and `expr2`.

#### Example

```typescript
const result: TMutez = (20 as TMutez) - (10 as TMutez) // 10
```

</Snippet>

<MichelsonDocLink placeholder="SUB" url="https://tezos.gitlab.io/michelson-reference/#instr-SUB"/>

### Multiplication

<Snippet syntax={SYNTAX.PY}>

**`sp.mul(<expr1>, <expr2>)`** <br />
Multiply a [sp.TMutez](/general/types#mutez) with
a [sp.TNat](/general/types#nat) and produce a [sp.TMutez](/general/types#mutez).

#### Example

```python
value = sp.mul(sp.nat(2), sp.mutez(2)) # 4 of type sp.TMutez
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr1>.multiply(<expr2>)`** <br />
Multiply a [TMutez](/general/types#mutez) with a
[TNat](/general/types#nat) and produces a [TMutez](/general/types#mutez).

#### Example

```typescript
const value: TMutez = (2 as TNat).multiply(3 as TMutez)
```

</Snippet>

### Division

<Snippet syntax={SYNTAX.PY}>

**`sp.ediv(expr1, expr2)`** <br />
Perform euclidean division, where `expr1` is the dividend, and `expr2` is the divisor.

- When `expr1` and `expr2` are both of type [sp.TMutez](/general/types#mutez), the returned value is of type [sp.TOption](/general/types#option)([sp.TPair](/general/types#pair)([sp.TNat](/general/types#nat), [sp.TMutez](/general/types#mutez))).

- When `expr1` is of type [sp.TMutez](/general/types#mutez) and `expr2` is of type [sp.TNat](/general/types#nat), the returned value is of type [sp.TOption](/general/types#option)([sp.TPair](/general/types#pair)([sp.TMutez](/general/types#mutez), [sp.TMutez](/general/types#mutez))).

#### Example

```python
(quotient, remainder) = sp.match_pair(sp.ediv(sp.tez(11), sp.tez(2)).open_some())
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.ediv(expr1, expr2)`** <br />
Perform euclidean division, where `expr1` is the dividend, and `expr2` is the divisor.

- When `expr1` and `expr2` are both of type [TMutez](/general/types#mutez), the returned value is of type [TOption](/general/types#option)([TPair](/general/types#pair)([TNat](/general/types#nat), [TMutez](/general/types#mutez))).

- When `expr1` is of type [TMutez](/general/types#mutez) and `expr2` is of type [TNat](/general/types#nat), the returned value is of type [TOption](/general/types#option)([TPair](/general/types#pair)([TMutez](/general/types#mutez), [TMutez](/general/types#mutez))).

#### Example

```typescript
const tuple = Sp.ediv(11 as TMutez, 1 as TMutez).openSome()
```

</Snippet>

<MichelsonDocLink placeholder="EDIV" url="https://tezos.gitlab.io/michelson-reference/#instr-EDIV"/>

### Split tokens

<Snippet syntax={SYNTAX.PY}>

**`sp.split_tokens(amount, quantity, totalQuantity)`** <br />
Compute `amount * quantity / totalQuantity` where `amount` is of type [sp.TMutez](/general/types#mutez), `quantity` and `totalQuantity` are of type [sp.TNat](/general/types#nat).

#### Example

```python
sp.split_tokens(sp.mutez(100), 1, 10) # 10 mutez
```

</Snippet>
