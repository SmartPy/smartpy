# Sapling Integration

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

[Sapling](https://blog.nomadic-labs.com/sapling-integration-in-tezos-tech-preview.html) is a recent feature in Michelson.

SmartPy.io is compatible with Sapling in different ways: types, michelson implementation, compiler, and a fake implementation for test scenarios.

See reference [Sapling](https://smartpy.io/ide?template=sapling.py) and [Sapling2](https://smartpy.io/ide?template=sapling2.py) templates.

There are two types for Sapling: `sp.TSaplingState(memo_size)` for states and `sp.TSaplingTransaction(memo_size)` for transactions where `memo_size` is a constant integer between `0` and `65535` and corresponds to potentially included data in transaction. The `memo_size` parameter is optional in types `sp.TSaplingState` and `sp.TSaplingTransaction` as it may be computed through type inference.

SmartPy doesn't generate proper literals for Sapling types but an ad-hoc, fake, test implementation is provided for test scenarios.<br/>
This is *not* a proper Sapling transaction but it is enough to test contracts.

## Methods

### Create sapling transaction for testing

This is a _fake / test_ version.

```python
sp.sapling_test_transaction(source, target, amount, memo_size)
```

- `source` is a Python string, empty string for minting tokens.

- `target` is a Python string, empty string for burning tokens.

- `amount` is a non-negative Python integer.

- `memo_size` is a non-negative Python integer between `0` and `65535`.


### Create a sapling empty state

Building an empty `sp.TSaplingState` with a default `memo_size` (Expected length for message of Sapling transaction)

```python
sp.sapling_empty_state(memo_size)
```

- `memo_size` is a uint16 value and represents the expected message length of the sapling transaction. **(must be between 0 and 65535)**

### Verify a sapling update

When `state` is a `sp.TSaplingState(memo_size)` and `transaction` a
`sp.TSaplingTransaction(memo_size)`, `sp.sapling_verify_update(state,
transaction)` applies the transaction to the state, validates it and
returns `sp.none` if it fails and `sp.some(x)` where `x` is of type
`sp.TTuple(sp.Bytes, sp.TInt, sp.TSaplingState(memo_size))`.

```python
sp.sapling_verify_update(state, transition)
```
