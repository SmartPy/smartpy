# Importing Code

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Importing regular Python code can be done in any template with the regular Python import.

Importing SmartPy code that uses the syntactic sugar is also doable but needs specific functions.

**`sp.io.import_template(template_name)`**

It only works in https://smartpy.io, it doesn't with the CLI.

```python
FA2 = sp.io.import_template("FA2.py")
class my_token(FA2.FA2):
    ...
```

<br/>

**`sp.io.import_script_from_url(url, name = None)`**

Same functionality as above but instead of using a template from within smartpy.io, it allows to fetch a file from a custom URL.

The `URL` is a string of the form `http://`, `https://`, `file://`, `file:`, etc.
The module obtained name is the optional `name` parameter, with default equal to `url`.

```python
FA2 = sp.io.import_script_from_url("file://<path>/FA2.py")
class my_token(FA2.FA2):
    ...
```

<br/>

**`sp.io.import_stored_contract(name)`**

Same functionality but instead of importing a file from a `URL`, import a script saved in the browser local storage.<br/>
It only works in https://smartpy.io, it doesn't with the CLI.

```python
FA2 = sp.io.import_stored_contract("FA2")
class my_token(FA2.FA2):
    ...
```

<br/>

**`sp.io.import_script_from_script(name, script)`**

Import some script where both `name` and `script` are strings.

```python
FA2 = sp.io.import_script_from_script(
    "FA2",
    """
    import smartpy as sp

    class FA2(sp.Contract):
        ...
    """
)
class my_token(FA2.FA2):
    ...
```

<br/>

**`Standard Python Imports`**

You can also import python libs to ease the development. It is only used for meta-programming and is not available at runtime.
```python
from datetime import datetime
a = datetime.now()
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The `URL` is a string of the form `http://`, `https://`, `./<relative_path>`.

```typescript
import * as Module from '<URL>';
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
