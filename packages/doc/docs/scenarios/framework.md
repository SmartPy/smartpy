import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

# General Framework

Scenarios describe a sequence of actions:
 - Originating contracts;
 - Computing expressions;
 - Calling entry points;
 - and more.

They are directly used in tests and implicitly in compilation targets.

## Tests

<Snippet syntax={SYNTAX.PY}>

**`@sp.add_test`**
```python
@sp.add_test(name, shortname=None, profile=False, is_default=True)
```
It decorates a test function.

| Property | Description | Default Value |
|--:|:--:|:---|
| name | Test Name | **required** |
| shortname | An optional parameter. Short names need to be unique. Used in smartpy-cli outputs. | None |
| profile | Computes and pretty-prints profiling data. | False |
| is_default | Determines if the test is performed by default when evaluating all tests. | True |

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Dev.test`**
```ts
// `Dev.test` interface
interface Dev {
    test: (
        {
            name: string,
            shortName?: string,
            profile?: boolean
            enabled?: boolean,
            flags?: string[][]
        },
        () => void
    ) => void;
}
```

| Property | Description | Default Value |
|--:|:--:|:---|
| name | Test Name | **required** |
| shortName | An optional parameter. Short names need to be unique. Used in smartpy-cli outputs. | undefined |
| profile | Computes and pretty-prints profiling data. | false |
| enabled | Determines if the test is performed by default when evaluating all tests. | true |
| flags | [Configuration flags](/general/flags#flags) | [] |

</Snippet>

<Snippet syntax={SYNTAX.ML}>

In `SmartML`, a test is introduced by `Target.register_test`.

</Snippet>

### Test Example

<Snippet syntax={SYNTAX.PY}>

```py
@sp.add_test(name = "First test")
def test():
    # We define a test scenario, called sc,
    # together with some outputs and checks
    sc = sp.test_scenario()
    # We first define a contract and add it to the scenario
    c1 = MyContract(12, 123)
    scenario += c1
    # And call some entry points of c1
    c1.my_entrypoint(12)
    c1.my_entrypoint(13)
    c1.my_entrypoint(14)
    c1.my_entrypoint(50)
    c1.my_entrypoint(50)
    c1.my_entrypoint(50).run(valid = False) # this is expected to fail
    # Finally, we check the final storage of c1
    sc.verify(c1.data.myParameter1 == 151)
    # and its balance
    sc.verify(c1.balance, sp.tez(0))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```ts
Dev.test({
    name: "First test"
}, () => {
    // Originates a contract
    const c1 = Scenario.originate(new MyContract({ value1: 12, value2: 123 }));
    // Calls an entry point called `my_entrypoint` from contract `c1`
    Scenario.transfer(c1.my_entrypoint(12));
    Scenario.transfer(c1.my_entrypoint(13));
    Scenario.transfer(c1.my_entrypoint(14));
    Scenario.transfer(c1.my_entrypoint(50));
    Scenario.transfer(c1.my_entrypoint(50));
    // This call is expected to fail
    Scenario.transfer(c1.my_entrypoint(12), { valid: false });
    // Finally, we check the final storage of c1
    Scenario.verify(c1.storage.myParameter1 === 151);
    // And its balance
    Scenario.verify(c1.balance === 0);
});
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
let () =
  Target.register_test
    ~name:"Welcome"
    [%actions
      h1 "Welcome";
      let c1 = register_contract (MyContract.init [%expr 12] [%expr 123]) in
      call c1.myEntryPoint 12;
      call c1.myEntryPoint 13;
      call c1.myEntryPoint 14;
      call c1.myEntryPoint 50;
      call c1.myEntryPoint 50;
      call c1.myEntryPoint 50 ~valid:false;
      verify (c1.data.myParameter1 = 151)]
```

</Snippet>
