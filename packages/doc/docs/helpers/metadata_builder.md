# Metadata Builder

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

Check metadata guides [here](/guides/metadata/overview).

## Build metadata

Generate a JSON metadata document for string name containing an arbitrary constant Python `expression` converted into JSON.

<Snippet syntax={SYNTAX.PY}>

```python
# A python dictionary that contains metadata entries
metadata = {
    "name": "Contract Name",
    "description": "A description about the contract",
    "version": 1,
    "views" : [self.get_x, self.get_storage],
}

self.init_metadata('SOME CONTRACT', metadata)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@MetadataBuilder
metadata = {
    name: 'SOME CONTRACT',
    decimals: 1,
    views: [this.state, this.increment, this.decrement],
    preferSymbol: true,
    source: {
        tools: ['SmartPy'],
    },
};
```

</Snippet>

## Defining an off-chain view


A decorator which defines an off-chain view.

<Snippet syntax={SYNTAX.PY}>

Off-chain views are defined using the **`@sp.offchain_view( name = <name>,  pure = <True|False>, doc = None)`** decorator inside the contract class.

By default, the `view` name is equal to the method name, where `<name>` is an optional argument and can be used to set a view name explicitly.

It has three optional parameters:

- `name`: Defines the view name explicitly;
- `pure` (**default is False**): Defines the purity of view (dependent only on storage and parameters);
- `doc`: Sets the view documentation. If doc is `None`, the documentation is the docstring of the method.

```python
@sp.offchain_view(pure = True)
def get_x(self, params):
    """blah blah ' fdsfds"""
    sp.result(sp.record(a = self.data.x, b = 12 + params))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Off-chain views are defined using the **`@OffChainView({ name?: string,  pure?: boolean, doc?: string })`** decorator inside the contract class.

By default, the `view` name is equal to the method name, where `<name>` is an optional argument and can be used to set a view name explicitly.

It has two optional parameters:

- `name`: Defines the view name explicitly;
- `pure` (**default is False**): Defines the purity of view (dependent only on storage and parameters)
- `doc`: Sets the view documentation. If doc is `None`, the documentation is the docstring of the method

```typescript
@OffChainView({ pure: true, description: 'Increment value' })
increment = (): TInt => {
    return this.storage + 1;
};
```

</Snippet>

## Testing an off-chain view

Off-chain views can be called from test scenarios the same way as entry points.

The example below shows how to do it.

#### Example

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, param):
        self.init(param)

    @sp.offchain_view()
    def state(self, param):
        sp.verify(param < 5, "This is false: param > 5")
        sp.result(self.data * param)

@sp.add_test(name = "Minimal")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract(1)
    scenario += c1

    """ Test views """

    # Display the offchain call result
    scenario.show(c1.state(1))

    # Assert the view result
    scenario.verify(c1.state(2) == 2)
    # Assert call failures
    scenario.verify(sp.is_failing(c1.state(6)));    # Expected to fail
    scenario.verify(~ sp.is_failing(c1.state(1)));   # Not expected to fail

    # Assert exception result
    # catch_exception returns an option:
    #      sp.none if the call succeeds
    #      sp.some(<exception>) if the call fails
    e = sp.catch_exception(c1.state(7), t = sp.TString)
    scenario.verify(e == sp.some("This is false: param > 5"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView
    state = (param: TInt): TInt => {
        Sp.verify(param < 5, "This is false: param > 5")
        return this.storage * param;
    };
}

Dev.test({ name: 'test' }, () => {
    const c1 = Scenario.originate(new OffChainViews());

    /** Test views */

    // Display the offchain call result
    Scenario.show(c1.state(1));

    // Assert the view result
    Scenario.verify(c1.state(2) === 2);
    // Assert call failures
    Scenario.verify(Scenario.isFailing(c1.state(6)));    // Expected to fail
    Scenario.verify(!Scenario.isFailing(c1.state(1)));   // Not expected to fail

    // Assert exception result
    // catchException returns an option:
    //      Sp.none if the call succeeds
    //      Sp.some(<exception>) if the call fails
    const e = Scenario.catchException<TString>(c1.state(7));
    Scenario.verify(e === Sp.some("This is false: param > 5"))
});
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Utils

<Snippet syntax={SYNTAX.PY}>

Convert a `URL` into a metadata `big_map`.

```python
sp.utils.metadata_of_url(url)
```
A simple alias for `sp.big_map({ "" : sp.utils.bytes_of_string(url) })`

</Snippet>

## Metadata example

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

        # A python dictionary that contains metadata entries
        metadata = {
            "name": "Contract Name",
            "description": "A description about the contract",
            "version": 1,
            "views" : [self.get_x, self.get_storage],
        }

        # Helper method that builds the metadata and produces the JSON representation as an artifact.
        self.init_metadata("example1", metadata)

    @sp.offchain_view(pure = True)
    def get_x(self, params):
        """blah blah ' some documentation """
        sp.result(sp.record(a = self.data.x, b = 12 + params))

    @sp.offchain_view(doc = "The storage")
    def get_storage(self):
        sp.result(self.data.x)

    @sp.entry_point()
    def change_metadata(self, metadata):
        self.data.metadata = metadata

@sp.add_test(name = "Metadata")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract(x=1, metadata = sp.utils.metadata_of_url("ipfs://Qme9L4y6ZvPwQtaisNGTUE7VjU7PRtnJFs8NjNyztE3dGT"))
    scenario += c1
    c1.change_metadata(sp.utils.metadata_of_url("https://cloudflare-ipfs.com/ipfs/Qme9L4y6ZvPwQtaisNGTUE7VjU7PRtnJFs8NjNyztE3dGT"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@Contract
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView({ name: 'getStorageState', description: 'Get Storage State' })
    state = () => {
        return this.storage;
    };

    @OffChainView({ pure: true, description: 'Increment value' })
    increment = () => {
        return this.storage + 1;
    };

    @OffChainView({ pure: true, description: 'Decrement value' })
    decrement = () => {
        return this.storage - 1;
    };

    @MetadataBuilder
    metadata = {
        name: 'SOME CONTRACT',
        decimals: 1,
        views: [this.state, this.increment, this.decrement],
        preferSymbol: true,
        source: {
            tools: ['SmartPy'],
        },
    };
}

Dev.test({ name: 'OffChainViews' }, () => {
    Scenario.originate(new OffChainViews());
});

```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
