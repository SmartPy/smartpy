---
sidebar_position: 2
---

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import Entrypoint from '@site/src/components/MichelsonDoc/Entrypoint';
import OffchainView from '@site/src/components/MichelsonDoc/OffchainView';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@site/src/components/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@site/src/components/CodeTabs';

# Tokens

The GamePlatform's Ledger contains:

+ 3 bigmaps related to tokens: `ledger`, `token_metadata` and `token_permissions`. <br/>
  The two first are those describe in [FA2 standard](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md).<br/>
  The `token_permissions` describes specific permissions associated with the token.<br/>
+ FA2 entrypoints + `transfer_and_call` + `update_token_permissions`,
`push_bonds`, `mint`, `burn`, `withdraw`.

Platform tokens should not be confused with bonds.
Tokens are stored outside of channels.

Token 0 often corresponds to wrapped XTZ (basically they can be minted
by sending xtz and burned so you receive xtz). Other tokens can
correspond to FA1.2/FA2 external tokens or internal tokens.

The admin can authorize a model or a specific game to distribute
platform tokens (like reputation tokens).

## Push bonds

See [push_bonds](/guides/state_channels/push_bonds).

## Mint tokens

Native tokens (tokens managed by the ledger) can be minted according
to the permissions.

<CodeTabs>
    <Entrypoint name="mint">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="address"
                type="address">
                Address of the receiver
            </MichelsonArg>
            <MichelsonArg
                name="token_id"
                type="nat">
                Id of the token
            </MichelsonArg>
            <MichelsonArg
                name="amount"
                type="nat">
                Amount of tokens
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
ledger.mint(
    address = player1.address,
    token_id = 0,
    amount = 100 * 10**6
).run(sender = player1, amount = sp.tez(100))
```
</block>
</CodeTabs>

## Burn tokens

<CodeTabs>
    <Entrypoint name="burn">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="address"
                type="address">
                Address of the receiver
            </MichelsonArg>
            <MichelsonArg
                name="token_id"
                type="nat">
                Id of the token
            </MichelsonArg>
            <MichelsonArg
                name="amount"
                type="nat">
                Amount of tokens
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
ledger.burn(
    address = player1.address,
    token_id = 0,
    amount = 100 * 10**6
).run(sender = player1, amount = sp.tez(100))
```
</block>
</CodeTabs>

## Unwrap native tokens

Certain native tokens can be unwrapped. In this case they are burned
and retransformed.

For example you can unwrap your wXTZ to receive XTZ.

<CodeTabs>
    <Entrypoint name="withdraw_ledger">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="receiver"
                type="address">
                Address of the receiver
            </MichelsonArg>
            <MichelsonArg
                name="tokens"
                type="map(nat, nat)">
                Map of <code>token_id => amount</code> you want to withdraw
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
platform.withdraw_ledger(
    receiver = player1.address,
    tokens = sp.map({0: 10_000_000})
).run(sender = player1)
```
</block>
</CodeTabs>

## Tokens metadata and authorization

### Token Metadata

Set the token metadata.

Token metadata corresponds to those described in
[TZIP-12](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md) and
[TZIP-16](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md).

<CodeTabs>
    <Entrypoint name="set_token_metadata">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="token_id"
                type="nat">
                The id of the token as represented in the platform
            </MichelsonArg>
            <MichelsonArg
                name="metadata"
                type="map(string, bytes">
                Metadata of the tokens
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
c1.set_token_metadata(
    token_id = 1,
    metadata = {
        "name"        : sp.utils.bytes_of_string("Wrapped Dummy FA2"),
        "decimals"    : sp.utils.bytes_of_string("%d" % 0),
        "symbol"      : sp.utils.bytes_of_string("WDFA2"),
        "type"        : sp.pack("FA2"),
        "max_supply"  : sp.pack(100_000_000_000),
        "fa2_address" : sp.pack(dummyToken.address),
        "fa2_token_id": sp.pack(0),
    }
).run(sender = admin)
```
</block>
</CodeTabs>

### Set Token Permissions (admin only)

Set the token permissions.

The token permissions are pack of the value.

| Key                | Value Type                                                     | Description                                                                                                                      |
| ------------------ | -------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| burn_permissions   | sp.TVariant("onlyOwner": Unit, "allow_only: Set(Address))      | Type of permission applied to burn (if not present no burn allowed) "allow_only": only addresses in this set are allowed to burn |
| fa_token           | Pair(Address, Nat)                                             | Contract address and token_id of the corresponding fa token                                                                      |
| fa2_token_id       | Nat                                                            | id of the corresponding fa2 token                                                                                                |
| mint_cost          | Mutez                                                          | number of mutez need to mint 1 token                                                                                             |
| mint_permissions   | sp.TVariant("allow_everyone": Unit, "allow_only: Set(Address)) | Type of permission applied to mint (if not present no mint allowed) "allow_only": only addresses in this set are allowed to mint |
| transfer_only_to   | Set(Address)                                                   | The token can only be transfered to one of those addresses                                                                       |
| transfer_only_from | Set(Address)                                                   | The token can only be transfered from one of those addresses                                                                     |
| type*              | String {`"FA2"`, `"Native"`}                                   | Type of token represented. <br/>                                                                                                 |


\* required

<CodeTabs>
    <Entrypoint name="update_token_permissions">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="token_id"
                type="nat">
                The id of the token as represented in the platform
            </MichelsonArg>
            <MichelsonArg
                name="metadata"
                type="map(string, TOption(bytes)">
                Permissions of the tokens. If None, the key is deleted.
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
lgr = sp.io.import_template("state_channel_games/ledger.py")
c1.update_token_permissions(
    token_id = 1,
    metadata = lgr.build_token_permissions({
    "type"            : "NATIVE",
    "mint_cost"       : sp.mutez(1),
    "mint_permissions": sp.variant("allow_everyone", sp.unit)
})
).run(sender = admin)
```
</block>
</CodeTabs>

## Game permissions

Each running game can have the permission to mint tokens.

The metadata of the game must contain the following key: `allowed_mint`.

Its value is a map of bonds and amount.

The game can at most ask the platform to mint this number of tokens when being settled.

### Change the game metadata

Only admins can change the game metadata.

```python
gameplatform.admin_game_metadata(game_id, "allowed_mint", sp.pack({42: 1000}))
```

## Model permissions

The models can have the permission to mint tokens.

The metadata of the model must contain the following key: `allowed_mint`.

Its value is a map of bonds and amount.

The model can at most ask the platform to mint this number of tokens.

The value is decreased or left unchanged after each settle of a game held by the model.

### Change the model metadata

Only admins can change the model metadata.

```python
gameplatform.update_model_metadata(model_id, "allowed_mint", {42: 100_000_000})
```
