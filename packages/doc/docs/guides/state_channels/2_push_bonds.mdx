---
sidebar_position: 3
---

# 2️ Push bonds

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import Entrypoint from '@site/src/components/MichelsonDoc/Entrypoint';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@site/src/components/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@site/src/components/CodeTabs';

Bonds consist of assets that a player posts into a channel.
They can be wrapped XTZ, other fungible tokens like `tzBTC`, or
non-fungible tokens.

Anyone can post bonds for any player in any channel. However, only the
owner can withdraw bonds, under the conditions described in [withdraw
bonds][withdraw_bonds].

The platform is associated with a ledger contract. This is the one you
interact with when you want to push bonds.
The tokens are configured by the admin (see [tokens](/guides/state_channels/more/tokens)).

In general, the token 0 corresponds to wrapped XTZ ("WXTZ").
For every WXTZ you want to post as bond you have to transfer one XTZ.

## Push internal token

Internal tokens are tokens that are managed by the ledger (like
wrapped XTZ).

1. Mint the token (if needed)
2. Call <a href="#push_bonds-entrypoint">push_bonds</a> on the ledger.

## Push external tokens

External tokens are tokens that are managed by another FA contract.

Depending on the case you have 2 solutions.

### (a) Compatible with <span className="entrypointName">transfer_and_call</span> (more secure)

This call has to be done on the FA2 contract that manage the external
tokens.

<CodeTabs>
    <Entrypoint name="transfer_and_call" type="pair">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="from_"
                type="address">
                Your address
            </MichelsonArg>
            <MichelsonArg
                name="txs"
                type="list($txs)">
                Your transaction
            </MichelsonArg>
        </MichelsonArgs>
        <MichelsonArgs name="txs" type="pair">
            <MichelsonArg
                name="to_"
                type="address">
                Platform address
            </MichelsonArg>
            <MichelsonArg
                name="callback"
                type="address">
                <code>&lt;ledger_address&gt;%on_received_bonds</code>
            </MichelsonArg>
            <MichelsonArg
                name="data"
                type="bytes">
                <code>sp.pack(sp.record(channel_id =
                &lt;channel_id&gt;, player_addr = &lt;your_address&gt;
                platform = &lt;platform_address&gt;))</code>
                <br/>See <a href="/guides/state_channels/open_channel#build-the-channel_id">channel id calculation</a>.
            </MichelsonArg>
            <MichelsonArg
                name="amount"
                type="nat">
                Amount of tokens you want to bond.
            </MichelsonArg>
            <MichelsonArg
                name="token_id"
                type="nat">
                Id of the tokens you want to bond.
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
    types = sp.io.import_template("state_channel_games/types.py").Types()

    my_address     = sp.address('tz1_MY_ADDRESS')
    ledger_address = sp.to_address("KT1_ADDRESS_OF_THE_LEDGER")
    callback       = sp.to_address(
        sp.contract(
            types.t_callback,
            ledger_address,
            entrypoint = "on_received_bonds"
        ).open_some()
    )

    fa2_contract.transfer_and_call([sp.record(
        from_ = my_address,
        txs = [
            sp.record(
                to_      = ledger_address,
                callback = callback,
                data     = sp.pack(sp.record(channel_id = channel_id, player_addr = my_address)),
                amount   = sp.nat(1337),
                token_id = sp.nat(42),
            )
    ])])
```
</block>
</CodeTabs>

### (b) With operator (legacy)

Set the ledger as your operator by calling <span className="entrypointName">update_operators</span> on the FA2
contract.

Call <a href="#push_bonds-entrypoint">push_bonds entrypoint</a> on the ledger.

## Push_bonds entrypoint

<CodeTabs>
    <Entrypoint name="push_bonds" type="pair">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="platform"
                type="address">
                Address of the platform
            </MichelsonArg>
            <MichelsonArg
                name="channel_id"
                type="bytes">
                The id of the channel (see <a href="/guides/state_channels/open_channel#build-the-channel_id">open_channel</a>)
            </MichelsonArg>
            <MichelsonArg
                name="player_addr"
                type="address">
                Receiver of the bond
            </MichelsonArg>
            <MichelsonArg
                name="tokens"
                type="map (nat, nat)">
                Map of <code>token</code> and <code>amount</code>
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
    # ... (new channel)

    platform.push_bonds(
        platform    = platform.address,
        channel_id  = channel_id,
        player_addr = player1.address,
        tokens      = {0: 10_000_000}
    ).run(sender = player1.address)
```
</block>
</CodeTabs>

### Withdraw bonds

See [withdraw bonds][withdraw_bonds]

[withdraw_bonds]: /guides/state_channels/withdraw_bonds
