# Using SmartPy with VSCode / other IDE

When you use SmartPy outside the web ide, you may encounter some problems:
+ the SmartPy library is not recognized by your ide
+ some SmartPy syntaxes are not understood

The purpose of this article is to help you to solve these problems.


## Command line interface

The command line interface documentation can be found [here](/cli/).

## SmartPy import

### VSCode

If you see the following error:

> Import "smartpy" could not be resolved.
> Pylance([reportMissingImports](https://github.com/microsoft/pylance-release/blob/main/DIAGNOSTIC_SEVERITY_RULES.md#diagnostic-severity-rules)).

You can add SmartPy to extra paths as explained
[here](https://github.com/microsoft/pylance-release/blob/main/TROUBLESHOOTING.md#unresolved-import-warnings).

The easiest way to do this is to create a file `.vscode/settings.json` in the
workspace's settings with the contents:

```json
{
    "python.analysis.extraPaths": ["<path_to_smartpy.py>"]
}
```

## Python syntax

SmartPy is written in Python except for some syntactic sugar.

If you want your ide to recognize the Python syntax, you can desugar it by using
the following transformation rules:


| SmartPy only                      | Pure Python                                           |
|:----------------------------------|:------------------------------------------------------|
| `sp.if <condition>:`              | `with sp.if_(<condition>):`                           |
| `sp.else:`                        | `with sp.else_():`                                    |
| `sp.for <element> in <elements>:` | `with sp.for_("<element>", <elements>) as <element>:` |

For example:

```python
sp.for member in members:
    sp.if self.data.visits.contains(member):
        self.data.visits[member] += 1
    sp.else:
        self.data.visits[member] = 1
```

Can be de-sugar into:

```python
with sp.for_("member", members) as member:
    with sp.if_(self.data.visits.contains(member)):
        self.data.visits[member] += 1
    with sp.else_():
        self.data.visits[member] = 1
```

Both code are equivalent. The second is a valid Python code so your linter will
agree with it.

## Compilation and testing

Compilation and testing will be performed using the command line interface, see
[cli](/cli).

Note that you can obtain the scenario output panel with the `--html`
[optional argument](/cli/#cli-optional-arguments). If will give you an html
file that you can open in your favorite browser.

