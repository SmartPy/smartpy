# Overview

## Definition

Metadata provides information that is not directly used for a contract's
operation. It describes the contract's code or the off-chain meaning of its
contents.

Metadata are standardized through the [TZIP] system.

They are two types of metadata:

| Name                    | Specification                      | Description                                                                                    |
| ----------------------- | ---------------------------------- | ---------------------------------------------------------------------------------------------- |
| [Contract metadata][L1] | [TZIP-16] ([TZIP-12] for FA2)      | Describe the whole contract (e.g. interface, versioning) and the off-chain views.              |
| [Token metadata][L2]    | [TZIP-16], [TZIP-12] and [TZIP-21] | Used in the [FA2] standard. They describe one token (e.g. an artwork corresponding to an NFT). |

[TZIP]: https://gitlab.com/tezos/tzip/
[TZIP-12]: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-16.md
[TZIP-16]: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md
[TZIP-21]: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-21/tzip-21.md
[FA2]: /guides/FA/FA2/overview

[L1]: ../contract_metadata
[L2]: /guides/FA/FA2/token_metadata

## Usage

The usage of metadata is very large.

Metadata are used by contracts explorers to show additional information about
the contracts: a name, an author, the source code... and additional features:
off-chain views. Off-chain views let people get more structured info from the
the contract without having to send a transaction nor use a wallet. They can
simplify decentralized applications job by abstracting the contract's storage.

Metadata are used by platforms like NFT marketplaces to list NFTs and
collections. They are used by wallets to show tokens and improve feedbacks when
interacting (by printing more explicit errors for example).

These usages are really open and you can invent your own like adding a link to
a CSV file or to real world geo-coordinates to a contract.