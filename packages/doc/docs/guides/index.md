---
sidebar_position: 1
---

# Guides

In addition to the SmartPy documentation, we also provide guides for further
study of certain related concepts.

The guides are:

| Guide                            | Description                                                                                                                                   |
| -------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| [FA Tokens - Digital Assets][g1] | Tezos token standards. <br/> See our [FA1.2.py][FA1.2] template and two FA2 implementations: new [fa2_lib.py][g1] or legacy [FA2.py][FA2.py]. |
| [Metadata][g2]                   | Information about metadata and off-chain views on Tezos and IPFS.                                                                             |
| [Mutation testing][g5]             | A simple explanation on how to use mutation testing.                                                        |
| [State Channels][g3]             | A complete guide about state channels layer 2 techniques with a game platform example.                                                        |
| [Using own IDE][g4]              | A simple guide on how to use VSCode or other IDE with SmartPy.                                                                                |

[g1]: /guides/FA/FA2/overview
[g2]: /guides/metadata/overview
[g3]: /guides/state_channels/overview
[g4]: /guides/IDE/own-ide
[g5]: /guides/mutation_testing
[FA2.py]: /guides/FA/legacy/FA2_legacy
[FA1.2]: /guides/FA/FA1.2
