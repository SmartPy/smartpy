---
sidebar_position: 10
---

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

# FA2 - legacy template

:::info
The template FA2.py will become obsolete in favor of the new
[fa2_lib.py](/guides/FA/FA2/overview), introduced on March 4, 2022.
:::

<Snippet syntax={SYNTAX.PY}>

> Template: [view](https://smartpy.io/ide?template=FA2.py),
> [download](https://smartpy.io/templates/fa2.py).<br/>
> This is the only authoritative source and must be read in order to dive into details.<br/>
Example: [FA2 contract](https://smartpy.io/ide?cid=QmdUKzyLX23vkrnHSpP3L5KvEbX9q9Hfj6c4HU55XcVHwo&k=aef3430fc87f6eac4454)
is a sample boilerplate.<br/>
TZIP specifications: [TZIP-12](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md)
and [TZIP-16](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md).

FA2/[TZIP-12](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md) is a standard for a unified token contract interface,
supporting a wide range of token types and implementations.

A token contract can be designed to support a single token type (e.g. ERC-20 or ERC-721) or multiple token
types (e.g. ERC-1155), to optimize batch transfers and atomic swaps of the tokens.

Tokens can be fungible tokens or non-fungible tokens (NFTs).

SmartPy provides a FA2 template that can be configured and adapted with custom logic to support a very wide range of your needs.

## Simple FA2 contract

### Import

To create a FA2 contract you need to import SmartPy and the FA2 template.

``` python
import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")
```

You can then create your FA2 contract by extending the template `FA2.FA2` class.

``` python
class ExampleFA2(FA2.FA2):
    pass
```

At this stage you have a complete FA2 contract with a lot of entry points and helpers.
We'll see how to choose more precisely what functionalities to keep, how to modify them and how to modify the storage later on.
For now, let's compile it.

### Compilation target

In order to instantiate the FA2 you need to give three parameters:
+ `admin`: the admin address
+ `config`: a dictionary that lets you tweak the contract
+ `metadata`: the [TZIP-16](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md) metadata of the contract.

The `config` can be built by calling `FA2.FA2_config()`. See [config](#config).

The `metadata` is explained in detail in [metadata](/guides/metadata/overview).

``` python
sp.add_compilation_target(
    "FA2_Tokens",
    ExampleFA2(
        admin   = sp.address("tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr"),
        config  = FA2.FA2_config(),
        metadata = sp.utils.metadata_of_url("https://example.com")
    )
)
```

### Test

You can now write a very basic test with your token contract.

``` python
@sp.add_test(name="FA2 tokens")
def test():
    sc = sp.test_scenario()
    sc.table_of_contents()
    FA2_admin = sp.test_account("FA2_admin")
    sc.h2("FA2")
    exampleToken = ExampleFA2(
        FA2.FA2_config(),
        admin = FA2_admin.address,
        metadata = sp.utils.metadata_of_url("https://example.com")
    )
    sc += exampleToken
```

## FA2 customisation

### Entry points and mixins

The FA2 template is divided into small classes that you can inherit separately if you don't need all the features.

In this example we don't implement the mint entry point.

``` python
class ExampleFA2(
        FA2_change_metadata
        FA2_token_metadata,
        # FA2_mint,
        FA2_administrator,
        FA2_pause,
        FA2_core):
    pass
```

Here are all the available mixins:

| mixins                | description                                                    |
| --------------------- | -------------------------------------------------------------- |
| `FA2_core`            | Implements the strict standard.                                |
| `FA2_administrator`   | `is_administrator` method and `set_administrator` entry point. |
| `FA2_change_metadata` | `set_metadata` entry point.                                    |
| `FA2_mint`            | `mint` entry point.                                            |
| `FA2_pause`           | `is_paused` method and `set_pause` entry point.                |
| `FA2_token_metadata`  | `set_token_metadata_view` and  `make_metadata` _methods.       |

The `FA2.FA2` class is implemented by inheriting from all the mixins.

### Init and storage

You can reimplement the `__init__` method and call the one of FA2_core.

``` python
class ExampleFA2(FA2.FA2_core):
    def __init__(self, config, admin):
        FA2.FA2_core.__init__(self, config,
                            paused = False,
                            administrator = admin)
```

This `__init__` method takes additional storage fields. They'll be added in the contract storage.

``` python
FA2.FA2_core.__init__(self, config, paused = False, administrator = admin,
                    my_custom_bigmap = sp.big_map(
                        tkey = sp.TAddress,
                        tvalue = sp.TNat,
                        l = {}
                    ) # Add a bigmap into the final storage
                    )
```

You can also call use `self.update_initial_storage()` to update the storage.

``` python
class ExampleFA2(FA2.FA2):
    def __init__(self, config, admin):
        FA2.FA2_core.__init__(self, config,
                            paused = False,
                            administrator = admin)
        self.update_initial_storage(
            x = 0,
            y = sp.map()
        )
```

### Custom entry points

You can reimplement the provided entry points or add yours exactly like you add entry points in SmartPy contracts.

In this example, we replace the `mint` entry point by our implementation.

``` python
class ExampleFA2(FA2.FA2):
    @sp.entry_point
    def mint(self, params):
        """ A very simple implementation of the mint entry point"""
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        with sp.if_(self.data.ledger.contains(user)):
            self.data.ledger[user].balance += params.amount
        with sp.else_():
            self.data.ledger[user] = Ledger_value.make(params.amount)
        with sp.if_(~ self.token_id_set.contains(self.data.all_tokens, params.token_id)):
            self.token_id_set.add(self.data.all_tokens, params.token_id)
            self.data.token_metadata[params.token_id] = sp.record(
                token_id    = params.token_id,
                token_info  = params.metadata
            )
```

## Basic usage

### Mint

:::caution
The mint entry point that we provide doesn't let you modify the `token_metadata` after the initial mint.
If you want to change this, see [custom entry points](#custom-entrypoints).
:::

Let's defined the metadata of the token we want to mint.

``` python
example_md = FA2.FA2.make_metadata(
    decimals = 0,
    name     = "Example FA2",
    symbol   = "DFA2" )
```

This is equivalent to

``` python
sp.map(l = {
    # Remember that michelson wants map already in ordered
    "decimals" : sp.utils.bytes_of_string("%d" % 0),
    "name" : sp.utils.bytes_of_string("Example FA2"),
    "symbol" : sp.utils.bytes_of_string("DFA2")
}
```

You can also add an icon url or other custom metadata by creating a custom map.

Example in a scenario:

``` python
    example_md = FA2.FA2.make_metadata(
        name     = "Example FA2",
        decimals = 0,
        symbol   = "DFA2" )
    exampleToken.mint(
        address  = FA2_admin.address, # Who will receive the original mint
        token_id = 0,
        amount   = 100_000_000_000,
        metadata = example_md
    ).run(sender = FA2_admin)
```

### Transfer

Transfers are a list of batches. A batch is a list of transactions from one sender.

Batch items can be created by the helper `contract.batch_transfer.item`.

Example:

``` python
c1.transfer(
    [
        c1.batch_transfer.item(from_ = alice.address,
                            txs = [
                                sp.record(to_ = bob.address,
                                            amount = 10,
                                            token_id = 0),
                                sp.record(to_ = bob.address,
                                            amount = 10,
                                            token_id = 1)]),
        c1.batch_transfer.item(from_ = bob.address,
                            txs = [
                                sp.record(to_ = alice.address,
                                            amount = 11,
                                            token_id = 0)])
    ]).run(sender = admin)
```

### Operators

Operators can be modified by calling `update_operators` with a list of variants
that remove or add operators.

Example:

``` python
c1.update_operators([
    sp.variant("remove_operator", c1.operator_param.make(
        owner = alice.address,
        operator = op1.address,
        token_id = 0)),
    sp.variant("add_operator", c1.operator_param.make(
        owner = alice.address,
        operator = op2.address,
        token_id = 0))
]).run(sender = alice)
```

### Ledger keys

All the info about how many tokens are held by an address are in the `ledger` bigmap.

The keys of the bigmap can be created by calling `contract.ledger_key.make(address, token_id)`.

Example:

``` python
scenario.verify(
    c1.data.ledger[c1.ledger_key.make(bob.address, 0)].balance == 10)
```

## Config

The config dictionary contains the meta-programming configuration. It
is used to modify global logic in the FA2 contract that
potentially affects multiple entry points.  All values of the
dictionary are boolean values.

| Key                                | Default | description                                                                                   |
| ---------------------------------- | ------- | --------------------------------------------------------------------------------------------- |
| `add_mutez_transfer`               | False   | Add an entry point for the admin to transfer tez from the contract's balance.                 |
| `allow_self_transfer`              | False   | This contract is as an operator for all addresses/tokens it contains.                         |
| `assume_consecutive_token_ids`     | True    | If `true` don't use a set of token ids, only keep how many there are.                         |
| `debug_mode`                       | False   | Use maps instead of big-maps to simplify the contract's state inspection.                     |
| `force_layouts`                    | True    | Legacy.                                                                                       |
| `lazy_entry_points`                | False   | add flag `lazy-entry-points`                                                                  |
| `non_fungible`                     | False   | Enforce the non-fungibility of the tokens (i.e. total supply has to be 1).                    |
| `readable`                         | True    | Legacy.                                                                                       |
| `single_asset`                     | False   | Save some gas and storage by working only for the token-id `0`.                               |
| `store_total_supply`               | True    | Store the total-supply for each token (next to the token-metadata).                           |
| `support_operator`                 | True    | If False, remove opeartor logic (maintain the presence of the entry point).                   |
| `use_token_metadata_offchain_view` | False   | Include offchain view for accessing the token metadata (requires TZIP-016 contract metadata). |

The config is returned by instantiating the class `FA2_config`.

Example:

``` python
FA2.FA2_config(assume_consecutive_token_ids = False, debug_mode = True)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
