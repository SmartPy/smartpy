# Instantiation

## Basic instantiation

### `Fa2Nft`

``` python

    ExampleFa2Nft(
        metadata = sp.utils.metadata_of_url("http://example.com")
    )
```

### `Fa2Fungible`

``` python
    ExampleFa2Fungible(
        metadata = sp.utils.metadata_of_url("http://example.com")
    )
```

### `Fa2SingleAsset`

``` python
    ExampleFa2SingleAsset(
        metadata = sp.utils.metadata_of_url("http://example.com")
    )

```

## Optional arguments

The base classes accept optional entrypoints.

+ policy: [policies](../policies)
+ metadata_base: [contract metadata](../contract_metadata)
+ ledger and token_metadata: see [pre-minted tokens](../pre-minted_tokens)