import React from 'react';
import CodeBlock from '@theme/CodeBlock';

interface OwnProps {
    title: string;
    language: string;
    metastring?: string;
}

const CodeBlockWithTitle: React.FC<OwnProps> = ({ title, children, language, metastring = '' }) => {
    return (
        <div className="code-with-header">
            <div className="code-header">{title}</div>

            <CodeBlock className={language} metastring={metastring}>
                {children}
            </CodeBlock>
        </div>
    );
};

export default CodeBlockWithTitle;
