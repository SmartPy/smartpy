import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TNat, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type nat_type = ${FrontendType.TNat};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        nat_type: {
            line: ExpectAnyLine,
            type: FrontendType.TNat,
        },
    });
});
