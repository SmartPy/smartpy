import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TMutez, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type mutez_type = ${FrontendType.TMutez};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        mutez_type: {
            line: ExpectAnyLine,
            type: FrontendType.TMutez,
        },
    });
});
