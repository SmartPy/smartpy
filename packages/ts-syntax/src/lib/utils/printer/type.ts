import type { ST_TypeDef } from '../../../@types/type';
import { FrontendType } from '../../../enums/type';
import guards from '../guard';

const toString = (typeDef: ST_TypeDef): string => {
    if (guards.type.hasProperties(typeDef)) {
        return `${typeDef.type}<${JSON.stringify(
            Object.entries<ST_TypeDef>(typeDef.properties).reduce((s, e) => ({ ...s, [e[0]]: toString(e[1]) }), {}),
        )}${typeDef.layout ? `, ${JSON.stringify(typeDef.layout)}` : ''}>`;
    }
    if (guards.type.hasOneArg(typeDef)) {
        return `${typeDef.type}<${toString(typeDef.innerType)}>`;
    }
    switch (typeDef.type) {
        case FrontendType.TList:
        case FrontendType.TSet:
        case FrontendType.TTuple:
            return `${typeDef.type}<[${typeDef.innerTypes.map((t) => toString(t)).join(', ')}]>`;
        case FrontendType.TBig_map:
        case FrontendType.TMap:
            return `${typeDef.type}<${toString(typeDef.keyType)}, ${toString(typeDef.valueType)}>`;
        case FrontendType.TContract:
            return `${typeDef.type}<${toString(typeDef.inputType)}>`;
        case FrontendType.TLambda:
            let inputType: ST_TypeDef | undefined;
            if (Object.keys(typeDef.inputTypes).length > 1) {
                inputType = {
                    type: FrontendType.TRecord,
                    properties: typeDef.inputTypes,
                };
            } else {
                inputType = typeDef.inputTypes[Object.keys(typeDef.inputTypes)[0]];
            }
            return `${typeDef.type}<${toString(inputType)}, ${toString(typeDef.outputType)}>`;
    }

    return typeDef.type;
};

const type = {
    toString,
};

export default type;
