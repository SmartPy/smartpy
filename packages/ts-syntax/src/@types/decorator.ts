import type { ST_DecoratorKind } from '../enums/decorator';
import type { ST_Expression } from './expression';
import type { ST_Flag } from './output';
import type { ST_TypeDef } from './type';

export interface ST_EntryPoint_Decorator {
    kind: ST_DecoratorKind.EntryPoint;
    name: string;
    mock: boolean;
    lazy: boolean;
    lazy_no_code: boolean;
    check_no_incoming_transfer?: boolean;
    type?: ST_TypeDef;
    functionRef: string;
}

export interface ST_View_Decorator {
    kind: ST_DecoratorKind.View;
    offchainOnly: boolean;
    name: string;
    pure: boolean;
    description?: string;
    functionRef: string;
}

export interface ST_Metadata_Decorator {
    kind: ST_DecoratorKind.MetadataBuilder;
    name: string;
    properties: Record<string, ST_Expression>;
}

export interface ST_PrivateLambda_Decorator {
    kind: ST_DecoratorKind.PrivateLambda;
    name: string;
    withOperations: boolean;
    withStorage?: 'read-only' | 'read-write';
    functionRef: string;
}

export interface ST_Contract_Decorator {
    kind: ST_DecoratorKind.Contract;
    name: string;
    flags: ST_Flag[];
}

export interface ST_Inlined_Decorator {
    kind: ST_DecoratorKind.Inline;
    name: string;
}

export type ST_Class_Decorator = ST_Contract_Decorator;
export type ST_Class_Decorators = Record<string, ST_Class_Decorator>;

export type ST_Property_Decorator =
    | ST_EntryPoint_Decorator
    | ST_View_Decorator
    | ST_Metadata_Decorator
    | ST_PrivateLambda_Decorator
    | ST_Inlined_Decorator;
export type ST_Property_Decorators = {
    [ST_DecoratorKind.Contract]?: ST_Contract_Decorator;
    [ST_DecoratorKind.EntryPoint]?: ST_EntryPoint_Decorator;
    [ST_DecoratorKind.Inline]?: ST_Inlined_Decorator;
    [ST_DecoratorKind.MetadataBuilder]?: ST_Metadata_Decorator;
    [ST_DecoratorKind.View]?: ST_View_Decorator;
    [ST_DecoratorKind.PrivateLambda]?: ST_PrivateLambda_Decorator;
};
