import type { ST_TypeDef } from './type';

export type ST_Parameter = {
    name: string;
    type: ST_TypeDef;
};
export type ST_Parameters = Record<string, ST_Parameter>;
