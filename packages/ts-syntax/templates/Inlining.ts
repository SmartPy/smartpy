@Contract
export class Inlining {
    storage = {
        value: 1,
    };

    @EntryPoint
    ep1(): void {
        for (let i = 0; i < 10; i += 1) {
            i += this.inlined1(i);
            this.inlined2(i);
        }
    }

    @EntryPoint
    ep2(): void {
        if (this.inlined1(this.storage.value) > 2) {
            this.storage.value = 0;
        }
    }

    @EntryPoint
    ep3(): void {
        this.storage.value = this.natOfMutez(10);
    }

    @Inline
    inlined1 = (v: TInt): TInt => v + 1;

    @Inline
    inlined2(v: TInt): void {
        for (let ii = 0; ii < 10; ii += 1) {
            this.storage.value += this.inlined1(v + ii);
        }
    }

    @Inline
    natOfMutez = (v: TMutez): TNat =>
        Sp.ediv(v, 1 as TMutez)
            .openSome('Failed to divide mutez')
            .fst();
}

Dev.test({ name: 'Inlining' }, () => {
    const c1 = Scenario.originate(new Inlining());
    Scenario.transfer(c1.ep1());
    Scenario.transfer(c1.ep2());
    Scenario.transfer(c1.ep3());
});

Dev.compileContract('compile_contract', new Inlining());
