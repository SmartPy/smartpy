@Contract
export class Loops {
    storage: TUnit = Sp.unit;

    @EntryPoint
    for(): void {
        for (let i = 0; i < 10; i += 1) {
            i += 1;
        }
    }

    @EntryPoint
    while(): void {
        let i = 1;
        while (i == 1) {
            i += 1;
        }
    }
}

Dev.test({ name: 'Loops' }, () => {
    const c1 = Scenario.originate(new Loops());
    Scenario.transfer(c1.for());
    Scenario.transfer(c1.while());
});

Dev.compileContract('compile_contract', new Loops());
