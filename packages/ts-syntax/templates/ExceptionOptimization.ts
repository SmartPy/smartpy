interface TStorage {
    storedValue: TNat;
}

const initial_storage: TStorage = {
    storedValue: 1,
};

@Contract({
    flags: [['exceptions', 'full-debug']],
})
export class ExceptionOptimization {
    constructor(public storage: TStorage = initial_storage) {}

    @EntryPoint
    check(): void {
        Sp.verify(this.storage.storedValue == 1);
    }
}

// Test

// - Here, the scenario uses the contract flag specified above
Dev.test(
    {
        name: 'test 1',
    },
    () => {
        const c1 = Scenario.originate(new ExceptionOptimization());
        Scenario.h1('Calling check Entrypoint');
        Scenario.transfer(c1.check());
    },
);

// - Here, the scenario overrides the contract flag specified above
Dev.test(
    {
        name: 'test 2',
        flags: [['exceptions', 'unit']],
    },
    () => {
        const c1 = Scenario.originate(new ExceptionOptimization());
        Scenario.h1('Calling check Entrypoint');
        Scenario.transfer(c1.check());
    },
);

// Compilation

Dev.compileContract(
    'compile_contract',
    new ExceptionOptimization({
        storedValue: 10,
    }),
);
