interface TStorage {
    currentDelegate: TOption<TKey_hash>;
    nominatedDelegates: TSet<TKey_hash>;
}

export class SetDelegate {
    constructor(public storage: TStorage) {}

    @EntryPoint
    testDelegate(baker: TKey_hash): void {
        if (Sp.some(baker) !== this.storage.currentDelegate) {
            Sp.setDelegate(Sp.some(baker));
            Sp.setDelegate(this.storage.currentDelegate);

            //this.storage.nominatedDelegates.add(baker);
        }
    }
}

Dev.test({ name: 'SetDelegate' }, () => {
    Scenario.h1('Set delegate');
    const c1 = Scenario.originate(
        new SetDelegate({
            currentDelegate: Sp.some('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
            nominatedDelegates: [],
        }),
    );

    const votingPowers: TMap<TKey_hash, TInt> = [
        ['tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w', 1],
        ['tz1A', 10],
        ['tz1B', 100],
        ['tz1C', 1000],
    ];

    Scenario.p('Test tz1A');
    Scenario.transfer(c1.testDelegate('tz1A'), { votingPowers });
    Scenario.p('Test tz1B');
    Scenario.transfer(c1.testDelegate('tz1B'), { votingPowers });
    Scenario.p('Test tz1C');
    Scenario.transfer(c1.testDelegate('tz1C'), { votingPowers });
    Scenario.p('Test tz1D');
    Scenario.transfer(c1.testDelegate('tz1D'), { votingPowers, valid: false });
});

Dev.compileContract(
    'SetDelegateCompilation',
    new SetDelegate({
        currentDelegate: Sp.some('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
        nominatedDelegates: [],
    }),
);
