# SmartPy frontend

## Technologies

### Package management

This project was created using [create-react-app](https://create-react-app.dev/).

Its configuration is overridden by [craco](https://www.npmjs.com/package/@craco/craco).

This package is maintained under a common folder with other packages which may
have cross-dependencies between each-others. As a consequence, it is managed
using [lerna](https://lerna.js.org/).

### Technologies included:

- [x] Typescript (4.3.4) Adds types on top of javascript
- [X] ReactJS (17.0.8) as web component framework
- [X] Material UI (5.8.4) as UI framework
- [X] Axios (0.21.4) as HTTP client
- [X] React Redux (7.2.5) as State Container
- [X] Jest (26.0.23) as Test framework

## Available Scripts

All the commands must be executed under the appropriate environment (see
[Install environment and dependencies](../../README.md#install-environment-and-dependencies)).

In the project directory, you can run:

### `cd .. && lerna bootstrap`

Install all the package dependencies and links any cross-dependencies.
`npm install` may also work in the package directory but the result can
differ from the CI expectation.

### `npx craco start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npx craco test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npx craco build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm run test -- -u`

Update the snapshots files.