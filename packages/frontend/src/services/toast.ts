import 'react-toastify/dist/ReactToastify.css';
import { toast as toastify } from 'react-toastify';
toastify.configure({ position: 'bottom-right' });

const toast = { ...toastify };

export default toast;
