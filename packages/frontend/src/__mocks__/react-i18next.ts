import { initReactI18next as _initReactI18next, useTranslation as _useTranslation } from 'react-i18next';

const i18next = jest.createMockFromModule<any>('react-i18next');

// eslint-disable-next-line react-hooks/rules-of-hooks
export const useTranslation = (namespaces: any) => _useTranslation(namespaces, { useSuspense: false });
export const initReactI18next = _initReactI18next;

export default i18next;
