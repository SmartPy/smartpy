import React from 'react';
import { Fab as MuiFab, FabProps } from '@mui/material';

const Fab: React.FC<FabProps & { component?: React.ReactNode }> = (props) => {
    return <MuiFab variant="extended" {...props}></MuiFab>;
};

export default Fab;
