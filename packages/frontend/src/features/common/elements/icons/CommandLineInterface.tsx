import React from 'react';

const CommandLineInterfaceIcon = (props: React.SVGProps<SVGSVGElement>) => {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height={24}
            width={24}
            viewBox="0 0 44.8 50.49"
            fill="currentColor"
            {...props}
        >
            <path d="M42.3,45.49H2.5a2.5,2.5,0,0,0,0,5H42.3a2.5,2.5,0,0,0,0-5Z" />
            <path d="M.73,38.38a2.5,2.5,0,0,0,3.54,0L21.33,21.33a2.5,2.5,0,0,0,0-3.54L4.27.73A2.5,2.5,0,1,0,.73,4.27L16,19.56.73,34.85A2.5,2.5,0,0,0,.73,38.38Z" />
        </svg>
    );
};

export default CommandLineInterfaceIcon;
