import httpRequester from '../../../services/httpRequester';

import { downloadFile, getFileContent } from '../../../utils/file';
import { getBase } from '../../../utils/url';
import { getAllTemplates as getPythonTemplates } from '../../py-ide/constants/templates';
import { getAllTemplates as getTypescriptTemplates } from '../../ts-ide/constants/templates';
import { getAllTemplates as getOcamlTemplates } from '../../ml-ide/constants/templates';

/**
 * Get template code.
 * @param {string} templateName - Template name.
 * @return {Promise<string | void>} A promise with the template code, or null if the template doesn't exist.
 */
export const getTemplateCode = async (templateName: string): Promise<string> => {
    if (templateName.endsWith('.py')) {
        return getFileContent(`${getBase()}/templates/${getPythonTemplates()[templateName]?.fileName || templateName}`);
    }
    if (templateName.endsWith('.ts')) {
        return getFileContent(
            `${getBase()}/typescript/templates/${getTypescriptTemplates()[templateName]?.fileName || templateName}`,
        );
    }
    if (templateName.endsWith('.ml')) {
        return getFileContent(
            `${getBase()}/ml_templates/${getOcamlTemplates()[templateName]?.fileName || templateName}`,
        );
    }
    return '';
};

export const downloadOutputPanel = async (
    contractName = 'output-panel',
    isDark: boolean,
    outputPanel: HTMLDivElement,
) => {
    let html = outputPanel.innerHTML;
    if (outputPanel.children[0]?.className.match('MuiAlert')) {
        html = Array.from(outputPanel.children)
            .slice(1)
            .map((c) => c.innerHTML)
            .join('');
    }
    const output = html.replace(/\/static/g, `${window.location.origin}/static`);

    let styles = await (await httpRequester.get(`${window.location.origin}/static/css/smart.css`)).data;
    styles += await (await httpRequester.get(`${window.location.origin}/static/css/typography.css`)).data;
    styles = styles.replace(/\/static/g, `${window.location.origin}/static`);
    const script = await (await httpRequester.get(`${window.location.origin}/static/js/smart.js`)).data;
    downloadFile(
        `${contractName}.html`,
        `<html>
            <head>
                <style>${styles}</style>
                <script>${script}</script>
            </head>
            <body>
                <div id="outputPanel">${output}</div>
            </body>
        </html>`,
    );
};
