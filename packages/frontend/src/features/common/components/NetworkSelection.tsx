import React from 'react';
import { useDispatch } from 'react-redux';

import {
    Divider,
    FormControl,
    Grid,
    ListSubheader,
    MenuItem,
    Paper as MuiPaper,
    Select as MuiSelect,
    SelectChangeEvent,
    styled,
    TextField as MuiTextField,
    Typography,
} from '@mui/material';

import NodeData from '../containers/NodeData';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Nodes from 'src/constants/rpc';
import { useNetworkInfo } from 'src/features/wallet/selectors/network';
import WalletActions from 'src/features/wallet/actions';
import { getRpcNetwork } from 'src/utils/tezosRpc';
import { DeprecatedNetworks } from 'src/constants/networks';

const Paper = styled(MuiPaper)(() => ({
    padding: 10,
    boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
}));

const Select = styled<any>(MuiSelect)`
    & .MuiSelect-filled {
        padding: 10px 12px 8px;
    }
`;

const TextField = styled<any>(MuiTextField)`
    & .MuiFilledInput-input {
        padding: 10px 12px 8px;
    }
`;

const NetworkSelection = () => {
    const t = useTranslation();
    const dispatch = useDispatch();
    const { rpc } = useNetworkInfo();

    const networkSelectValue = React.useMemo(() => (rpc && Object.keys(Nodes).includes(rpc) ? rpc : 'CUSTOM'), [rpc]);

    const onNetworkSelection = (event: SelectChangeEvent<string>) => {
        const rpc = event.target.value;
        if (Nodes[rpc]) {
            dispatch(
                WalletActions.updateNetworkInfo({
                    network: Nodes[rpc],
                    rpc,
                }),
            );
        }
    };

    const onRpcChange = async (event: React.ChangeEvent<{ value: string }>) => {
        const rpc = event.target.value;
        dispatch(WalletActions.updateNetworkInfo({ rpc: rpc }));

        const network = Nodes[rpc] || (await getRpcNetwork(rpc));

        dispatch(WalletActions.updateNetworkInfo({ network, rpc }));
    };

    return (
        <Paper>
            <Typography variant="h6">
                {t('network.nodeAndNetwork')} <Typography variant="caption">{t('network.useYourOwnNode')}</Typography>
            </Typography>
            <Divider sx={{ margin: 1 }} />
            <Grid container spacing={2}>
                <Grid item xs={12} sm={3}>
                    <FormControl variant="filled" fullWidth>
                        <Select value={networkSelectValue} onChange={onNetworkSelection}>
                            <ListSubheader>SmartPy.io Nodes</ListSubheader>
                            {Object.keys(Nodes)
                                .filter((key) => key.includes('smartpy'))
                                .filter((key) => !DeprecatedNetworks.includes(Nodes[key]))
                                .map((rpc) => (
                                    <MenuItem key={rpc} value={rpc}>
                                        {Nodes[rpc]}
                                    </MenuItem>
                                ))}

                            {networkSelectValue === 'CUSTOM' ? (
                                <MenuItem disabled value="CUSTOM" sx={{ display: 'none' }}>
                                    <em>{t('common.customRPC')}</em>
                                </MenuItem>
                            ) : null}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField fullWidth variant="filled" value={rpc || ''} onChange={onRpcChange} />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <NodeData />
                </Grid>
            </Grid>
        </Paper>
    );
};

export default NetworkSelection;
