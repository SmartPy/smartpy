import { styled, Paper as MuiPaper } from '@mui/material';

const Paper = styled(MuiPaper)(({ theme }) => ({
    boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
    padding: 10,
}));

export default Paper;
