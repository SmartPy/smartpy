import React from 'react';

import { Dialog as MuiDialog, DialogTitle, DialogActions, DialogContent, Button, Slide } from '@mui/material';

import useTranslation from '../../i18n/hooks/useTranslation';

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="down" ref={ref} {...props} />;
});

interface OwnProps {
    open: boolean;
    onClose: () => void;
    title: string;
}

const Dialog: React.FC<OwnProps> = ({ children, open, onClose, title }) => {
    const t = useTranslation();

    return (
        <MuiDialog
            maxWidth="md"
            fullWidth
            open={open}
            onClose={onClose}
            aria-labelledby="dialog-title"
            TransitionComponent={Transition}
        >
            <DialogTitle id="dialog-title">{title}</DialogTitle>
            <DialogContent dividers>{children}</DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    {t('common.close')}
                </Button>
            </DialogActions>
        </MuiDialog>
    );
};

export default Dialog;
