import React from 'react';

// Material UI
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
// Material Icons
import SaveIcon from '@mui/icons-material/Save';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    open: boolean;
    handleClose: () => void;
    handleCreateContract: (name: string) => void;
}

const CreateContractDialog: React.FC<OwnProps> = ({ open, handleClose, handleCreateContract }) => {
    const t = useTranslation();
    const textInputRef = React.useRef(null as unknown as HTMLInputElement);
    const [name, setName] = React.useState('');

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleSave = () => {
        handleCreateContract(name);
        handleClose();
        setName('');
    };

    return (
        <Dialog fullWidth open={open} onClose={handleClose}>
            <DialogTitle>{t('ide.contractManagement.createContract')}</DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputProps={{
                        maxLength: 32,
                        minLength: 1,
                    }}
                    ref={textInputRef}
                    fullWidth
                    label={t('common.contract.name')}
                    value={name}
                    variant="filled"
                    size="small"
                    onChange={handleNameChange}
                />
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose}>
                    {t('common.cancel')}
                </Button>
                <Button startIcon={<SaveIcon />} color="primary" onClick={handleSave}>
                    {t('common.save')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateContractDialog;
