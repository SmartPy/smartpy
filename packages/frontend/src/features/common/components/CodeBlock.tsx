import React from 'react';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import json from 'react-syntax-highlighter/dist/cjs/languages/hljs/json';
import python from 'react-syntax-highlighter/dist/cjs/languages/hljs/python';
import typescript from 'react-syntax-highlighter/dist/cjs/languages/hljs/typescript';
import a11yDark from 'react-syntax-highlighter/dist/cjs/styles/hljs/a11y-dark';
import github from 'react-syntax-highlighter/dist/cjs/styles/hljs/github';

import { Theme, styled, Box } from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

// Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
// Services
import toast from '../../../services/toast';
// Utils
import { copyToClipboard } from '../../../utils/clipboard';
// State Management
import selectors from '../../../store/selectors';
import { appendClasses } from '../../../utils/style/classes';
import CodeEditor from './CodeEditor';
import BaseButton from '../elements/Button';

SyntaxHighlighter.registerLanguage('json', json);
SyntaxHighlighter.registerLanguage('python', python);
SyntaxHighlighter.registerLanguage('typescript', typescript);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        syntaxHighlighter: {
            margin: 0,
            minHeight: 70,
            height: '100%',
        },
        round: {
            borderRadius: 10,
        },
    }),
);

const Button = styled(BaseButton)(({ theme }) => ({
    margin: theme.spacing(1),
}));

interface OwnProps {
    square?: boolean;
    withCopy?: boolean;
    language: string;
    showLineNumbers: boolean;
    text: string;
    wrapLongLines?: boolean;
    editProps?: {
        open: boolean;
        onOpenChange: () => void;
        onCodeChange: (value: string) => void;
    };
}

const CodeBlockWithCopy: React.FC<OwnProps> = ({
    withCopy = true,
    text,
    square = true,
    wrapLongLines = false,
    editProps,
    ...props
}) => {
    const classes = useStyles();
    const isDarkMode = selectors.theme.useThemeMode();
    const t = useTranslation();

    const copy = () => {
        copyToClipboard(text);
        toast.info(t('common.copied'));
    };

    const renderButtons = () => (
        <Box
            sx={{
                position: 'absolute',
                top: 10,
                right: 20,
                zIndex: 10,
            }}
        >
            {withCopy ? (
                <Button variant="outlined" onClick={copy}>
                    {t('common.copy')}
                </Button>
            ) : null}
            {!!editProps ? (
                <Button variant="contained" onClick={editProps.onOpenChange}>
                    {editProps.open ? t('common.viewMode') : t('common.editMode')}
                </Button>
            ) : null}
        </Box>
    );

    return (
        <Box
            sx={{
                position: 'relative',
                height: 'calc(100% - 13px)',
            }}
        >
            {renderButtons()}
            {editProps && editProps.open ? (
                <CodeEditor mode="json" value={text || ''} onChange={editProps.onCodeChange} />
            ) : (
                <SyntaxHighlighter
                    {...{
                        ...props,
                        className: square
                            ? classes.syntaxHighlighter
                            : appendClasses(classes.syntaxHighlighter, classes.round),
                    }}
                    style={isDarkMode ? a11yDark : github}
                    wrapLines
                    wrapLongLines={wrapLongLines}
                >
                    {text}
                </SyntaxHighlighter>
            )}
        </Box>
    );
};

export default CodeBlockWithCopy;
