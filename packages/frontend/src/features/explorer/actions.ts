import { ContractInformation } from 'SmartPyModels';
import { createAction } from 'typesafe-actions';

export enum Actions {
    EXPLORER_UPDATE_CONTRACT_ADDRESS = 'EXPLORER_UPDATE_CONTRACT_ADDRESS',
    // Contract
    EXPLORER_CLEAR_CONTRACT_INFO = 'EXPLORER_CLEAR_CONTRACT_INFO',
    EXPLORER_UPDATE_CONTRACT_INFO = 'EXPLORER_UPDATE_EXPLORER_INFO',
    EXPLORER_LOADING_CONTRACT_INFO = 'EXPLORER_LOADING_EXPLORER_INFO',
    // ERROR DIALOG
    EXPLORER_SHOW_ERROR = 'EXPLORER_SHOW_ERROR',
    EXPLORER_HIDE_ERROR = 'EXPLORER_HIDE_ERROR',
}

export const updateContractInfo = createAction(Actions.EXPLORER_UPDATE_CONTRACT_INFO)<ContractInformation>();
export const clearContractInfo = createAction(Actions.EXPLORER_CLEAR_CONTRACT_INFO)();
export const loadingContractInfo = createAction(Actions.EXPLORER_LOADING_CONTRACT_INFO)<boolean>();

// ERROR

export const showError = createAction(Actions.EXPLORER_SHOW_ERROR, (error: string) => error)<string>();
export const hideError = createAction(Actions.EXPLORER_HIDE_ERROR)<void>();

const actions = {
    updateContractInfo,
    clearContractInfo,
    loadingContractInfo,
    showError,
    hideError,
};

export default actions;
