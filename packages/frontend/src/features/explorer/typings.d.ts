declare module 'SmartPyModels' {
    type operationsDetails = {
        ops: any;
        nOps?: int;
        firstSeen?: string;
        lastSeen?: string;
    };

    export interface CallError {
        url: string;
        status: number;
        message: string;
    }

    export type ContractInformation = {
        isLoading?: boolean;
        isOperationsLoading?: boolean;
        operationsErrors?: string;
        opDetails?: operationsDetails;
        callErrors?: CallError[];
        address?: string;
        balance?: string;
        valid?: boolean;
        storage?: string;
    };

    export interface ExplorerSmartMLOutputs {
        errors?: string;
        entrypoint?: string;
        paramsJSON?: string;
        paramsMicheline?: string;
        paramsJSONFull?: string;
        paramsMichelineFull?: string;
        messageBuilder?: JSX.Element;
        storageDiv?: JSX.Element;
        operationsList?: JSX.Element;
        types?: JSX.Element;
    }
}

declare module 'TransferTypes' {
    export type TransferParameters = {
        destination: string;
        parameters?: {
            entrypoint: string;
            value: any;
        };
        amount: string;
        fee: string;
        gas_limit: string;
        storage_limit: string;
    };
}
