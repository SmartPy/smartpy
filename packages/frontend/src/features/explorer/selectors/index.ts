import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { ContractInformation } from 'SmartPyModels';

/**
 * @summary Get the contract information
 * @param {RootState} state - Redux state
 * @return {ContractInformation} contract information
 */
export const useContractInfo = (): ContractInformation =>
    useSelector<RootState, ContractInformation>((state) => state.explorer.contractInfo);
