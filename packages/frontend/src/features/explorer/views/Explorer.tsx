import React from 'react';

// Material UI
import { styled, Box, Typography, Link } from '@mui/material';

import ContractInfo from '../components/ContractInfo';
import ContractCard from '../components/ContractCard';
import ExplorerSetup from '../components/ExplorerSetup';
import { ExplorerSmartMLOutputs } from 'SmartPyModels';

const Container = styled(Box)(() => ({
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    padding: 20,
}));

interface OwnProps {
    outputs: ExplorerSmartMLOutputs;
    resetSmartMLOutputs: () => void;
}

const ExplorerView: React.FC<OwnProps> = ({ outputs, resetSmartMLOutputs }) => {
    return (
        <Container>
            <ExplorerSetup resetSmartMLOutputs={resetSmartMLOutputs} />
            <ContractCard />
            <ContractInfo outputs={outputs} />
            <Link
                variant="caption"
                sx={{ textAlign: 'right', marginTop: 1 }}
                target="_blank"
                href="/node_explorer.html"
            >
                Node Explorer
            </Link>
        </Container>
    );
};

export default ExplorerView;
