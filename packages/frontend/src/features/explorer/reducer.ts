import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

import { clearContractInfo, hideError, loadingContractInfo, showError, updateContractInfo } from './actions';
import { ContractInformation } from 'SmartPyModels';

const contractInfoReducer = createReducer<ContractInformation>({})
    .handleAction(updateContractInfo, (state, { payload }) => ({ ...state, ...payload }))
    .handleAction(clearContractInfo, () => ({ isLoading: false }))
    .handleAction(loadingContractInfo, (state, { payload }) => ({
        ...state,
        isLoading: payload,
    }));

// ERRORS REDUCER

const errorReducer = createReducer('')
    .handleAction(hideError, () => '')
    .handleAction(showError, (_, { payload }) => payload);

const persistConfig = {
    keyPrefix: 'smartpy@',
    key: 'explorer',
    storage: storage,
    whitelist: [],
};

const explorerReducer = combineReducers({
    contractInfo: contractInfoReducer,
    error: errorReducer,
});

export default persistReducer(persistConfig, explorerReducer);
