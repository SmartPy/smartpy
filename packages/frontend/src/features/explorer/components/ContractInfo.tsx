import React from 'react';
import { useDispatch } from 'react-redux';

import { Alert, AppBar, Box, Divider, Tab, Tabs } from '@mui/material';

import TabPanel, { TabPanelInnerHTML } from 'src/features/common/elements/TabPanel';
import OutputCss from 'src/features/common/styles/OutputCss';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import WalletServices from 'src/features/wallet/services';
import { useContractInfo } from '../selectors';
import TransferParameters, { DEFAULT_PARAMETERS, FIELDS } from './TransferParameters';
import { AmountUnit, convertUnitNumber } from 'src/utils/units';
import Logger from 'src/services/logger';
import { showError } from '../actions';
import { useAccountInfo } from 'src/features/wallet/selectors/account';
import Paper from 'src/features/common/components/Paper';
import CodeBlock from 'src/features/common/components/CodeBlock';
import { ExplorerSmartMLOutputs } from 'SmartPyModels';
import Dialog from 'src/features/common/components/Dialog';
import CircularProgressWithText from '../../loader/components/CircularProgressWithText';
import { Trans } from 'react-i18next';

interface ContractInfoProps {
    outputs: ExplorerSmartMLOutputs;
}

const ContractInfo: React.FC<ContractInfoProps> = ({ outputs }) => {
    const t = useTranslation();
    const contractInfo = useContractInfo();
    const accountInfo = useAccountInfo();
    const [tab1, setTab1] = React.useState(0);
    // const [operations, setOperations] = React.useState<Operation[]>([]);
    const [parameters, setParameters] = React.useState(DEFAULT_PARAMETERS);
    const [operation, setOperation] = React.useState<string>();
    const dispatch = useDispatch();

    React.useEffect(() => {
        (window as any).popupJson = (_title: string, opJSON: string) => {
            setOperation(JSON.stringify(opJSON, null, 4));
        };
        // if (contractInfo.address) {
        //      fetchContractOperations(contractInfo.address).then(setOperations);
        // }
    }, []);

    const estimateTransactionCost = React.useCallback(async () => {
        try {
            if (
                accountInfo?.source &&
                WalletServices[accountInfo.source] &&
                outputs.entrypoint &&
                outputs.paramsJSON &&
                contractInfo.address
            ) {
                const estimation = await WalletServices[accountInfo.source].estimateTransaction(contractInfo.address, {
                    entrypoint: outputs.entrypoint,
                    value: JSON.parse(outputs.paramsJSON),
                });

                if (estimation) {
                    setParameters((state) => ({
                        ...state,
                        [FIELDS.FEE]: convertUnitNumber(estimation.fee, AmountUnit.uTez, AmountUnit.tez).toNumber(),
                        [FIELDS.GAS_LIMIT]: estimation.gasLimit,
                        [FIELDS.STORAGE_LIMIT]: estimation.storageLimit,
                    }));
                }
            }
        } catch (e: any) {
            const translation = t('origination.couldNotEstimateCosts');
            dispatch(showError(e.message));
            Logger.warn(translation, e);
        }
    }, [accountInfo.source, contractInfo.address, dispatch, outputs.entrypoint, outputs.paramsJSON, t]);

    const handleTab1Change = (event: unknown, newValue: number) => {
        setTab1(newValue);
    };

    const handleParameterInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setParameters((parameters) => ({ ...parameters, [event.target.id]: event.target.value }));
    };

    if (!contractInfo.valid || !contractInfo.address) {
        return null;
    }

    return (
        <Box sx={{ width: '100%' }}>
            {outputs['types'] ? (
                <OutputCss>
                    <AppBar position="static" color="secondary">
                        <Tabs value={tab1} onChange={handleTab1Change} aria-label="Contract Menu" variant="fullWidth">
                            <Tab label={t('explorer.tabLabels.storage')} />
                            <Tab label={t('explorer.tabLabels.interact')} />
                            <Tab label={t('explorer.tabLabels.operations')} />
                            <Tab label={t('explorer.tabLabels.codeRaw')} />
                        </Tabs>
                    </AppBar>
                    <TabPanel value={tab1} index={0}>
                        <Paper sx={{ padding: 2, overflow: 'auto', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                            {outputs['storageDiv'] || null}
                        </Paper>
                    </TabPanel>
                    <TabPanelInnerHTML value={tab1} index={1}>
                        <Paper sx={{ padding: 2, overflow: 'auto', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                            <TransferParameters
                                messageBuilder={outputs['messageBuilder'] as JSX.Element}
                                parameters={parameters}
                                handleParameterInput={handleParameterInput}
                                estimateTransactionCost={estimateTransactionCost}
                                outputs={outputs}
                            />
                        </Paper>
                    </TabPanelInnerHTML>
                    <TabPanel value={tab1} index={2}>
                        <Paper sx={{ padding: 2, overflow: 'auto', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                            {contractInfo.isOperationsLoading ? (
                                <CircularProgressWithText size={64} margin={40} msg={t('common.loading')} />
                            ) : contractInfo.operationsErrors ? (
                                <Alert sx={{ marginTop: 2 }} severity="error">
                                    {contractInfo.operationsErrors}
                                </Alert>
                            ) : contractInfo.opDetails?.nOps > 0 ? (
                                <>
                                    <p>
                                        {t('explorer.operations.summary', contractInfo.opDetails)}
                                        <br />
                                        <Trans i18nKey="explorer.operations.builtWith">
                                            Built with <a href="https://tzkt.io">TzKT API</a>.
                                        </Trans>
                                        {contractInfo.opDetails?.nOps > 100 && (
                                            <>
                                                <br />
                                                {t('explorer.operations.details', {
                                                    firstOp: contractInfo.opDetails?.nOps - 100,
                                                    lastOp: contractInfo.opDetails?.nOps + 1,
                                                })}
                                            </>
                                        )}
                                    </p>
                                    <button
                                        onClick={() =>
                                            setOperation(JSON.stringify(contractInfo.opDetails?.ops, null, 4))
                                        }
                                    >
                                        View full data
                                    </button>
                                    <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                                    {outputs['operationsList']}
                                </>
                            ) : (
                                <p>{t('explorer.errors.noOperationFound')}</p>
                            )}
                            {/* <Operations rowsPerPage={20} operations={operations} /> */}
                        </Paper>
                        <Dialog title="Operation Details" open={!!operation} onClose={() => setOperation(undefined)}>
                            <CodeBlock
                                language="json"
                                text={operation || ''}
                                withCopy={false}
                                showLineNumbers={false}
                            />
                        </Dialog>
                    </TabPanel>
                    <TabPanelInnerHTML value={tab1} index={3}>
                        <Paper sx={{ padding: 2, overflow: 'auto', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                            {outputs['types'] || null}
                        </Paper>
                    </TabPanelInnerHTML>
                </OutputCss>
            ) : (
                <></>
            )}
        </Box>
    );
};

export default ContractInfo;
