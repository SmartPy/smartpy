import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { ST_Error } from '@smartpy/ts-syntax/dist/types/lib/utils/Error';

export const useError = () => useSelector<RootState, string | ST_Error>((state) => state.tsIDE.error);
