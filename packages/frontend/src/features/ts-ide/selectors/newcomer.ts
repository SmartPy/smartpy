import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';

export const useNewcomerMode = () =>
    useSelector<RootState, boolean>((state) => state.tsIDE.settings.newcomersMode || false);

export const useNewcomerDialogOpen = () => useSelector((state: RootState) => state.tsIDE.newcomerDialogOpen);

export const useNewcomerGuideStep = () => useSelector((state: RootState) => state.tsIDE.newcomerGuideStep);

export const useFavoriteTemplates = () =>
    useSelector<RootState, string[]>((state) => state.tsIDE?.favoriteTemplates || []);
