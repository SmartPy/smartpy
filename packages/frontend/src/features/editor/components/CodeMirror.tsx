import React from 'react';
import { defineSmartPyComponents } from '../../explorer/components/smartPyGlue';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace JSX {
        interface IntrinsicElements {
            'sp-codemirror': any;
        }
    }
}

defineSmartPyComponents();

interface OwnProps {
    theme: string;
    value?: string;
    language?: string;
    onChange: (code?: any) => void;
    onMount: (editor: any) => void;
    fontSize?: number;
    compileContract?: () => Promise<void>;
}

const CodeMirror: React.FC<OwnProps> = ({ ...props }) => {
    const ref = React.useRef<any>();
    const [value, setValue] = React.useState(props.value);

    React.useEffect(() => {
        window.showLine = (line: number, column = 0, endLine?: number, endColumn?: number) => {
            if (ref.current) {
                const cmView = ref.current.shadowRoot.querySelector('.cm-content').cmView.valueOf().view;
                const lineSelection = cmView.state.doc.line(line);
                if (endLine) {
                    const endLineSelection = cmView.state.doc.line(endLine);
                    cmView.dispatch({
                        selection: { anchor: endLineSelection.from + endColumn, head: lineSelection.from + column },
                        scrollIntoView: true,
                    });
                } else {
                    cmView.dispatch({ selection: { anchor: lineSelection.from + column }, scrollIntoView: true });
                }
            }
        };

        const onChange = (event: any) => {
            const value = event.detail[0].value;
            setValue(event.detail[0].value);
            props.onChange(value);
        };

        const keydown = (event: KeyboardEvent) => {
            if (event.ctrlKey && event.key === 'Enter') {
                if (props.compileContract !== undefined) {
                    props.compileContract();
                }
            }
        };

        const element = ref.current;
        if (element) {
            element.addEventListener('onChange', onChange);
            element.getValue = () => value;
            props.onMount(element);
            element.addEventListener('keydown', keydown);
        }

        return () => {
            if (element) {
                element.removeEventListener('onChange', onChange);
                element.removeEventListener('keydown', keydown);
            }
        };
    }, [ref, props, value]);

    return (
        <sp-codemirror
            theme={props.theme}
            disabled={false}
            indent_with_tab={true}
            tab_size={4}
            autofocus={true}
            height="100%"
            language="python"
            code={props.value}
            ref={ref}
            font_size={props.fontSize}
            style={{ flexGrow: 1 }}
        />
    );
};

export default CodeMirror;
