import React, { useEffect } from 'react';
import Logger from 'src/services/logger';
import WalletContext from './WalletContext';
import WalletStorage from 'smartpy-wallet-storage';
import { OriginatedContract } from 'smartpy-wallet-storage/dist/types/services/contract/storage';
import { FaucetAccount } from 'smartpy-wallet-storage/dist/types/services/faucet/storage';
import { TezosAccount } from 'smartpy-wallet-storage';
import { lookupContractNetwork } from 'src/utils/tezosRpc';

const WalletProvider: React.FC = ({ children }) => {
    const [loading, setLoading] = React.useState(true);
    const [contracts, setContracts] = React.useState<OriginatedContract[]>([]);
    const [faucetAccounts, setFaucetAccounts] = React.useState<FaucetAccount[]>([]);
    const [accounts, setAccounts] = React.useState<TezosAccount[]>([]);

    useEffect(() => {
        const onReceiveMessage = (e: any) => {
            if (e.key === 'wallet') {
                WalletStorage.account.reloadFromStorage();
                fetchAccountsFromStorage();
            }
        };
        window.addEventListener('storage', onReceiveMessage);
        return () => window.removeEventListener('storage', onReceiveMessage);
    }, [accounts, setAccounts]);

    const fetchContractsFromStorage = () => {
        const contracts = WalletStorage.contract.findAll();
        setContracts(Object.values(contracts));
    };

    const fetchFaucetAccountsFromStorage = () => {
        const accounts = WalletStorage.faucet.findAll();
        setFaucetAccounts(Object.values(accounts));
    };

    const fetchAccountsFromStorage = () => {
        const accounts = WalletStorage.account.findAll();
        setAccounts(Object.values(accounts));
    };

    React.useEffect(() => {
        try {
            fetchContractsFromStorage();
            fetchFaucetAccountsFromStorage();
            fetchAccountsFromStorage();
            setLoading(false);
        } catch (e) {
            Logger.debug(e);
        }
    }, []);

    const resetAccounts = () => {
        WalletStorage.account.clear();
        fetchAccountsFromStorage();
    };

    const removeContracts = (ids: string[]) => {
        try {
            ids.forEach(WalletStorage.contract.removeById);
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const removeFaucetAccounts = (ids: string[]) => {
        try {
            ids.forEach(WalletStorage.faucet.removeById);
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const removeAccounts = (ids: string[]) => {
        try {
            ids.forEach(WalletStorage.account.removeById);
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addContract = async (contract: { name: string; address: string }) => {
        try {
            WalletStorage.contract.persist({
                ...contract,
                network: (await lookupContractNetwork(contract.address)) || undefined,
            });
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateContract = async (contract: OriginatedContract) => {
        try {
            WalletStorage.contract.merge({
                ...contract,
            });
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addFaucetAccount = async (account: FaucetAccount) => {
        try {
            WalletStorage.faucet.persist(account);
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateFaucetAccount = async (account: FaucetAccount) => {
        try {
            WalletStorage.faucet.merge({
                ...account,
            });
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addAccount = async (account: Omit<TezosAccount, 'id'>, password?: string) => {
        try {
            WalletStorage.account.addAccount(
                { ...account, id: '' } as TezosAccount,
                password ? btoa(password) : undefined,
            );
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const getDecryptedAccount = async (account: TezosAccount, password: string): Promise<TezosAccount> => {
        return WalletStorage.account.getDecryptedAccount(account, password);
    };

    const updateAccountName = async (id: string, name: string) => {
        try {
            WalletStorage.account.updateName(id, name);
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateAccountPreferredRpc = async (id: string, preferredRpc: string) => {
        try {
            WalletStorage.account.updatePreferredRpc(id, preferredRpc);
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    return (
        <WalletContext.Provider
            value={{
                loading,
                contracts,
                accounts,
                faucetAccounts,
                removeContracts,
                removeFaucetAccounts,
                removeAccounts,
                addContract,
                addFaucetAccount,
                addAccount,
                updateContract,
                updateFaucetAccount,
                updateAccountName,
                updateAccountPreferredRpc,
                resetAccounts,
                getDecryptedAccount,
            }}
        >
            {children}
        </WalletContext.Provider>
    );
};

export default WalletProvider;
