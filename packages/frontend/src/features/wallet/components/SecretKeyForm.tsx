import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';

import useTranslation from '../../i18n/hooks/useTranslation';
import { useDispatch } from 'react-redux';
import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import WalletServices from '../services';
import { getRpcNetwork } from '../../../utils/tezosRpc';
import { useNetworkInfo } from '../selectors/network';
import AccountInfo from './AccountInfo';
import logger from '../../../services/logger';
import { AccountSource } from '../constants/sources';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 5,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
        },
        fullHeight: {
            height: '100%',
        },
        section: {
            padding: 20,
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
    }),
);

enum FormField {
    SECRET_KEY = 'secretKey',
    PASSWORD = 'password',
}

const DEFAULT_FORM_VALUES = {
    [FormField.SECRET_KEY]: '',
    [FormField.PASSWORD]: '',
};

interface OwnProps {
    disableNetworkSelection?: boolean;
}

const SecretKeyForm: React.FC<OwnProps> = ({ disableNetworkSelection = true }) => {
    const classes = useStyles();
    const [values, setValues] = React.useState(DEFAULT_FORM_VALUES);
    const [showPassword, setShowPassword] = React.useState(false);
    const t = useTranslation();
    const dispatch = useDispatch();
    const { rpc } = useNetworkInfo();

    const showPasswordSwitch = () => {
        setShowPassword((v) => !v);
    };

    const handleInputChange = (
        event: React.ChangeEvent<{
            name?: string | undefined;
            value: string;
        }>,
    ) => {
        const { name, value } = event.target;
        if (name) {
            setValues((state) => ({ ...state, [name]: value }));
        }
    };

    const onConnect = async () => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.SMARTPY_SECRET_KEY].import(rpc, values);
                const accountInformation = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e: any) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    return (
        <div className={classes.root}>
            <Typography variant="overline">{t('wallet.secretKeyForm.title')}</Typography>
            <Paper className={classes.section}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            name={FormField.SECRET_KEY}
                            fullWidth
                            variant="filled"
                            label={t('wallet.secretKeyForm.secretKey')}
                            value={values[FormField.SECRET_KEY]}
                            onChange={handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            name={FormField.PASSWORD}
                            fullWidth
                            variant="filled"
                            label={t('wallet.secretKeyForm.passphrase')}
                            value={values[FormField.PASSWORD]}
                            onChange={handleInputChange}
                            InputProps={{
                                type: showPassword ? 'text' : 'password',
                                endAdornment: (
                                    <Tooltip
                                        title={
                                            (showPassword
                                                ? t('wallet.secretKeyForm.hidePassphrase')
                                                : t('wallet.secretKeyForm.showPassphrase')) as string
                                        }
                                        aria-label="show-password"
                                        placement="left"
                                    >
                                        <span>
                                            <IconButton color="primary" onClick={showPasswordSwitch} size="large">
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </span>
                                    </Tooltip>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.fullHeight}
                            onClick={onConnect}
                        >
                            {t('common.connect')}
                        </Button>
                    </Grid>
                </Grid>
            </Paper>

            <AccountInfo disableNetworkSelection={disableNetworkSelection} />
        </div>
    );
};

export default SecretKeyForm;
