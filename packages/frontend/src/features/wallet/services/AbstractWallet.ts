import TezMonitor from 'tezmonitor';
import { localForger } from '@taquito/local-forging';
import { RpcClient } from '@taquito/rpc';

import { OperationKindType, TezosConstants, TezosNodeReader, TezosNodeWriter } from '../../../services/conseil';

import WalletError, { WalletGenericErrorCodes } from '../exceptions/WalletError';
import {
    CallParams,
    OperationCostsEstimation,
    OperationInformation,
    OperationResults,
    ScriptParams,
} from 'SmartPyWalletTypes';
import { Signer } from './Signer';
import { OriginationParameters } from 'OriginationTypes';
import { getBlake2BHash } from '../../../utils/hash';
import { arrayOrEmptyArray } from '../../../utils/object';
import { getContractFromOriginationResult } from '../../origination/util/operation';
import { AccountSource } from '../constants/sources';
import { AccountInformation } from 'SmartPyModels';
import { AmountUnit, convertUnitNumber } from '../../../utils/units';
import { firstValueFrom } from 'rxjs';
import { TransferParameters } from 'TransferTypes';

abstract class AbstractWallet {
    protected source;
    protected rpc: string | null = null;
    protected pkh?: string;
    protected signer?: Signer;

    constructor(source: AccountSource) {
        this.source = source;
    }

    /**
     * @description Set RPC address.
     *
     * @returns {void}
     */
    public async setRPC(rpc: string) {
        this.rpc = rpc;
    }

    /**
     * @description Estimate origination costs
     *
     * @param parameters Script parameters
     *
     * @returns {Promise<OperationCostsEstimation>}
     */
    public async estimateOrigination(parameters: ScriptParams): Promise<OperationCostsEstimation> {
        if (!this.pkh) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_SOURCE);
        }
        if (!this.rpc) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_RPC);
        }

        const counter = await TezosNodeReader.getCounterForAccount(this.rpc, this.pkh);
        const estimate = await this.estimateOperation(this.rpc, 'main', {
            kind: OperationKindType.Origination,
            script: {
                code: parameters.code,
                storage: parameters.storage,
            },
            balance: '0',
            fee: String(TezosConstants.DefaultAccountOriginationFee),
            gas_limit: String(TezosConstants.OperationGasCap),
            storage_limit: String(32768),
            counter: String(counter + 1),
            source: this.pkh,
        });
        return {
            fee: estimate?.estimatedFee || TezosConstants.DefaultAccountOriginationFee,
            gasLimit: estimate?.gas || TezosConstants.DefaultAccountOriginationGasLimit,
            storageLimit: estimate?.storageCost || TezosConstants.DefaultAccountOriginationStorageLimit,
        };
    }

    /**
     * @description Estimate transaction costs
     *
     * @param {CallParams} parameters Script parameters
     *
     * @returns {Promise<OperationCostsEstimation>}
     */
    public async estimateTransaction(destination: string, parameters: CallParams): Promise<OperationCostsEstimation> {
        if (!this.pkh) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_SOURCE);
        }
        if (!this.rpc) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_RPC);
        }

        const counter = await TezosNodeReader.getCounterForAccount(this.rpc, this.pkh);
        const estimate = await this.estimateOperation(this.rpc, 'main', {
            kind: OperationKindType.Transaction,
            destination,
            parameters: parameters,
            amount: '0',
            fee: String(TezosConstants.DefaultAccountOriginationFee),
            gas_limit: String(TezosConstants.OperationGasCap),
            storage_limit: String(32768),
            counter: String(counter + 1),
            source: this.pkh,
        });
        return {
            fee: estimate?.estimatedFee || TezosConstants.DefaultAccountOriginationFee,
            gasLimit: estimate?.gas || TezosConstants.DefaultAccountOriginationGasLimit,
            storageLimit: estimate?.storageCost || TezosConstants.DefaultAccountOriginationStorageLimit,
        };
    }

    /**
     * Override conseiljs implementation
     */
    async estimateOperation(
        server: string,
        chainid: string,
        ...operations: any[]
    ): Promise<{ gas: number; storageCost: number; estimatedFee: number; estimatedStorageBurn: number }> {
        const naiveOperationGasCap = Math.min(
            Math.floor(TezosConstants.BlockGasCap / operations.length),
            TezosConstants.OperationGasCap,
        ).toString();
        const localOperations = [...operations].map((o) => {
            return {
                ...o,
                gas_limit: naiveOperationGasCap,
                storage_limit: TezosConstants.OperationStorageCap.toString(),
            };
        });

        const responseJSON = await TezosNodeWriter.dryRunOperation(server, chainid, localOperations);

        let gas = 0;
        let storageCost = 0;
        let staticFee = 0;
        for (const c of responseJSON['contents']) {
            // Process main operation.
            try {
                gas +=
                    parseInt(c['metadata']['operation_result']['consumed_gas']) ||
                    Math.ceil(parseInt(c['metadata']['operation_result']['consumed_milligas']) / 1000) ||
                    0;
                storageCost += parseInt(c['metadata']['operation_result']['paid_storage_size_diff']) || 0;

                if (c.kind === 'origination' || c['metadata']['operation_result']['allocated_destination_contract']) {
                    storageCost += TezosConstants.EmptyAccountStorageBurn;
                } else if (c.kind === 'reveal') {
                    staticFee += 1270;
                }
            } catch {}

            // Process internal operations if they are present.
            const internalOperations = c['metadata']['internal_operation_results'];
            if (internalOperations === undefined) {
                continue;
            }

            for (const internalOperation of internalOperations) {
                const result = internalOperation['result'];
                gas += parseInt(result['consumed_gas']) || 0;
                storageCost += parseInt(result['paid_storage_size_diff']) || 0;
                if (internalOperation.kind === 'origination') {
                    storageCost += TezosConstants.EmptyAccountStorageBurn;
                }
            }
        }

        const validBranch = 'BMLxA4tQjiu1PT2x3dMiijgvMTQo8AVxkPBPpdtM8hCfiyiC1jz';
        const forgedOperationGroup = TezosNodeWriter.forgeOperations(validBranch, operations);
        const operationSize = forgedOperationGroup.length / 2 + 64; // operation bytes + signature bytes
        const estimatedFee =
            staticFee +
            Math.ceil(gas / 10) +
            TezosConstants.BaseOperationFee +
            operationSize +
            TezosConstants.DefaultBakerVig;
        const estimatedStorageBurn = Math.ceil(storageCost * TezosConstants.StorageRate);
        console.log(
            `AbstractWallet.estimateOperation; gas: ${gas}, storage: ${storageCost}, fee estimate: ${estimatedFee}, burn estimate: ${estimatedStorageBurn}`,
        );

        return { gas, storageCost, estimatedFee, estimatedStorageBurn };
    }

    /**
     * @description Retrieve the Public Key Hash of the account that is currently in use by the wallet.
     *
     * @param options Options to use while fetching the PKH.
     *
     * @returns A promise that resolves to the Public Key Hash string.
     */
    protected async getPkh() {
        return this.pkh;
    }

    /**
     * @description Get the account balance.
     *
     * @returns A promise that resolves to a BigNumber.
     */
    public getBalance = async () => {
        if (this.rpc && this.pkh) {
            return TezosNodeReader.getSpendableBalanceForAccount(this.rpc, this.pkh);
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_BALANCE);
    };

    /**
     * @description Verifies if the selected account needs to be revealed.
     *
     * @returns {boolean} true if the account was not yet revealed, false otherwise
     */
    public async isRevealRequired() {
        if (this.rpc && this.pkh) {
            return !(await TezosNodeReader.isManagerKeyRevealedForAccount(this.rpc, this.pkh));
        }
        return false;
    }

    /**
     * @description Prepare the origination operation.
     *
     * @param originationParams Origination Parameters.
     *
     * @returns A promise that results to the origination information, it includes a send callback to commit the operation.
     */
    public prepareOrigination = async (originationParams: OriginationParameters): Promise<OperationInformation> => {
        const source = await this.getPkh();
        if (this.rpc && this.signer && source) {
            const counter = await TezosNodeReader.getCounterForAccount(this.rpc, source);
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (counter !== undefined && blockHeader && originationParams.code && originationParams.storage) {
                const operationContents = {
                    kind: 'origination',
                    counter: String(counter + 1),
                    source,
                    fee: String(convertUnitNumber(originationParams.fee, AmountUnit.tez).toNumber()),
                    balance: String(convertUnitNumber(originationParams.balance, AmountUnit.tez).toNumber()),
                    storage_limit: String(originationParams.storage_limit),
                    gas_limit: String(originationParams.gas_limit),
                    script: {
                        code: JSON.parse(originationParams.code),
                        storage: JSON.parse(originationParams.storage),
                    },
                    delegate: originationParams.delegate || undefined,
                };

                let bytes = '';
                if (this.rpc.includes('hangzhou')) {
                    // Its fine to forge remotely on the new testnet
                    const rpc = new RpcClient(this.rpc);
                    bytes = await rpc.forgeOperations({
                        branch: blockHeader.hash,
                        contents: [operationContents as any],
                    });
                } else {
                    bytes = await localForger.forge({
                        branch: blockHeader.hash,
                        contents: [operationContents as any],
                    });
                }

                return {
                    bytes,
                    blockHash: blockHeader.hash,
                    operationContents,
                    blake2bHash: getBlake2BHash(bytes),
                    // The lambda function is required to keep the batch context.
                    send: ((rpc: string, signer: Signer) => async (): Promise<OperationResults> => {
                        const { sbytes, prefixSig } = await signer.sign(bytes);

                        const signedOpBytes = Buffer.from(sbytes, 'hex');
                        const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                        const preApplyResults = await TezosNodeWriter.preapplyOperation(
                            rpc,
                            blockHeader.hash,
                            blockHeader.protocol,
                            [operationContents],
                            signedOpGroup,
                        );

                        if (preApplyResults && preApplyResults.length > 0) {
                            const results = preApplyResults[0];
                            const opHash = JSON.parse(await TezosNodeWriter.injectOperation(rpc, signedOpGroup));

                            return {
                                hash: opHash,
                                results: arrayOrEmptyArray(results.contents) as any,
                                contractAddress: getContractFromOriginationResult(results.contents),
                                monitor: TezMonitor.operationConfirmations(rpc, opHash, 10),
                            };
                        }
                        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_ORIGINATE_CONTRACT);
                    })(this.rpc, this.signer),
                };
            }
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_PREPARE_ORIGINATION);
    };

    /**
     * @description reveal account
     *
     * @throws {WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_REVEAL_ACCOUNT)} Throws this exception if reveal operation failed.
     */
    public async reveal() {
        const source = await this.getPkh();
        if (this.rpc && this.signer && source) {
            const counter = await TezosNodeReader.getCounterForAccount(this.rpc, source);
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (blockHeader) {
                const operationContents = {
                    kind: OperationKindType.Reveal,
                    counter: String(counter + 1),
                    source,
                    fee: String(TezosConstants.DefaultKeyRevealFee),
                    storage_limit: String(TezosConstants.DefaultKeyRevealStorageLimit),
                    gas_limit: String(TezosConstants.DefaultKeyRevealGasLimit),
                    public_key: await this.signer.publicKey(),
                };

                const bytes = await localForger.forge({
                    branch: blockHeader.hash,
                    contents: [operationContents as any],
                });
                const { sbytes, prefixSig } = await this.signer.sign(bytes);

                const signedOpBytes = Buffer.from(sbytes, 'hex');
                const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                const preApplyResults = await TezosNodeWriter.preapplyOperation(
                    this.rpc,
                    blockHeader.hash,
                    blockHeader.protocol,
                    [operationContents],
                    signedOpGroup,
                );

                if (preApplyResults && preApplyResults.length > 0) {
                    const results = preApplyResults[0];
                    const opHash = JSON.parse(await TezosNodeWriter.injectOperation(this.rpc, signedOpGroup));

                    await firstValueFrom(TezMonitor.operationConfirmations(this.rpc, opHash, 1));

                    return {
                        hash: opHash,
                        results: arrayOrEmptyArray(results.contents),
                    };
                }
            }
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_REVEAL_ACCOUNT);
    }

    /**
     * @description Prepare transaction operation
     * @param {string} destination Destination address
     * @param {string} amount Transaction amount
     * @throws {WalletError(WalletGenericErrorCodes.NOT_ABLE_SUBMIT_TRANSACTION)}
     */
    public async prepareTransfer({
        amount,
        destination,
        parameters,
        fee,
        storage_limit,
        gas_limit,
    }: TransferParameters): Promise<OperationInformation> {
        const source = await this.getPkh();
        if (this.rpc && this.signer && source) {
            const counter = await TezosNodeReader.getCounterForAccount(this.rpc, source);
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (blockHeader) {
                const operationContents = {
                    kind: OperationKindType.Transaction,
                    counter: String(counter + 1),
                    source,
                    amount,
                    destination,
                    parameters,
                    fee,
                    storage_limit,
                    gas_limit,
                };

                const bytes = await localForger.forge({
                    branch: blockHeader.hash,
                    contents: [operationContents as any],
                });

                console.error(
                    {
                        branch: blockHeader.hash,
                        contents: [operationContents as any],
                    },
                    bytes,
                );
                return {
                    bytes,
                    blockHash: blockHeader.hash,
                    operationContents,
                    blake2bHash: getBlake2BHash(bytes),
                    // The lambda function is required to keep the batch context.
                    send: ((rpc: string, signer: Signer) => async (): Promise<OperationResults> => {
                        const { sbytes, prefixSig } = await signer.sign(bytes);

                        const signedOpBytes = Buffer.from(sbytes, 'hex');
                        const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                        const preApplyResults = await TezosNodeWriter.preapplyOperation(
                            rpc,
                            blockHeader.hash,
                            blockHeader.protocol,
                            [operationContents],
                            signedOpGroup,
                        );

                        if (preApplyResults && preApplyResults.length > 0) {
                            console.error(preApplyResults);
                            const results = preApplyResults[0];
                            const opHash = JSON.parse(await TezosNodeWriter.injectOperation(rpc, signedOpGroup));

                            return {
                                hash: opHash,
                                results: arrayOrEmptyArray(results.contents),
                                monitor: TezMonitor.operationConfirmations(rpc, opHash, 10),
                            };
                        }
                        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_CALL_CONTRACT);
                    })(this.rpc, this.signer),
                };
            }
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_PREPARE_TRANSACTION);
    }

    /**
     * @description Get Account Information.
     *
     * @returns A promise that resolves to the account information.
     */
    public getInformation = async (): Promise<AccountInformation> => {
        const balance = await this.getBalance();
        const revealRequired = await this.isRevealRequired();

        return {
            isLoading: false,
            source: this.source,
            pkh: this.pkh,
            balance: balance,
            // balanceTimestamp: Date.now(),
            revealRequired,
        };
    };

    /**
     * @description Return the public key of the account used by the signer
     */
    public publicKey = () => this.signer?.publicKey();

    /**
     * @description Optionally return the secret key of the account used by the signer
     */
    public secretKey = () => this.signer?.secretKey();

    /**
     * @description Optionally return the public key hash of the account used by the signer
     */
    public publicKeyHash = () => this.pkh;
}

export default AbstractWallet;
