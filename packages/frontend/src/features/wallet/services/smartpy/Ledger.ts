import { AccountSource } from '../../constants/sources';
import WalletError from '../../exceptions/WalletError';
import LedgerErrorCodes from '../../exceptions/LedgerErrorCodes';
import { LedgerSigner, LedgerWalletOptions } from './LedgerSigner';
import AbstractWallet from '../AbstractWallet';

class Ledger extends AbstractWallet {
    signer: LedgerSigner | undefined;
    constructor() {
        super(AccountSource.SMARTPY_LEDGER);
    }

    /**
     * @description Connect to the wallet.
     *
     * @param network Network options.
     * @param options Options passed to the wallet.
     *
     * @returns A promise that resolves to void;
     */
    public connect = async (network: { name: string; rpc: string }, options: LedgerWalletOptions) => {
        this.rpc = network.rpc;
        if (!this.signer) {
            this.signer = new LedgerSigner(options);
        }
        this.signer.bip32Path = options.path;
        this.signer.curve = options.curve;

        if (!(await this.signer.isReady())) {
            throw new WalletError(LedgerErrorCodes.NOT_READY);
        }

        this.pkh = await this.signer.publicKeyHash();
    };
}

export default Ledger;
