import LedgerXTZ from 'ledger-xtz';
import LedgerErrorCodes from '../../exceptions/LedgerErrorCodes';
import WalletError from '../../exceptions/WalletError';
import { Signer } from '../Signer';

export enum Curve {
    ED25519 = 0x00, // tz1
    SECP256K1 = 0x01, // tz2
    P256 = 0x02, // tz3
}

export type LedgerWalletOptions = { path: string; curve: Curve };

export class LedgerSigner implements Signer {
    private ledger: LedgerXTZ;
    bip32Path: string;
    curve: Curve;
    private _publicKey: string | undefined;
    private _publicKeyHash: string | undefined;

    constructor(options: LedgerWalletOptions) {
        this.bip32Path = options.path;
        this.curve = options.curve;
        this.ledger = new LedgerXTZ();
    }

    public publicKeyHash = async () => {
        if (!this._publicKeyHash) {
            await this.getAddress();
        }
        if (!this._publicKeyHash) {
            throw new WalletError(LedgerErrorCodes.NOT_ABLE_TO_GET_ACCOUNT);
        }
        return this._publicKeyHash;
    };

    public publicKey = async () => {
        if (!this._publicKey) {
            await this.getAddress();
        }
        if (!this._publicKey) {
            throw new WalletError(LedgerErrorCodes.NOT_ABLE_TO_GET_ACCOUNT);
        }
        return this._publicKey;
    };

    public secretKey = () => {
        throw new WalletError(LedgerErrorCodes.NOT_ALLOWED);
    };

    public sign = async (bytes: string) => {
        const result = await this.ledger.signOperation(this.bip32Path, bytes, this.curve);
        return {
            bytes,
            sig: result.signature,
            prefixSig: result.encodedSignature,
            sbytes: `${bytes}${result.signature}`,
        };
    };

    /**
     *  @description Waits for the ledger to be ready or timeouts after 3 seconds
     */
    public isReady = async () => {
        const MAX_WAIT = 3000; // 3 Seconds
        return new Promise((resolve) => {
            const interval = setInterval(() => {
                if (this.ledger.ready()) {
                    resolve(true);
                }
            }, 500);
            setTimeout(() => {
                resolve(false);
                clearInterval(interval);
            }, MAX_WAIT);
        });
    };

    private getAddress = async () => {
        const account = await this.ledger.getAddress(this.bip32Path, true, this.curve);
        this._publicKey = account.publicKey;
        this._publicKeyHash = account.pkh;
    };
}
