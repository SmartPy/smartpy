enum TempleErrorCodes {
    NOT_INSTALLED = 'wallet.temple.not_installed',
    NOT_CONNECTED = 'wallet.temple.not_connected',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.temple.not_able_to_originate_contract',
    NOT_ABLE_TO_GET_ACCOUNT = 'wallet.temple.not_able_to_get_account',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.temple.not_able_to_get_balance',
}

export default TempleErrorCodes;
