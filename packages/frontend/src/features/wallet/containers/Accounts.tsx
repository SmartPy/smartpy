import React from 'react';

import { styled, Container as MuiContainer, Typography, Divider } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import Fab from 'src/features/common/elements/Fab';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import AddAccountDialog from '../views/AddAccountDialog';
import AccountsView from '../views/Accounts';
import WalletStorage from 'smartpy-wallet-storage';

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
}));

const TopDiv = styled('div')(() => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
}));

const Accounts = () => {
    const [addingAccount, setAddingAccount] = React.useState(false);
    const { accounts, removeAccounts, addAccount } = useWalletContext();
    const t = useTranslation();

    const storedFaucet = WalletStorage.faucet.findAll();
    const transferFaucet = () => {
        // Move all stored faucets into stored accounts.
        Object.entries(storedFaucet).forEach(([, faucetAccount]) => {
            addAccount({
                address: faucetAccount.address,
                accountType: WalletStorage.constants.accountTypes.NORMAL,
                name: faucetAccount.name,
                privateKey: faucetAccount.privateKey,
                publicKey: faucetAccount.publicKey,
            });
        });
        WalletStorage.faucet.clear();
    };

    if (Object.entries(storedFaucet).length > 0) {
        transferFaucet();
    }

    return (
        <Container>
            <TopDiv>
                <RouterFab color="primary" aria-label={t('wallet.labels.goBackToMenu')} to="/wallet">
                    <ArrowBackOutlinedIcon />
                    {t('wallet.labels.goBackToMenu')}
                </RouterFab>
                <Fab
                    color="secondary"
                    sx={{ borderWidth: 2, borderStyle: 'solid', borderColor: 'primary.main' }}
                    onClick={() => setAddingAccount(true)}
                >
                    <AddIcon /> {t('wallet.labels.addAccount')}
                </Fab>
            </TopDiv>
            <Typography textAlign="center" variant="overline">
                {t('wallet.labels.accounts')}
            </Typography>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <AccountsView
                removeAccounts={removeAccounts}
                accounts={accounts.map(({ name, id, address, encryptedContent }) => {
                    return {
                        name,
                        id,
                        address,
                        encrypted: encryptedContent ? true : false,
                    };
                })}
            />
            <AddAccountDialog
                fullWidth
                open={addingAccount}
                onClose={() => setAddingAccount(false)}
                aria-labelledby="add-account-dialog-title"
            />
        </Container>
    );
};

export default Accounts;
