import { RootState } from 'SmartPyTypes';
import { IDESettings } from 'SmartPyModels';
import { useSelector } from 'react-redux';

export const useSettings = () => useSelector<RootState, IDESettings>((state: RootState) => state.editor.settings);
