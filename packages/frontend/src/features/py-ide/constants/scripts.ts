import { ScriptProps } from '../../../utils/asyncScriptInjector';
import { getBase } from '../../../utils/url';

export default [
    {
        name: 'brython.js',
        src: `${getBase()}/static/js/brython.js`,
        type: 'text/javascript',
    },
    {
        name: 'brython_stdlib.js',
        src: `${getBase()}/static/js/brython_stdlib.js`,
        type: 'text/javascript',
    },
    {
        name: 'smart.js',
        src: `${getBase()}/static/js/smart.js`,
        type: 'text/javascript',
    },
    {
        name: 'smartmljs.bc.js',
        src: `${getBase()}/static/js/smartmljs.bc.js`,
        type: 'text/javascript',
    },
    {
        name: 'smartpyio.py',
        src: `${getBase()}/static/python/smartpyio.py`,
        type: 'text/python',
        afterAppend: async () => {
            window.brython({
                debug: 1,
                indexedDB: true,
                pythonpath: [`${getBase()}/static/python`],
            } as any);
        },
    },
] as ScriptProps[];
