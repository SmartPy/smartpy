import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import {
    updateContract,
    addContract,
    removeContract,
    updateSettings,
    selectContract,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
    loadTemplate,
    updateNewcomerGuideStep,
    toggleNewcomerDialog,
    toggleFavoriteTemplate,
    setVolatileContract,
} from './actions';
import { IDEContract, IDESettings } from 'SmartPyModels';
import { generateID } from '../../utils/rand';
import { IDENewcomerGuideSteps } from '../common/enums/ide';

const DEFAULT_SETTINGS: IDESettings = {
    layout: 'side-by-side',
    newcomersMode: true,
    fontSize: 14,
};

// CONTRACTS REDUCER

const currentContractReducer = createReducer('')
    // Reset the current contract
    .handleAction(removeContract, (state, { payload }) => (payload === state ? '' : state))
    .handleAction(selectContract, (_, { payload }) => payload);

const contractsReducer = createReducer([] as IDEContract[])
    .handleAction(removeContract, (state, { payload }) => state.filter(({ id }) => id !== payload))
    .handleAction(addContract, (state, { payload, meta: overrideTemplate }) => {
        const contractID = generateID();
        return [
            ...state,
            {
                name: `unnamed_${contractID}`,
                ...payload,
                id: contractID,
                updatedAt: new Date().toISOString(),
            },
        ];
    })
    .handleAction(updateContract, (state, { payload }) => {
        const contractIndex = state.findIndex(({ id }) => id === payload.id);
        if (contractIndex !== -1) {
            state[contractIndex] = {
                ...state[contractIndex],
                ...payload,
                name: payload.name || `unnamed_${payload.id}`,
                updatedAt: new Date().toISOString(),
            };
        }
        return [...state];
    });

// VOLATILE CONTRACT REDUCER

const volatileContractReducer = createReducer('').handleAction(setVolatileContract, (_, { payload }) => payload);

// ERRORS REDUCER

const errorReducer = createReducer('')
    .handleAction(hideError, () => '')
    .handleAction(showError, (_, action) => action.payload);

// Templates

const favoriteTemplatesReducer = createReducer([] as string[]).handleAction(
    toggleFavoriteTemplate,
    (state, { payload }) => {
        let newState = [...state];
        if (state.includes(payload)) {
            newState = newState.filter((template) => template !== payload);
        } else {
            newState.push(payload);
        }

        return newState;
    },
);

// SHORTCUTS

const shortcutsReducer = createReducer('')
    .handleAction(hideShortcuts, () => '')
    .handleAction(showShortcuts, (_, action) => action.payload);

const templateReducer = createReducer('').handleAction(loadTemplate, (_, action) => action.payload);

// SETTINGS

const settingsReducer = createReducer(DEFAULT_SETTINGS).handleAction(updateSettings, (state, action) => ({
    ...state,
    ...action.payload,
}));

// NEWCOMER GUIDE
const newcomerGuideStepReducer = createReducer(IDENewcomerGuideSteps.NONE).handleAction(
    updateNewcomerGuideStep,
    (_, action) => action.payload,
);

const newcomerDialogReducer = createReducer(false).handleAction(toggleNewcomerDialog, (state, { payload }) => {
    if (payload !== undefined) {
        return payload;
    } else {
        return !state;
    }
});

const persistConfig = {
    key: 'editor',
    storage: storage,
    whitelist: ['favoriteTemplates', 'currentContract', 'contracts', 'settings', 'template'],
};

const editorReducer = combineReducers({
    volatileContract: volatileContractReducer,
    favoriteTemplates: favoriteTemplatesReducer,
    currentContract: currentContractReducer,
    contracts: contractsReducer,
    settings: settingsReducer,
    error: errorReducer,
    shortcuts: shortcutsReducer,
    template: templateReducer,
    newcomerGuideStep: newcomerGuideStepReducer,
    newcomerDialogOpen: newcomerDialogReducer,
});

export default persistReducer(persistConfig, editorReducer);
export type EditorState = ReturnType<typeof editorReducer>;
