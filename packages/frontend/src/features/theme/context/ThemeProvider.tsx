import React from 'react';

import { ThemeProvider as MuiThemeProvider, createTheme, StyledEngineProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { useThemeMode } from '../selectors';

const ThemeProvider: React.FC = ({ children }) => {
    const isDarkMode = useThemeMode();

    const themeConfig = React.useMemo(
        () =>
            createTheme({
                palette: {
                    mode: isDarkMode ? 'dark' : 'light',

                    primary: {
                        light: '#69a9ff',
                        main: '#007bff',
                        dark: '#0050cb',
                    },
                    secondary: {
                        light: '#FFF',
                        main: '#FFF',
                        dark: '#cccccc',
                    },
                },
            }),
        [isDarkMode],
    );

    return (
        <MuiThemeProvider theme={themeConfig}>
            {/* JSS must be injected before emotion */}
            <StyledEngineProvider injectFirst>
                <CssBaseline />
                {children}
            </StyledEngineProvider>
        </MuiThemeProvider>
    );
};

export default ThemeProvider;
