export const templates = [
    {
        name: 'Empty',
        code: `parameter unit;
storage   unit;
code
    {

    };`,
    },
    {
        name: 'Minimal',
        code: `parameter int;
storage   int;
code
    {
    CAR;
    NIL operation;
    PAIR;
    };`,
    },
];
