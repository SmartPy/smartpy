import React from 'react';

// Material UI
import MenuItem from '@mui/material/MenuItem';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

// Local Components
import HelpMenuFab from '../../common/components/HelpMenuFab';

// Local Utils
import useTranslation from '../../i18n/hooks/useTranslation';

const HelpMenuItem: React.FC = () => {
    const t = useTranslation();
    return (
        <>
            <HelpMenuFab>
                <MenuItem component={Link} href="https://tezos.gitlab.io/michelson-reference" target="_blank">
                    <Typography variant="inherit">
                        {t('michelsonIde.helpMenu.michelsonReferenceByNomadicLabs')}
                    </Typography>
                </MenuItem>
            </HelpMenuFab>
        </>
    );
};

export default HelpMenuItem;
