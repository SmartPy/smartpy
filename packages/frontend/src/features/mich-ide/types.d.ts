import { IDESettings } from 'SmartPyModels';

declare module 'SmartPyModels' {
    export interface MichelsonIDESettings extends IDESettings {
        electricEvaluation?: boolean;
        protocol?: string;
    }
}
