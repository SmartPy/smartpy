import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';

export const useNewcomerMode = () =>
    useSelector<RootState, boolean>((state) => state.mlIDE.settings.newcomersMode || false);

export const useNewcomerDialogOpen = () => useSelector((state: RootState) => state.mlIDE.newcomerDialogOpen);

export const useNewcomerGuideStep = () => useSelector((state: RootState) => state.mlIDE.newcomerGuideStep);

export const useFavoriteTemplates = () =>
    useSelector<RootState, string[]>((state) => state.mlIDE?.favoriteTemplates || []);
