export enum Protocol {
    JAKARTA = 'Jakarta',
    KATHMANDU = 'Kathmandu',
    LIMA = 'Lima',
}

export const ProtocolHash: Record<string, string> = {
    [Protocol.JAKARTA]: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
    [Protocol.KATHMANDU]: 'PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg',
    [Protocol.LIMA]: 'PtLimaPtLMwfNinJi9rCfDPWea8dFgTZ1MeJ9f1m2SRic6ayiwW',
};

export const DefaultProtocol = Protocol.KATHMANDU;
