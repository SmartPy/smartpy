import { Protocol, ProtocolHash, DefaultProtocol } from './protocol';

export enum Network {
    MAINNET = 'Mainnet',
    GHOSTNET = 'Ghostnet',
    KATHMANDUNET = 'Kathmandunet',
    LIMANET = 'Limanet',
    CUSTOM = 'CUSTOM',
}

export const NetworkProtocolHash: Record<string, string> = {
    [Network.MAINNET]: ProtocolHash[DefaultProtocol],
    [Network.GHOSTNET]: ProtocolHash[Protocol.JAKARTA],
    [Network.KATHMANDUNET]: ProtocolHash[Protocol.KATHMANDU],
    [Network.LIMANET]: ProtocolHash[Protocol.LIMA],
};

export const DeprecatedNetworks: Network[] = [];
