import { Network } from './networks';

export const smartpy: Record<string, string> = {
    [Network.MAINNET]: 'https://mainnet.smartpy.io',
    [Network.GHOSTNET]: 'https://ghostnet.smartpy.io',
    [Network.KATHMANDUNET]: 'https://kathmandunet.smartpy.io',
    [Network.LIMANET]: 'https://limanet.smartpy.io',
};

export const Nodes = {
    // SMARTPY
    [smartpy.Mainnet]: Network.MAINNET,
    [smartpy.Ghostnet]: Network.GHOSTNET,
    [smartpy.Kathmandunet]: Network.KATHMANDUNET,
    [smartpy.Limanet]: Network.LIMANET,
};

export enum Endpoint {
    HEADER = '/chains/main/blocks/head/header',
    ERRORS = '/chains/main/blocks/head/context/constants/errors',
    VERSION = '/version',
    CHECKPOINT = '/chains/main/checkpoint',
    CONSTANTS = '/chains/main/blocks/head/context/constants',
    METADATA = '/chains/main/blocks/head/metadata',
    CONTRACT_BALANCE = '/chains/main/blocks/head/context/contracts/:contract_id/balance',
    CONTRACT = '/chains/main/blocks/head/context/contracts/:contract_id',
}

export default Nodes;
