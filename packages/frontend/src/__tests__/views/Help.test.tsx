import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';

import Help from '../../features/help/views/Help';
import renderWithStore from '../test-helpers/renderWithStore';

describe('Help Page', () => {
    it('Help page renders correctly', () => {
        let container;
        act(() => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Help />
                </Suspense>,
            ).container;
        });
        expect(container).toMatchSnapshot();
    });
});
