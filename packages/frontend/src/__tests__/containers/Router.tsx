import { act, waitFor } from '@testing-library/react';

import renderWithRouter from '../test-helpers/renderWithRouter';
import routes from '../../features/navigation/constants/routes';

jest.mock('../../utils/rand.ts', () => ({
    generateID: (): string => {
        return '123'; // Mock random value for tests
    },
}));

/**
 * This snapshot test was disabled because Pagination component generates dynamic ids
 */

describe('Application Routing', () => {
    for (const route of routes) {
        let path = route.routeProps.path as string;
        let testDescription = `Test Route ${route.title}`;

        if (route.routeProps.path === '*') {
            path = '/not-found';
            testDescription = 'Test Non Existent Route';
        }

        it(testDescription, async () => {
            let _container: HTMLElement | undefined;
            await act(async () => {
                const { container } = await renderWithRouter({
                    route: path,
                });
                _container = container;
            });

            expect(_container?.innerHTML).toMatchSnapshot();
            await waitFor(() => expect(document.title).toMatch(route.title));
        });
    }
});
