# Create SmartPy projects

This starter kit is used to bootstrap smartpy projects.

## Bootstrap a project for `typescript` syntax.

### npx
```console
npx create-smartpy-project@latest install <project-directory> --typescript
```

### npm
```console
npm exec create-smartpy-project@latest -- install <project-directory> --typescript
```

## Update boilerplate


```console
# Inside <project-directory>
npm run update
```


## Development Info

Edit the boilerplates at: [boilerplates](./boilerplates)

### Package boilerplates
```console
npm run bundle
```
