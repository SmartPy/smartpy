type TType = TRecord<
    {
        a: TNat;
        c: TBool;
        b: TString;
    },
    ['a', ['c', 'b']]
>;

class ArgumentOrderOfEntryPoints {
    storage: TType = {
        a: 1,
        c: true,
        b: '',
    };

    @EntryPoint<TType>()
    ep(a: TNat, c: TBool, b: TString) {
        this.storage.b = b;
        this.storage.a = a;
        this.storage.c = c;
    }
}

Dev.test({ name: 'ArgumentOrderOfEntryPoints_test' }, () => {
    const c1 = Scenario.originate(new ArgumentOrderOfEntryPoints());

    Scenario.transfer(c1.ep(1, true, ''));
});

Dev.compileContract('ArgumentOrderOfEntryPoints_compilation', new ArgumentOrderOfEntryPoints());
