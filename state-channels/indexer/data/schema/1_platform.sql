-- ////////////////////
--
-- Platform Information
--
-- ////////////////////

--
-- Name: platform; Type: TABLE;
--
CREATE TABLE public.platform (
    id TEXT PRIMARY KEY,
    ledger TEXT NOT NULL,
    metadata TEXT
);
--
-- Name: TABLE platform; Type: COMMENT;
--
COMMENT ON TABLE public.platform IS 'Platform information';


--
-- Name: channel; Type: TABLE;
--
CREATE TABLE public.channel (
    id TEXT PRIMARY KEY,
    platform_id TEXT NOT NULL,
    closed BOOLEAN NOT NULL,
    nonce TEXT NOT NULL,
    withdraw_delay TEXT NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_at TIMESTAMP NOT NULL
);
--
-- Name: TABLE channel; Type: COMMENT;
--
COMMENT ON TABLE public.channel IS 'Channel Information';
--
-- Name: fk_channel__platform_id; Type: FOREIGN KEY;
--
ALTER TABLE channel
ADD CONSTRAINT fk_channel__platform_id
FOREIGN KEY (platform_id)
REFERENCES platform (id);


--
-- Name: channel_participant; Type: TABLE;
--
CREATE TABLE public.channel_participant (
    id BIGINT PRIMARY KEY,
    channel_id TEXT NOT NULL,
    public_key_hash TEXT NOT NULL,
    public_key TEXT NOT NULL,
    withdraw_id INTEGER NOT NULL,
    withdraw JSONB
);
--
-- Name: TABLE channel_participant; Type: COMMENT;
--
COMMENT ON TABLE public.channel_participant IS 'Channel participant information';
--
-- Name: channel_participant_id_seq; Type: SEQUENCE;
--
CREATE SEQUENCE public.channel_participant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY public.channel_participant.id;
ALTER TABLE public.channel_participant ALTER id SET DEFAULT nextval('channel_participant_id_seq');
--
-- Name: fk_channel_participant__channel_id; Type: FOREIGN KEY;
--
ALTER TABLE channel_participant
ADD CONSTRAINT fk_channel_participant__channel_id
FOREIGN KEY (channel_id)
REFERENCES channel (id);


--
-- Name: channel_participant_bond; Type: TABLE;
--
CREATE TABLE public.channel_participant_bond (
    channel_participant_id BIGINT NOT NULL,
    bond_id INTEGER NOT NULL,
    amount BIGINT NOT NULL
);
--
-- Name: TABLE channel_participant_bond; Type: COMMENT;
--
COMMENT ON TABLE public.channel_participant_bond IS 'Channel participant bond information';
--
-- Name: fk_channel_participant_bond__channel_participant_id; Type: FOREIGN KEY;
--
ALTER TABLE channel_participant_bond
ADD CONSTRAINT fk_channel_participant_bond__channel_participant_id
FOREIGN KEY (channel_participant_id)
REFERENCES channel_participant (id);


--
-- Name: model; Type: TABLE;
--
CREATE TABLE public.model (
    id TEXT PRIMARY KEY,
    platform_id TEXT NOT NULL,
    metadata JSONB,
    outcomes TEXT[] NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_at TIMESTAMP NOT NULL
);
--
-- Name: TABLE model; Type: COMMENT;
--
COMMENT ON TABLE public.model IS 'Model Information';
--
-- Name: fk_model__platform_id; Type: FOREIGN KEY;
--
ALTER TABLE model
ADD CONSTRAINT fk_model__platform_id
FOREIGN KEY (platform_id)
REFERENCES platform (id);


--
-- Name: game; Type: TABLE;
--
CREATE TABLE public.game (
    id TEXT PRIMARY KEY,
    on_chain BOOLEAN NOT NULL,
    init_params TEXT,
    settled BOOLEAN,
    state TEXT,
    metadata JSONB,
    updated_at TIMESTAMP DEFAULT NOW(),
    created_at TIMESTAMP DEFAULT NOW(),
    -- current
    outcome JSONB,
    move_nb INTEGER NOT NULL,
    current_player INTEGER NOT NULL,
    -- constants
    players JSONB NOT NULL,
    bonds JSONB NOT NULL,
    channel_id TEXT NOT NULL,
    game_nonce TEXT NOT NULL,
    model_id TEXT NOT NULL,
    play_delay INTEGER NOT NULL,
    settlements JSONB NOT NULL
);
--
-- Name: TABLE game; Type: COMMENT;
--
COMMENT ON TABLE public.game IS 'Game information';
--
-- Name: fk_game__model_id; Type: FOREIGN KEY;
--
ALTER TABLE game
ADD CONSTRAINT fk_game__model_id
FOREIGN KEY (model_id)
REFERENCES model (id);
--
-- Name: fk_game__channel_id; Type: FOREIGN KEY;
--
ALTER TABLE game
ADD CONSTRAINT fk_game__channel_id
FOREIGN KEY (channel_id)
REFERENCES channel (id);


--
-- Name: game_timeout; Type: TABLE;
--
CREATE TABLE public.game_timeout (
    player_id BIGINT NOT NULL,
    game_id TEXT NOT NULL,
    timeout TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(player_id, game_id)
);
--
-- Name: TABLE game_timeout; Type: COMMENT;
--
COMMENT ON TABLE public.game_timeout IS 'Game timeouts';
--
-- Name: fk_game_timeout__game_id; Type: FOREIGN KEY;
--
ALTER TABLE game_timeout
ADD CONSTRAINT fk_game_timeout__game_id
FOREIGN KEY (game_id)
REFERENCES game (id);


--
-- Name: game_signature; Type: TABLE;
--
CREATE TABLE public.game_signature (
    game_id TEXT NOT NULL,
    public_key TEXT NOT NULL,
    signature TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);
--
-- Name: TABLE game_signature; Type: COMMENT;
--
COMMENT ON TABLE public.game_signature IS 'Game signatures';
--
-- Name: fk_game_signature__game_id; Type: FOREIGN KEY;
--
ALTER TABLE game_signature
ADD CONSTRAINT fk_game_signature__game_id
FOREIGN KEY (game_id)
REFERENCES game (id);


--
-- Name: game_move; Type: TABLE;
--
CREATE TABLE public.game_move (
    id BIGINT PRIMARY KEY,
    game_id TEXT NOT NULL,
    player_number INTEGER NOT NULL,
    move_number INTEGER NOT NULL,
    outcome JSONB,
    move_bytes TEXT,
    move_data JSONB,
    state TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);
--
-- Name: TABLE game_move; Type: COMMENT;
--
COMMENT ON TABLE public.game_move IS 'Game move information';
--
-- Name: game_move_id_seq; Type: SEQUENCE;
--
CREATE SEQUENCE public.game_move_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY public.game_move.id;
ALTER TABLE public.game_move ALTER id SET DEFAULT nextval('game_move_id_seq');
--
-- Name: fk_game_move__game_id; Type: FOREIGN KEY;
--
ALTER TABLE game_move
ADD CONSTRAINT fk_game_move__game_id
FOREIGN KEY (game_id)
REFERENCES game (id);


--
-- Name: game_move_signature; Type: TABLE;
--
CREATE TABLE public.game_move_signature (
    game_move_id BIGINT NOT NULL,
    public_key TEXT NOT NULL,
    signature TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);
--
-- Name: TABLE game_move; Type: COMMENT;
--
COMMENT ON TABLE public.game_move IS 'Game move signatures';
--
-- Name: fk_game_move_signature__game_move_id; Type: FOREIGN KEY;
--
ALTER TABLE game_move_signature
ADD CONSTRAINT fk_game_move_signature__game_move_id
FOREIGN KEY (game_move_id)
REFERENCES game_move (id);
