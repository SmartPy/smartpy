declare namespace NodeJS {
    export interface ProcessEnv {
        PORT: string; // Port where the REST service will be listening.
    }
}
