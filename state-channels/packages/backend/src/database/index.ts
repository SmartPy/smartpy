import { Pool, PoolClient } from 'pg';

const DB_CONNECTION = {
    user: process.env.DB_USERNAME,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 5432,
};

const pool = new Pool(DB_CONNECTION);

/**
 * Run sql statement
 * @param func Function to be executed inside the transaction
 * @returns {Promise<T>}
 */
export async function runStatement<T>(func: (client: PoolClient) => Promise<T>): Promise<T> {
    const client = await pool.connect();
    try {
        return await func(client);
    } catch (e) {
        throw e;
    } finally {
        client.release();
    }
}

/**
 * Run sql transaction
 * @param func Function to be executed inside the transaction
 * @returns {Promise<T>}
 */
export async function runTransaction<T>(func: (client: PoolClient) => Promise<T>): Promise<T> {
    const client = await pool.connect();
    try {
        await client.query('BEGIN');
        const result = await func(client);
        await client.query('COMMIT');
        return result;
    } catch (e) {
        await client.query('ROLLBACK');
        throw e;
    } finally {
        client.release();
    }
}
