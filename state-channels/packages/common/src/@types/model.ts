export interface SC_NewModelRequest {
    metadata: Record<string, string>;
    modelBytes: string;
}

export interface SC_Model {
    id: string;
    metadata: Record<string, string>;
    outcomes: string[];
    updatedAt: string;
    createdAt: string;
}
