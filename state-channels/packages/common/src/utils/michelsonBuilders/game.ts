import { MichelsonData } from '@taquito/michel-codec';

import { GameConstants, GameID, SC_Settlement, SC_SettlementKind, SC_SettlementOrder } from '../../@types/game';
import { compare } from '../comparison';

/**
 * @description Build outcome michelson
 * @param {SC_GameOutcome} outcome Game outcome
 * @returns {MichelsonData} Game outcome michelson
 */
export function buildOutcome(outcome?: Record<string, string> | string): MichelsonData {
    if (!outcome) {
        return { prim: 'None' };
    }

    if (typeof outcome === 'string') {
        return { prim: 'Some', args: [{ prim: 'Left', args: [{ prim: 'Left', args: [{ string: outcome }] }] }] };
    }

    if (outcome[SC_SettlementKind.GameFinished]) {
        return {
            prim: 'Some',
            args: [
                { prim: 'Left', args: [{ prim: 'Left', args: [{ string: outcome[SC_SettlementKind.GameFinished] }] }] },
            ],
        };
    }
    if (outcome[SC_SettlementKind.PlayerDoublePlayed]) {
        return {
            prim: 'Some',
            args: [{ prim: 'Right', args: [{ prim: 'Left', args: [{ string: String(outcome) }] }] }],
        };
    }
    if (outcome[SC_SettlementKind.PlayerInactive]) {
        return {
            prim: 'Some',
            args: [{ prim: 'Right', args: [{ prim: 'Right', args: [{ string: String(outcome) }] }] }],
        };
    }

    throw new Error(`Outcome ${JSON.stringify(outcome)} not expected.`);
}

export const buildID = (data: GameID): MichelsonData => ({
    prim: 'Pair',
    args: [
        { bytes: data.channelID },
        {
            prim: 'Pair',
            args: [{ bytes: data.modelID }, { string: data.gameNonce }],
        },
    ],
});

export const buildNewGameAction = (initParams: string, constants: GameConstants): MichelsonData => {
    const constantsMichelson = buildConstants(constants);

    return {
        prim: 'Pair',
        args: [{ string: 'New Game' }, constantsMichelson, { bytes: initParams }],
    };
};

export function buildSettlement(settlement: SC_Settlement) {
    const bondsMichelson =
        settlement.sender && settlement.recipient
            ? [
                  {
                      prim: 'Pair',
                      args: [
                          Object.entries(settlement.bonds).map(([tokenID, amount]) => ({
                              prim: 'Elt',
                              args: [{ int: String(tokenID) }, { int: String(amount) }],
                          })),
                          {
                              prim: 'Pair',
                              args: [{ int: String(settlement.sender) }, { int: String(settlement.recipient) }],
                          },
                      ],
                  },
              ]
            : [];
    switch (settlement.kind) {
        case SC_SettlementKind.GameFinished:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Left',
                        args: [
                            {
                                prim: 'Left',
                                args: [{ string: String(settlement.value) }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
        case SC_SettlementKind.GameAborted:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Left',
                        args: [
                            {
                                prim: 'Right',
                                args: [{ prim: 'Unit' }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
        case SC_SettlementKind.PlayerDoublePlayed:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Right',
                        args: [
                            {
                                prim: 'Left',
                                args: [{ int: String(settlement.value) }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
        case SC_SettlementKind.PlayerInactive:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Right',
                        args: [
                            {
                                prim: 'Right',
                                args: [{ int: String(settlement.value) }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
    }
}

export const buildConstants = (constants: GameConstants): MichelsonData => {
    const settlements = constants.settlements
        .sort(({ kind: kind1 }, { kind: kind2 }) => compare(SC_SettlementOrder[kind1], SC_SettlementOrder[kind2]))
        .reduce<MichelsonData[]>((settlements, cur) => {
            const settlementMichelson = buildSettlement(cur);

            return [...settlements, (settlementMichelson as unknown) as MichelsonData];
        }, []);
    return ({
        prim: 'Pair',
        args: [
            Object.entries(constants.bonds)
                .sort(([i1], [i2]) => compare(i1, i2))
                .map(([pIndex, pBonds]) => ({
                    prim: 'Elt',
                    args: [
                        { int: pIndex },
                        Object.entries(pBonds)
                            .sort(([i1], [i2]) => compare(i1, i2))
                            .map(([bIndex, bAmount]) => ({
                                prim: 'Elt',
                                args: [{ int: bIndex }, { int: String(bAmount) }],
                            })),
                    ],
                })),
            {
                prim: 'Pair',
                args: [
                    { bytes: constants.channelID },
                    {
                        prim: 'Pair',
                        args: [
                            { string: constants.gameNonce },
                            {
                                prim: 'Pair',
                                args: [
                                    {
                                        bytes: constants.modelID,
                                    },
                                    {
                                        prim: 'Pair',
                                        args: [
                                            { int: String(constants.playDelay) },
                                            {
                                                prim: 'Pair',
                                                args: [
                                                    Object.entries(constants.players)
                                                        .sort((i1, i2) => compare(i1[1], i2[1]))
                                                        .map(([address, index]) => ({
                                                            prim: 'Elt',
                                                            args: [
                                                                { int: String(index) },
                                                                {
                                                                    string: address,
                                                                },
                                                            ],
                                                        })),
                                                    settlements,
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    } as unknown) as MichelsonData;
};
