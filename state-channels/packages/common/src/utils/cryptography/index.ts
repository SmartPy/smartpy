import sodium from 'libsodium-wrappers';
import { Encoders } from '..';

/**
 * @description Verify signature
 * @param {string} content Raw content
 * @param {string} signature Content signature
 * @param {string} publicKey Public key of the entity that signed the content
 * @returns {Promise<boolean>} A promise that resolves to true the signature is valid
 */
export async function verifySignature(content: string, signature: string, publicKey: string): Promise<boolean> {
    await sodium.ready;
    const verify = sodium.crypto_sign_verify_detached(
        Encoders.Bs58.decodeCheck(signature, Encoders.Bs58.PREFIX_STRING.edsig),
        sodium.crypto_generichash(32, Encoders.Hex.hexToBuffer(content)),
        Encoders.Bs58.decodeCheck(publicKey, Encoders.Bs58.PREFIX_STRING.edpk),
    );
    return verify;
}

/**
 * @description Hash public key
 * @param {string} prefixedPublicKey base58 encoded public key
 * @returns {Promise<string>} base58 encoded public key hash
 */
export async function hashPublicKey(prefixedPublicKey: string): Promise<string> {
    await sodium.ready;
    const rawPublicKey = Encoders.Bs58.decodeCheck(prefixedPublicKey, Encoders.Bs58.PREFIX_STRING.edpk);
    const publicKeyHash = Encoders.Bs58.encodeCheck(
        Buffer.from(sodium.crypto_generichash(20, rawPublicKey)),
        Encoders.Bs58.PREFIX_STRING.tz1,
    );

    return publicKeyHash;
}
