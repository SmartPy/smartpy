import { blake2bHex } from 'blakejs';

export function hashID(modelBytes: string): string {
    return blake2bHex(Buffer.from(modelBytes, 'hex'), undefined, 32);
}
