import type { MichelsonData } from '@taquito/michel-codec';
import { Comparison } from '..';

/**
 * @description Build parameters for "new_channel" entry point call
 * @param nonce
 * @param withdrawDelay
 * @param players
 * @returns entry point parameters
 */
export function newChannel(
    nonce: string,
    withdrawDelay: number,
    players: { address: string; publicKey: string }[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    players = players.sort(({ address: a1 }, { address: a2 }) => Comparison.compare(a1, a2));
    return {
        entrypoint: 'new_channel',
        value: {
            prim: 'Pair',
            args: [
                { string: nonce },
                {
                    prim: 'Pair',
                    args: [
                        [
                            {
                                prim: 'Elt',
                                args: [{ string: players[0].address }, { string: players[0].publicKey }],
                            },
                            {
                                prim: 'Elt',
                                args: [{ string: players[1].address }, { string: players[1].publicKey }],
                            },
                        ],
                        { int: String(withdrawDelay) },
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "push_bonds" entry point call
 * @param channel_id
 * @param player
 * @param bonds
 * @returns entry point parameters
 */
export function pushBonds(
    channel_id: string,
    platformAddress: string,
    player: string,
    bonds: { tokenId: number; amount: number }[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'push_bonds',
        value: {
            prim: 'Pair',
            args: [
                {
                    prim: 'Pair',
                    args: [
                        bonds.map(({ tokenId, amount }) => ({
                            prim: 'Elt',
                            args: [{ int: `${tokenId}` }, { int: `${amount}` }],
                        })),
                        {
                            bytes: channel_id,
                        },
                    ],
                },
                {
                    prim: 'Pair',
                    args: [
                        {
                            string: platformAddress,
                        },
                        {
                            string: player,
                        },
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "withdraw_request" entry point call
 * @param chainID
 * @param bonds
 * @returns entry point parameters
 */
export function withdrawRequest(
    chainID: string,
    bonds: { tokenId: number; amount: number }[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'withdraw_request',
        value: {
            prim: 'Pair',
            args: [
                {
                    bytes: chainID,
                },
                bonds.map(({ tokenId, amount }) => ({
                    prim: 'Elt',
                    args: [{ int: `${tokenId}` }, { int: `${amount}` }],
                })),
            ],
        },
    };
}

/**
 * @description Build parameters for "withdraw_cancel" entry point call
 * @param bytes
 * @returns entry point parameters
 */
export function withdrawCancel(
    bytes: string,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'withdraw_cancel',
        value: {
            bytes,
        },
    };
}

/**
 * @description Build parameters for "withdraw_challenge" entry point call
 * @param bytes
 * @returns entry point parameters
 */
export function withdrawChallenge(
    chainID: string,
    gameIDs: string[],
    withdrawer: string,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'withdraw_challenge',
        value: {
            prim: 'Pair',
            args: [
                {
                    bytes: chainID,
                },
                {
                    prim: 'Pair',
                    args: [
                        gameIDs.sort().map((bytes) => ({ bytes })),
                        {
                            string: withdrawer,
                        },
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "withdraw_finalise" entry point call
 * @param bytes
 * @returns entry point parameters
 */
export function withdrawFinalise(
    bytes: string,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'withdraw_finalise',
        value: {
            bytes,
        },
    };
}
