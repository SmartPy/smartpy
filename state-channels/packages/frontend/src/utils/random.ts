/**
 * @description Get random nonce
 */
export function randomNonce(): string {
    const values = new Uint32Array(4);
    window.crypto.getRandomValues(values);

    return values.reduce((acc, cur) => `${acc}${cur}`, '');
}
