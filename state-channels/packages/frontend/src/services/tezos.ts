import { TezosToolkit } from '@taquito/taquito';
import { Constants } from 'state-channels-common';

const Tezos = new TezosToolkit(Constants.rpc);

export default Tezos;
