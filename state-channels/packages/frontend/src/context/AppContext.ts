import { createContext } from 'react';
import { SC_Platform } from '../../../common/src';

export interface IAppContext {
    platform: SC_Platform;
    changePlatform: (platform: SC_Platform) => void;
}

const contextStub = {
    platform: {
        id: '',
        ledger: '',
        metadata: '',
    },
    changePlatform: () => {
        // stub
    },
};

const AppContext = createContext<IAppContext>(contextStub);

export default AppContext;
