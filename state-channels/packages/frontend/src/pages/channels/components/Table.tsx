import React from 'react';
import { useSubscription } from '@apollo/client';
import {
    TableCell,
    TableHead as MuiTableHead,
    TableBody as MuiTableBody,
    TableRow as MuiTableRow,
    Chip,
    IconButton,
    Collapse,
    Box,
    Typography,
    Select,
    Stack,
    MenuItem,
    TextField,
    Divider,
    SelectChangeEvent,
    Alert,
    Theme,
    Dialog,
    Grid,
} from '@mui/material';
import { makeStyles, createStyles } from '@mui/styles';

import AddIcon from '@mui/icons-material/Add';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import {
    SC_Channel,
    SC_ChannelParticipant,
    EntryPoints,
    TezUnit,
    convertUnitNumber,
    convertUnitWithSymbol,
    Time,
    SC_Platform,
} from 'state-channels-common';

import Table from '../../../components/base/Table';
import CopyButton from '../../../components/base/CopyButton';
import Button from '../../../components/base/Button';
import { Subscription } from '../../../services/graphql';
import wallet from '../../../services/wallet';
import { copyToClipboard } from '../../../utils/clipboard';
import useWalletContext from '../../../hooks/useWalletContext';

import Logger from '../../../services/logger';
import RouterFab from '../../../components/base/RouterFab';
import BondIcon from '../../../components/base/BondIcon';
import GamesTable from '../../games/components/Table';
import useTokens from 'src/hooks/useTokens';
import useAppContext from 'src/hooks/useAppContext';
import Tezos from 'src/services/tezos';
import TableRow from 'src/components/base/TableRow';
import Withdraws from './Withdraws';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

interface SC_Bond {
    address: string;
    amount: number;
}

const extractBonds = (participants: SC_ChannelParticipant[]): Record<string, SC_Bond[]> => {
    return participants.reduce<Record<string, SC_Bond[]>>((bonds, participant) => {
        participant.bonds.forEach((bond) => {
            bonds[bond.id] = [
                ...(bonds[bond.id] || []),
                {
                    address: participant.publicKeyHash,
                    amount: bond.amount,
                },
            ];
        });
        return bonds;
    }, {});
};

interface TableRowProps {
    platform: SC_Platform;
    address: string;
    channel: SC_Channel;
}

const ChannelRow: React.FC<TableRowProps> = ({ platform, address, channel }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [tokenId, setToken] = React.useState(0);
    const [amount, setAmount] = React.useState('0');
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const tokens = useTokens();

    const opponent = React.useMemo(
        () =>
            channel.participants.reduce<string>(
                (opponent, { publicKeyHash }: any) => (publicKeyHash !== address ? publicKeyHash : opponent),
                '',
            ),
        [address, channel.participants],
    );

    const bonds = React.useMemo(() => {
        return extractBonds(channel.participants);
    }, [channel.participants]);

    const handleTokenSelection = React.useCallback((e: SelectChangeEvent<number>) => {
        setToken(Number(e.target.value));
    }, []);

    const handleTokenBalance = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        setAmount(e.target.value);
    }, []);

    const updateBonds = React.useCallback(async () => {
        try {
            const tokenAmount = Number(amount);
            const parameters = EntryPoints.Channel.pushBonds(channel.id, platform.id, address, [
                {
                    tokenId,
                    amount: tokenId === 0 ? convertUnitNumber(tokenAmount, TezUnit.tez).toNumber() : tokenAmount,
                },
            ]);

            const storage = await Tezos.contract.getStorage<{ ledger: string }>(platform.id);

            const transfer = await wallet.Beacon.transfer(
                storage.ledger,
                parameters,
                tokenId === 0 ? tokenAmount : undefined /*tokenID == 0 means xtz and an amount must also be provided*/,
            );

            setSuccess(transfer.hash);
            setAmount('0');
        } catch (e) {
            Logger.debug(e);
            setError(e.message);
        }
    }, [channel.id, address, tokenId, amount]);

    return (
        <>
            <TableRow>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    <CopyButton
                        onClick={() => copyToClipboard(channel.id)}
                        label={channel.id}
                        sx={{ maxWidth: 120 }}
                    ></CopyButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    <CopyButton
                        color="info"
                        onClick={() => copyToClipboard(opponent)}
                        label={opponent}
                        sx={{ maxWidth: 120 }}
                    />
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    {Time.prettifyMilliseconds(channel.withdrawDelay * 1000)}
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    {Time.prettifyTimestamp(channel.updatedAt)}
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    <Chip label={closed ? 'Closed' : 'Opened'} color={closed ? 'error' : 'success'} />
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Withdraws channel={channel} platform={platform} tokens={tokens} onError={setError} />
                        <Divider flexItem />
                        <Box margin={2}>
                            <Typography variant="h6" gutterBottom>
                                Bonds
                            </Typography>
                            <Table
                                header={
                                    <MuiTableHead>
                                        <MuiTableRow>
                                            <TableCell>Token</TableCell>
                                            <TableCell align="right">Your Balance</TableCell>
                                            <TableCell align="right">Opponent Balance</TableCell>
                                        </MuiTableRow>
                                    </MuiTableHead>
                                }
                                body={
                                    <MuiTableBody>
                                        {bonds && Object.keys(bonds).length ? (
                                            Object.entries<SC_Bond[]>(bonds).map(([tokenId, bonds]) => {
                                                const [opponentBalance, yourBalance] = bonds.reduce(
                                                    (balances, bond) => {
                                                        balances[bond.address === address ? 1 : 0] = bond.amount;
                                                        return balances;
                                                    },
                                                    [0, 0],
                                                );

                                                return (
                                                    <TableRow key={tokenId}>
                                                        <TableCell>
                                                            <BondIcon token={tokens?.[Number(tokenId)]} />
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            {convertUnitWithSymbol(
                                                                yourBalance,
                                                                TezUnit.uTez,
                                                                TezUnit.tez,
                                                            )}
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            {convertUnitWithSymbol(
                                                                opponentBalance,
                                                                TezUnit.uTez,
                                                                TezUnit.tez,
                                                            )}
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })
                                        ) : (
                                            <MuiTableRow>
                                                <TableCell colSpan={6}>
                                                    <Box
                                                        sx={{
                                                            display: 'flex',
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                        }}
                                                    >
                                                        <Typography variant="overline" textAlign="center">
                                                            No tokens yet
                                                        </Typography>
                                                    </Box>
                                                </TableCell>
                                            </MuiTableRow>
                                        )}
                                    </MuiTableBody>
                                }
                            />
                            <Box margin={3}>
                                <Stack
                                    direction="row"
                                    spacing={2}
                                    alignItems="stretch"
                                    justifyContent="center"
                                    divider={<Divider orientation="vertical" flexItem />}
                                >
                                    <Select value={tokenId} onChange={handleTokenSelection}>
                                        {Object.values(tokens || []).map((token) => (
                                            <MenuItem value={token.id} key={token.id}>
                                                <BondIcon token={token} />
                                            </MenuItem>
                                        ))}
                                    </Select>
                                    <TextField
                                        onChange={handleTokenBalance}
                                        value={amount}
                                        id="amount"
                                        required
                                        type="number"
                                        inputProps={{
                                            min: 0,
                                        }}
                                        label="Amount"
                                    />
                                    <Button onClick={updateBonds}>Add bond to the channel</Button>
                                </Stack>
                            </Box>
                        </Box>
                        <Divider flexItem />
                        <Box margin={2}>
                            <Grid container spacing={2} alignItems="center" sx={{ marginBottom: 2 }}>
                                <Grid item>
                                    <Typography variant="h6">Games</Typography>
                                </Grid>
                                <Grid item>
                                    <RouterFab
                                        variant="extended"
                                        to={`/channels/${channel.id}/new-game`}
                                        color="primary"
                                        size="small"
                                    >
                                        <AddIcon sx={{ mr: 1 }} />
                                        New game
                                    </RouterFab>
                                </Grid>
                            </Grid>
                            <GamesTable channelID={channel.id} />
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Alert variant="outlined" severity="success" icon={false} classes={{ message: classes.alertMessage }}>
                    Bond update was submitted successfully!
                </Alert>
            </Dialog>
            <Dialog open={!!error} onClose={() => setError('')}>
                <Alert variant="outlined" severity="error" icon={false} classes={{ message: classes.alertMessage }}>
                    {error}
                </Alert>
            </Dialog>
        </>
    );
};

const TableTemplate = (Component: React.FC) => () =>
    (
        <Table
            header={
                <MuiTableHead>
                    <MuiTableRow>
                        <TableCell></TableCell>
                        <TableCell>Channel ID</TableCell>
                        <TableCell>Opponent</TableCell>
                        <TableCell align="right">Withdraw Delay</TableCell>
                        <TableCell align="right">Last Update</TableCell>
                        <TableCell align="right">State</TableCell>
                    </MuiTableRow>
                </MuiTableHead>
            }
            body={<MuiTableBody>{<Component />}</MuiTableBody>}
        />
    );

const ChannelsTable = () => {
    const { pkh } = useWalletContext();
    const { platform } = useAppContext();
    const { loading, error, data } = useSubscription<
        { channels: SC_Channel[] },
        { address?: string; platform: string }
    >(Subscription.Channel.CHANNELS, {
        variables: { address: pkh, platform: platform.id },
    });

    if (loading || !pkh) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            Loading...
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (error) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            {error?.message || 'Something went wrong'}
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (!data?.channels?.length) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            No channels yet
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }

    return (
        <>
            {data.channels.map((channel) => (
                <ChannelRow platform={platform} key={channel.id} address={pkh} channel={channel} />
            ))}
        </>
    );
};

export default TableTemplate(ChannelsTable);
