import React from 'react';

import {
    Divider,
    Fab,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField,
    Theme,
    Typography,
} from '@mui/material';
import { makeStyles, createStyles } from '@mui/styles';
import Add from '@mui/icons-material/Add';

import { SC_Settlement, convertUnitNumber, TezUnit, SC_SettlementKind } from 'state-channels-common';

import BondIcon from 'src/components/base/BondIcon';
import useWalletContext from 'src/hooks/useWalletContext';
import useTokens from 'src/hooks/useTokens';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 5px 20px rgba(0,0,0,0.30), 0 10px 10px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

interface OwnProps {
    player1?: string;
    player2?: string;
    onAdd: (settlement: SC_Settlement) => void;
    outcomes: string[];
}

const AddSettlements: React.FC<OwnProps> = ({ player1 = '', player2 = '', outcomes, onAdd }) => {
    const classes = useStyles();
    const { pkh } = useWalletContext();
    const [senders, setSenders] = React.useState<Record<string, string>>({});
    const [recipients, setRecipients] = React.useState<Record<string, string>>({});
    const [tokens, setTokens] = React.useState<Record<string, number>>({});
    const [amounts, setAmount] = React.useState<Record<string, number>>({});
    const allTokens = useTokens();

    const handleSenderSelection = React.useCallback((e: SelectChangeEvent<string>): void => {
        setSenders((senders) => ({
            ...senders,
            [e.target.name]: String(e.target.value),
        }));
    }, []);

    const handleRecipientsSelection = React.useCallback((e: SelectChangeEvent<string>): void => {
        setRecipients((recipients) => ({
            ...recipients,
            [e.target.name]: String(e.target.value),
        }));
    }, []);

    const handleTokenSelection = React.useCallback((e: SelectChangeEvent<string | number>) => {
        setTokens((tokens) => ({
            ...tokens,
            [e.target.name]: Number(e.target.value),
        }));
    }, []);

    const handleAmountChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        return setAmount((amounts) => ({
            ...amounts,
            [e.target.id]: Number(e.target.value),
        }));
    }, []);

    const handleAdd: React.MouseEventHandler<HTMLButtonElement> = React.useCallback(
        (event) => {
            const idTokens = event.currentTarget.id.split(`--`);
            const id = idTokens[0];
            const value = idTokens[idTokens.length - 1];
            switch (id) {
                case SC_SettlementKind.PlayerDoublePlayed:
                    {
                        const amount = amounts[event.currentTarget.id] || 0;
                        const tokenID = tokens[event.currentTarget.id] || 0;
                        const sender = senders[event.currentTarget.id];
                        const recipient = recipients[event.currentTarget.id];
                        onAdd({
                            kind: SC_SettlementKind.PlayerDoublePlayed,
                            value: Number(value),
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: sender === player1 ? 1 : 2,
                            recipient: recipient === player1 ? 1 : 2,
                        });
                    }
                    break;
                case SC_SettlementKind.PlayerInactive:
                    {
                        const amount = amounts[event.currentTarget.id] || 0;
                        const tokenID = tokens[event.currentTarget.id] || 0;
                        const sender = senders[event.currentTarget.id];
                        const recipient = recipients[event.currentTarget.id];
                        onAdd({
                            kind: SC_SettlementKind.PlayerInactive,
                            value: Number(value),
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: sender === player1 ? 1 : 2,
                            recipient: recipient === player1 ? 1 : 2,
                        });
                    }
                    break;
                case SC_SettlementKind.GameFinished:
                    {
                        const amount = amounts[event.currentTarget.id] || 0;
                        const tokenID = tokens[event.currentTarget.id] || 0;
                        const sender = senders[event.currentTarget.id];
                        const recipient = recipients[event.currentTarget.id];
                        onAdd({
                            kind: SC_SettlementKind.GameFinished,
                            value: value,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: sender === player1 ? 1 : 2,
                            recipient: recipient === player1 ? 1 : 2,
                        });
                    }
                    break;
            }
        },
        [amounts, player1, player2, tokens, senders, recipients],
    );

    if (player1 && player2) {
        return (
            <>
                {Object.values(outcomes).map((st) => (
                    <React.Fragment key={st}>
                        <div className={classes.divider} />
                        <Typography variant="caption">Game Finished ({st})</Typography>
                        <Divider flexItem sx={{ marginBottom: 1, marginTop: 1 }} />
                        <Grid container justifyContent="space-between">
                            <Grid item xs={3}>
                                <FormControl fullWidth size="small">
                                    <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}--${st}_bond`}>
                                        Bond
                                    </InputLabel>
                                    <Select
                                        name={`${SC_SettlementKind.GameFinished}--${st}`}
                                        labelId={`settlement_${SC_SettlementKind.GameFinished}--${st}_bond`}
                                        label="Bond"
                                        value={tokens[`${SC_SettlementKind.GameFinished}--${st}`] || 0}
                                        onChange={handleTokenSelection}
                                    >
                                        {Object.values(allTokens || []).map((token) => (
                                            <MenuItem value={token.id} key={token.id}>
                                                <BondIcon token={token} />
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                                <TextField
                                    fullWidth
                                    id={`${SC_SettlementKind.GameFinished}--${st}`}
                                    required
                                    type="number"
                                    inputProps={{
                                        min: 0,
                                    }}
                                    value={amounts[`${SC_SettlementKind.GameFinished}--${st}`] || 0}
                                    onChange={handleAmountChange}
                                    label="Amount"
                                    size="small"
                                />
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={2}>
                                <FormControl
                                    fullWidth
                                    size="small"
                                    error={!senders?.[`${SC_SettlementKind.GameFinished}--${st}`]}
                                >
                                    <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}--${st}_sender`}>
                                        Sender
                                    </InputLabel>
                                    <Select
                                        name={`${SC_SettlementKind.GameFinished}--${st}`}
                                        labelId={`settlement_${SC_SettlementKind.GameFinished}--${st}_sender`}
                                        label="Sender"
                                        value={senders?.[`${SC_SettlementKind.GameFinished}--${st}`] || ''}
                                        onChange={handleSenderSelection}
                                    >
                                        {[player1, player2]?.map((player) => (
                                            <MenuItem value={player} key={player}>
                                                {player === pkh ? 'You' : 'Opponent'}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={2}>
                                <FormControl
                                    fullWidth
                                    size="small"
                                    error={!recipients[`${SC_SettlementKind.GameFinished}--${st}`]}
                                >
                                    <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}--${st}_sender`}>
                                        Recipient
                                    </InputLabel>
                                    <Select
                                        name={`${SC_SettlementKind.GameFinished}--${st}`}
                                        labelId={`settlement_${SC_SettlementKind.GameFinished}--${st}_recipient`}
                                        label="Recipient"
                                        value={recipients[`${SC_SettlementKind.GameFinished}--${st}`] || ''}
                                        onChange={handleRecipientsSelection}
                                    >
                                        {[player1, player2]?.map((player) => (
                                            <MenuItem value={player} key={player}>
                                                {player === pkh ? 'You' : 'Opponent'}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={1} display="flex" justifyContent="center" alignItems="center">
                                <Fab
                                    color="primary"
                                    id={`${SC_SettlementKind.GameFinished}--${st}`}
                                    variant="extended"
                                    size="small"
                                    onClick={handleAdd}
                                    sx={{ width: '100%' }}
                                >
                                    <Add sx={{ mr: 0.1 }} /> Add
                                </Fab>
                            </Grid>
                        </Grid>
                    </React.Fragment>
                ))}
                <Typography variant="overline" sx={{ marginTop: 2 }}>
                    Player Inactive
                </Typography>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player 1
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small">
                            <InputLabel id={`${SC_SettlementKind.PlayerInactive}_bond_id_1`}>Bond</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--1`}
                                labelId={`${SC_SettlementKind.PlayerInactive}_bond_id_1`}
                                label="Bond"
                                value={tokens[`${SC_SettlementKind.PlayerInactive}--1`] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`${SC_SettlementKind.PlayerInactive}--1`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[`${SC_SettlementKind.PlayerInactive}--1`] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl fullWidth size="small" error={!senders[`${SC_SettlementKind.PlayerInactive}--1`]}>
                            <InputLabel id={`${SC_SettlementKind.PlayerInactive}_sender_1`}>Sender</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--1`}
                                labelId={`${SC_SettlementKind.PlayerInactive}_sender_1`}
                                label="Sender"
                                value={senders[`${SC_SettlementKind.PlayerInactive}--1`] || ''}
                                onChange={handleSenderSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!recipients[`${SC_SettlementKind.PlayerInactive}--1`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerInactive}_recipient_1`}>Recipient</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--1`}
                                labelId={`${SC_SettlementKind.PlayerInactive}_recipient_1`}
                                label="Recipient"
                                value={recipients[`${SC_SettlementKind.PlayerInactive}--1`] || ''}
                                onChange={handleRecipientsSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={1} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`${SC_SettlementKind.PlayerInactive}--1`}
                            color="primary"
                            aria-label="Add player 1 inactive settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player 2
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_player_inactive_bond_id_2">Bond</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--2`}
                                labelId="settlement_player_inactive_bond_id_2"
                                label="Bond"
                                value={tokens[`${SC_SettlementKind.PlayerInactive}--2`] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`${SC_SettlementKind.PlayerInactive}--2`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[`${SC_SettlementKind.PlayerInactive}--2`] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl fullWidth size="small" error={!senders[`${SC_SettlementKind.PlayerInactive}--2`]}>
                            <InputLabel id={`${SC_SettlementKind.PlayerInactive}--2_sender`}>Sender</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--2`}
                                labelId={`${SC_SettlementKind.PlayerInactive}--2_sender`}
                                label="Sender"
                                value={senders[`${SC_SettlementKind.PlayerInactive}--2`] || ''}
                                onChange={handleSenderSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!recipients[`${SC_SettlementKind.PlayerInactive}--2`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerInactive}--2_recipient`}>Recipient</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerInactive}--2`}
                                labelId={`${SC_SettlementKind.PlayerInactive}--2_recipient`}
                                label="Recipient"
                                value={recipients[`${SC_SettlementKind.PlayerInactive}--2`] || ''}
                                onChange={handleRecipientsSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={1} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`${SC_SettlementKind.PlayerInactive}--2`}
                            color="primary"
                            aria-label="Add player 2 inactive settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
                <Typography variant="overline" sx={{ marginTop: 2 }}>
                    Player Double Played
                </Typography>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player 1
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_double_played_bond_id_1">Bond</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                                labelId="settlement_double_played_bond_id_1"
                                label="Bond"
                                value={tokens?.[`${SC_SettlementKind.PlayerDoublePlayed}--1`] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[`${SC_SettlementKind.PlayerDoublePlayed}--1`] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!senders[`${SC_SettlementKind.PlayerDoublePlayed}--1`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerDoublePlayed}--1`}>Sender</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                                labelId={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                                label="Sender"
                                value={senders[`${SC_SettlementKind.PlayerDoublePlayed}--1`] || ''}
                                onChange={handleSenderSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!recipients[`${SC_SettlementKind.PlayerDoublePlayed}--1`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerDoublePlayed}--1`}>Recipient</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                                labelId={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                                label="Recipient"
                                value={recipients[`${SC_SettlementKind.PlayerDoublePlayed}--1`] || ''}
                                onChange={handleRecipientsSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={1} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`${SC_SettlementKind.PlayerDoublePlayed}--1`}
                            color="primary"
                            aria-label="Add player 1 double played settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player 2
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_double_played_bond_id_2">Bond</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--2`}
                                labelId="settlement_double_played_bond_id_2"
                                label="Bond"
                                value={tokens?.[`${SC_SettlementKind.PlayerDoublePlayed}--2`] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`${SC_SettlementKind.PlayerDoublePlayed}--2`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[`${SC_SettlementKind.PlayerDoublePlayed}--2`] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!senders[`${SC_SettlementKind.PlayerDoublePlayed}--2`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerDoublePlayed}--2_sender`}>Sender</InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--2`}
                                labelId={`${SC_SettlementKind.PlayerDoublePlayed}--2_sender`}
                                label="Sender"
                                value={senders[`${SC_SettlementKind.PlayerDoublePlayed}--2`] || ''}
                                onChange={handleSenderSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={2}>
                        <FormControl
                            fullWidth
                            size="small"
                            error={!recipients[`${SC_SettlementKind.PlayerDoublePlayed}--2`]}
                        >
                            <InputLabel id={`${SC_SettlementKind.PlayerDoublePlayed}--2_recipient`}>
                                Recipient
                            </InputLabel>
                            <Select
                                name={`${SC_SettlementKind.PlayerDoublePlayed}--2`}
                                labelId={`${SC_SettlementKind.PlayerDoublePlayed}--2_recipient`}
                                label="Recipient"
                                value={recipients[`${SC_SettlementKind.PlayerDoublePlayed}--2`] || ''}
                                onChange={handleRecipientsSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={1} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`${SC_SettlementKind.PlayerDoublePlayed}--2`}
                            color="primary"
                            aria-label="Add player 2 double played settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
            </>
        );
    }

    return null;
};

export default AddSettlements;
