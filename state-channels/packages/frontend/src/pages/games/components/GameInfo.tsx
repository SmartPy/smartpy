import React from 'react';
import {
    Grid,
    Typography,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    TableBody,
    TableRow,
    TableCell,
    TableHead,
    useTheme,
    useMediaQuery,
} from '@mui/material';
import { convertUnitWithSymbol, TezUnit, SC_Game, Packers } from 'state-channels-common';

import BondIcon from 'src/components/base/BondIcon';
import CodeBlock from 'src/components/base/CodeBlock';
import CopyButton from 'src/components/base/CopyButton';
import Settlements from './Settlements';
import useWalletContext from 'src/hooks/useWalletContext';
import Table from 'src/components/base/Table';
import useTokens from 'src/hooks/useTokens';
import Fab from 'src/components/base/Fab';
import Avatar from 'src/components/base/Avatar';

interface OwnProps {
    game: SC_Game;
    gameAccepted?: boolean;
    settle?: () => void;
}

const GameInfo: React.FC<OwnProps> = ({ game, gameAccepted, settle }) => {
    const { pkh } = useWalletContext();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('md'));
    const tokens = useTokens();

    const initParams = React.useMemo(() => (game ? Packers.unpackData(game.initParams) : undefined), [game]);
    if (!pkh) {
        return null;
    }

    const Bonds = React.useMemo(
        () =>
            Object.entries(game.bonds).reduce<React.ReactElement[]>((acc, [player, bonds]) => {
                if (!bonds || !Object.keys(bonds).length) {
                    return acc;
                }
                return [
                    ...acc,
                    <Grid item xs={5} display="flex" justifyContent="center" key={player}>
                        <Table
                            header={
                                <TableHead>
                                    <TableRow>
                                        <TableCell colSpan={2}>
                                            <Typography variant="caption">
                                                {String(game.players[pkh]) === player ? 'You' : 'Opponent'}
                                            </Typography>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Bond</TableCell>
                                        <TableCell align="right">Amount</TableCell>
                                    </TableRow>
                                </TableHead>
                            }
                            body={
                                <TableBody>
                                    {Object.entries(bonds).map(([tokenID, amount]) => (
                                        <TableRow key={`${player}-${tokenID}`}>
                                            <TableCell component="th" scope="row">
                                                <BondIcon token={tokens?.[Number(tokenID)]} />
                                            </TableCell>
                                            <TableCell component="th" scope="row" align="right">
                                                {Number(tokenID) === 0
                                                    ? convertUnitWithSymbol(amount, TezUnit.uTez, TezUnit.tez)
                                                    : amount}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            }
                        />
                    </Grid>,
                ];
            }, []),
        [],
    );

    return (
        <Grid container direction="column" justifyContent="center">
            <Grid item display="flex" justifyContent="center" sx={{ marginBottom: 2 }}>
                {game.outcome && !game.settled && settle ? (
                    <Fab onClick={settle} color="primary">
                        Settle Game
                    </Fab>
                ) : null}
                {gameAccepted ? null : (
                    <Typography variant="overline" textAlign="center" color="text.secondary" gutterBottom>
                        You have not yet accepted this game
                    </Typography>
                )}
            </Grid>
            <Grid item display="flex" justifyContent="center">
                <Card variant="outlined" style={matches ? { maxWidth: 600 } : undefined}>
                    <CardHeader title={game.model?.metadata['name']?.replaceAll(/[_-]/g, ' ').toUpperCase()} />
                    {game.model?.metadata['image'] ? (
                        <CardMedia
                            sx={{ height: 200 }}
                            image={game.model?.metadata['image']}
                            title={game.model?.metadata['name']}
                        />
                    ) : null}
                    <CardContent>
                        <Table
                            body={
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Game Identifier
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            <CopyButton label={game.id} color="info" sx={{ maxWidth: 200 }} />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Game Nonce
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            <CopyButton label={game.nonce} color="info" sx={{ maxWidth: 200 }} />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Play Delay
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            {game.playDelay / 60} Minutes
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Players
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            <Table
                                                header={
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell></TableCell>
                                                            <TableCell>Player Number</TableCell>
                                                            <TableCell align="right">Address</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                }
                                                body={
                                                    <TableBody>
                                                        {Object.entries(game.players).map(([address, index]) => (
                                                            <TableRow key={address}>
                                                                <TableCell component="th" scope="row">
                                                                    <Avatar value={address} />
                                                                </TableCell>
                                                                <TableCell component="th" scope="row">
                                                                    {index}
                                                                    {String(address) === pkh ? ' (You)' : ' (Opponent)'}
                                                                </TableCell>
                                                                <TableCell component="th" scope="row" align="right">
                                                                    <CopyButton
                                                                        label={address}
                                                                        color="info"
                                                                        sx={{ maxWidth: 200 }}
                                                                    />
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                }
                                            />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Bonds
                                        </TableCell>
                                        <TableCell align="right" component="th" scope="row">
                                            {Bonds.length ? (
                                                <Grid container direction="row" justifyContent="space-between">
                                                    {Bonds}
                                                </Grid>
                                            ) : (
                                                <Typography variant="caption">No Bonds were provided</Typography>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Settlements
                                        </TableCell>
                                        <TableCell align="right" component="th" scope="row">
                                            {game.settlements && game.settlements.length ? (
                                                <Settlements
                                                    settlements={game.settlements}
                                                    yourPlayerNumber={game.players[pkh]}
                                                />
                                            ) : (
                                                <Typography variant="caption">No settlements were provided</Typography>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Initialization Parameters
                                        </TableCell>
                                        <TableCell align="right" component="th" scope="row">
                                            <CodeBlock code={JSON.stringify(initParams, null, 4)} language="json" />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            }
                        />
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
};

export default GameInfo;
