import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import { Box, Toolbar, AppBar, Divider } from '@mui/material';

import DarkLightSwitch from '../theme/DarkLightSwitch';
import BeaconButton from '../Beacon/Button';
import RouterButton from '../base/RouterButton';

const NavigationBar: React.FC = () => {
    return (
        <AppBar position="static" color="default">
            <Toolbar>
                <RouterButton to="/" variant="text" startIcon={<HomeIcon />}>
                    Home
                </RouterButton>
                <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                <RouterButton to="/channels" variant="text">
                    Channels
                </RouterButton>
                <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                <RouterButton to="/models" variant="text">
                    Models
                </RouterButton>
                <Box sx={{ flexGrow: 1 }} />
                <BeaconButton />
                <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                <DarkLightSwitch />
            </Toolbar>
        </AppBar>
    );
};

export default NavigationBar;
