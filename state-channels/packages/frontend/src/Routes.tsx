import React from 'react';
import { Switch, Router } from 'react-router';
import { Helmet } from 'react-helmet';
import { Route } from 'react-router';
import { createBrowserHistory } from 'history';

import Home from './pages/Home';
import NewChannel from './pages/channels/NewChannel';
import Channels from './pages/channels/Channels';
import MustBeConnected from './components/auth/MustBeConnected';
import ViewContainer from './components/base/ViewContainer';
import Models from './pages/models/Models';
import NewModel from './pages/models/NewModel';
import NewGame from './pages/games/NewGame';
import Game from './pages/games/Game';

const history = createBrowserHistory({ basename: process.env.PUBLIC_URL });

const AppRouter: React.FC = () => {
    return (
        <Router history={history}>
            <ViewContainer>
                <Switch>
                    <Route exact path="/">
                        <Helmet>
                            <title>Home Page</title>
                        </Helmet>
                        <Home />
                    </Route>
                    <Route exact path="/channels">
                        <Helmet>
                            <title>Channels</title>
                        </Helmet>
                        <MustBeConnected>
                            <Channels />
                        </MustBeConnected>
                    </Route>
                    <Route path="/new-channel">
                        <Helmet>
                            <title>New Channel</title>
                        </Helmet>
                        <MustBeConnected>
                            <NewChannel />
                        </MustBeConnected>
                    </Route>
                    <Route exact path="/models">
                        <Helmet>
                            <title>Models</title>
                        </Helmet>
                        <MustBeConnected>
                            <Models />
                        </MustBeConnected>
                    </Route>
                    <Route path="/new-model">
                        <Helmet>
                            <title>New Model</title>
                        </Helmet>
                        <MustBeConnected>
                            <NewModel />
                        </MustBeConnected>
                    </Route>
                    <Route path="/channels/:id/new-game">
                        <Helmet>
                            <title>New Game</title>
                        </Helmet>
                        <MustBeConnected>
                            <NewGame />
                        </MustBeConnected>
                    </Route>
                    <Route path="/channels/:channelID/games/:gameID">
                        <Helmet>
                            <title>Game View</title>
                        </Helmet>
                        <MustBeConnected>
                            <Game />
                        </MustBeConnected>
                    </Route>
                </Switch>
            </ViewContainer>
        </Router>
    );
};

export default AppRouter;
